<?php
    /*
    |--------------------------------------------------------------------------
    | Modul untuk mencetak spesifikasi komputer beserta historis tiket 15kali terakhir
    |--------------------------------------------------------------------------
    |modul cetak pdf berita acara pengembalian komputer
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */


    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.date.php");
    require_once($SYSTEM['DIR_PATH']."/class/fpdf/class.fpdf.php");
    require_once($SYSTEM['DIR_PATH']."/class/fpdf/class.qrcode.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    $oTicket = new Ticket();

    $oKomputer = new Computer();

    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "";

    $i_margin_height_header = 4;
    $i_margin_height_body = 5;
    $i_margin_height_table = 4;
    //garis debugger   
    $b_debug = 0;
    if($b_debug == 1)
    {
        $i_border = 1;
    }
    else
    {
        $i_border = 0;
    }

    $a_komputer_item['APLIKASI'] = 'KMITAPT0000000000001';
    $a_komputer_item['CAM'] = 'KMITCAM0000000000001';
    $a_komputer_item['SPEAKER'] = 'KMITDPK0000000000001';
    $a_komputer_item['CD_ROM'] = 'KMITCDR0000000000001';
    $a_komputer_item['DVD_ROM'] = 'KMITDVR0000000000001';
    $a_komputer_item['FLOPPY'] = 'KMITFLP0000000000001';
    $a_komputer_item['HARDDISK'] = 'KMITHDK0000000000001';
    $a_komputer_item['KEYBOARD'] = 'KMITKYB0000000000001';
    $a_komputer_item['LANCARD'] = 'KMITLNC0000000000001';
    $a_komputer_item['MEMORY'] = 'KMITMEM0000000000001';
    $a_komputer_item['MONITOR'] = 'KMITMNT0000000000001';
    $a_komputer_item['MOTHERBOARD'] = 'KMITMTH0000000000001';
    $a_komputer_item['MOUSE'] = 'KMITMUS0000000000001';
    $a_komputer_item['SISTEM_OPERASI'] = 'KMITOS00000000000001';
    $a_komputer_item['PROCESSOR'] = 'KMITPRC0000000000001';
    $a_komputer_item['PRINTER'] = 'KMITPRN0000000000001';
    $a_komputer_item['SCANNER'] = 'KMITSCN0000000000001';


    $a_no_satuan[] = $a_komputer_item['APLIKASI'];
    $a_no_satuan[] = $a_komputer_item['SISTEM_OPERASI'];

    $a_satuan_biji[] = $a_komputer_item['CAM'];
    $a_satuan_biji[] = $a_komputer_item['SPEAKER'];
    $a_satuan_biji[] = $a_komputer_item['CD_ROM'];
    $a_satuan_biji[] = $a_komputer_item['DVD_ROM'];
    $a_satuan_biji[] = $a_komputer_item['FLOPPY'];
    $a_satuan_biji[] = $a_komputer_item['KEYBOARD'];
    $a_satuan_biji[] = $a_komputer_item['MONITOR'];
    $a_satuan_biji[] = $a_komputer_item['MOTHERBOARD'];
    $a_satuan_biji[] = $a_komputer_item['MOUSE'];
    $a_satuan_biji[] = $a_komputer_item['PRINTER'];
    $a_satuan_biji[] = $a_komputer_item['SCANNER'];


    $a_satuan_mb[] = $a_komputer_item['HARDDISK'];
    $a_satuan_mb[] = $a_komputer_item['LANCARD'];
    $a_satuan_mb[] = $a_komputer_item['MEMORY'];
    $a_satuan_mb[] = $a_komputer_item['PROCESSOR'];
    //jika idnya sudah diset
    if(isset($_REQUEST['komputerID']))
    {

        $a_komputer_detail = $oKomputer->getList("WHERE `komputerID` = '".$_REQUEST['komputerID']."'","","");
        
        $oPDF=new FPDF();
        $oPDF->AddPage();
        if(count($a_komputer_detail)>0)
        {
            $a_item = $oKomputer->getSpek($a_komputer_detail[0]['komputerID']);

            //Logo
            $oPDF->Image('assets/img/pln-medium.jpg',11,10,11,17);
            //Arial bold 15
            $oPDF->SetFont('Arial','B',25);
            //lebar, tinggi ,text , border , ln,align
            $oPDF->Cell(15);
            $oPDF->Cell(52,8,'PT. PLN (Persero)',$i_border,1,'L');
            //Line break
            $oPDF->Ln(1);
            //Arial bold 15
            $oPDF->SetFont('Arial','B',10);
            //lebar, tinggi ,text , border , ln,align
            $oPDF->Cell(15);
            $oPDF->Cell(50,$i_margin_height_header,'DISTRIBUSI JAWA TIMUR',$i_border,1,'L');
            //Line break
            $oPDF->Ln(1);

            $oPDF->SetFont('Arial','',9);
            //lebar, tinggi ,text , border , ln,align
            $oPDF->Cell(15);
            $oPDF->Cell(12,$i_margin_height_header,'APJ SURABAYA UTARA',$i_border,0,'L');
            //Line break
            $oPDF->Ln(0);
            //membuat garis
            $oPDF->Line(10, 30, 200, 30);
            $oPDF->Line(10, 30.5, 200, 30.5);
            $oPDF->Ln(0);

            $oPDF->setXY(10,35);
            $oPDF->SetFont('Arial','',14);
            $oPDF->Cell(190,$i_margin_height_header,'BERITA ACARA PENGEMBALIAN KOMPUTER',$i_border,1,'C');

            $oPDF->SetFont('Arial','',10);

            $oPDF->setXY(10,45);
            $oPDF->Cell(50,$i_margin_height_body,'Nomor Komputer',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->SetFont('Arial','B',14);
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerID'],$i_border,1,'L');

            $oPDF->SetFont('Arial','',10);
            $oPDF->Cell(50,$i_margin_height_body,'Hostname',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerIdentifikasi'],$i_border,1,'L');

            $oPDF->Cell(50,$i_margin_height_body,'NIP',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerUser'],$i_border,1,'L');
            $oPDF->Cell(50,$i_margin_height_body,'Nama Pengguna',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['realName'],$i_border,1,'L');

            $oPDF->Cell(50,$i_margin_height_body,'Jenis',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerPosisi'],$i_border,1,'L');
            $oPDF->Cell(50,$i_margin_height_body,'IP',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerIPUtama'],$i_border,1,'L');
            $oPDF->Ln();

            $qrcode = new QRcode($a_komputer_detail[0]['komputerID'],'H');
            $qrcode->displayFPDF($oPDF,170 ,45 ,30);

           
           
            $oPDF->Ln();
            if(count($a_item)>0){
                
                $oPDF->SetFont('Arial','',10);
                $oPDF->Cell(30,$i_margin_height_table,"ITEM",1,0,'C');
                $oPDF->Cell(30,$i_margin_height_table,"MERK",1,0,'C');
                $oPDF->Cell(25,$i_margin_height_table,"KAPASITAS",1,0,'C');
                $oPDF->Cell(105,$i_margin_height_table,"Keterangan",1,0,'C');
                $oPDF->Ln();
                
                $oPDF->SetFont('Arial','',8);
                for($i=0;$i<count($a_item);$i++){
                     
                     
                    $s_satuan = "";
                    if(in_array($a_item[$i]['komputerItemID'], $a_no_satuan))
                    {
                        $s_satuan = "-";
                    }
                    elseif(in_array($a_item[$i]['komputerItemID'], $a_satuan_biji))
                    {
                        $s_satuan = "1 biji";
                    }
                    elseif(in_array($a_item[$i]['komputerItemID'], $a_satuan_mb))
                    {
                        $s_satuan = number_format($a_item[$i]['komputerSpekKapasitas'])." (MB)";
                    }
                    else
                    {
                        $s_satuan = "";
                    }
                    $oPDF->Cell(30,$i_margin_height_table,$a_item[$i]['komputerItemNama'],1,0,'L');
                    $oPDF->Cell(30,$i_margin_height_table,substr($a_item[$i]['komputerSpekMerk'],0,20),1,0,'L');
                    $oPDF->Cell(25,$i_margin_height_table,$s_satuan,1,0,'R');
                    $oPDF->Cell(105,$i_margin_height_table,substr($a_item[$i]['komputerSpekKeterangan'],0,60),1,0,'L');
                    $oPDF->Ln();
                }
            }

            $s_condition    = "WHERE ticketKomputerID='".$_REQUEST['komputerID']."'";
            $s_order = " ORDER BY ticketProblemDesc DESC ";
            $s_limit = " LIMIT 10 ";
            $a_data_ticket = $oTicket->getList($s_condition, $s_order, $s_limit);


            if(count($a_data_ticket)>0)
            {

                $oPDF->Ln();
                $oPDF->Ln();
                $oPDF->SetFont('Arial','',14);
                $oPDF->Cell(190,$i_margin_height_header,'RIWAYAT TIKET TERAKHIR',$i_border,1,'C');
                $oPDF->SetFont('Arial','',10);
                $oPDF->Ln();
                $oPDF->Ln();
                $oPDF->Ln();
                for($i_ticket=0;$i_ticket<count($a_data_ticket);$i_ticket++)
                {
                    $s_keterangan_ticket = "";
                    $s_status = "";
                    if($a_data_ticket[$i_ticket]['ticketStatus'] == '8')
                    {
                        $s_keterangan_ticket = $a_data_ticket[$i_ticket]['ticketSolveDesc'] ;
                        $s_status = "selesai";
                    }
                    elseif($a_data_ticket[$i_ticket]['ticketStatus'] == '3')
                    {
                        $s_keterangan_ticket = $a_data_ticket[$i_ticket]['ticketStatusDesc'] ;
                        $s_status = "menunggu material";
                    }
                    elseif($a_data_ticket[$i_ticket]['ticketStatus'] == '2')
                    {
                        $s_keterangan_ticket = $a_data_ticket[$i_ticket]['ticketStatusDesc'] ;
                        $s_status = "menunggu persetujuan";
                    }

                    $oPDF->Cell(140,$i_margin_height_table,"status tiket : ".$s_status,$i_border,0,'L');
                    $oPDF->Cell(50,$i_margin_height_table,$a_data_ticket[$i_ticket]['ticketRequestDate'],$i_border,1,'R');
                    $oPDF->MultiCell(190,$i_margin_height_header,$a_data_ticket[$i_ticket]['ticketProblemDesc'],0,'L');
                    $oPDF->MultiCell(190,$i_margin_height_header,$s_keterangan_ticket ,0,'L');
                    
                    $oPDF->Line(10, $oPDF->GetY(), 200, $oPDF->GetY());
                    $oPDF->Ln();
                }
            }






           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Ln();
        }
        else
        {

        }
        $oPDF->Output("komputer_detail_".$_REQUEST['komputerID'].".pdf","D");
    }

    $oKomputer->closeDB();
    $oTicket->closeDB();


?>