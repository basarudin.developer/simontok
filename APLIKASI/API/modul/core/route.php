<?php
    /*
    |--------------------------------------------------------------------------
    | Route
    |--------------------------------------------------------------------------
    |Routing dari parameter URI
    |Khusus untuk modul login menyertakan modul firewall.php,selain modul login menggunakan modul secure.php
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string

    |masih belum memanfaatkan htaccess
    |variable standart :
    |   page -> digunakan untuk mengakses halaman
    |   type -> 0 = view, 1 = model ; 0 default
    |   action -> list,detail,edit,delete
    |   record_id -> string untuk menujukan id dari primary key table
    */


    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUserInfo = new UserInfo();

    //default adalah login
    include_once($SYSTEM['DIR_MODUL_CORE']."/firewall.php");
    //jika sudah login
    if(isset($_POST['TOKEN_KEY']))
    {

        if(isset($_REQUEST['page']))
        {
            //include("secure.php");
            switch ($_REQUEST['page']) 
            {
                // About page
                case '':
                    require_once($SYSTEM['DIR_MODUL_CORE']."/welcome.php");
                    break;
                case 'login':
                    require_once($SYSTEM['DIR_MODUL']."/login/login.php");
                    break;
                case 'ticket':
                    require_once($SYSTEM['DIR_MODUL']."/ticket/ticket.php");
                    break;
                case 'komputer':
                    require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.php");
                    break;
                case 'app':
                    require_once($SYSTEM['DIR_MODUL_CORE']."/application.php");
                    break;
                // Everything else
                default:
                    //header('HTTP/1.0 404 Not Found');
                    require_once($SYSTEM['DIR_MODUL']."/login/login.php");
                    break;
            }//end switch   
        }
        else
        {
            require_once($SYSTEM['DIR_MODUL_CORE']."/welcome.php");
        }
        
    }
    //jika belum login
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/login/login.php");
    }
?>