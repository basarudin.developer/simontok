<?php
/*
|--------------------------------------------------------------------------
| Login Model
|--------------------------------------------------------------------------
|
|Model login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

	$PAGE_ID="LOGIN";
	require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.client.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.log.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.firewall.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.device.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.app.php");
	$oApp = new Application();
	$oUser = new UserInfo();
	$oLog = new Log();
	$oClient = new Client();
	$oDevice = new Device();
	//jika variable error sudah terisi maka kosongkanlah
	$result['result'] = "error";
	$result['desc'] = "";

	$sError  = "";
    $a_errors = array();

	if(isset($_REQUEST['action']))
	{
		if($_REQUEST['action'] == "registerdevice"){


			if($_REQUEST['appName'] == "")
            {
                $a_errors[] = "nama aplikasi tidak ada";
            }
			if($_REQUEST['appVersion'] == "")
            {
                $a_errors[] = "versi aplikasi tidak ada";
            }


			if($_REQUEST['userID'] == "")
            {
                $a_errors[] = "nip tidak ada";
            }
            if($_REQUEST['email'] == "")
            {
                $a_errors[] = "email tidak boleh kosong";
            }
            if($_REQUEST['firebaseToken'] == "")
            {
                $a_errors[] = "firebase tidak boleh kosong";
            }
            if($_REQUEST['deviceModel'] == "")
            {
                $a_errors[] = "device model tidak boleh kosong";
            }
            if($_REQUEST['deviceMerk'] == "")
            {
                $a_errors[] = "merk hp tidak boleh kosong";
            }
            if($_REQUEST['deviceSDK'] == "")
            {
                $a_errors[] = "device sdk tidak boleh kosong";
            }





            if (!$a_errors) 
            {
            	foreach ($_REQUEST as $key => $value) 
            	{
            		$_REQUEST[$key] = $oDevice->antiInjection($_REQUEST[$key]);
            	}

            	//periksa nama dan versi aplikasi apakah sudah ada disistem
                $s_where_app = " WHERE appName = '{$_REQUEST['appName']}' ";
                $a_data_app = $oApp->getList($s_where_app,"","");
                if(isset($a_data_app))
                {
                	if(count($a_data_app)>0)
                	{
                		if($_REQUEST['appVersion'] >= $a_data_app[0]['lastVersion'])
                		{

							//cek apakah ada di pengguna
			                $s_where_user = " WHERE `userID` = '{$_REQUEST['userID']}' AND mail = '{$_REQUEST['email']}' ";
			                if($oUser->getUserCount($s_where_user)> 0)
			                {

				            	$s_where_device = " WHERE `userID` = '{$_REQUEST['userID']}' AND deviceModel =   '{$_REQUEST['deviceModel']}' AND registerStatus = 0";
				            	if($oDevice->getCount($s_where_device) > 0)
				            	{

				                    $result['result'] = 'error';
				                    $result['desc'] = "device ini sudah pernah didaftarkan dan sedang menunggu aktifasi dari helpdesk";
				            	}
				            	else
				            	{
									//membuat register untuk diantrikn
				                	if($oDevice->create(
				                		$_REQUEST['appName'],
				                		$_REQUEST['appVersion'],
				                		$_REQUEST['userID'],
				                		$_REQUEST['firebaseToken'],
				                		$_REQUEST['deviceModel'],
				                		$_REQUEST['deviceSDK'],
				                		$_REQUEST['deviceMerk'],
				                		$_REQUEST['deviceVersion'],
				                		$_REQUEST['devicePixel'],
				                		$_REQUEST['deviceDPI'],
				                		$_REQUEST['deviceInch'],
				                		$_REQUEST['deviceCountry'],
				                		$_REQUEST['deviceLanguage'],
				                		$_REQUEST['deviceUUID'],
				                		$_REQUEST['deviceAndroidID'],
				                		$_REQUEST['deviceNetworkCarrier'],
				                		$_REQUEST['deviceNetworkID']


				                		 ))
									{
					                    $result['result'] = 'success';
					                    $result['desc'] = "pengguna sudah dimasukan dalam antrian akun";
				                		$result['token_login'] = "";
				                		$result['register_status'] = "0";
									}
									else
									{

					                    $result['result'] = 'error';
					                    $result['desc'] = "gagal dalam memasukan antrian akun";
									}
				            	}
				            	
				                	
			                }
			                else
			                {
			                    $result['result'] = 'error';
			                    $result['desc'] = "kombinasi nik dan email salah";

			                } 

                		}
                		else
                		{
		                    $result['result'] = 'error';
		                    $result['desc'] = "versi aplikasi sudah kadaluarsa. harap download apk yang terbaru.";

                		}
                	}
                	else
                	{

	                    $result['result'] = 'error';
	                    $result['desc'] = "aplikasi ini tidak terdaftar didalam sistem. harap mengunduh apk resmi dari helpdesk.";
                	}
                }
                else
                {
                    $result['result'] = 'error';
                    $result['desc'] = "aplikasi yang anda install bukan resmi dari helpdesk. harap download dan install ulang aplikasi resminya";

                }

			            	  
//-----------------------------------
            	/*

            	//cek apakah ada di pengguna
                $s_where_user = " WHERE `userID` = '{$_REQUEST['userID']}' or mail = '{$_REQUEST['email']}' ";
                
                if($oUser->getUserCount($s_where_user)> 0)
                {



                	//cek apakah token firebase sudah diaktifkan felpdesk
                	$s_where_device = " WHERE `firebaseToken` = '{$_REQUEST['firebaseToken']}'  AND  registerStatus = 1";
                	if($oDevice->getCount($s_where_device) > 0)
                	{
                		$s_where_user = " WHERE `firebaseToken` = '{$_REQUEST['firebaseToken']}'  ";
						$a_userinfo = $oUser->getUserList($s_where_user,"","");

	                    $result['result'] = 'success';
	                    $result['token_login'] = $a_userinfo[0]['loginToken'];
	                    $result['desc'] = "register anda sudah masuk dalah daftar tunggu. harap mengghubungi helpdesk";
                	}
                	else
                	{
	                	//cek apakah sudah pernah register sebelumnya yg belum di confirm helpdesk
	                	$s_where_device = " WHERE `userID` = '{$_REQUEST['userID']}'  AND  registerStatus = 0";
	                	if($oDevice->getCount($s_where_device) > 0)
	                	{
		                    $result['result'] = 'error';
		                    $result['desc'] = "register anda sudah masuk dalah daftar tunggu. harap mengghubungi helpdesk";
	                	}
	                	else
	                	{


							if($oDevice->create($_REQUEST['userID'],$_REQUEST['firebaseToken'],$_REQUEST['deviceModel'] ))
							{

			                    $result['result'] = 'success';
			                    $result['desc'] = "pengguna sudah dimasukan dalam antrian akun";
	                    		$result['token_login'] = "";
							}
							else
							{

			                    $result['result'] = 'error';
			                    $result['desc'] = "gagal dalam memasukan antrian akun";
							}

	                	}

                	}
                }
                else
                {
                    $result['result'] = 'error';
                    $result['desc'] = "pengguna tidak terdaftar didatabase";

                }
                */
            }

		}
		elseif($_REQUEST['action'] == 'checkwaiting')
		{
			if($_REQUEST['firebaseToken'] == "")
            {
                $a_errors[] = "firebase token masih kosong";
            }
            if (!$a_errors) 
            {
            	$s_where_device = " WHERE DUR.`firebaseToken` = '{$_REQUEST['firebaseToken']}' ";
            	$a_data  = $oDevice->getList($s_where_device,"","");
				if(count($a_data ) > 0)
            	{
            		if($a_data[0]['registerStatus'] == 1)
            		{
            			$result['result'] = 'success';
	                    $result['token_login'] = $a_data[0]['loginToken'];
	                    $result['desc'] = "akun sudah aktif";
                		$result['register_status'] = "1";
                		$result['realName'] = $a_data[0]['realName'];
            		}
            		elseif($a_data[0]['registerStatus'] == 0)
            		{

	                    $result['result'] = 'error';
	                    $result['desc'] = "register anda sudah masuk dalam daftar tunggu. harap menghubungi helpdesk";
            		}
            		else
            		{
    					$result['result'] = 'error';
	                    $result['desc'] = "akun anda sudah expired. harap install ulang aplikasi anda";
            		
            		}
            	}
            	else
            	{
                    $result['result'] = 'error';
                    $result['desc'] = "firebase tidak terdaftar didatabase";

            	}
            }
		}

		

	}
	elseif(LOGIN_REFERENCE == "PASSWORD")
	{
		if( (!isset($_REQUEST['username']) ) || ($_REQUEST['username'] == "") )
		{
			$result['result'] = "error";
			$aErrors[] = "Username harus diisi";
		}
		if( (!isset($_REQUEST['password']) ) || ($_REQUEST['password'] == "") )
		{
			$result['result'] = "error";
			$aErrors[] = "Password harus diisi";
		}
		//periksa username apakah ada di dalam database
		if(isset($_REQUEST['username']) && isset($_REQUEST['password']) &&($_REQUEST['username'] != "") && ($_REQUEST['password'] != "") )
		{
			//validasi login ke system
			//jika username dan password ditemukan disystem
			$USERNAME = $oUser->antiInjection($_REQUEST['username']);
			$PASSWORD = $oUser->antiInjection($_REQUEST['password']);
			$s_condition  = " WHERE username= '".$_REQUEST['username']."' ";
			if($oUser->getUserCount($s_condition)<1)
			{
				$result['result'] = "error";
				$result['desc']  = "username tidak ditemukan";
			}else{
				$a_username =$oUser->getUserList($s_condition,"","");
				if($a_username[0]['active'] == 2)
				{
					$result['result'] = "error";
					$result['desc']  = "username telah dinonaktifkan";
				}
				elseif($a_username[0]['active'] == 0)
				{
					$result['result'] = "error";
					$result['desc']  = "username telah dihapus";
				}
				elseif($a_username[0]['active'] == 1)
				{
					$s_condition  = " WHERE username= '{$USERNAME}' AND password=md5('{$PASSWORD}') AND active=1 ";
					if($oUser->getUserCount($s_condition)> 0)
					{
						if($oUser->updateSessionLogin($USERNAME))
						{
	                        $_SESSION['LoggedIn'] = true;
	                        $_SESSION['username'] = $USERNAME;
	                        $a_userinfo = $oUser->getUserIDByUsername($USERNAME);

							if($oLog->logging($a_userinfo[0]['userID'],$oClient->getUserAgent(),$CLIENT_ADDRESS,$oClient->getClientUrl(),$PAGE_ID,"telah berhasil login"))
							{
								$result['result'] = "success";
								$result['desc']  = "Login berhasil";
							}
							else
							{
								$result['result'] = "error";
								$result['desc']  = "Autentifikasi gagal di log";
							}

						}
						else
						{
							$result['result'] = "error";
							$result['desc']  = "sesi login error";
						}

					}
					else
					{
						$result['result'] = "error";
						$result['desc']  = "password salah";
					}
				}
				else
				{
					$result['result'] = "error";
					$result['desc']  = "status username tidak diketahui";
				}
			}
		}
		else{
		   	foreach ($aErrors as $error) {
		        $sError .= "$error  <br />";
		   	}
	  	 	$result['desc']  = $sError;
		}
	}

    if ($a_errors) 
    {
    	$sError =  '';
        foreach ($a_errors as $error) 
        {
            $sError .= "$error<br />";
        }
        $result['result'] = 'error';
        $result['desc'] = $sError;
    }
	echo json_encode($result);
	$oUser->closeDB();
	$oLog->closeDB();
	$oDevice->closeDB();
	$oApp->closeDB();

?>