<?php
    /*
    |--------------------------------------------------------------------------
    | Ticket
    |--------------------------------------------------------------------------
    |model  modul ticket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";
    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.firebase.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");


    $oTicket = new Ticket();
    $oUser = new UserInfo();
    $oNotif = new Notification();
    $oHelpdesk = new Helpdesk();

    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "tidak tereksekusi";


    if(isset($_REQUEST['action']))
    {
        if(isset($_REQUEST['TOKEN_KEY']))
        {
            if($_REQUEST['TOKEN_KEY'] == "")
            {
                $result['result'] = 'error';
                $result['desc'] = "TOKEN kosong";
            }
            else
            {

                $_REQUEST['TOKEN_KEY'] = $oTicket->antiInjection($_REQUEST['TOKEN_KEY']);
                $s_where_user = " WHERE firebaseToken = '{$_REQUEST['TOKEN_KEY']}' ";
                if($oUser->getUserCount($s_where_user) > 0)
                {
                    if($_REQUEST['action'] == 'create')
                    {
                        $_REQUEST['problem_desc'] = $oTicket->antiInjection($_REQUEST['problem_desc']);
                        $a_data_user = $oUser->getUserList($s_where_user ,"","");
                        if($oTicket->create($a_data_user[0]['userID'],$a_data_user[0]['wilayahPenempatan'],$_REQUEST['problem_desc'])){



                            //kirim notifikasi
                            if($oTicket->getHelpdeskActive() != "")
                            {
                                //dapatkan token firebase dari helpdesk
                                $a_helpdesk = $oUser->getUserDetail($oTicket->getHelpdeskActive());
                                if(count($a_helpdesk) > 0)
                                {
                                    $message['message'] = "{$a_data_user[0]['realName']} : '{$_REQUEST['problem_desc']}'";
                                    $a_tokens[] = $a_helpdesk[0]['firebaseToken'];

                                    if($oNotif->create(

                                        "request tiket simontok" ,
                                        $message['message'] ,
                                        "" ,
                                        $a_helpdesk[0]['userID'] ,
                                        $a_helpdesk[0]['firebaseToken'] ,
                                        $a_data_user[0]['userID'] ,
                                        ""
                                    ))
                                    {
                                        $result['result'] = 'success';
                                        $result['desc'] = "terimakasih. permintaan anda akan segera ditangani oleh helpdesk ".strtoupper($a_helpdesk[0]['realName']).".";
                                    }
                                    else
                                    {
                                        $result['result'] = 'error';
                                        $result['desc'] = "tidak dapat memberi notifikasi ke helpdesk";
                                    }
                                    //$hasil = send_notification($a_tokens,$a_message);
                                    //echo $hasil;
                                }
                                else
                                {
                                    $result['result'] = 'error';
                                    $result['desc'] = "helpdesk tidak menginstall aplikasi android";
                                }
                            }
                            else
                            {
                                $result['result'] = 'error';
                                $result['desc'] = "helpdesk tidak ada yang aktif / menghandle";

                            }
                        }
                        else
                        {
                            $result['result'] = 'error';
                            $result['desc'] = "tiket gagal dibuat";
                        }
                    }
                    elseif($_REQUEST['action'] == 'list')
                    {

                        $a_data_user = $oUser->getUserList($s_where_user ,"","");
                        if($oHelpdesk->isHelpdesk($a_data_user[0]['userID']))
                        {
                            if(isset($_REQUEST['status']))
                            {
                                if($_REQUEST['status'] =="open")
                                {
                                    $s_where = " WHERE HH.firebaseToken = '{$_REQUEST['TOKEN_KEY']}'  AND ticketStatus =0";
                                }
                                elseif($_REQUEST['status'] =="pending")
                                {
                                    $s_where = " WHERE HH.firebaseToken = '{$_REQUEST['TOKEN_KEY']}'  AND (ticketStatus =2) OR (ticketStatus =3 )";

                                }
                                elseif($_REQUEST['status'] =="closed")
                                {
                                    
                                    $s_where = " WHERE HH.firebaseToken = '{$_REQUEST['TOKEN_KEY']}'  AND ticketStatus =8";
                                }
                            }
                        }
                        else
                        {

                            $s_where = " WHERE UR.firebaseToken = '{$_REQUEST['TOKEN_KEY']}' ";
                        }

                        $a_data = $oTicket->getList($s_where, "  ORDER BY `ticketRequestDate` DESC "," LIMIT 7 ");
                        
                        $result['result'] = 'success';
                        $result['desc'] = "";
                        $result['data'] = $a_data;   

                    }
                    elseif($_REQUEST['action'] == 'detail')
                    {

                        if(isset($_REQUEST['ticket_id']))
                        {
                            $s_where = " WHERE UR.firebaseToken = '{$_REQUEST['TOKEN_KEY']}'  AND T.ticketID = '{$_REQUEST['ticket_id']}'";
                            $a_data = $oTicket->getList($s_where, "  ","  ");
                            
                            $s_where = " WHERE ticketHelpdeskHandle = '{$a_data[0]['ticketHelpdeskHandle']}'  AND ticketStatus = '0'";
                            $a_data[0]['countAtrian'] = $oTicket->getCount($s_where);
                            $result['result'] = 'success';
                            $result['desc'] = "";
                            $result['data'] = $a_data;   
                        }
                        else
                        {
                            $result['result'] = 'error';
                            $result['desc'] = "ID tiket tidak ada";
                        }

                    }
                    elseif($_REQUEST['action'] == 'rating')
                    {

                        if(isset($_REQUEST['ticket_id']))
                        {
                            $i_rating = $oTicket->antiInjection($_REQUEST['rating']);
                            $s_feedback = $oTicket->antiInjection($_REQUEST['feedback']);
                            if($oTicket->setRating($_REQUEST['ticket_id'],$i_rating,$s_feedback))
                            {
                                $result['result'] = 'success';
                                $result['desc'] = "";
                            }
                            else
                            {
                                $result['result'] = 'error';
                                $result['desc'] = "tiket tidak dapat diberi penilaian";
                            }
                        }
                        else
                        {
                            $result['result'] = 'error';
                            $result['desc'] = "ID tiket tidak ada";
                        }

                    }

                    elseif($_REQUEST['action'] == 'resume_onmonth')
                    {
                        $s_where_open = " WHERE HH.firebaseToken = '{$_REQUEST['TOKEN_KEY']}' AND  ticketStatus = '0' ";
                        $s_where_pending = " WHERE HH.firebaseToken = '{$_REQUEST['TOKEN_KEY']}' AND  (ticketStatus = '2' OR ticketStatus = '3') ";
                        $s_where_finish = " WHERE HH.firebaseToken = '{$_REQUEST['TOKEN_KEY']}' AND  (ticketStatus = '8') ";

                        $a_ticket_open = $oTicket->getList($s_where_open, "","");
                        if(isset($a_ticket_open))
                        {
                            $i_open = count($a_ticket_open);
                        }
                        else
                        {
                            $i_open  =  0;
                        } 

                        $a_ticket_pending = $oTicket->getList($s_where_pending, "","");
                        if(isset($a_ticket_pending))
                        {
                            $i_pending = count($a_ticket_pending);
                        }
                        else
                        {
                            $i_pending  =  0;
                        } 

                        $a_ticket_finish = $oTicket->getList($s_where_finish, "","");
                        if(isset($a_ticket_finish))
                        {
                            $i_finish = count($a_ticket_finish);
                        }
                        else
                        {
                            $i_finish  =  0;
                        } 
                        $data['open'] = $i_open;
                        $data['pending'] = $i_pending;
                        $data['finish'] = $i_finish;
                        $result['result'] = 'success';
                        $result['desc'] = "";
                        $result['data'] = $data;   
                    }
                }
                else
                {
                    $result['result'] = 'error';
                    $result['desc'] = "TOKEN tidak ada dalam database";
                }   
            }
        }
        else
        {
            $result['result'] = 'error';
            $result['desc'] = "TOKEN tidak ada";

        }
    }
    echo json_encode($result);
    $oTicket->closeDB();
    $oUser->closeDB();
    $oNotif->closeDB();
    $oHelpdesk->closeDB();

?>