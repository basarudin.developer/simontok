<?php
/*
|--------------------------------------------------------------------------
| Config
|--------------------------------------------------------------------------
|
|Config system
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    DEFINE("DB_USER" , "root");
    DEFINE("DB_PASSWORD" ,"");
    DEFINE("DB_NAME" ,"simontok");
    DEFINE("DB_HOST" , "localhost");

    DEFINE("SUPER_ADMIN_GROUP","SUPER_ADMINISTRATOR");
    DEFINE("LOGIN_REFERENCE","PASSWORD");


    $SYSTEM['DIR_PATH'] = "/Applications/XAMPP/htdocs/simontok/APLIKASI/API";
    //$SYSTEM['DIR_PATH'] = "/opt/lampp/htdocs/simontok-api";
    //$SYSTEM['DIR_PATH'] = "D:/xampp/htdocs/simontok-api";
    $SYSTEM['DIR_MODUL'] = $SYSTEM['DIR_PATH'] ."/modul";
    $SYSTEM['DIR_MODUL_CORE'] = $SYSTEM['DIR_MODUL'] ."/core";


    $SYSTEM['FIREWALL'] ='OFF'; //OFF, ON


?>