<?php
/*
|--------------------------------------------------------------------------
| date
|--------------------------------------------------------------------------
|
|mempermudah dalam pengolahan tanggal
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

    //mendapatkan parameter dari mysql dengan format yyyy-mm-dd
    function formatTanggalPanjang($_date){
        if(isset($_date)){
            $tempSplit = explode("-",$_date);
            if(isset($tempSplit[1])){
                switch ($tempSplit[1]){
                    case "01":
                        $month = "Januari";
                        break;
                    case "02":
                        $month = "Februari";
                        break;
                    case "03":
                        $month = "Maret";
                        break;
                    case "04":
                        $month = "April";
                        break;
                    case "05":
                        $month = "Mei";
                        break;
                    case "06":
                        $month = "Juni";
                        break;
                    case "07":
                        $month = "Juli";
                        break;
                    case "08":
                        $month = "Agustus";
                        break;
                    case "09":
                        $month = "September";
                        break;
                    case "10":
                        $month = "Oktober";
                        break;
                    case "11":
                        $month = "Nopember";
                        break;
                    case "12":
                        $month = "Desember";
                        break;
                }
            }
            return $tempSplit[2]. " ". $month." ".$tempSplit[0];
        }
    }

?>