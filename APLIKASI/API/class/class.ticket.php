<?php
/*
|--------------------------------------------------------------------------
| Ticket
|--------------------------------------------------------------------------
|
|class untuk menghandle ticket
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
include_once($SYSTEM['DIR_PATH']."/class/class.master_db.php");
class Ticket extends  masterDB
{

    private $s_helpdesk_active = "";
    function Ticket()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }
    
    /**
    * periksa list ticket yang ada didatabase
    */
    function getList($_condition,$_order,$_limit)
    {
        $sql =      "   SELECT T.*,UR.realName as userRequestName ,HH.realName as helpdeskHandleName
                        FROM `ticket`  T
                        LEFT JOIN `user` UR  ON T.ticketRequestID = `UR`.`userID`
                        LEFT JOIN `user` HH  ON T.ticketHelpdeskHandle = `HH`.`userID`
                        {$_condition}  {$_order} {$_limit} 

                    " ;
        return $this->getResult($sql);
    }

    function getCount($_condition)
    {
        $sql =  "   SELECT count(*) as total 
                    FROM `ticket` 
                    {$_condition} ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];

    }
    function create($_user_request_ID,$_unit_id,$_problem_desc)
    {
    	$a_helpdesk_handle = array();
    	$a_helpdesk_counter = array();
    	$a_query = array();

    	//mencari user helpdesk yang handle dan sedang aktif
        $sql = "        SELECT * FROM `helpdeskHandle`  WHERE unitHandle = '$_unit_id'  AND isActive='1' ORDER BY hhID ASC ";
        $a_helpdesk_handle =  $this->getResult($sql);
    	$sql = "        SELECT * FROM `helpdeskCounter`  WHERE unitID = '$_unit_id'  ";
    	$a_helpdesk_counter =  $this->getResult($sql);

    	$giliran_helpdesk = ""; // helpdeskID
    	$i_counter  = 0; //index record array
        $i_count_ticket = $this->getCount(" WHERE YEAR(NOW()) = YEAR(ticketRequestDate) AND MONTH(NOW()) = MONTH(ticketRequestDate) ") + 1;
        $s_ticket_number = "TICKET".$_unit_id.date('Ym').str_pad($i_count_ticket,6,"0",STR_PAD_LEFT);
        if(count($a_helpdesk_handle) > 0)
        {
	        if(count($a_helpdesk_counter) >=  1)
	        {
	        	if($a_helpdesk_counter[0]['lastHelpdesk'] >= (count($a_helpdesk_handle) -1) )
	        	{
	        		$i_counter = 0;
	        	}
	        	else
	        	{
	        		$i_counter = $a_helpdesk_counter[0]['lastHelpdesk'] +1 ;

	        	}
				$a_query[] = " 
								UPDATE `helpdeskCounter` 
								SET `lastHelpdesk` = '$i_counter' 
								WHERE `unitID` = '{$_unit_id}'; ";
	        }
	        else
	        {
                $i_counter = 0;
				$a_query[] = " 
								INSERT INTO `helpdeskCounter` 
									(`unitID`, `lastHelpdesk`) 
								VALUES ('{$_unit_id}', '$i_counter'); ";


	        }
            $giliran_helpdesk = $a_helpdesk_handle[$i_counter]['helpdeskID'];
            $a_query[] = " 
                            INSERT INTO `ticket`( 
                                    `ticketNumber`, 
                                    `ticketRequestID`,
                                    `ticketRequestDate`, 
                                    `ticketProblemDesc`, 
                                    `ticketHelpdeskHandle`
                                ) 
                            VALUES (
                                    '$s_ticket_number',
                                    '{$_user_request_ID}',
                                    now(),
                                    '$_problem_desc',
                                    '{$giliran_helpdesk}'

                                ); ";
            $this->s_helpdesk_active = $a_helpdesk_handle[$i_counter]['helpdeskID'];

            return $this->queryTransaction($a_query);
        }
        else
        {
        	return false;
        }

    }

    function getHelpdeskActive()
    {
        return $this->s_helpdesk_active;
    }
    function setRating($_ticket_id,$_rating,$_feedback)
    {
        $a_query[] = " 
                        UPDATE `ticket` 
                        SET 
                            `ticketStar` = '$_rating',
                            `ticketUserFeedback` = '$_feedback' 
                        WHERE `ticketID` = '{$_ticket_id}'; ";
                        //print_r($a_query);
        return $this->queryTransaction($a_query);
    }
}
?>