ALTER TABLE `helpdeskHandle` ADD `isActive` BOOLEAN NULL DEFAULT TRUE AFTER `unitHandle`;
INSERT INTO `menu` (`id`, `parentID`, `icon`, `title`, `url`, `menuOrder`, `keterangan`, `status`) VALUES (NULL, '0', 'fa  fa-power-off', 'OFFLINE', 'index.php?page=offline', '20', 'offline helpdesk', '1');
INSERT INTO `userMenu` (`userMenuID`, `menuID`, `groupID`, `status`) VALUES (NULL, '20', 'GROUP_SPV_IT', '1');
ALTER TABLE `ticket` ADD `ticketStar` TINYINT NULL DEFAULT '0' AFTER `ticketSolveDesc`;
ALTER TABLE `ticket` ADD `ticketUserFeedback` VARCHAR(255) NULL AFTER `ticketStar`;

ALTER TABLE `group` ADD `image` VARCHAR(100) NULL AFTER `name`;


INSERT INTO `menu` (`id`, `parentID`, `icon`, `title`, `url`, `menuOrder`, `keterangan`, `status`) VALUES
(22, 10, '', 'LAP ASSET PUSAT', 'index.php?page=laporan&action=asset', 3, NULL, 1);


INSERT INTO `userMenu` (`userMenuID`, `menuID`, `groupID`, `status`) VALUES
(34, 22, 'GROUP_HELPDESK', 1);