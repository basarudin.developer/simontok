
TRUNCATE `appVersion`;
TRUNCATE `counterDB`;
TRUNCATE `firewall`;
TRUNCATE `helpdeskCounter`;
TRUNCATE `komputerItem`;
TRUNCATE `userMenu`;
TRUNCATE `wilayahKerja`;

INSERT INTO `appVersion` VALUES ('simontok',1);
INSERT INTO `counterDB` VALUES ('komputer','KOM','2019-01-07',4,1,NULL),('log','LOG','2019-01-07',9,1,NULL),('user','USER','2012-10-15',2,1,NULL);
INSERT INTO `firewall` VALUES (1,'basar','10.5.11.3','2018-06-05'),(2,'basar','10.5.11.4','2018-06-05');
INSERT INTO `group` VALUES 
('GROUP_HELPDESK','HELPDESK',1,''),
('GROUP_SPV_IT','SUPERVISOR IT',1,''),
('GROUP_PEGAWAI','GROUP PEGAWAI',1,''),
('GRP00000000000000000','COMMON',1,'common user'),
('SUPER_ADMINISTRATOR','ADMINISTRATOR',1,'mengurusi pengguna berserta group akses level');
INSERT INTO `helpdeskCounter` VALUES ('51100',0);
INSERT INTO `komputerItem` VALUES ('KMITAPT0000000000001','APLIKASI'),('KMITCAM0000000000001','CAM'),('KMITCDR0000000000001','CD ROM'),('KMITDPK0000000000001','SPEAKER'),('KMITDVR0000000000001','DVD ROM'),('KMITFLP0000000000001','FLOPPY'),('KMITHDK0000000000001','HARDDISK'),('KMITKYB0000000000001','KEYBOARD'),('KMITLNC0000000000001','LANCARD'),('KMITMEM0000000000001','MEMORY'),('KMITMNT0000000000001','MONITOR'),('KMITMTH0000000000001','MOTHERBOARD'),('KMITMUS0000000000001','MOUSE'),('KMITOS00000000000001','SISTEM OPERASI'),('KMITPRC0000000000001','PROCESSOR'),('KMITPRN0000000000001','PRINTER'),('KMITSCN0000000000001','SCANNER');
INSERT INTO `menu` VALUES 
(1,0,'fa fa-cubes','MASTER','',1,NULL,1),
(2, 1, 'fa fa-home', 'WILAYAH KERJA', 'index.php?page=wilayah', '0', NULL, '1'),
(3,1,'fa fa-user','PENGGUNA','index.php?page=user',1,NULL,1),
(4,1,'fa  fa-user-secret','HELPDESK','index.php?page=helpdesk',2,NULL,1),
(5,0,'fa fa-tablet','DEVICE','index.php?page=device',3,NULL,1),
(6,0,'fa fa-tv','KOMPUTER','index.php?page=komputer',4,NULL,1),
(7,0,'fa fa-tags','TIKET','index.php?page=tiket',5,NULL,1),
(8,0,'fa  fa-line-chart','DASHBOARD','index.php?page=dashboard 	',11,NULL,1),
(9, 0, 'fa fa-bell', 'NOTIFIKASI', 'index.php?page=notification', 9, NULL, 1),
(10, '0', 'fa fa-file', 'LAPORAN', '', '10', NULL, '1'),
(11, '10', '', 'LAP TIKET', 'index.php?page=laporan&action=ticket', '0', NULL, '1'),
(12, '10', '', 'LAP KOMPUTER', 'index.php?page=laporan&action=komputer_csv&type=komputer_csv', '2', NULL, '1'),
(13, '6', '', 'LIST KOMPUTER', 'index.php?page=komputer', '0', NULL, '1'),
(14, '6', '', 'PERUBAHAN KOMPONEN', 'index.php?page=komputer&action=delete-item', '1', NULL, '1'),
(15, '6', '', 'PENGHAPUSAN KOMPUTER', 'index.php?page=komputer&action=delete-confirm', '1', NULL, '1'),
(16, '10', '', 'LAP SIS. OP.', 'index.php?page=laporan&action=os_csv&type=os_csv', '2', NULL, '1'),
(17, '10', '', 'LAP DEVICE', 'index.php?page=laporan&action=device_csv&type=device_csv', '2', NULL, '1'),
(18,	0,	'fa fa-ellipsis-h',	'GANTI PASSWORD',	'index.php?page=user&action=ganti_password',	99,	NULL,	1),
(19, '10', '', 'DETIL RECOVERY', 'index.php?page=laporan&action=detil_recovery_csv', '1', NULL, '1'),
(20, '10', '', 'REKAP RECOVERY','index.php?page=laporan&action=rekap_recovery_csv&type=rekap_recovery_csv', '2', NULL, '1');
INSERT INTO `pageRegistered` 
VALUES 
('CHP001','changePassword.svr.pln',''),
('CHP002','changePassword.pln',''),
('DASH01','dashboard',''),
('IND001','index.pln','halaman utama'),
('MNU01','menu.pln',''),
('MSG001','message.pln',''),
('PRF001','profile.pln',''),
('PRV001','privileges.pln',''),
('USR001','user.pln',''),
('LAP001', 'laporan', ''),
('LAP100', 'laporan admin', ''),
('KOM009', 'aproval delete item', '');
INSERT INTO `userAccessLevel` VALUES
 (1,'GRP00000000000000000','IND001',1,''),
 (2,'GRP00000000000000000','MNU01',1,NULL),
 (4,'GRP00000000000000000','CHP002',1,NULL),
 (5,'GRP00000000000000000','CHP001',1,NULL),
 (7,'GRP00000000000000000','MSG001',1,NULL),
 (8,'GRP00000000000000000','PRF001',1,NULL),
 (9,'SUPER_ADMINISTRATOR','USR001',1,NULL),
 (10,'SUPER_ADMINISTRATOR','PRF001',1,NULL),
 (11,'SUPER_ADMINISTRATOR','PRV001',1,NULL),
 (12,'GROUP_HELPDESK','DASH01',1,NULL),
 (13,'SUPER_ADMINISTRATOR','DASH01',1,NULL),
 (NULL, 'GROUP_HELPDESK', 'LAP001', '1', NULL),
 (NULL, 'GROUP_SPV_IT', 'KOM009', '1', NULL),
 (NULL, 'GROUP_SPV_IT', 'DASH01', '1', NULL),
 (NULL, 'GROUP_SPV_IT', 'LAP001', '1', NULL),
  (NULL, 'SUPER_ADMINISTRATOR', 'LAP001', '1', NULL),
  (NULL, 'SUPER_ADMINISTRATOR', 'LAP100', '1', NULL);
 
INSERT INTO `userMenu` VALUES 
(null,1,'SUPER_ADMINISTRATOR',1),
(null,2,'SUPER_ADMINISTRATOR',1),
(null,3,'SUPER_ADMINISTRATOR',1),
(null,4,'SUPER_ADMINISTRATOR',1),
(null,5,'SUPER_ADMINISTRATOR',1),
(null,7,'SUPER_ADMINISTRATOR',1),
(null,8,'SUPER_ADMINISTRATOR',1),
(null,9,'SUPER_ADMINISTRATOR',1),
(null,10,'SUPER_ADMINISTRATOR',1),
(null,19,'SUPER_ADMINISTRATOR',1),
(null,20,'SUPER_ADMINISTRATOR',1),

(null,5,'GROUP_HELPDESK',1),
(null,6,'GROUP_HELPDESK',1),
(null,10,'GROUP_HELPDESK',1),
(null,11,'GROUP_HELPDESK',1),
(null,12,'GROUP_HELPDESK',1),
(null,13,'GROUP_HELPDESK',1),
(null,14,'GROUP_HELPDESK',1),
(null,7,'GROUP_HELPDESK',1),
(null,8,'GROUP_HELPDESK',1),
(null,9,'GROUP_HELPDESK',1),

(null,6,'GROUP_SPV_IT',1),
(null,8,'GROUP_SPV_IT',1),
(null,10,'GROUP_SPV_IT',1),
(null,11,'GROUP_SPV_IT',1),
(null,12,'GROUP_SPV_IT',1),
(null,14,'GROUP_SPV_IT',1),
(null,15,'GROUP_SPV_IT',1),
(null,16,'GROUP_SPV_IT',1),
(null,17,'GROUP_SPV_IT',1),

(null,	18,	'GRP00000000000000000',	1);

INSERT INTO `wilayahKerja`  (`idWilayah`, `namaWilayah`, `parentWilayah`) VALUES 
('51100','AREA SURABAYA UTARA',null),
('51101','RAYON INDRAPURA','51100'),
('51102','RAYON PLOSO','51100'),
('51201','AREA SIDOARJO GEDANGAN',null);



INSERT INTO `user` (`userID`, `wilayahPenempatan`, `username`, `realName`, `lastLoginIP`, `password`, `mail`, `phone`, `messenger`, `bod`, `join`, `lastLogin`, `lastSession`, `firebaseToken`, `loginToken`, `language`, `gender`, `avatar`, `notes`, `isDC`, `active`) VALUES
('basar', '51100', 'basar', 'ahmad basarudin', NULL, '271989bd84d694a533692a6227c43a4d', '@basar', NULL, NULL, NULL, NULL, '2019-01-07 22:25:00', 'bg89sfpqk3b9kkgm9439tic1f6', 'fjYV9LbvKBU:APA91bHRIHOpr-wAqwglG9NExdcZKPVTGpqjFmE1Ik0ncEl-RfQ9YiQon3O9iNnp0zpXZ4hH6-otk1jEzccAd51G4ZO27PcxaC4ERtQgsGM9Dxo9rQYAUjbxmaX_72xM8NwCtwixU-xY', 'bg89sfpqk3b9kkgm9439tic1f6', NULL, 'm', NULL, NULL, 1, 1);

INSERT INTO `userGroup` (`userGroupID`, `userID`, `groupID`, `status`) VALUES
(null, 'basar', 'GRP00000000000000000', 1),
(null, 'basar', 'SUPER_ADMINISTRATOR', 1);