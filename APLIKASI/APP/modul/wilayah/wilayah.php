<?php
    /*
    |--------------------------------------------------------------------------
    | wilayah
    |--------------------------------------------------------------------------
    |Controler  modul wilayah
    |
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";

    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    if(isset($_REQUEST['type']))
    {
        if ($_REQUEST['type'] == "model") 
        {
            require_once($SYSTEM['DIR_MODUL']."/wilayah/wilayah.model.php");
        } 
        elseif($_REQUEST['type'] != "")
        { 
            include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
            require_once($SYSTEM['DIR_MODUL']."/wilayah/wilayah.view.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/wilayah/wilayah.view.php");
        include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
    }

?>