<?php
    /*
    |--------------------------------------------------------------------------
    | wilayah Create 
    |--------------------------------------------------------------------------
    |Form untuk entry wilayah
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oWilayah = new Wilayah();

    $LAYOUT_JS_EXTENDED .= "

                    <script src='assets/bower_components/select2/dist/js/select2.full.min.js'></script>
                    <script src='modul/wilayah/wilayah.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/bower_components/select2/dist/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/adminlte.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button'  class='button-wilayah-create  btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus'></i> Tambah
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> kembali
                        </button>
                    ";
    $s_form_input = "";

    $a_data_list_wilayah = $oWilayah->getList(" WHERE TRUE ","","");

    $s_wilayah = "<select class='form-control select2' name='parent_wilayah'   >";
                $s_wilayah .= "<option value=''>tidak ada induk </option>";
                for($i=0;$i < count($a_data_list_wilayah);$i++)
                {
                    $s_wilayah .= "<option value='{$a_data_list_wilayah[$i]['idWilayah']}'>{$a_data_list_wilayah[$i]['namaWilayah']}</option>";
                }
                $s_wilayah .= "</select>";

    $s_form_input = "
                    <form id='form-create' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                            <!-- Sistem Operasi -->
                                            <div class='form-group'>
                                                <label>Wilayah Induk:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-home'></i>
                                                    </div>
                                                    $s_wilayah
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>

                                    <div class='col-md-4'>
                                            <!-- Sistem Operasi -->
                                            <div class='form-group'>
                                                <label>Kode Wilayah:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa  fa-key'></i>
                                                    </div>
                                                    <input type='text' class='form-control' name='kode_wilayah'>
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-8'>
                                            <!-- Hostname -->
                                            <div class='form-group'>
                                                <label>Nama Wilayah:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-text-width'></i>
                                                    </div>
                                                    <input type='text' class='form-control' name='nama_wilayah'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
                        
            
    $s_info = "
        <div class='alert alert-warning alert-dismissible'>
            <h4> Cara Pengisian</h4>
            <b>kode wilayah</b> parent merupakan induk dari wilayah yang akan diisi. dibiarkan kosong jika wilayah yang akan diisi merupakan wilayah induk<br />
            <b>kode wilayah</b> diisi dengan 6 angka  berdasarkan kode dari masing masing wilayah.<br />
            <b>nama wilayah</b> diisi dengan alpanumerik yang merupakan nama dari wilayah tersebut.<br />
        </div>";
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>TAMBAH WILAYAH</h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                          {$s_info}
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oWilayah->closeDB();

?>