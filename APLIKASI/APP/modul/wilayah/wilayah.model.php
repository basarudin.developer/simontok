<?PHP
     /**
      * wilayah.model.php
      * 
      * File ini digunakan untuk model wilayah
      *  
      *
      *
      * prefix parameter:
      *    n  - node
      *    o  - object
      *    a  - array
      *    s  - string
      *    b  - boolean
      *    f  - float
      *    i  - integer
      *    uk  - unknown
      *    fn - function
      *    _  - parameter
      **/
    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oWilayah = new Wilayah();
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");
    $oHelpdesk = new Helpdesk();

    require_once($SYSTEM['DIR_PATH']."/class/function.notification.local.php");

    $a_errors = array();
    $respone['result'] = 'error';
    $respone['desc'] = "";


     
    if(isset($_REQUEST['action']))
    {

        if($_REQUEST['action'] == 'delete')
        {
            if($_REQUEST['wilayah_id'] == "")
            {
                $a_errors[] = "wilayah id tidak ada";
            }
            if (!$a_errors) 
            {


                $s_condition = " WHERE parentWilayah = '{$_REQUEST['wilayah_id']}'; " ;
                if($oWilayah->getCount($s_condition)==0)
                {
                    //jika ada helpdesk yang menghandle wilayah id

                    if($oHelpdesk->getCount(" WHERE unitHandle='{$_REQUEST['wilayah_id']}' ") == 0)
                    {

                        if($oWilayah->delete($_REQUEST['wilayah_id']))
                        {

                            $respone['result'] = 'success';
                            $respone['desc'] = "wilayah  berhasil dihapus.";
                            $s_notif_message = "{$USER[0]['realName']} menghapus wilayah  <span class='label label-danger'>{$_REQUEST['wilayah_id']}";  
                            create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
                                              
                        }
                        else
                        {

                            $respone['result'] = 'error';
                            $respone['desc'] = "wilayah tidak dapat dihapus.";
                        }
                    }
                    else
                    {

                        $respone['result'] = 'error';
                        $respone['desc'] = "wilayah ini sudah dihandle oleh helpdesk. jika anda ingin menghapus wilayah ini, harap menghapus wilayah yang dihandle oleh helpdesk.";
                    }
                }
                else
                {

                    $respone['result'] = 'error';
                    $respone['desc'] = "wilayah tidak dapat dihapus karena masih memiliki wilayah dibawahnya. untuk menghapus wilayah ini, anda harus menghapus wilayah yang berada dibawahnya terlebih dahulu.";
                }

            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) 
                {
                    $sError .= "$error<br />";
                }
                $respone['result'] = 'error';
                $respone['desc'] = $sError;
            }

        }
        elseif($_REQUEST['action'] == 'create')
        {
            if(!isset($_REQUEST['kode_wilayah']))
            {
                $a_errors[] = "wilayah id tidak ada";
            }
            else
            {
                if($_REQUEST['kode_wilayah'] == "")
                {
                    $a_errors[] = "kode wilayah harus diisi";
                }
            }
            if(!isset($_REQUEST['nama_wilayah']))
            {
                $a_errors[] = "nama wilayah  tidak ada";
            }
            else
            {
                if($_REQUEST['nama_wilayah'] == "")
                {
                    $a_errors[] = "nama wilayah harus diisi";
                }
            }
            if (!$a_errors) 
            {
                $_REQUEST['kode_wilayah'] = $oWilayah->antiInjection($_REQUEST['kode_wilayah']);
                //jika kode pernah dimasukan sebelumnya
                if($oWilayah->getCount(" WHERE idWilayah = '{$_REQUEST['kode_wilayah']}'") > 0)
                {

                    $respone['result'] = 'error';
                    $respone['desc'] = "kode wilayah sudah pernah dimasukan sebelumnya";
                }
                else
                {
                    $a_wilayah['idWilayah'] = $_REQUEST['kode_wilayah'];
                    $a_wilayah['namaWilayah'] = $_REQUEST['nama_wilayah'];
                    $a_wilayah['parentWilayah'] = $_REQUEST['parent_wilayah'];
                    
                    if($oWilayah->create($a_wilayah))
                    {

                        $respone['result'] = 'success';
                        $respone['desc'] = "data wilayah berhasil dimasukan";
                        $s_notif_message = "{$USER[0]['realName']} menambah wilayah  <span class='label label-success'>{$a_wilayah['namaWilayah']}";  
                        create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
                        
                    }
                    else
                    {
                        $respone['result'] = 'error';
                        $respone['desc'] = "sistem tidak dapat memasukan data wilayah";

                    }
                }
            }
        }


          
    }
    echo json_encode($respone);
    $oWilayah->closeDB();
    $oHelpdesk->closeDB();

?>