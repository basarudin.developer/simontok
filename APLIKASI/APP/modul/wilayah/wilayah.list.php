<?php
    /*
    |--------------------------------------------------------------------------
    | wilayah view
    |--------------------------------------------------------------------------
    |view  modul wilayah 
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oWilayah = new Wilayah();


    $a_title[] = "KODE WILAYAH";
    $a_title[] = "NAMA WILAYAH";
    $a_title[] = "PARENT";
    $a_title[] = "";
    $a_title_class[] = " style='width:50px;' ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='width:10px;' ";

    $s_table_container = "";
    $s_condition = " WHERE TRUE";
    $s_limit = "  ";
    $s_order = "  ORDER BY `idWilayah` ASC ";


    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/wilayah/wilayah.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";


    $a_data = $oWilayah->getList($s_condition, $s_order, $s_limit);
    if(isset($a_data))
    {
          
        $s_table_container ="";
        $s_table_container .="
                    <table  id='table-wilayah' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
        $s_table_container .="</tr>
                        </thead>";
        $s_table_container .="<tbody>";

        for($i=0;$i<count($a_data);$i++)
        {
            $s_status = "";
            $s_button = "";
            $s_bg_column = "";

            $s_condition = " WHERE parentWilayah = '{$a_data[$i]['idWilayah']}'; " ;
            if($oWilayah->getCount($s_condition)==0)
            {
                $s_button = "<button class='button-wilayah-delete btn btn-flat   btn-danger btn-sm '  record-id='{$a_data[$i]['idWilayah']}'  style='margin-left: 2px;'>Hapus</button>";
            }

                    
            //untuk informasi jumlah soal
            $s_table_container .="<tr >";
            $s_table_container .= "<td  align='left'>"
                                     .strtoupper ($a_data[$i]['idWilayah'])
                                ."</td>";
            $s_table_container .= "<td  align='left'>"
                                     .strtoupper ($a_data[$i]['namaWilayah'])
                                ."</td>";
            $s_table_container .= "<td  align='left' >"
                                     .strtoupper ($a_data[$i]['namaParent'])
                                ."</td>";
            $s_table_container .= "<td  > $s_button</td>";
            $s_table_container .="</tr>";
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
    }
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>WILAYAH KERJA</h4>
                                </div>
                                <div style='float:right'>


                                    <button type='button' class='button-wilayah-create btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                                        <i class='  fa fa-home'></i> Tambah
                                    </button>
                                    <button type='button' class='button-wilayah-simplemode btn btn-flat  btn-sm btn-warning pull-right' style='margin-left: 5px;'>
                                        <i class='  fa fa-eye'></i> Tampilan Sederhana
                                    </button>
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oWilayah->closeDB();

?>