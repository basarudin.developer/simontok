$(document).ready(function() {

    /*************************************************
    |   BEGIN TABLE-COMPUTER
    **************************************************/
    if($('#table-wilayah').length)
    {
        jQuery('.button-wilayah-create').click(function() {
            document.location='index.php?page=wilayah&action=create'
        });
        jQuery('.button-wilayah-simplemode').click(function() {
            document.location='index.php?page=wilayah&action=simplemode'
        });

        $('#table-wilayah').DataTable
        ({        
            rowReorder: true,
            columnDefs: [
                { orderable: true, className: 'reorder', targets:[ 0 ] },
                { orderable: false, targets: '_all' }
            ],
            lengthMenu: [[10, 25, -1], [10, 25, "All"]],
             order: [[ 0, "asc" ]]
        });


        $('#table-wilayah tbody').on( 'click', '.button-wilayah-delete', function () 
        {
            wilayah_id = $(this).attr('record-id');
            //alert(wilayah_id);
            //alert(device_register_id);
            $.ajax({
                type: 'POST',
                url: "index.php?page=wilayah&type=model&action=delete",
                dataType:"json",
                data: 'wilayah_id='+wilayah_id,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        $('#notification-container').hide();
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(100);
                            setTimeout(function() {
                                document.location='index.php?page=wilayah&action=list'
                            }, 150);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(100); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#container-alert').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        $('#notification-container').hide();
                        $('#notification-container').removeClass('alert-success');
                        $('#notification-container').addClass('alert-danger'); 
                        $('#notification-content').html('Terjadi kegagalan proses transaksi');
                        $('#notification-container').show(); 
                        $('html, body').animate({
                        scrollTop: $('#container-alert').offset().top
                        }, 500);
                    }
            });//end of ajax
        });


    }
    /*   END TABLE-WILAYAH
    **************************************************/
        
    if($('#button-back').length)
    {
        jQuery('#button-back').click(function() {
            document.location='index.php?page=wilayah&action=list'
        });
    }
    if($('#button-wilayah-create-apply').length)
    {
        jQuery('#button-back').click(function() {
            document.location='index.php?page=wilayah&action=list'
        });
        $('#button-wilayah-create-apply').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=wilayah&type=model&action=create",
                    dataType:"json",
                    data: $("#form-create").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            $('#notification-container').hide();
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(100);
                                setTimeout(function() {
                                    document.location='index.php?page=wilayah&action=list'
                                }, 150);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(100); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            $('#notification-container').hide();
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html('Terjadi kegagalan proses transaksi');
                            $('#notification-container').show(); 
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);
                        }
                });//end of ajax

            });
    }

    

});