<?php
    /*
    |--------------------------------------------------------------------------
    | Audit Config Export
    |--------------------------------------------------------------------------
    |Sebagai file configurasi vbscript
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
	require_once("audit.config.php");
	require_once("../../config.php");
	$s_path_form="/audit.form.php";
	$s_path_upload="/audit.upload.php";

	$host_name="";
	if (isset($_GET['hostname'])and ($_GET['hostname'] <>"")) 
	{
	    $host_name=$_GET['hostname'];
	} else 
	{
	    $host_name=".";
	}

	$application="";
	if (isset($_GET['application'])and ($_GET['application'] <>"")) 
	{
	    $application=$_GET['application'];
	} else 
	{
	    $application="audit.vbs";
	}

	$_REAL_SCRIPT_DIR=realpath(dirname($_SERVER['SCRIPT_FILENAME'])); 
	$_REAL_BASE_DIR=realpath(dirname(__FILE__));  
	$_MY_PATH_PART=substr( $_REAL_SCRIPT_DIR, strlen($_REAL_BASE_DIR)); 

	$INSTALLATION_PATH=$_MY_PATH_PART ? substr( dirname($_SERVER['SCRIPT_NAME']), 0, -strlen($_MY_PATH_PART) ) : dirname($_SERVER['SCRIPT_NAME']);

	$our_host= "http://".$_SERVER['HTTP_HOST'];
	$remote_addr= $_SERVER['REMOTE_ADDR'];

	$remote_host=gethostbyaddr($_SERVER['REMOTE_ADDR']);
	if ( $remote_host == "") 
	{
		$remote_host= $_SERVER['REMOTE_HOST'];
	} else {}

	$our_instance=$INSTALLATION_PATH;

	$config_newline="\r\n";
	$this_config='audit_location="r"'.$config_newline;


	$this_config=$this_config.'server_install_path="'.$INSTALLATION_PATH.'"'.$config_newline;
	$this_config=$this_config.'verbose="y"'.$config_newline; 
	$this_config=$this_config.'audit_host="'.$our_host.'"'.$config_newline;
	$this_config=$this_config.'online="yesxml"'.$config_newline; 
	$this_config=$this_config.'strComputer="'.$host_name.'"'.$config_newline; 
	$this_config=$this_config.'strUser=""'.$config_newline; 
	$this_config=$this_config.'strPass=""'.$config_newline; 
	$this_config=$this_config.'ie_visible="n"'.$config_newline;
	$this_config=$this_config.'ie_auto_submit="y"'.$config_newline;
	$this_config=$this_config.'ie_submit_verbose="n"'.$config_newline;
	$this_config=$this_config.'ie_form_page="'.$our_host.$our_instance.$s_path_form.'"'.$config_newline; 
	$this_config=$this_config.'non_ie_page="'.$our_host.$our_instance.$s_path_upload.'"'.$config_newline; 
	$this_config=$this_config.'input_file=""'.$config_newline; 
	$this_config=$this_config.'audit_local_domain="n"'.$config_newline;
	$this_config=$this_config.'domain_type="ldap"'.$config_newline;
	$this_config=$this_config.'local_domain="LDAP://mydomain.local"'.$config_newline; 
	$this_config=$this_config.'hfnet="n"'.$config_newline; 
	$this_config=$this_config.'Count=0'.$config_newline; 
	$this_config=$this_config.'number_of_audits=10'.$config_newline; 
	$this_config=$this_config.'script_name="'.$application.'"'.$config_newline; 
	$this_config=$this_config.'monitor_detect="y"'.$config_newline; 
	$this_config=$this_config.'printer_detect="y"'.$config_newline; 
	$this_config=$this_config.'software_audit="y"'.$config_newline; 
	//$this_config=$this_config.'software_file_audit="y"'.$config_newline; 
	$this_config=$this_config.'uuid_type="uuid"'.$config_newline;

	$this_config=$this_config.'keep_this_config="'.$AUDIT['KEEP_CONFIG'].'"'.$config_newline;

	$this_config=$this_config.'keep_audit_log="'.$AUDIT['KEEP_CONFIG'].'"'.$config_newline;

	$this_config=$this_config.'requesting_host="'.$remote_host.'"'.$config_newline;
	$this_config=$this_config.'requesting_addr="'.$remote_addr.'"'.$config_newline;


	//new config
	$this_config=$this_config.'strcomputer="'.$strcomputer.'"'.$config_newline;
	$this_config=$this_config.'submit_online="'.$submit_online.'"'.$config_newline;
	$this_config=$this_config.'create_file="'.$AUDIT['KEEP_CONFIG'].'"'.$config_newline;
	$this_config=$this_config.'discovery_id="'.$discovery_id.'"'.$config_newline;
	$this_config=$this_config.'use_proxy="'.$use_proxy.'"'.$config_newline;
	$this_config=$this_config.'struser="'.$struser.'"'.$config_newline;
	$this_config=$this_config.'strpass="'.$strpass.'"'.$config_newline;
	$this_config=$this_config.'ignore_invalid_ssl="'.$ignore_invalid_ssl.'"'.$config_newline;
	$this_config=$this_config.'org_id="'.$org_id.'"'.$config_newline;
	$this_config=$this_config.'use_active_directory="'.$use_active_directory.'"'.$config_newline;
	$this_config=$this_config.'windows_user_work_1="'.$windows_user_work_1.'"'.$config_newline;
	$this_config=$this_config.'windows_user_work_2="'.$windows_user_work_2.'"'.$config_newline;
	$this_config=$this_config.'audit_mount_point="'.$audit_mount_point.'"'.$config_newline;
	$this_config=$this_config.'audit_software="'.$audit_software.'"'.$config_newline;
	$this_config=$this_config.'audit_win32_product="'.$audit_win32_product.'"'.$config_newline;
	$this_config=$this_config.'audit_dns="'.$audit_dns.'"'.$config_newline;
	$this_config=$this_config.'audit_netstat="'.$audit_netstat.'"'.$config_newline;
	$this_config=$this_config.'self_delete="'.$self_delete.'"'.$config_newline;
	$this_config=$this_config.'debugging="'.$debugging.'"'.$config_newline;
	$this_config=$this_config.'version="'.$version.'"'.$config_newline;
	$this_config=$this_config.'ldap="'.$ldap.'"'.$config_newline;
	$this_config=$this_config.'ldap_seen_days="'.$ldap_seen_days.'"'.$config_newline;
	$this_config=$this_config.'ldap_seen_date="'.$ldap_seen_date.'"'.$config_newline;
	$this_config=$this_config.'ping_target="'.$ping_target.'"'.$config_newline;
	$this_config=$this_config.'system_id="'.$system_id.'"'.$config_newline;
	$this_config=$this_config.'last_seen_by="'.$last_seen_by.'"'.$config_newline;
	$this_config=$this_config.'details_to_lower="'.$details_to_lower.'"'.$config_newline;
	$this_config=$this_config.'help="'.$help.'"'.$config_newline;
	$this_config=$this_config.'hide_audit_window="'.$hide_audit_window.'"'.$config_newline;
	$this_config=$this_config.'san_audit="'.$san_audit.'"'.$config_newline;
	$this_config=$this_config.'san_discover="'.$san_discover.'"'.$config_newline;
	$this_config=$this_config.'url="'.$our_host.$our_instance.$s_path_upload.'"'.$config_newline;
	$this_config=$this_config.'yes="y"'.$config_newline;
	$this_config=$this_config.'nol="0"'.$config_newline;
	echo $this_config;

?>