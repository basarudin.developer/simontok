<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer Audit Upload
    |--------------------------------------------------------------------------
    |Upload data PC / Laptop dengan mengeksekusi vbs
    |
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */




    require_once("../../config.php");
    require_once($SYSTEM['DIR_MODUL_CORE']."/init.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.auditconvert.php");




    
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.tokenhandle.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");

    $oComputer = new Computer();
    $oComputerItem = new Computer();
    $oTokenHandle = new TokenHandle();
    $oNotification = new Notification();
    $oHelpdesk = new Helpdesk();
    $oUser = new UserInfo();
    
    $response['result'] = "success";
    $response['desc'] = "";

    $net_mac_address = "";
    $s_komputer_id = "";
    $s_komputer_tipe = "CLIENT";
    $s_komputer_posisi = "";
    $s_komputer_ip = "0.0.0.0";
	
	
	$block_lan_adapter = "FORTINET";


    
    require_once($SYSTEM['DIR_MODUL']."/audit/audit.config.php");
    $s_komputer_name = "";
     
    if(isset($_POST['data']))
    {
        $DATA = audit_convert($_POST['data']); 
        //echo '<pre>' . var_export($DATA['sys']['config_token'], true) . '</pre>';
        //echo '<pre>' . var_export($DATA['sys']['config_helpdesk_group'], true) . '</pre>';
        //echo '<pre>' . var_export($DATA['network'], true) . '</pre>';
        //periksa group helpdesk
        //if(isset($DATA['sys']['config_helpdesk_group']))
        //{
            //jika group helpdesk ada di group user 
            //if($oHelpdesk->getCountGroupHelpdeskByGroup($DATA['sys']['config_helpdesk_group']) > 0)
            //{

                //jika token belum expired dan belum dipakai
                if(isset($DATA['sys']['config_token']))
                {
                    if($oTokenHandle->isFree($DATA['sys']['config_token']))
                    {
                        if(!$oTokenHandle->isExpired($DATA['sys']['config_token']))
                        {
                            //jika mac sudah terdaftar
                            if(isset($DATA['network']['item']))
                            {
                                if(count($DATA['network']['item']) > 0)
                                {
                                    $found_mac_address = false;
                                    if(isset($DATA['network']['item']['mac']))
                                    {
                                        if($oComputerItem->ifExistMacAdress($DATA['network']['item']['mac']))
                                        {
                                            $found_mac_address = true;
                                            $s_komputer_id = $oComputerItem->ifExistMacAdress($DATA['network']['item']['mac']);
                                        }
                                    }
                                    else
                                    {
                                        for($i=0;$i<count($DATA['network']['item']);$i++)
                                        {
                                            if($oComputerItem->ifExistMacAdress($DATA['network']['item'][$i]['mac']))
                                            {
                                                $found_mac_address = true;
                                                $s_komputer_id = $oComputerItem->ifExistMacAdress($DATA['network']['item'][$i]['mac']);
                                            }
                                        }    
                                    }
                                    

                                    //------------------------------jika mac sudah terdaftar maka  scan jika ada upgrade
                                    //------------------------------jika mac belum maka masukan sebagai device baru
                                    if(!$found_mac_address)
                                    {
                                        //print_r($DATA['sys']);
                                        if(isset($DATA['sys']['ip']))
                                        {
                                            if(is_array($DATA['sys']['ip']))
                                            {
                                                if(count($DATA['sys']['ip']) == 0)
                                                {
                                                    $DATA['sys']['ip'] = $s_komputer_ip;
                                                }   
                                            }
                                        }
                                        else
                                        {
                                            $DATA['sys']['ip'] = $s_komputer_ip;
                                        }
                                        $a_data_token = $oTokenHandle->detail($DATA['sys']['config_token']);
                                        if($oHelpdesk->isHelpdesk($a_data_token[0]['createUser']))
                                        {
                                            $a_data_user = $oUser->getUserDetail($a_data_token[0]['createUser']);

                                            if($oComputer->createAudit(strtoupper($s_komputer_tipe),strtoupper($DATA['sys']['form_factor']),$DATA['sys']['ip'],$DATA['sys']['hostname'],$a_data_user[0]['wilayahPenempatan'],$DATA['sys']['config_token']))
                                            {

                                                $s_komputer_id = $oComputer->lastInsertKomputerID();
                                                $response['logs'][] = "ID Komputer : ".$s_komputer_id."\n";

                                                //memasukan lancard
                                                //jika network tunggal
                                                if(isset($DATA['network']['item']['mac']))
                                                {
													if(!preg_match("/{$block_lan_adapter}/i", strtoupper($DATA['network']['item']['manufacturer']))) 
													{
														$oComputerItem->createServiceSpesification( 
																$s_komputer_id, 
																$a_komputer_item['LANCARD'], 
																$DATA['network']['item']['speed'], 
																$DATA['network']['item']['manufacturer'], 
																$DATA['network']['item']['description'],
																$DATA['network']['item']['mac'],
																$DATA['sys']['config_token']);
													} 
                                                }
                                                else
                                                {
                                                    for($i=0;$i<count($DATA['network']['item']);$i++)
                                                    {
														if(!preg_match("/{$block_lan_adapter}/i", strtoupper($DATA['network']['item'][$i]['manufacturer']))) 
														{
															$oComputerItem->createServiceSpesification( 
																$s_komputer_id, 
																$a_komputer_item['LANCARD'], 
																$DATA['network']['item'][$i]['speed'], 
																$DATA['network']['item'][$i]['manufacturer'], 
																$DATA['network']['item'][$i]['description'],
																$DATA['network']['item'][$i]['mac'],
																$DATA['sys']['config_token']);  
														} 
                                                    }
                                                }

                                                //Motherboard
                                                if(isset($DATA['motherboard']['item']))
                                                {
                                                    $oComputerItem->createServiceSpesification( 
                                                            $s_komputer_id, 
                                                            $a_komputer_item['MOTHERBOARD'], 
                                                            '1', 
                                                            $DATA['motherboard']['item']['manufacturer'], 
                                                            $DATA['motherboard']['item']['model'], 
                                                            $DATA['motherboard']['item']['serial'],
                                                            $DATA['sys']['config_token']);  
                                                
                                                }

                                                //processor
                                                if(isset($DATA['processor']['item']))
                                                {
                                                    $oComputerItem->createServiceSpesification( 
                                                            $s_komputer_id, 
                                                            $a_komputer_item['PROCESSOR'], 
                                                            $DATA['processor']['item']['speed'], 
                                                            $DATA['processor']['item']['manufacturer'], 
                                                            $DATA['processor']['item']['description'], 
                                                            $DATA['processor']['item']['core_count'],
                                                            $DATA['sys']['config_token']);  
                                                
                                                }


                                                //memori
                                                if(isset($DATA['memory']['item']))
                                                {
                                                    if(isset($DATA['memory']['item']['size']))
                                                    {
                                                        $oComputerItem->createServiceSpesification( 
                                                            $s_komputer_id, 
                                                            $a_komputer_item['MEMORY'], 
                                                            $DATA['memory']['item']['size'], 
                                                            $DATA['memory']['item']['speed'], 
                                                            $DATA['memory']['item']['form_factor'],
                                                            $DATA['memory']['item']['serial'],
                                                            $DATA['sys']['config_token']);   
                                                    }
                                                    else
                                                    {
                                                        for($i=0;$i<count($DATA['memory']['item']);$i++)
                                                        {
                                                            $oComputerItem->createServiceSpesification( 
                                                                $s_komputer_id, 
                                                                $a_komputer_item['MEMORY'], 
                                                                $DATA['memory']['item'][$i]['size'], 
                                                                $DATA['memory']['item'][$i]['speed'], 
                                                                $DATA['memory']['item'][$i]['form_factor'],
                                                                $DATA['memory']['item'][$i]['serial'],
                                                                $DATA['sys']['config_token']);   
                                                        }
                                                    }
                                                }
                                                //disk
                                                if(isset($DATA['disk']['item']))
                                                {
                                                    $status = "";
                                                    if(isset($DATA['disk']['item']['size']))
                                                    {
                                                        if(isset($DATA['disk']['item']['status']))
                                                        {

                                                            if(!is_array($DATA['disk']['item']['status']))
                                                            {
                                                                $status = $DATA['disk']['item']['status'];
                                                            }   
                                                        }
                                                        $oComputerItem->createServiceSpesification( 
                                                            $s_komputer_id, 
                                                            $a_komputer_item['HARDDISK'], 
                                                            $DATA['disk']['item']['size'], 
                                                            $DATA['disk']['item']['manufacturer'], 
                                                            $DATA['disk']['item']['model'],
                                                            $status,
                                                            $DATA['sys']['config_token']);   
                                                    }
                                                    else
                                                    {
                                                        //echo "masuk sana"; exit();
                                                        for($i=0;$i<count($DATA['disk']['item']);$i++)
                                                        {

                                                            $status = "";
                                                            if(isset($DATA['disk']['item'][$i]['status']))
                                                            {
                                                                if(!is_array($DATA['disk']['item'][$i]['status']))
                                                                {
                                                                    $status = $DATA['disk']['item'][$i]['status'];
                                                                }
                                                            }
                                                            $oComputerItem->createServiceSpesification( 
                                                                $s_komputer_id, 
                                                                $a_komputer_item['HARDDISK'], 
                                                                $DATA['disk']['item'][$i]['size'], 
                                                                $DATA['disk']['item'][$i]['manufacturer'], 
                                                                $DATA['disk']['item'][$i]['model'],
                                                                $status,
                                                                $DATA['sys']['config_token']);   
                                                        }   
                                                    }
                                                }
                                                //monitor
                                                if(isset($DATA['monitor']['item']))
                                                {
													
                                                    if(isset($DATA['monitor']['item']['size']))
                                                    {
														$oComputerItem->createServiceSpesification( 
																$s_komputer_id, 
																$a_komputer_item['MONITOR'], 
																$DATA['monitor']['item']['size'],
																$DATA['monitor']['item']['manufacturer'], 
																$DATA['monitor']['item']['description'], 
																$DATA['monitor']['item']['serial'],
																$DATA['sys']['config_token']); 
													} 
													else
													{
														
                                                        for($i=0;$i<count($DATA['monitor']['item']);$i++)
                                                        {
                                                            $oComputerItem->createServiceSpesification( 
                                                                $s_komputer_id, 
                                                                $a_komputer_item['MONITOR'], 
                                                                $DATA['monitor']['item'][$i]['size'], 
                                                                $DATA['monitor']['item'][$i]['manufacturer'], 
                                                                $DATA['monitor']['item'][$i]['model'],
                                                                $status,
                                                                $DATA['sys']['config_token']); 
														}
													}
                                                
                                                }
                                                //os
                                                if(isset($DATA['sys']))
                                                {
                                                    $oComputerItem->createServiceSpesification( 
                                                        $s_komputer_id, 
                                                        $a_komputer_item['SISTEM_OPERASI'],
                                                        1, 
                                                        $DATA['sys']['os_group'], 
                                                        $DATA['sys']['os_name']." ". $DATA['sys']['os_bit'],
                                                        $DATA['sys']['os_family'],
                                                        $DATA['sys']['config_token']);   
                                                }
                                                //software
                                                if(isset($DATA['software']['item']))
                                                {
                                                    for($i=0;$i<count($DATA['software']['item']);$i++)
                                                    {
                                                        if(isset($DATA['software']['item'][$i]))
                                                        {
                                                            //browser
                                                            for($j=0;$j<count($a_aplikasi_browser_keyword);$j++)
                                                            {
                                                                if (!preg_match("/\b{$a_aplikasi_browser_keyword[$j]}\b/i", $DATA['software']['item'][$i]['name'])) 
                                                                {
                                                                    $oComputerItem->createServiceSpesification( 
                                                                        $s_komputer_id, 
                                                                        $a_komputer_item['APLIKASI'], 
                                                                        1, 
                                                                        $DATA['software']['item'][$i]['name'], 
                                                                        $DATA['software']['item'][$i]['publisher'],
                                                                        $DATA['software']['item'][$i]['version'],
                                                                        $DATA['sys']['config_token']);   
                                                                }
                                                            }    
                                                            //office
                                                            for($j=0;$j<count($a_aplikasi_office_keyword);$j++)
                                                            {
                                                                if (!preg_match("/\b{$a_aplikasi_office_keyword[$j]}\b/i", $DATA['software']['item'][$i]['name'])) 
                                                                {
                                                                    $oComputerItem->createServiceSpesification( 
                                                                        $s_komputer_id, 
                                                                        $a_komputer_item['APLIKASI'], 
                                                                        1, 
                                                                        $DATA['software']['item'][$i]['name'], 
                                                                        $DATA['software']['item'][$i]['publisher'],
                                                                        $DATA['software']['item'][$i]['version'],
                                                                        $DATA['sys']['config_token']);   
                                                                }
                                                            }
                                                        }
                                                                
                                                    }
                                                }
                                                //prosesor

                                                   //$oComputerItem->createServiceSpesification( $s_komputer_id, $a_komputer_item['PROCESSOR'], $processor_current_clock_speed, $processor_manufacturer, $processor_name." ".$processor_socket_designation,"",$token_server);        
                                                        
                                                $s_notif_komputer_id = "";
                                                if(isset($s_komputer_id))
                                                {
                                                    $s_notif_komputer_id = $s_komputer_id;
                                                }
                                                /*
                                                echo "<pre>";
                                                print_r($a_data_user);
                                                echo "</pre>";
                                                */
                                                $oNotification->create(

                                                    "audit komputer" ,
                                                    "komputer $s_notif_komputer_id  berhasil dimasukan" ,
                                                    "" ,
                                                    $a_data_user[0]['userID'] ,
                                                    $a_data_user[0]['firebaseToken'] ,
                                                    $a_data_user[0]['userID'] ,
                                                    ""
                                                );
                                                $response['result'] = "success";
                                                $response['desc'] = "komputer berhasil dibuat";
                                            }
                                            else
                                            {
                                                $response['result'] = "error";
                                                $response['desc'] = "komputer tidak dapat dibuat disystem";
                                            }

                                        }
                                        else
                                        {

                                            $response['result'] = "error";
                                            $response['desc'] = "anda bukan sebagai helpdesk. data tidak dapat diupload";
                                        }

                                            
                                    }
                                    else
                                    {
                                        //---------------------------------UPGRADE---------------------------------
                                        //jika sudah pernah dimasukan maka yang dicek hanya harddisk,memory dan os
                                        $b_update = false;
                                        if($s_komputer_id != "")
                                        {
                                            $response['logs'][] = "komputer sudah pernah dimasukan. ID perangkat : ".$s_komputer_id."\n";

                                            //memori
                                            if(isset($DATA['memory']['item']))
                                            {
                                                for($i=0;$i<count($DATA['memory']['item']);$i++)
                                                {
                                                    $where = " WHERE 
                                                                    komputerID ='$s_komputer_id' AND 
                                                                    komputerSpekSerialNumber= '{$DATA['memory']['item'][$i]['serial']}' AND 
                                                                    komputerSpekStatus = 1";
                                                    if($oComputer->getCountSpek($where)==0 &&  $oComputer->getCountSpekUpdate($where)==0)
                                                    {

                                                        $response['logs'][] = "ditemukan perubahan memori serial {$DATA['memory']['item'][$i]['serial']}"."\n";
                                                        $b_update = true;
                                                        $oComputerItem->createServiceSpesification( 
                                                            $s_komputer_id, 
                                                            $a_komputer_item['MEMORY'], 
                                                            $DATA['memory']['item'][$i]['size'], 
                                                            $DATA['memory']['item'][$i]['speed'], 
                                                            $DATA['memory']['item'][$i]['form_factor'],
                                                            $DATA['memory']['item'][$i]['serial'],
                                                            $DATA['sys']['config_token']);  
                                                    }
                                                }
                                            }

                                            //disk
                                            if(isset($DATA['disk']['item']))
                                            {
                                                if(isset($DATA['disk']['item']['size']))
                                                {
                                                    $where = " WHERE 
                                                                komputerID ='$s_komputer_id' AND 
                                                                komputerSpekKeterangan = '{$DATA['disk']['item']['model']}' AND
                                                                komputerSpekStatus = 1";
                                                    if($oComputer->getCountSpek($where)==0 &&  $oComputer->getCountSpekUpdate($where)==0)
                                                    {
                                                        $oComputerItem->createServiceSpesification( 
                                                            $s_komputer_id, 
                                                            $a_komputer_item['HARDDISK'], 
                                                            $DATA['disk']['item']['size'], 
                                                            $DATA['disk']['item']['manufacturer'], 
                                                            $DATA['disk']['item']['model'],
                                                            $DATA['disk']['item']['status'],
                                                            $DATA['sys']['config_token']);
                                                    }   
                                                }
                                                else
                                                {
                                                    for($i=0;$i<count($DATA['disk']['item']);$i++)
                                                    {
                                                        $where = " WHERE 
                                                                    komputerID ='$s_komputer_id' AND 
                                                                    komputerSpekKeterangan = '{$DATA['disk']['item'][$i]['model']}' AND
                                                                    komputerSpekStatus = 1";
                                                        if($oComputer->getCountSpek($where)==0 &&  $oComputer->getCountSpekUpdate($where)==0)
                                                        {

                                                            $response['logs'][] = "perubahan harddisk model {$DATA['disk']['item'][$i]['model']}"."\n";
                                                            $b_update = true;
                                                            $oComputerItem->createServiceSpesification( 
                                                                $s_komputer_id, 
                                                                $a_komputer_item['HARDDISK'], 
                                                                $DATA['disk']['item'][$i]['size'], 
                                                                $DATA['disk']['item'][$i]['manufacturer'], 
                                                                $DATA['disk']['item'][$i]['model'],
                                                                $DATA['disk']['item'][$i]['status'],
                                                                $DATA['sys']['config_token']);  
                                                        }
                                                             
                                                    }
                                                }
                                                    
                                            }

                                            //monitor
                                            if(isset($DATA['monitor']['item']))
                                            {
                                                $where = " WHERE 
                                                            komputerID ='$s_komputer_id' AND 
                                                            komputerSpekMerk = '{$DATA['monitor']['item']['manufacturer']}' AND
                                                            komputerSpekStatus = 1";
                                                if($oComputer->getCountSpek($where)==0 &&  $oComputer->getCountSpekUpdate($where)==0)
                                                {
                                                    $response['logs'][] = "perubahan monitor  {$DATA['monitor']['item']['manufacturer']} {$DATA['monitor']['item']['description']}"."\n";
                                                    $b_update = true;
                                                        
                                                    $oComputerItem->createServiceSpesification( 
                                                            $s_komputer_id, 
                                                            $a_komputer_item['MONITOR'], 
                                                            $DATA['monitor']['item']['size'],
                                                            $DATA['monitor']['item']['manufacturer'], 
                                                            $DATA['monitor']['item']['description'], 
                                                            $DATA['monitor']['item']['serial'],
                                                            $DATA['sys']['config_token']);  
                                                }
                                            
                                            }

                                            //os
                                            if(isset($DATA['sys']))
                                            {
                                                $where = " WHERE 
                                                            komputerID ='$s_komputer_id' AND 
                                                            komputerSpekKeterangan= '".$DATA['sys']['os_name']." ". $DATA['sys']['os_bit']."'  AND
                                                            komputerSpekStatus = 1";
                                                if($oComputer->getCountSpek($where)==0 &&  $oComputer->getCountSpekUpdate($where)==0)
                                                {
                                                    $response['logs'][] = "perubahan sistem operasi {$DATA['sys']['os_name']} {$DATA['sys']['os_bit']}" ."\n";
                                                    $b_update = true;
                                                    $oComputerItem->createServiceSpesification( 
                                                        $s_komputer_id, 
                                                        $a_komputer_item['SISTEM_OPERASI'],
                                                        1, 
                                                        $DATA['sys']['os_group'], 
                                                        $DATA['sys']['os_name']." ". $DATA['sys']['os_bit'],
                                                        $DATA['sys']['os_family'],
                                                        $DATA['sys']['config_token']); 
                                                } 
                                            }
                                            //software
                                            if(isset($DATA['software']['item']))
                                            {
                                                for($i=0;$i<count($DATA['software']['item']);$i++)
                                                {
                                                    if(isset($DATA['software']['item'][$i]))
                                                    {
                                                        //browser
                                                        for($j=0;$j<count($a_aplikasi_browser_keyword);$j++)
                                                        {
                                                            if (preg_match("/\b{$a_aplikasi_browser_keyword[$j]}\b/i", $DATA['software']['item'][$i]['name'])) 
                                                            {

                                                                $where = " WHERE 
                                                                            komputerID ='$s_komputer_id' AND 
                                                                            komputerSpekMerk = '{$DATA['software']['item'][$i]['name']}' AND
                                                                            komputerSpekStatus = 1";

                                                                if($oComputer->getCountSpek($where)==0 &&  $oComputer->getCountSpekUpdate($where)==0)
                                                                {
                                                                    $a_data_item  =$oComputer->getListSpek($where,"","");
                                                                    //menghapus software
                                                                    if($oComputer->deleteItem($a_data_item[0]['komputerSpekID']))
                                                                    {
                                                                        
                                                                        $response['logs'][] = "mengganti " . $DATA['software']['item'][$i]['name']." ".$a_data_item[0]['komputerSpekSerialNumber']."=>".$DATA['software']['item'][$i]['version'];
                                                                        $oComputerItem->createServiceSpesification( 
                                                                            $s_komputer_id, 
                                                                            $a_komputer_item['APLIKASI'], 
                                                                            1, 
                                                                            $DATA['software']['item'][$i]['name'], 
                                                                            $DATA['software']['item'][$i]['publisher'],
                                                                            $DATA['software']['item'][$i]['version'],
                                                                            $DATA['sys']['config_token']);   
                                                                    }
                                                                }
                                                            }
                                                        }    
                                                        //office
                                                        for($j=0;$j<count($a_aplikasi_office_keyword);$j++)
                                                        {
                                                            if (preg_match("/\b{$a_aplikasi_office_keyword[$j]}\b/i", $DATA['software']['item'][$i]['name'])) 
                                                            {
                                                                $oComputerItem->createServiceSpesification( 
                                                                    $s_komputer_id, 
                                                                    $a_komputer_item['APLIKASI'], 
                                                                    1, 
                                                                    $DATA['software']['item'][$i]['name'], 
                                                                    $DATA['software']['item'][$i]['publisher'],
                                                                    $DATA['software']['item'][$i]['version'],
                                                                    $DATA['sys']['config_token']);   
                                                            }
                                                        }
                                                    }
                                                            
                                                }
                                            }
                                        }
                                        //----------------------end upgade-------------------
                                        if($b_update)
                                        {
                                            $response['result'] = "success";
                                            $response['desc'] = "terjadi perubahan spesifikasi device.";
                                        }
                                        else
                                        {
                                            $response['result'] = "success";
                                            $response['desc'] = "device sudah pernah dimasukan sebelumnya. \n data device tidak diproses";
                                        }
                                        $s_notif_komputer_id = "";
                                        if(isset($s_komputer_id))
                                        {
                                            $s_notif_komputer_id = $s_komputer_id;
                                        }
                                        /*
                                        echo "<pre>";
                                        print_r($a_data_user);
                                        echo "</pre>";
                                        */
                                        $oNotification->create(

                                            "update info komputer" ,
                                            "komputer $s_notif_komputer_id  berhasil diupdate" ,
                                            "" ,
                                            $a_data_user[0]['userID'] ,
                                            $a_data_user[0]['firebaseToken'] ,
                                            $a_data_user[0]['userID'] ,
                                            ""
                                        );
                                    }

                                }
                                else
                                {
                                    $response['result'] = "error";
                                    $response['desc'] = "tidak ada perangkat jaringan";
                                }
                            }
                            else
                            {
                                $response['result'] = "error";
                                $response['desc'] = "jaringan network tidak ada";
                            }
                        }
                        else
                        {

                            $response['result'] = "error";
                            $response['desc'] = "script sudah kadaluarsa.silahkan download ulang script";
                        }
                    }
                    else
                    {
                        $response['result'] = "error";
                        $response['desc'] = "script sudah pernah dipakai.silahkan download ulang script";
                    }
                }
                else
                {
                    $response['result'] = "error";
                    $response['desc'] = "token tidak ada";
                }
        /*
                            
            }
            else
            {
                $response['result'] = "error";
                $response['desc'] = "group helpdesk tidak terdaftar";
            }
        }
        else
        {
            $response['result'] = "error";
            $response['desc'] = "group helpdesk tidak ada";
        }*/

    }
    else
    {
          $response['result'] = "error";
          $response['desc'] = "Tidak ada parameter input";
    }

    echo "*******************************************************************\n";
    echo "*******************************************************************\n";
    echo $response['result']. " ->".$response['desc']."\n";
    echo "*******************************************************************\n";
    if($response['result'] == 'success')
    {
        $oTokenHandle->setUsed($DATA['sys']['config_token']);
    }
    if(isset($response['logs']))
    {
        for($i=0;$i<count($response['logs']);$i++)
        {
            echo "->".$response['logs'][$i]."\n";
        }
    }
    echo "*******************************************************************\n";
    echo "selesai\n";
    echo "*******************************************************************\n";
    $oComputer->closeDB();
    $oComputerItem->closeDB();
    $oTokenHandle->closeDB();
    $oNotification->closeDB();
    $oUser->closeDB();
    $oHelpdesk->closeDB();
?>