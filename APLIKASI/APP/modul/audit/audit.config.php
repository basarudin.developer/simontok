<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer Audit Config
    |--------------------------------------------------------------------------
    |Configurasi untuk config kebutuhan data transfer komputer/laptop
    |
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    /*******************************************************************************
    |CONFIGURATION AUDIT FOR SERVER
    ********************************************************************************/

    // ****************  General Settings *******************************************
    $language = 'en';
    $mysql_server = 'localhost';
    $mysql_user = 'openaudit';
    $mysql_password = 'openaudit';
    $mysql_database = 'openaudit';

    // ****************  Security Settings *******************************************
    $use_https = 'n';
    $use_pass = 'n';
    // An array of allowed users and their passwords (set use_pass = "n" if you do not wish to use passwords)
    $users = array
            (
                'admin' => 'simontok'
            );
    $use_ldap_integration= 'n';
    $ldap_base_dn = 'dc=mydomain,dc=local';
    $ldap_server = 'domain.local';
    $ldap_user = 'unknown@domain.local';
    $ldap_secret = 'password';
    $use_ldap_login = 'n';
    $full_details = 'y';
    $human_readable_ldap_fields = 'y';
    $image_link_ldap_attribute = "name";

    // ****************  Homepage Settings *******************************************
    $show_other_discovered = 'y';
    $other_detected = '3';
    $show_system_discovered = 'y';
    $system_detected = '3';
    $show_systems_not_audited = 'y';
    $days_systems_not_audited = '3';
    $show_partition_usage = 'y';
    $partition_free_space = '1000';
    $show_software_detected = 'y';
    $days_software_detected = '1';
    $show_patches_not_detected = 'y';
    $number_patches_not_detected = '5';
    $show_detected_servers = 'y';
    $show_detected_xp_av = 'y';
    $show_detected_rdp = 'y';
    $show_os = 'y';
    $show_date_audited = 'y';
    $show_type = 'y';
    $show_description = 'n';
    $show_domain = 'n';
    $show_service_pack = 'n';
    $count_system = '30';
    $vnc_type = 'real';
    $round_to_decimal_places = '2';
    $management_domain_suffix = 'domain.local';
    $show_systems_audited_graph = 'y';
    $systems_audited_days='45';
    $show_ldap_changes = 'y';
    $ldap_changes_days = 7;

    // ****************  Settings that have no associated GUI *******************************************
    $enable_remote_management = 'y';
    $max_log_entries = 1000;
    $utf8 = 'y';
    $show_dell_warranty = 'y';
    $show_tips = 'n';
    $admin_list = Array('Domain Admins');
    $user_list = Array('Domain Admins');
    $user_ldap_attributes = Array("company","department","description","displayname","mail","manager","msexchhomeservername","name","physicaldeliveryofficename","samaccountname","telephonenumber");
    $computer_ldap_attributes = Array("description","name","operatingsystem","operatingsystemversion","operatingsystemservicepack");
    $timezone = 'Europe/London';
    $show_summary_barcode = FALSE ;
    $summary_barcode = "name";

    //END AUDIT CONFIGURATION SERVER
    // ****************  BEGIN NEW CONFIG *******************************************

    //------------------------------end kunci utama------------------------------
    //------------------------------begin kunci utama------------------------------



    /*default to localhost*/
    $strcomputer = ".";

    /*submit the audit to the Open-AudIT server*/
    $submit_online = 'y';


    $discovery_id = "";



    /*submit via a proxy (using the settings of the user running the script)*/
    $use_proxy = "n";

    /*the username (if not using the user running the script)*/
    $struser = "";

    /*the password (if not using the user running the script)*/
    $strpass = "";

    /*If our URL uses https, but the certificate is invalid or unrecognised (self signed), we should submit anyway.*/
    $ignore_invalid_ssl = "y";

    /*optional - assign any PCs audited to this Org - take the OrgId from Open-AudIT web interface*/
    $org_id = "";


    /*
    ' optional - query this Active Directory attribute to determine the users work unit
    ' if attribute #1 produces nothing, then try attribute #2
    */
    $use_active_directory = "y";
    $windows_user_work_1 = "physicalDeliveryOfficeName";
    $windows_user_work_2 = "company";

    /* should we attempt to query mount points*/
    $audit_mount_point = "y";


    /*should we audit the installed software*/
    $audit_software = "n";

    /*
    ' use the win32_product WMI class (not recommended by Microsoft).
    ' https://support.microsoft.com/kb/974524
    ' added and set to disabled by default in 1.5.2
    */
    $audit_win32_product = "n";

    /*' retrieve all DNS names*/
    $audit_dns = "y";

    /*
    ' run netstat on the target
    ' n = no
    ' y = yes
    ' s = servers only
    */
    $audit_netstat = "s";
    
    /*
    ' if set then delete the audit script upon completion
    ' useful when starting the script on a remote machine and leaving no trace
    */
    $self_delete = "y";

    /*
    ' 0 = no debug
    ' 1 = basic debug
    ' 2 = verbose debug
    ' 3 = very verbose debug
    */
    $debugging = "3";

    /*
    ' Version - NOTE, special formatted so we match the *nix scripts and can do find/replace
    */
    $version="3.0.2";

    /*
    ' In normal use, DO NOT SET THIS.
    ' This value is passed in when running the audit_domain script.
    ' Only set this value if your audit host is on a different domain than audit targets and you are not using audit_domain.vbs - IE, you are running "cscript audit_windows.vbs COMPUTER" where COMPUTER is on a seperate domain than the PC you are running the command on. This would then apply to ALL systems audited like this. This would be the exception rather than the rule. Do not do this unless you know what you are doing :-)
    */
    $ldap = "";

    /*' set this greater than 0 if you wish to insert systems from AD that have only been seen in the last XX days*/
    $ldap_seen_days = "0";

    /*' set this greater than 2000-01-01 if you wish to insert systems from AD that have only been seen since XX date
    */
    $ldap_seen_date = "2012-06-30";

    /*' attempt to ping a target computer before audit?*/
    $ping_target = "n";

    /*' set by the Discovery function - do not normally set this manually*/
    $system_id = "";
    $last_seen_by = "audit";

    $details_to_lower = "y";

    $help = "y";

    $hide_audit_window = "n";

    /*' Should we attempt to audit any connected SAN's*/
    $san_audit="y";

    /*' If we detect the san management software, should we run an auto-discover before the query*/
    $san_discover = "n";
    // ****************  END NEW CONFIG *******************************************
    /*******************************************************************************/
?>