<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer Audit
    |--------------------------------------------------------------------------
    |Download script audit
    |
    |
    |
    |Digunakan untuk membuat script download vbs
    |script ini dikirim untuk mengaudit komputer klien dengan mengesekusinya melalui command promt
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */


    $s_token_server = "";
    $s_group_helpdesk = "";
    //init definition

    $linux_template = "modul/audit/audit.bash.template";
    $windows_template = "modul/audit/audit.vbs.template";
    $this_file = "";
    $server_config_path  = "/modul/audit/audit.config.export.php";
    //expied +20  menit
    $s_expired =  date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))+(60*20));

    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.tokenhandle.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");



    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $platform = 'Unknown';
    // First get the platform?
    if (preg_match('/linux/i', $u_agent)) 
    {
        $platform = 'linux';
        $this_file = $linux_template;

    } 
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) 
    {
        $platform = 'mac';
    } 
    elseif (preg_match('/windows|win32|win64/i', $u_agent)) 
    {
        $platform = 'windows';
        $this_file = $windows_template;
    }


    
    $oToken = new TokenHandle();
    if($oToken->create($USER[0]['userID'],$s_expired))
    {
        $s_token_server = $oToken->lastToken();        
    }
    $oToken->closeDB();

    $oHelpdesk = new Helpdesk();
    $b_is_helpdesk = $oHelpdesk->isHelpdesk($USER[0]['userID']); 
    $oHelpdesk->closeDB();

    if($b_is_helpdesk)
    {
        if($s_token_server != "")
        {

            $host_name = "";
            if (isset($_GET['hostname'])and ($_GET['hostname'] <>"")) {
                $host_name=$_GET['hostname'];
            } else {
                $host_name=".";
            }

            //path page direkstori filesistem
            $_REAL_SCRIPT_DIR = realpath(dirname($_SERVER['SCRIPT_FILENAME'])); 
            //path file direkstori filesistem
            $_REAL_BASE_DIR = realpath(dirname(__FILE__)); 
            //fullpath filesistem
            $_MY_PATH_PART = substr( $_REAL_SCRIPT_DIR, strlen($_REAL_BASE_DIR)); 

            $INSTALLATION_PATH = $_MY_PATH_PART ? substr( dirname($_SERVER['SCRIPT_NAME']), 0, -strlen($_MY_PATH_PART) ) : dirname($_SERVER['SCRIPT_NAME']);

            //client host
            $requesting_host = $_SERVER['REMOTE_ADDR'];
            //REMOTE host
            $our_host= "http://".$_SERVER['HTTP_HOST'];
            $our_instance = $INSTALLATION_PATH;

            $application = "pln-komputer-audit-".$host_name."-to-".$_SERVER['HTTP_HOST']."-from-".$requesting_host.".vbs";
            $host_url = $our_host.$our_instance."{$server_config_path}?hostname=".$host_name."&application=".$application;



            // Define the donor script name.

            SWITCH($application){
                case "http":
                case "https":
                case "ftp":
                    header("Location: ".$application."://".$host_url);
                break;
                default:
                    //baca template vbs
                    if($this_file!="")
                    {
                        $buffer=file($this_file);
                        $buffer=implode("",$buffer);
                        
                        // Replace Hostname etc
                        // ubah the Host URL
                        $buffer=str_replace ( "%host_url%", $host_url, $buffer );
                        // Change references to audit.vbs to the script name...
                        $buffer=str_replace ( "audit.vbs", $application, $buffer );
                        //replace token
                        $buffer=str_replace ( "%server_token%", $s_token_server, $buffer );
                        //replace token
                        $file_size = strlen($buffer);
                        //Send to Browser
                        
                        header("Content-type: application/force-download");
                        header("Content-Transfer-Encoding: Binary");
                        header("Content-length: ".$file_size);
                        if($platform == 'linux')
                        {
                            $application = "simontok_audit_linux.sh";   
                        }
                        header("Content-disposition: attachment; filename=\"".$application."\"");
                        // Throw the $buffer as the page, as it now contains the script.
                        echo $buffer;
                    }
                    else
                    {
                        echo "Template Bermasalah";
                    }
                        

                break;

            }
        }
        else
        {
            echo "Token Bermasalah";
        }
    }
    else
    {

        echo "Anda bukan helpdesk";
    }
        


?>
