<?php
    /*
    |--------------------------------------------------------------------------
    | Audit
    |--------------------------------------------------------------------------
    |Controler  modul audit
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    if(isset($_REQUEST['type']))
    {
        if ($_REQUEST['type'] == "model") 
        {
            require_once($SYSTEM['DIR_MODUL']."/audit/audit.model.php");
        }


        elseif ($_REQUEST['type'] == "auditscriptdownload") 
        {
            require_once($SYSTEM['DIR_MODUL']."/audit/audit.download.php");
        } 
        elseif ($_REQUEST['type'] == "auditscriptupload") 
        {
            require_once($SYSTEM['DIR_MODUL']."/audit/audit.upload.php");
        } 
        elseif ($_REQUEST['type'] == "config") 
        {
            require_once($SYSTEM['DIR_MODUL']."/audit/audit.config.export.php");
        } 
        elseif ($_REQUEST['type'] == "form") 
        {
            require_once($SYSTEM['DIR_MODUL']."/audit/audit.form.php");
        } 
        elseif($_REQUEST['type'] != "")
        { 
            include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
            require_once($SYSTEM['DIR_MODUL']."/audit/audit.view.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/audit/audit.view.php");
        include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
    }

?>
