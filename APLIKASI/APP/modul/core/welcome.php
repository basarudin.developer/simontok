
<?php
/**
 *
 * @version     1.0
 * @author      basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/

    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    $LAYOUT_CSS_CONTENT=    "
        <link rel='stylesheet' href='assets/css/welcome.css'>
                            ";
    $LAYOUT_JS_CONTENT =   "    
        <script src='assets/js/welcome.js'></script>
                           ";

?>
<?php
    $CONTENT_MAIN = "
                

<!-- BEGIN PAGE CONTENT -->

<section class='slide-wrapper'>
        <div class='container'>
            <div id='myCarousel' class='carousel slide'>
                <!-- Indicators -->
                <ol class='carousel-indicators'>
                    <li data-target='#myCarousel' data-slide-to='0' class='active'></li>
                    <li data-target='#myCarousel' data-slide-to='1'></li>
                    <li data-target='#myCarousel' data-slide-to='2'></li>
                 </ol>

                <!-- Wrapper for slides -->
                <div class='carousel-inner'>
                    <div class='item item1 active' >
                        <div class='fill'>
                            <div class='inner-content'>
                                <div class='col-md-4'>
                                    <div class='carousel-img'>
                                        <img src='assets/img/slider/presenter.png' alt='sofa' class='img img-responsive' />
                                    </div>
                                </div>

                                <div class='col-md-8  mid-content'>
                                    <div class='carousel-desc mid-text'>

                                        <h2>Selamat datang  {$USER[0]['panggilan']}</h2>
                                        <p>Selamat berkarya, jadikan perkerjaan anda berkah. Kontribusi anda akan sangat bermanfaat bagi perusahaan. Semoga aktifitas kita hari ini dicatat sebagai amal kebaikan. amien</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='item item2 '>
                        <div class='fill'>
                            <div class='inner-content'>
                                <div class='carousel-img'>
                                    <img src='assets/img/slider/app.png' alt='sofa' class='img img-responsive' />
                                </div>
                                <div class='carousel-desc'>

                                    <h2>Simple Usefull Powerfull</h2>
                                    <p>Sistem ini dibuat sesederhana mungkin yang ditujukan untuk membantu perkerjaan administrasi komputer dan layanan helpdesk. </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='item item3'>
                        <div class='fill' >
                            <div class='inner-content'>
                                <div class='carousel-img'>
                                    <img src='assets/img/slider/helpdesk.png' alt='white-sofa' class='img img-responsive' />
                                </div>
                                <div class='carousel-desc'>

                                    <h2>Anda Butuh Bantuan?</h2>
                                    <p>Silahkan menghubungi administrator jika anda mengalami kesulitan dalam mengoperasikan aplikasi sistem ini.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>
    </section>


    <!-- END PAGE CONTENT -->
              ";
    include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
?>