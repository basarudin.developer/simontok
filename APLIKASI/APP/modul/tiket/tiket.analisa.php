<?php
    /*
    |--------------------------------------------------------------------------
    | Tiket Analisa
    |--------------------------------------------------------------------------
    |Analisa tiket untuk penyelesaian masalah
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    $oTicket = new Ticket();
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    $oKomputer = new Computer();

    $LAYOUT_JS_EXTENDED .= "
                    <script src='modul/tiket/tiket.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-apply-analisa' class='btn btn-flat  btn-sm btn-primary pull-right' style='margin-left: 5px;'>
                            <i class='  fa '></i> Terapkan
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> kembali
                        </button>
                    ";
    $s_form_input = "";
    $FORM_MAIN  = "";
    if(isset($_REQUEST['ticketID']))
    {

        $s_condition = " WHERE ticketID =  '{$_REQUEST['ticketID']}' ";
        $s_limit = "  ";
        $s_order = " ";
        $a_data_ticket = $oTicket->getList($s_condition, $s_order, $s_limit);
        if(count($a_data_ticket)>0)
        {
            $FORM_MAIN =    "<form id='form-main' action='' target='' method='post'>
                                <input type='hidden' name='ticket_id'  value='{$a_data_ticket[0]['ticketID']}'>
                                <input type='hidden' name='komputer_id' id='komputer_id' value=''>
                                <input type='hidden' name='status_id' id='status_id' value=''>
                                <input type='hidden' name='kategori' id='kategori' value=''>
                                <input type='hidden' name='keterangan' id='keterangan' value=''>
                            </form>";
            $a_data_komputer = $oKomputer->getList(" WHERE komputerUser = '{$a_data_ticket[0]['ticketRequestID']}' ", $s_order, $s_limit);
            $s_ticket_komputer = "";
            if(isset($a_data_komputer))
            {
                if(count($a_data_komputer)>0)
                {
                    $s_ticket_komputer = "<select class='form-control ' name='ticket-komputer'  id='ticket-komputer' >";
                    if(count($a_data_komputer) > 1)
                    {
                        $s_ticket_komputer .= "<option value='kosong'>---Pilih Komputer yang digunakan---</option>";
                    }
                    for($i=0;$i < count($a_data_komputer);$i++)
                    {
                        $s_selected = "";
                        if($a_data_komputer[$i]['komputerID'] == $a_data_ticket[0]['ticketKomputerID'])
                        {
                            $s_selected = " selected ";
                        }
                        $s_ticket_komputer .= "<option value='{$a_data_komputer[$i]['komputerID']}'  $s_selected>{$a_data_komputer[$i]['komputerIdentifikasi']}</option>";
                    } 
                    $s_ticket_komputer .= "</select>";
                }
                else
                {
                    $s_ticket_komputer .= " <input type='text' class='form-control' name='ticket-komputer' value='' disabled>";

                }
                
            }
            else
            {
                $s_ticket_komputer .= " <input type='text' class='form-control' name='ticket-komputer' value='' disabled>";

            }
            //---begin tiket status
            $a_data_ticket_status['8'] = 'Selesai';
            $a_data_ticket_status['2'] = 'Menunggu Persetujuan';
            $a_data_ticket_status['3'] = 'Menunggu Material';

            $s_ticket_status =  "<select class='form-control '   id='ticket-status' >
                                    <option value=''>---pilih status pengerjaan tiket---</option>";
            foreach ($a_data_ticket_status as $key => $value) 
            {
                $s_selected = "";
                if($key == $a_data_ticket[0]['ticketStatus'])
                {
                    $s_selected = " selected ";
                }
                $s_ticket_status .= "<option value='{$key}' $s_selected >$value</option>";
            }
            $s_ticket_status .=  "</select>";
            //---end tiket status

            //---begin tiket kategori
            $a_data_ticket_kategori['software'] = 'Software';
            $a_data_ticket_kategori['hardware'] = 'Hardware';
            $a_data_ticket_kategori['jaringan'] = 'Jaringan';
            $a_data_ticket_kategori['virus'] = 'virus';
            $a_data_ticket_kategori['email_dan_domain'] = 'Email / Domain';

            $s_ticket_kategori =  "<select class='form-control '   id='ticket-kategori' >
                                    <option value=''>---pilih status pengerjaan tiket---</option>";
            foreach ($a_data_ticket_kategori as $key => $value) 
            {
                $s_selected = "";
                if($key == $a_data_ticket[0]['ticketSolveCategory'])
                {
                    $s_selected = " selected ";
                }
                $s_ticket_kategori .= "<option value='{$key}' $s_selected >$value</option>";
            }
            $s_ticket_kategori .=  "</select>";
            //---end tiket kategori

     
            $s_form_input = "
                            <div >
                                <input type='hidden' id='ticket-id' value='{$_REQUEST['ticketID']}'>
                                <!-- /.box-header -->
                                <div class='box-body'>

                                    <div class='row'>
                                        <div class='col-md-12'>
                                            <div class='callout callout-info' style='margin-bottom: 0!important;'>
                                                <h4><i class='fa fa-wechat'></i> {$a_data_ticket[0]['userRequestName']}:</h4>
                                                {$a_data_ticket[0]['ticketProblemDesc']}
                                            </div>
                                        </div>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                        
                                        <div class='col-md-12'>
                                            
                                            <!-- status ticket -->
                                            <div class='form-group'>
                                                <label>Komputer:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-tv'></i>
                                                    </div>
                                                    $s_ticket_komputer
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.status ticket -->
                                        </div>
                                        <!-- /.col -->
                                        <div class='col-md-12'>
                                            
                                            <!-- status ticket -->
                                            <div class='form-group'>
                                                <label>Kategori:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-folder'></i>
                                                    </div>
                                                    $s_ticket_kategori
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.status ticket -->
                                        </div>
                                        <div class='col-md-12'>
                                            
                                            <!-- status ticket -->
                                            <div class='form-group'>
                                                <label>Status Keluhan:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-ticket'></i>
                                                    </div>
                                                    $s_ticket_status
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.status ticket -->
                                        </div>
                                        <!-- /.col -->
                                        <div class='col-md-12'>
                                            
                                            <!-- keterangan -->
                                            <div class='form-group'>
                                                <label id='desc-label'>Perbaikan:</label>
                                                    <textarea id='ticket-desc' class='form-control' rows='3' placeholder='keterangan  ...'></textarea>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.keterangan ticket -->
                                        </div>
                                        <!-- /.col -->
                                        
                                        
                                    </div>
                                    <!-- /.row -->
                                    
                                </div>
                                <!-- /.box-body -->
                            </div>";
        }
        else
        {
            $s_form_input ="tiket tidak ditemukan di database";    
        }



    }
    else
    {
        $s_form_input ="tiket tidak terdefinisi";    
    } 
            
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>ANALISA KELUHAN </h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>
                            $FORM_MAIN
                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oTicket->closeDB();
    $oKomputer->closeDB();
?>