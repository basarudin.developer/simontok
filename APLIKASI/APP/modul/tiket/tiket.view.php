<?php
    /*
    |--------------------------------------------------------------------------
    | Tiket
    |--------------------------------------------------------------------------
    |view  modul tiket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "list") 
        {
            require_once($SYSTEM['DIR_MODUL']."/tiket/tiket.list.php");
        } 
        if ($_REQUEST['action'] == "analisa") 
        {
            require_once($SYSTEM['DIR_MODUL']."/tiket/tiket.analisa.php");
        } 
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/tiket/tiket.list.php");
    }

?>