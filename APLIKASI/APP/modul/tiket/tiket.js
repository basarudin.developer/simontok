$(document).ready(function() {

    /*************************************************
    |   BEGIN BUTTON MAIN
    **************************************************/
    jQuery('#button-back').click(function() 
    {
        document.location='index.php?page=tiket&action=list';
    });

    /*   END  BUTTON MAIN
    **************************************************/
    /*************************************************
    |   BEGIN TABLE-TIKET
    **************************************************/
    if($('#table-tiket').length)
    {
        $('#table-tiket').DataTable
        ({        
            rowReorder: false,
            ordering: false,
            lengthMenu: [[25, 50, -1], [25, 50, "All"]]
        });
        
        $('#table-tiket tbody').on( 'click', '.button-analisa', function () 
        {
            ticket_id = $(this).attr('record-id');
            document.location='index.php?page=tiket&action=analisa&ticketID='+ticket_id;
        });
    }
    /*   END FORM MANUAL CREATE
    **************************************************/
    
    /*************************************************
    |   BEGIN ANALISA-TIKET
    **************************************************/
    if($('#ticket-status').length)
    {
        $('#ticket-status').on('change', function (e) {
            var option_selected = $("option:selected", this);
            var value_selected = this.value;
            if(value_selected == '8')
            {
                $('#desc-label').html('Perbaikan');
            }
            else
            {
                $('#desc-label').html('Keterangan pending');
            }
        });
        jQuery('#button-apply-analisa').click(function() 
        {
            error_desc = "";
            if($('#ticket-status').val() == "")
            {
                error_desc = error_desc +"status tiket harus diisi.<br/>";
            }
            if($('#ticket-kategori').val() == "")
            {
                error_desc = error_desc +"kategori harus diisi.<br/>";
            }
            if( ($('#ticket-kategori').val() == "software") || ($('#ticket-kategori').val() == "hardware") )
            {
                if($('#ticket-komputer').val() == "kosong")
                {
                    error_desc = error_desc + "komputer harus diisi.<br/>";
                }
            }
            if($('#ticket-desc').val() == "")
            {
                error_desc = error_desc + "keterangan harus diisi.<br/>";
            }


            if(error_desc == "")
            {
                $('#komputer_id').val($('#ticket-komputer').val());
                $('#status_id').val($('#ticket-status').val());
                $('#kategori').val($('#ticket-kategori').val());
                $('#keterangan').val($('#ticket-desc').val());
                $.ajax({
                type: 'POST',
                url: "index.php?page=tiket&type=model&action=analisa",
                dataType:"json",
                data: $("#form-main").serialize(),
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();
                            setTimeout(function() {
                                document.location='index.php?page=tiket&action=list'
                            }, 1000);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#notification-container').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        $.Notify({
                            caption: '',
                            content: 'Terjadi kegagalan proses transaksi',
                            keepOpen: true,
                            type: 'alert'
                        });
                    }
            });//end of ajaxs
            }
            else
            {
                $('#notification-container').removeClass('alert-success');
                $('#notification-container').addClass('alert-danger'); 
                $('#notification-content').html(error_desc);
                $('#notification-container').show(); 
            }


        });
    }

    /*   END  ANALISA TIKET
    **************************************************/

});