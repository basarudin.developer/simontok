<?php
    /*
    |--------------------------------------------------------------------------
    | Ticket
    |--------------------------------------------------------------------------
    |model  modul ticket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";

    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.firebase.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");


    $oTicket = new Ticket();
    $oUser = new UserInfo();
    $oNotification = new Notification();

    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "tidak tereksekusi";
    $s_error = "";

    if(isset($_REQUEST['action']))
    {
        if($_REQUEST['action'] == 'analisa')
        {
            if($_REQUEST['ticket_id'] == "")
            {
                $a_errors[] = "id tiket kosong";
            }
            if(!isset($_REQUEST['komputer_id']))
            {
                $a_errors[] = "komputer kosong";
            }
            if($_REQUEST['status_id'] == "")
            {
                $a_errors[] = "status tiket kosong";
            }
            if($_REQUEST['kategori'] == "")
            {
                $a_errors[] = "kategori kosong";
            }
            if($_REQUEST['keterangan'] == "")
            {
                $a_errors[] = "keterangan kosong";
            }
            //print_r($_REQUEST); exit();
            if (!$a_errors) 
            {
                foreach ($_REQUEST as $key => $value) 
                {
                    $_REQUEST[$key] = $oTicket->antiInjection($_REQUEST[$key]);
                }
                $s_problem_solve  = "";
                $s_pending_desc = "";
                $notif_message = "";
                //jika selesai selesai 
                if($_REQUEST['status_id'] == '8')
                {
                    $s_problem_solve = $_REQUEST['keterangan'];
                    $notif_message = "tiket selesai.\n" . $s_problem_solve; 
                }
                //jika pending
                else
                {
                    $s_pending_desc = $_REQUEST['keterangan'];
                    $notif_message = "tiket pending.\n" . $s_pending_desc;
                }
                if($oTicket->analisa(
                                        $_REQUEST['ticket_id'],
                                        $_REQUEST['komputer_id'],
                                        $_REQUEST['status_id'],
                                        $_REQUEST['kategori'],
                                        $s_problem_solve,
                                        $s_pending_desc
                    ))
                {
                    $a_ticket = $oTicket->getList(" WHERE ticketID='{$_REQUEST['ticket_id']}' ","","");
                    if(count($a_ticket) > 0 )
                    {

                        $a_message['message'] = "{$a_ticket[0]['helpdeskHandleName']} : '$notif_message'";
                        $a_tokens[] = $a_ticket[0]['userRequestFirebaseToken'];
                        if($oNotification->create(

                            "register device" ,
                            "{$a_ticket[0]['helpdeskHandleName']} : '$notif_message'" ,
                            "" ,
                            $a_ticket[0]['requestUserID'] ,
                            $a_ticket[0]['userRequestFirebaseToken'] ,
                            $a_ticket[0]['helpdeskID'] ,
                            ""
                        ))
                        {

                            $result['result'] = "success";
                            $result['desc'] = "tiket sudah berhasil dianalisa";
                        }
                        else
                        {

                            $result['result'] = "error";
                            $result['desc'] = "tiket tidak dapat  dianalisa. error";
                        }

                        
                        // $hasil = send_notification($a_tokens,$a_message);
                    }
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "tiket tidak dapat dianalisa";
                }
            }
        }
        else
        {
            foreach ($a_errors as $error) {
                $s_error .= "$error  <br />";
            }
            $result['desc']  = $s_error;
        }
    }
    echo json_encode($result);
    $oTicket->closeDB();
    $oUser->closeDB();
    $oNotification->closeDB();
?>