<?php
    /*
    |--------------------------------------------------------------------------
    | Tiket
    |--------------------------------------------------------------------------
    |Controler  modul tiket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");

    $oHelpdesk = new Helpdesk();
    $oTicket = new Ticket();
    $oGroup = new Group();
 
    //---table
    $a_title  = array();
    $a_title[] = "KELUHAN";
    $a_title[] = "NAMA / TANGGAL  PELAPOR";
    $a_title[] = "HELPDESK";
    $a_title[] = "";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:150px;' ";
    $a_title_class[] = " style='width:150px;' ";
    $a_title_class[] = " style='width:50px;' ";

    $s_table_container = "";
    $s_condition = " WHERE false ";
    $s_limit = "  ";
    $s_order = "  ORDER BY `ticketRequestDate` DESC ";
    $s_group_name = '';
    $s_wilayah_kerja = "";

    $s_group_id_admin = 'SUPER_ADMINISTRATOR';
    $s_group_id_helpdesk = 'GROUP_HELPDESK';
    if($oGroup->isGroup($USER[0]['userID'],$s_group_id_admin))
    {
        $s_group_name = "ADMINISTRATOR"; 
        $s_condition = " WHERE true ";       
    }
    elseif($oGroup->isGroup($USER[0]['userID'],$s_group_id_helpdesk))
    {
        $s_group_name = "HELPDESK";
        $s_where_helpdesk = " WHERE helpdeskID = '{$USER[0]['userID']}' ";
        $i_count_wilayah_handle = $oHelpdesk->getCount($s_where_helpdesk);  
        if( $i_count_wilayah_handle > 0)
        {
            $a_wilayah_kerja = $oHelpdesk->getList($s_where_helpdesk,"","");
            for($i=0;$i<$i_count_wilayah_handle;$i++)
            {
                if($i == 0)
                {
                    $s_wilayah_kerja = "'".$a_wilayah_kerja[$i]['unitHandle']."'";
                }
                else
                {
                    $s_wilayah_kerja .= ","."'".$a_wilayah_kerja[$i]['unitHandle']."'";   
                }
            }
            $s_wilayah_kerja = "(".$s_wilayah_kerja.")";
        }

        $s_condition = " WHERE UR.wilayahPenempatan IN {$s_wilayah_kerja} ";

    }


    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/tiket/tiket.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";


     $a_data = $oTicket->getList($s_condition, $s_order, $s_limit);
     if(isset($a_data))
     {
          
          $s_table_container ="";
          $s_table_container .="
                    <table  id='table-tiket' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
          $s_table_container .="</tr>
                         </thead>";
          $s_table_container .="<tbody>";



               for($i=0;$i<count($a_data);$i++)
               {
                    $s_status = "";
                    $s_button = "";
                    $s_bg_column = "";

                    if($a_data[$i]['ticketHelpdeskHandle'] == $USER[0]['userID'])
                    {

                      $s_button =  "<button class='btn  btn-success btn-sm button-analisa'  record-id='{$a_data[$i]['ticketID']}' >Analisa</button>";
                    }

                    //------------- begin status-------------------
                    $s_status ="";
                    if($a_data[$i]['ticketStatus'] == "0")
                    {
                        $s_status = "<span class='label label-danger'>baru masuk</span>";
                    }
                    elseif($a_data[$i]['ticketStatus'] == "1")
                    {
                        $s_status = "<span class='label label-warning'>sedang pengerjaan</span>";
                    }
                    elseif($a_data[$i]['ticketStatus'] == "2")
                    {
                        $s_status = "<span class='label label-warning'>menunggu persetujuan</span>";
                    }
                    elseif($a_data[$i]['ticketStatus'] == "3")
                    {
                        $s_status = "<span class='label label-warning'>menunggu material</span>";
                    }
                    elseif($a_data[$i]['ticketStatus'] == "8")
                    {
                        $s_status = "<span class='label label-success'>selesai</span>";
                        $s_button = "";
                    }
                    //-------------end status-------------------

                    
                    //untuk informasi jumlah soal
                    $s_table_container .="<tr >";
                    $s_table_container .= "<td  align='left'>"
                                             .$s_status.strtoupper ($a_data[$i]['ticketNumber'])."<br>"
                                             .strtoupper ($a_data[$i]['ticketProblemDesc'])
                                        ."</td>";
                    $s_table_container .= "<td  align='left'>"
                                             .strtoupper ($a_data[$i]['userRequestName'])."<br>"
                                             .strtoupper ($a_data[$i]['ticketRequestDate'])
                                        ."</td>";
                    $s_table_container .= "<td  align='left' >"
                                             .strtoupper ($a_data[$i]['helpdeskHandleName'])
                                        ."</td>";
                    $s_table_container .= "<td  > $s_button</td>";
                    $s_table_container .="</tr>";
               }    
          $s_table_container .="</tbody>";
          $s_table_container .="</table>";
     }



    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>KOMPUTER / LAPTOP</h4>
                                </div>
                                <div style='float:right'></div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oHelpdesk->closeDB();
    $oGroup->closeDB();
    $oTicket->closeDB();

?>