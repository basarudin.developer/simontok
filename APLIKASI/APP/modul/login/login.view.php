<?php
/*
|--------------------------------------------------------------------------
| Login View
|--------------------------------------------------------------------------
|
|View login
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

require_once($SYSTEM['DIR_MODUL_LAYOUT']."/meta.php");
require_once($SYSTEM['DIR_MODUL_LAYOUT']."/css.php");
require_once($SYSTEM['DIR_MODUL_LAYOUT']."/js.php");

$LAYOUT_CSS .= "
        <link rel='stylesheet' href='assets/css/adminlte.css'>
        <link rel='stylesheet' href='modul/login/login.css'>
";
$LAYOUT_JS .= "
                <script src='assets/bower_components/jquery-validation/js/jquery.validate.js'></script>
                <script src='modul/login/login.js'></script>
                ";
  $CONTENTS .= "
  <!DOCTYPE html>
<html>
<head>
";
  $CONTENTS .= $LAYOUT_META;
  $CONTENTS .= $LAYOUT_CSS;
  $CONTENTS .= "
</head>
<body class='hold-transition login-page'>
<div class='login-box'>
  <!-- /.login-logo -->
  <div class='login-box-body'>
    <div class='login-logo'>
      <b>{$SYSTEM['APP_NAME']}</b>
    </div>
    <p class='login-box-msg'>Silahkan login untuk memulai</p>

    <form action='asset/index2.html' method='post' class='login-form'>

      <div class='alert alert-danger display-hide' style='display:none'>
        <button class='close' data-close='alert'></button>
        <span  id='info'>
        </span>
      </div>
      <div class='form-group has-feedback'>
        <input class='form-control' placeholder='Username' name='username'>
        <span class='glyphicon glyphicon-user form-control-feedback'></span>
      </div>
      <div class='form-group has-feedback'>
        <input type='password' class='form-control' placeholder='Password' name='password'>
        <span class='glyphicon glyphicon-lock form-control-feedback'></span>
      </div>
      <div class='row'>
        <div class='col-xs-8'>
        </div>
        <!-- /.col -->
        <div class='col-xs-4'>
          <button type='submit' class='btn btn-success btn-block btn-flat'>Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

";

  $CONTENTS .= $LAYOUT_JS;

  $CONTENTS .= "
</body>
</html>
  ";
?>