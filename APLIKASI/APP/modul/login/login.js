
        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            errorLabelContainer : '#info',
            focusInvalid: false, // do not focus the last invalid input
            rules: {
            },

            messages: {
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).hide();
            },

            highlight: function(element) { // hightlight error inputs  

                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                $('.alert-danger', $('.login-form')).hide();
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
                $('.alert-danger', $('.login-form')).hide();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
                $('.alert-danger', $('.login-form')).hide();
            },

            submitHandler: function(form) {
                $('.alert-danger').hide();
                $.ajax({
                   type: 'POST',
                   url: "index.php?page=login&type=model",
                   dataType:"json",
                   data: $(".login-form").serialize(),
                   cache:false,
                   //jika complete maka
                   complete:
                   function(data,status){
                        return false;
                   },
                   success:
                        function(msg,status){
                            //alert(msg);
                            //jika menghasilkan result
                            if(msg.result == 'success'){
                                document.location='index.php?page=welcome';
                            }else{
                                $('#info').html(msg.desc);
                                $('#info').show();
                                $('.alert-danger', $('.login-form')).show();
                            }
                        },
                   //untuk sementara tidak digunakan
                   error:
                        function(msg,textStatus, errorThrown){
                            $('#info').html('Terjadi error saat proses kirim data');
                        }
              });//end of ajax
              return false;
                //form.submit(); // form validation success, call ajax form submit
            }
        });


        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });