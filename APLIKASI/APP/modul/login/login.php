<?php
/*
|--------------------------------------------------------------------------
| Login
|--------------------------------------------------------------------------
|
|Controler login
|    
|Digunakan untuk membuat controler login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
	//jika ditemukan error dengan nilai id
	if(isset($_REQUEST['type']))
	{
		if ($_REQUEST['type'] == "model") 
		{
    		require_once($SYSTEM['DIR_MODUL']."/login/login.model.php");
	 	} 
		elseif($_REQUEST['type'] != "")
		{
			$error_login[12] = "akun anda telah digunakan login di tempat/device lain";
			$error_login[15] = "username tidak ditemukan";
			$error_login[14] = "anda belum mempunyai hak akses sama sekali";
			if(!isset($_REQUEST['record_id']))
			{
				$_REQUEST['record_id'] = "";
			}
			$error_login_id = $_REQUEST['record_id'];
			//mengkaitkan kode error dengan parameter request  URI
			if(array_key_exists($error_login_id, $error_login))
			{
			    $result['result'] = 'error';
			    $result['desc'] = $error_login[$error_login_id] ;
			}
			else
			{
			    $result['result'] = 'error';
			    $result['desc'] = "kode error tidak ditemukan";
			}
    		require_once($SYSTEM['DIR_MODUL']."/login/login.view.php");	
    		require_once($SYSTEM['DIR_MODUL']."/login/login.view.php");
		}
	}
	else
	{
    	require_once($SYSTEM['DIR_MODUL']."/login/login.view.php");
	}
?>