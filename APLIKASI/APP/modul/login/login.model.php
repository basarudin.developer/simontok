<?php
/*
|--------------------------------------------------------------------------
| Login Model
|--------------------------------------------------------------------------
|
|Model login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

	$PAGE_ID="LOGIN";
	require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.client.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.log.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.firewall.php");
	$oUser = new UserInfo();
	$oLog = new Log();
	$oClient = new Client();
	//jika variable error sudah terisi maka kosongkanlah
	$result['result'] = "error";
	$result['desc'] = "";

	$sError  = "";

	if(isset($_REQUEST['TOKEN_KEY']))
	{
		if($_REQUEST['TOKEN_KEY'] != "")
		{
			$s_condition = " WHERE loginToken = '{$_REQUEST['TOKEN_KEY']}' AND active =1; ";

			if($oUser->getUserCount($s_condition)> 0)
			{
				/**
				* dialihkan ke halaman yang telah disecure
				*/
				$a_userinfo = $oUser->getUserList($s_condition,"","");
	      
				/*
				|$CLIENT_ADDRESS didapat dari modul firewall
				*/
				if($oLog->logging($a_userinfo[0]['userID'],$oClient->getUserAgent(),$CLIENT_ADDRESS,$oClient->getClientUrl(),$PAGE_ID,"telah berhasil login"))
				{
					$result['result'] = "success";
					$result['desc']  = "Login berhasil";
					$result['token_key'] = session_id();
				}
				else
				{
					$result['result'] = "error";
					$result['desc']  = "Autentifikasi gagal";
				}
			}
			//jika username dan password tidak ditemukan disystem
			else
			{
				$result['result'] = "error";
				$result['desc']  = "TOKEN KEY gagal diidentifikasi";

			}
		}
		else
		{	
			$result['result'] = "error";
			$result['desc']  = "TOKEN KEY tidak boleh kosong";

		}
			

	}
	elseif(LOGIN_REFERENCE == "PASSWORD")
	{
		if( (!isset($_REQUEST['username']) ) || ($_REQUEST['username'] == "") )
		{
			$result['result'] = "error";
			$aErrors[] = "Username harus diisi";
		}
		if( (!isset($_REQUEST['password']) ) || ($_REQUEST['password'] == "") )
		{
			$result['result'] = "error";
			$aErrors[] = "Password harus diisi";
		}
		//periksa username apakah ada di dalam database
		if(isset($_REQUEST['username']) && isset($_REQUEST['password']) &&($_REQUEST['username'] != "") && ($_REQUEST['password'] != "") )
		{
			//validasi login ke system
			//jika username dan password ditemukan disystem
			$USERNAME = $oUser->antiInjection($_REQUEST['username']);
			$PASSWORD = $oUser->antiInjection($_REQUEST['password']);
			$s_condition  = " WHERE username= '".$_REQUEST['username']."' ";
			if($oUser->getUserCount($s_condition)<1)
			{
				$result['result'] = "error";
				$result['desc']  = "username tidak ditemukan";
			}else{
				$a_username =$oUser->getUserList($s_condition,"","");
				if($a_username[0]['active'] == 2)
				{
					$result['result'] = "error";
					$result['desc']  = "username telah dinonaktifkan";
				}
				elseif($a_username[0]['active'] == 0)
				{
					$result['result'] = "error";
					$result['desc']  = "username telah dihapus";
				}
				elseif($a_username[0]['active'] == 1)
				{
					$s_condition  = " WHERE username= '{$USERNAME}' AND password=md5('{$PASSWORD}') AND active=1 ";
					if($oUser->getUserCount($s_condition)> 0)
					{
						if($oUser->updateSessionLogin($USERNAME))
						{
	                        $_SESSION['LoggedIn'] = true;
	                        $_SESSION['username'] = $USERNAME;
	                        $a_userinfo = $oUser->getUserIDByUsername($USERNAME);

							if($oLog->logging($a_userinfo[0]['userID'],$oClient->getUserAgent(),$CLIENT_ADDRESS,$oClient->getClientUrl(),$PAGE_ID,"telah berhasil login"))
							{
								$result['result'] = "success";
								$result['desc']  = "Login berhasil";
							}
							else
							{
								$result['result'] = "error";
								$result['desc']  = "Autentifikasi gagal di log";
							}

						}
						else
						{
							$result['result'] = "error";
							$result['desc']  = "sesi login error";
						}

					}
					else
					{
						$result['result'] = "error";
						$result['desc']  = "password salah";
					}
				}
				else
				{
					$result['result'] = "error";
					$result['desc']  = "status username tidak diketahui";
				}
			}
		}
		else{
		   	foreach ($aErrors as $error) {
		        $sError .= "$error  <br />";
		   	}
	  	 	$result['desc']  = $sError;
		}
	}

	echo json_encode($result);
	$oUser->closeDB();
	$oLog->closeDB();
?>