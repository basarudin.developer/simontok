<?php
/*
|--------------------------------------------------------------------------
| Login Model
|--------------------------------------------------------------------------
|
|Model login
|    
|Digunakan untuk membuat model data login
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
	$PAGE_ID="LOGIN";
	require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.client.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.log.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.firewall.php");
	require_once($SYSTEM['DIR_PATH']."/class/class.device.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.firebase.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");
	$oUser = new UserInfo();
	$oLog = new Log();
	$oClient = new Client();
	$oDevice = new Device();
    $oNotif = new Notification();
	//jika variable error sudah terisi maka kosongkanlah
	$result['result'] = "error";
	$result['desc'] = "";

	$sError  = "";
    $a_errors = array();
    if(isset($_REQUEST['action']))
    {

        if($_REQUEST['action'] == 'aktifasi')
        {

            if($_REQUEST['durID'] == "")
            {
                $a_errors[] = "device register ID tidak ada";
            }
            if (!$a_errors) 
            {
            	//update di datauser
            	if($oDevice->activationDeviceRegister($_REQUEST['durID']))
            	{
                    $message = "perangkat anda telah diregistrasi oleh ".$USER[0]['realName'];
                    //kirim pesan 
                    $a_data = $oDevice->getList(" WHERE durID = '{$_REQUEST['durID']}' ","","");
                    if($oNotif->create(

                        "register device" ,
                        $message ,
                        "" ,
                        $a_data[0]['userID'] ,
                        $a_data[0]['firebaseToken'] ,
                        $USER[0]['userID'] ,
                        ""
                    ))
                    {

                        $result['result'] = 'success';
                        $result['desc'] = "device berhasil diregister";
                    }
                    else
                    {

                        $result['result'] = 'error';
                        $result['desc'] = "ada error waktu notifikasi";
                    }
                    
                    //$a_result = json_decode(send_notification($a_tokens,$a_message));


            	}
            	else
            	{

                    $result['result'] = 'error';
                    $result['desc'] = "device gagal diregister";
            	}
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) 
                {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
        }
        elseif($_REQUEST['action'] == 'testnotif')
        {

            if($_REQUEST['durID'] == "")
            {
                $a_errors[] = "device register ID tidak ada";
            }
            if (!$a_errors) 
            {
                $a_data = $oDevice->getList(" WHERE durID = '{$_REQUEST['durID']}' ","","");
                if(count($a_data)>=1)
                {
                    $a_message['notifID'] = $oNotif->getCount("");
                    $a_message['title'] = "test ".$SYSTEM['APP_NAME'];
                    $a_message['message'] = "test notifikasi ".$SYSTEM['APP_NAME'];

                    $a_tokens[] = $a_data[0]['firebaseToken'];
                    $a_result = json_decode(send_notification($a_tokens,$a_message));
                    if($a_result->success == "1")
                    {

                        $result['result'] = 'success';
                        $result['desc'] = "pesan  di device {$a_data[0]['deviceModel']} sudah terkirim ";
                    }
                    else
                    {
                        $result['result'] = 'error';
                        $result['desc'] = "device tidak ditemukan";
                    }
                }
                else
                {

                    $result['result'] = 'error';
                    $result['desc'] = "device tidak ditemukan";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) 
                {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
        }
        elseif($_REQUEST['action'] == 'delete')
        {

            if($_REQUEST['durID'] == "")
            {
                $a_errors[] = "device register ID tidak ada";
            }
            if (!$a_errors) 
            {
                //update di datauser
                if($oDevice->delete($_REQUEST['durID']))
                {
                    $result['result'] = 'success';
                    $result['desc'] = "device berhasil dihapus";

                }
                else
                {

                    $result['result'] = 'error';
                    $result['desc'] = "device gagal dihapus";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) 
                {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
        }
    }
	echo json_encode($result);
	$oUser->closeDB();
	$oLog->closeDB();
	$oDevice->closeDB();
    $oNotif->closeDB();

?>