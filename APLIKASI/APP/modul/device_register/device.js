$(document).ready(function() {

    /*************************************************
    |   BEGIN TABLE-COMPUTER
    **************************************************/
    if($('#table-device').length)
    {
        
        $('#table-device').DataTable
        ({        
            rowReorder: true,
            columnDefs: [
                { orderable: true, className: 'reorder', targets:[ 0, 2 ] },
                { orderable: false, targets: '_all' }
            ],
            lengthMenu: [[10, 25, -1], [10, 25, "All"]],
             order: [[ 2, "desc" ]]
        });


        $('#table-device tbody').on( 'click', '.button-device-activation', function () 
        {
            device_register_id = $(this).attr('record-id');
            //alert(device_register_id);
            $.ajax({
                type: 'POST',
                url: "index.php?page=device&type=model&action=aktifasi",
                dataType:"json",
                data: 'durID='+device_register_id,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();
                            setTimeout(function() {
                                document.location='index.php?page=device&action=list'
                            }, 1500);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#container-alert').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        $.Notify({
                            caption: '',
                            content: 'Terjadi kegagalan proses transaksi',
                            keepOpen: true,
                            type: 'alert'
                        });
                    }
            });//end of ajax
        });

        $('#table-device tbody').on( 'click', '.button-device-delete', function () 
        {

            device_register_id = $(this).attr('record-id');
            //alert(device_register_id);
            perangkat = $(this).parent().parent().find(".perangkat").html(); 
            $('#modal-content').html('apakah yakin menghapus perangkat <b>'+perangkat+'</b> ?');
            $('#modal-container').modal('show');
            jQuery('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=device&type=model&action=delete",
                    dataType:"json",
                    data: 'durID='+device_register_id,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=device&action=list'
                                }, 1500);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            $.Notify({
                                caption: '',
                                content: 'Terjadi kegagalan proses transaksi',
                                keepOpen: true,
                                type: 'alert'
                            });
                        }
                });//end of ajax
            });
        });


        $('#table-device tbody').on( 'click', '.button-send-message', function () 
        {
            $('#notification-container').hide(); 
            device_register_id = $(this).attr('record-id');
            $.ajax({
                type: 'POST',
                url: "index.php?page=device&type=model&action=testnotif",
                dataType:"json",
                data: 'durID='+device_register_id,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#container-alert').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        //alert('asa');
                    }
            });//end of ajax
        });

    }
    /*   END TABLE-COMPUTER
    **************************************************/
        

    

});