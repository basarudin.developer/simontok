<?php
    /*
    |--------------------------------------------------------------------------
    | device view
    |--------------------------------------------------------------------------
    |view  modul device register
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    require_once($SYSTEM['DIR_PATH']."/class/class.device.php");
    $oDevice = new Device();
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    $oGroup = new Group();
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");
    $oHelpdesk = new Helpdesk();


    $a_title[] = "PENGGUNA";
    $a_title[] = "PERANGKAT";
    $a_title[] = "TANGGAL DAFTAR";
    $a_title[] = "";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='width:10px;' ";

    $s_table_container = "";
    $s_condition = " WHERE registerStatus !=2 ";
    $s_limit = "  ";
    $s_order = "  ORDER BY `dateRegister` DESC ";


    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/device_register/device.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";


    $s_group_id_admin = 'SUPER_ADMINISTRATOR';
    $s_group_id_helpdesk = 'GROUP_HELPDESK';
    if($oGroup->isGroup($USER[0]['userID'],$s_group_id_admin))
    {
        $s_group_name = "ADMINISTRATOR"; 
        $s_condition = " WHERE registerStatus !=2   ";       
    }
    elseif($oGroup->isGroup($USER[0]['userID'],$s_group_id_helpdesk))
    {
        $s_group_name = "HELPDESK";
        $s_where_helpdesk = " WHERE helpdeskID = '{$USER[0]['userID']}' ";
        $i_count_wilayah_handle = $oHelpdesk->getCount($s_where_helpdesk);  
        if( $i_count_wilayah_handle > 0)
        {
            $a_wilayah_kerja = $oHelpdesk->getList($s_where_helpdesk,"","");
            for($i=0;$i<$i_count_wilayah_handle;$i++)
            {
                if($i == 0)
                {
                    $s_wilayah_kerja = "'".$a_wilayah_kerja[$i]['unitHandle']."'";
                }
                else
                {
                    $s_wilayah_kerja .= ","."'".$a_wilayah_kerja[$i]['unitHandle']."'";   
                }
            }
            $s_wilayah_kerja = "(".$s_wilayah_kerja.")";
        }

        $s_condition = " WHERE U.wilayahPenempatan IN {$s_wilayah_kerja}  AND registerStatus !=2 ";

    }

    $a_data = $oDevice->getList($s_condition, $s_order, $s_limit);
    if(isset($a_data))
    {
          
        $s_table_container ="";
        $s_table_container .="
                    <table  id='table-device' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
        $s_table_container .="</tr>
                        </thead>";
        $s_table_container .="<tbody>";

        for($i=0;$i<count($a_data);$i++)
        {
            $s_status = "";
            $s_button = "";
            $s_bg_column = "";

            //------------- begin button-------------------
            if($a_data[$i]['registerStatus'] == '0')
            {
                $s_button = "<button class='button-device-activation btn btn-flat   btn-success btn-sm '  record-id='{$a_data[$i]['durID']}'  style='margin-left: 2px;'>Aktifasi</button>";
            }
            if($a_data[$i]['registerStatus'] == '1')
            {
                if($a_data[$i]['firebaseToken'] == $a_data[$i]['mainFirebase'] )
                {
                    $s_status = "  <span class='label label-primary'>device utama</span> ";
                    $s_button = "<button class='button-send-message btn btn-flat   btn-warning btn-sm '  record-id='{$a_data[$i]['durID']}'  style='margin-left: 2px;'>Uji Pesan</button>";
                }
                else
                {

                    $s_button = "<button class='button-device-delete btn btn-flat   btn-danger btn-sm '  record-id='{$a_data[$i]['durID']}'  style='margin-left: 2px;'>Hapus</button>";
                }
            }
            //------------- end button---------------------

                    
            //untuk informasi jumlah soal
            $s_table_container .="<tr >";
            $s_table_container .= "<td  align='left'>"
                                     .strtoupper ($a_data[$i]['userID'])."<br>"
                                     ."<p class='hostname'>".$a_data[$i]['realName']."</p>"
                                ."</td>";
            $s_table_container .= "<td  align='left'>"
                                     ."<p class='perangkat'>".strtoupper ($a_data[$i]['deviceModel'])."</p><br>$s_status"
                                ."</td>";
            $s_table_container .= "<td  align='left' >"
                                     .strtoupper ($a_data[$i]['dateRegister'])
                                ."</td>";
            $s_table_container .= "<td  > $s_button</td>";
            $s_table_container .="</tr>";
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
    }
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>DEVICE PENGGUNA MOBILE</h4>
                                </div>
                                <div style='float:right'></div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oDevice->closeDB();
    $oGroup->closeDB();
    $oHelpdesk->closeDB();

?>