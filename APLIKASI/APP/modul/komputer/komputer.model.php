<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer Model
    |--------------------------------------------------------------------------
    |model  modul komputer
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");

    $oHelpdesk = new Helpdesk();
    $oKomputer = new Computer();

    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "";


     
    if(isset($_REQUEST['action']))
    {
        if($_REQUEST['action'] == 'pengadaan')
        {
            if($_REQUEST['komputerID'] == "")
            {
                $a_errors[] = "Komputer ID tidak ada";
            }
            if($_REQUEST['vendor'] == "")
            {
                $a_errors[] = "vendor pengadaan harus diisi";
            }

			
            if (!$a_errors) 
            {
                if($oKomputer->setPengadaan($_REQUEST['komputerID'],$_REQUEST['vendor']))
                {
                    $result['result'] = 'success';
                    $result['desc'] = "vendor pengadaan berhasil di update";
                }
                else
                {
                    $result['result'] = 'error';
                    $result['desc'] = "gagal update vendor pengadaan";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            
            echo json_encode($result);
        }
        if($_REQUEST['action'] == 'user_on')
        {
            if($_REQUEST['komputerID'] == "")
            {
                $a_errors[] = "Komputer ID tidak ada";
            }
            if($_REQUEST['userID'] == "")
            {
                $a_errors[] = "Pengguna harus diisi";
            }

            if (!$a_errors) 
            {
                if($oKomputer->setUserOn($_REQUEST['komputerID'],$_REQUEST['userID']))
                {
                    $result['result'] = 'success';
                    $result['desc'] = "komputer berhasil di update pengguna";
                }
                else
                {
                    $result['result'] = 'error';
                    $result['desc'] = "gagal update pengguna komputer";
                }
            }
            
            echo json_encode($result);
        }
        elseif($_REQUEST['action'] == 'user_off')
        {
            if($_REQUEST['komputerID'] == "")
            {
                $a_errors[] = "Komputer ID tidak ada";
            }

            if (!$a_errors) 
            {
                $a_data = $oKomputer->getList(" WHERE komputerID = '".$_REQUEST['komputerID']."'; ","","");
                if(count($a_data)>0)
                {
                    if($a_data[0]['komputerUser'] != "")
                    {
                        if($oKomputer->setUserOff($_REQUEST['komputerID'], $a_data[0]['komputerUser']))
                        {
                            $result['result'] = "success";
                            $result['desc'] = "";
                        }
                        else
                        {
                            $result['result'] = "error";
                            $result['desc'] = "System gagal diupdate";
                        }
                    }
                    else
                    {
                        $result['result'] = "error";
                        $result['desc'] = "Pengguna tidak ada / sudah kosong.system tidak merubah apapun";
                    }
                }
                else
                {

                    $result['result'] = "error";
                    $result['desc'] = "Komputer tidak ditemukan 1";
                }
            }
            echo json_encode($result);
        }

        elseif($_REQUEST['action'] == 'delete_item')
        {
            if($_REQUEST['itemID'] == "")
            {
                $a_errors[] = "Item ID tidak ada";
            }

            if (!$a_errors) 
            {
                if($oKomputer->deleteItem($_REQUEST['itemID']))
                {
                    $result['result'] = "success";
                    $result['desc'] = "item sudah berhasil dihapus";
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "item gagal dihapus";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }
        elseif($_REQUEST['action'] == 'delete_computer_confirm')
        {
            if($_REQUEST['komputerID'] == "")
            {
                $a_errors[] = "komputer ID tidak ada";
            }

            if (!$a_errors) 
            {
                if($oKomputer->deleteConfirm($_REQUEST['komputerID']))
                {
                    $result['result'] = "success";
                    $result['desc'] = "komputer sudah berhasil dihapus";
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "komputer gagal dihapus";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }
        elseif($_REQUEST['action'] == 'delete')
        {
            if($_REQUEST['komputerID'] == "")
            {
                $a_errors[] = "komputer ID tidak ada";
            }

            if (!$a_errors) 
            {
                if($oKomputer->delete($_REQUEST['komputerID']))
                {
                    $result['result'] = "success";
                    $result['desc'] = "komputer sudah masuk daftar pengajuan penghapusan";
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "komputer gagal dihapus";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }
        elseif($_REQUEST['action'] == 'cancel_delete_computer')
        {
            if($_REQUEST['komputerID'] == "")
            {
                $a_errors[] = "komputer ID tidak ada";
            }

            if (!$a_errors) 
            {
                if($oKomputer->cancel_delete($_REQUEST['komputerID']))
                {
                    $result['result'] = "success";
                    $result['desc'] = "komputer berhasil dibatalkan penghapusan";
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "gagal dibatalkan";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }
        elseif($_REQUEST['action'] == 'cancel_item')
        {
            if($_REQUEST['itemID'] == "")
            {
                $a_errors[] = "Item ID tidak ada";
            }

            if (!$a_errors) 
            {
                if($oKomputer->cancelItem($_REQUEST['itemID']))
                {
                    $result['result'] = "success";
                    $result['desc'] = "item sudah berhasil dibatalkan";
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "item gagal dibatalkan";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }
        elseif($_REQUEST['action'] == 'delete_item_confirm')
        {
            if($_REQUEST['itemID'] == "")
            {
                $a_errors[] = "Item ID tidak ada";
            }

            if (!$a_errors) 
            {
                if($oKomputer->deleteItemConfirm($_REQUEST['itemID']))
                {
                    $result['result'] = "success";
                    $result['desc'] = "penghapusan item  sudah disetujui";
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "gagal disetujui";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }

        elseif($_REQUEST['action'] == 'update')
        {
            if($_REQUEST['ipaddress'] == "")
            {
                $a_errors[] = " IP address tidak boleh kosong";
            }

            if($_REQUEST['hostname'] == "")
            {
                $a_errors[] = "Hostname tidak boleh kosong";
            }

            if($_REQUEST['komputerID'] == "")
            {
                $a_errors[] = "Komputer ID tidak ada";
            }

            if (!$a_errors) 
            {
                if($oKomputer->update($_REQUEST['komputerID'] ,$_REQUEST['hostname'] ,$_REQUEST['ipaddress'] ))
                {
                    $result['result'] = "success";
                    $result['desc'] = "komputer berhasil diubah";
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "item gagal diubah";
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }
        elseif($_REQUEST['action'] == 'add')
        {
            if($_REQUEST['ipaddress'] == "")
            {
                $a_errors[] = " IP address tidak boleh kosong";
            }
            if($_REQUEST['hostname'] == "")
            {
                $a_errors[] = "Hostname tidak boleh kosong";
            }
            if($_REQUEST['kategori'] == "")
            {
                $a_errors[] = " Kategori tidak boleh kosong";
            }
            if($_REQUEST['macaddress'] == "")
            {
                $a_errors[] = " Mac address tidak boleh kosong";
            }

            if($_REQUEST['os'] == "")
            {
                $a_errors[] = " Sistem operasi tidak boleh kosong";
            }

            if($_REQUEST['prosesor'] == "")
            {
                $a_errors[] = " Prosesor tidak boleh kosong";
            }
            if($_REQUEST['monitor'] == "")
            {
                $a_errors[] = " Monitor tidak boleh kosong";
            }
            if($_REQUEST['memori'] == "")
            {
                $a_errors[] = " Memori tidak boleh kosong";
            }
            if($_REQUEST['harddisk'] == "")
            {
                $a_errors[] = " Harddisk tidak boleh kosong";
            }

            if (!$a_errors) 
            {
                //cek mac address
                if($oKomputer->ifExistMacAdress($_REQUEST['macaddress']))
                {
                    echo "masuk mac";
                    $result['result'] = "error";
                    $result['desc'] = "Komputer sudah terdaftar";
                }
                //cek hostname
                elseif($oKomputer->getCount(" WHERE komputerIdentifikasi = '{$_REQUEST['hostname']}' ; ")>0)
                {
                    $result['result'] = "error";
                    $result['desc'] = "Hostname sudah terdaftar";
                }
                //cek hostname
                elseif($oKomputer->getCount(" WHERE komputerIPUtama = '{$_REQUEST['ipaddress']}' ; ")>0)
                {
                    $result['result'] = "error";
                    $result['desc'] = "ip address sudah dipakai";
                }

                else
                {
                    $a_item[0]['komputerItemID'] = 'KMITOS00000000000001';
                    $a_item[0]['komputerSpekKapasitas'] = '1';
                    $a_item[0]['komputerSpekMerk'] = $_REQUEST['os'];
                    $a_item[0]['komputerSpekSerialNumber'] = '';

                    $a_item[1]['komputerItemID'] = 'KMITLNC0000000000001';
                    $a_item[1]['komputerSpekKapasitas'] = '1';
                    $a_item[1]['komputerSpekMerk'] = "";
                    $a_item[1]['komputerSpekSerialNumber'] = $_REQUEST['macaddress'];

                    $a_item[2]['komputerItemID'] = 'KMITLNC0000000000001';
                    $a_item[2]['komputerSpekKapasitas'] = $_REQUEST['prosesor'];
                    $a_item[2]['komputerSpekMerk'] = "";
                    $a_item[2]['komputerSpekSerialNumber'] = "";

                    $a_item[3]['komputerItemID'] = 'KMITMEM0000000000001';
                    $a_item[3]['komputerSpekKapasitas'] = $_REQUEST['memori'];
                    $a_item[3]['komputerSpekMerk'] = "";
                    $a_item[3]['komputerSpekSerialNumber'] = "";

                    $a_item[4]['komputerItemID'] = 'KMITMNT0000000000001';
                    $a_item[4]['komputerSpekKapasitas'] = $_REQUEST['monitor'];
                    $a_item[4]['komputerSpekMerk'] = "";
                    $a_item[4]['komputerSpekSerialNumber'] = "";

                    $a_item[5]['komputerItemID'] = 'KMITHDK0000000000001';
                    $a_item[5]['komputerSpekKapasitas'] = $_REQUEST['harddisk'];
                    $a_item[5]['komputerSpekMerk'] = "";
                    $a_item[5]['komputerSpekSerialNumber'] = "";

                    if($_REQUEST['kategori'] == "SERVER")
                    {
                        $computer_tipe = 'SERVER';
                    }
                    else
                    {
                        $computer_tipe = 'CLIENT';
                    }
                    if($oKomputer->createManual(
                        $computer_tipe,
                        $_REQUEST['kategori'],
                        $_REQUEST['ipaddress'],
                        $_REQUEST['hostname'],
                        $USER[0]['wilayahPenempatan'],
                        $a_item
                    ))
                    {
                        $result['result'] = "success";
                        $result['desc'] = "komputer berhasil dientry";
                        $result['komputerID'] =$oKomputer->lastInsertKomputerID();
                    }
                    else
                    {
                        $result['result'] = "error";
                        $result['desc'] = "komputer gagal dientry";
                    }
                    
                }
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }


          
     }
    $oKomputer->closeDB();
    $oHelpdesk->closeDB();

?>