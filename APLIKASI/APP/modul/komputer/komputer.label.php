<?php
    /*
    |--------------------------------------------------------------------------
    | Label inverntasi komputer
    |--------------------------------------------------------------------------
    |modul cetak pdf berita acara penyerahan komputer kepada pegawai
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/fpdf/class.fpdf.php");
    require_once($SYSTEM['DIR_PATH']."/class/fpdf/class.qrcode.php");

    $oKomputer = new Computer();
    $oWilayah = new Wilayah();

    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "";

    $i_container_height = 19.5;
    $i_container_width = 50;
    $i_image_height =  14.5;
    $i_image_width =  11.5;

    //maksimal banyak label perhalaman
    $i_max_container_per_page = 11;

    //point start label pada halaman pertama
    $i_col_start =12;
    $i_row_start =11;

    //batas pada tiap tiap label
    $i_row_margin =5;

    $i_label_title_height = 4;


    $i_label_subtitle_height = 5;

    //detil
    $i_label_detil_width =50;
    $i_label_subdetil_width =14;
    $i_label_detil_height =4;




    //garis debugger   
    $b_debug = 1;
    if($b_debug == 1)
    {
        $i_border = 1;
    }
    else
    {
        $i_border = 0;
    }
    $pdf=new FPDF('P','mm','A4');
    $pdf->SetFont('Arial','B',16);
    if(isset($_REQUEST['komputerID']))
    {
        $iN = count($_REQUEST['komputerID']);
        if($iN > 0)
        {
            for($i=0;$i<$iN;$i++)
            {

                //komputer detail

                $a_item = $oKomputer->getList("WHERE komputerID = '".$_REQUEST['komputerID'][$i]."'","","");
				
				$a_komputer_detail = $oKomputer->getList("WHERE `komputerID` = '".$_REQUEST['komputerID'][$i]."'","","");
				$a_wilayah = $oWilayah->getList("WHERE child.`idWilayah` = '".$a_komputer_detail[0]['helpdeskGroupHandle']."'","","");
				
				$s_label_title = $a_wilayah[0]['namaWilayah'];
				$s_label_detil_kontak_phone = $a_wilayah[0]['kontakWilayah'];
				
                if($i % $i_max_container_per_page == 0)
                {
                    $pdf->AddPage();
                    $i_col_x = $i_col_start;
                    $i_row_y = $i_row_start;

                }
                $pdf->setXY($i_col_x,$i_row_y);
                //border container utama
                $pdf->Cell($i_container_width,$i_container_height,'',1,1,'C'); 
                $pdf->setXY($i_col_x,$i_row_y + $i_image_height);
                //border  sub container  
                $pdf->Cell($i_container_width,$i_container_height - $i_image_height,'',1,1,'C');                    
                $pdf->Image('assets/img/pln-medium.jpg',$i_col_x +1 ,$i_row_y +1,9.5,12.5);
                $pdf->setXY($i_col_x+$i_image_width,$i_row_y);
                //label
                $pdf->SetFont('Arial','',7);
                $pdf->Cell($i_container_width-$i_image_width,$i_label_title_height,$s_label_title,$b_debug ,1,'C');   
                //sub label
                $pdf->SetFont('Arial','B',11);
                $pdf->setXY($i_col_x+$i_image_width,$i_row_y+$i_label_title_height );
                $pdf->Cell($i_container_width-$i_image_width,$i_label_subtitle_height,$a_item[0]['komputerIdentifikasi'],$b_debug ,1,'C');   

                $pdf->SetFont('Arial','',10);
                $pdf->setXY($i_col_x+$i_image_width,$i_row_y +$i_label_title_height+$i_label_subtitle_height  );
                $pdf->Cell($i_container_width-$i_image_width,$i_label_subtitle_height,$a_item[0]['komputerID'],$b_debug ,1,'C');   

                $pdf->SetFont('Arial','B',8);
                $pdf->setXY($i_col_x,$i_row_y + $i_image_height);
                $pdf->Cell($i_label_detil_width,$i_label_detil_height+1,$s_label_detil_kontak_phone,1 ,1,'C');    
                  

                $qrcode = new QRcode($a_item[0]['komputerID'],'H');
                $qrcode->displayFPDF($pdf,$i_col_x +$i_label_detil_width,$i_row_y ,19.5);

                $i_row_y = $i_row_y + $i_container_height + $i_row_margin;
            }
        }
    }



    $oKomputer->closeDB();
    $oWilayah->closeDB();
    $pdf->Output();

?>