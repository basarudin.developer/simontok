<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer update
    |--------------------------------------------------------------------------
    |Update jenis komputer
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUser = new UserInfo();
    $oKomputer = new Computer();
	
    $a_pengadaan_id[] = 'PLN';
    $a_pengadaan_id[] = 'ICON';
	
    $a_pengadaan_name[] = 'PLN';
    $a_pengadaan_name[] = 'ICON';

    $LAYOUT_JS_EXTENDED .= "
                    <!-- DataTables -->
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>
                    <!-- InputMask -->
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.date.extensions.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.extensions.js'></script>
                    <script src='modul/komputer/komputer.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                        <link rel='stylesheet' href='assets/css/adminlte.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-pengadaan-apply' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus-square-o'></i> Terapkan
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> Kembali
                        </button>
                    ";
    $s_form_input = "";
    $s_hostname = "";
    $s_ip = "";
	
    if(isset($_REQUEST['komputerID']))
    {
     
        $_REQUEST['komputerID'] =  $oKomputer->antiInjection($_REQUEST['komputerID']); 
        $a_data = $oKomputer->getList("WHERE komputerID = '{$_REQUEST['komputerID']}'","","");

        $a_data_item = $oKomputer->getSpek($_REQUEST['komputerID']);
        if(isset($a_data))
        {
            if(isset($_REQUEST['hostname']))
            {
                $s_hostname = $_REQUEST['hostname'];
            }
            else
            {
                $s_hostname = $a_data[0]['komputerIdentifikasi'];
            }
            if(isset($_REQUEST['ipaddress']))
            {
                $s_ip = $_REQUEST['ipaddress'];
            }
            else
            {
                $s_ip = $a_data[0]['komputerIPUtama'];
            }
			
			$s_pengadaan = "";
			if(isset($a_pengadaan_id))
			{
				
				if(count($a_pengadaan_id) > 0)
				{
						
					for($i=0;$i<count($a_pengadaan_id);$i++)
					{
						$s_checked = "  ";
						if($a_data[0]['komputerPengadaan'] == $a_pengadaan_id[$i] )
						{
							$s_checked = " checked ";
						}
						$s_pengadaan .= "	
							<div class='radio'>
								<label>
								  <input type='radio' name='options_pengadaan'  value='{$a_pengadaan_id[$i]}' $s_checked  >
								  $a_pengadaan_name[$i]
								</label>
							</div>		
							";
					}
					$s_pengadaan .= "			
						";
					$s_pengadaan .= "				
						</div>";
					
				}
				$s_pengadaan_container = "
                                                <div class='form-group'>
                                                    <label>Pengadaan:</label>
                                                    <div class='input-group'>
                                                        $s_pengadaan;
                                                     </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->";
			}
            $s_form_input = "
                            <div >
                                <input type='hidden' id='komputer-id' value='{$_REQUEST['komputerID']}'>
                                <!-- /.box-header -->
                                <div class='box-body'>
                                    <div class='row'>
                                        <div class='col-md-12'>
                                            <!-- KomputerID -->
                                            <div class='form-group'>
                                                <label>Komputer ID:</label>
                                                <input type='text' class='form-control'  value='{$a_data[0]['komputerID']}' disabled>
                                                <input type='hidden'   id='komputer-id'  value='{$a_data[0]['komputerID']}'>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                        </div>
                                        <div class='col-md-6'>
                                                <!-- Hostname -->
                                                <div class='form-group'>
                                                    <label>Hostname:</label>
                                                    <div class='input-group'>
                                                        <div class='input-group-addon'>
                                                            <i class='fa fa-text-width'></i>
                                                        </div>
                                                        <input type='text' class='form-control'  value='{$s_hostname}' id='hostname' disabled>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                        <div class='col-md-6'>
                                                <!-- IP mask -->
                                                <div class='form-group'>
                                                    <label>IP mask:</label>
                                                    <div class='input-group'>
                                                        <div class='input-group-addon'>
                                                            <i class='fa fa-laptop'></i>
                                                        </div>
                                                        <input type='text' class='form-control inputmask' data-inputmask=\"'alias': 'ip'\" data-mask  value='{$s_ip}' id='ipaddress' disabled>
                                                     </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                        <div class='col-md-12'>
                                                <!-- pengadaan -->
                                                $s_pengadaan_container
                                        </div>
                                        <div class='box-body'>





                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                            </div>";



            
        }
        else
        {
            $s_form_input ="komputer tidak dikenali";
        }

    }
    else
    {
        $s_form_input ="komputer tidak terdefinisi";    
    } 
            
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>UPDATE KOMPUTER / LAPTOP</h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                                {$s_form_input}

                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oUser->closeDB();
    $oKomputer->closeDB();
?>