

    /*************************************************
    |   BEGIN TABLE ITEM DELETE
    **************************************************/
    if($('#table-item-delete').length)
    {
        $('#table-item-delete').DataTable();


        $('#table-item-delete tbody').on( 'click', '.button-delete', function () 
        {
            item_id = $(this).attr('record-id');

            item_name = $(this).parent().parent().find(".item-name").html() +  " " +  $(this).parent().parent().find(".item-desc").html();  
        	
            $('#modal-content').html('apakah yakin menghapus  komponen <b>'+item_name+'</b> ?');
            $('#modal-container').modal('show');
            jQuery('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=komputer&type=model&action=delete_item_confirm",
                    dataType:"json",
                    data: 'itemID='+item_id,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=komputer&action=delete'
                                }, 1000);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                        
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html('terjadi kesalahan pengiriman data');
                            $('#notification-container').show(); 
                        }
                });//end of ajax
            });
        });

    }
    /*   END TABLE ITEM DELETE
    **************************************************/

    /*************************************************
    |   BEGIN TABLE KOMPUTER DELETE
    **************************************************/
    if($('#table-komputer-delete').length)
    {
        $('#table-komputer-delete').DataTable();
        $('#table-komputer-delete tbody').on( 'click', '.button-delete', function () 
        {
            comp_id = $(this).attr('record-id');

            comp_name = $(this).parent().parent().find(".hostname").html() ;  
            
            $('#modal-content').html('apakah yakin menghapus   <b>'+comp_name+'</b> ?');
            $('#modal-container').modal('show');
            jQuery('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=komputer&type=model&action=delete_computer_confirm",
                    dataType:"json",
                    data: 'komputerID='+comp_id,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=komputer&action=delete-confirm'
                                }, 300);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                        
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html('terjadi kesalahan pengiriman data');
                            $('#notification-container').show(); 
                        }
                });//end of ajax
            });
        });

    }
    /*   END TABLE KOMPUTER DELETE
    **************************************************/