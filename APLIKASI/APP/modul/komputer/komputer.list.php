<?php
/**
 *
 * @version         1.0
 * @author          basarudin
 * @created     17 November 2018
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   spenulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    $oUser = new UserInfo();
    $oKomputer = new Computer();
    $oGroup = new Group();
 
    //---table
    $a_title  = array();
    $a_title[] = "<input type='checkbox' class='group-checkable' data-set='#table-computer .checkboxes'/>";
    $a_title[] = "HOSTNAME";
    $a_title[] = "IP ADDRESS";
    $a_title[] = "PENGGUNA";
    $a_title[] = "";
    $a_title_class[] = " style='width:5px;' ";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " style='width:400px;' ";

    $s_table_container = "";
    $s_condition = " WHERE true ";
    $s_limit = "  ";
    $s_order = " ";


    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $platform = 'Unknown';
    $manual_help = "";
    // First get the platform?
    if (preg_match('/linux/i', $u_agent)) 
    {    
        $manual_help =  "<div>
                            untuk os linux jalankan perintah seperti berikut <br />
                            <div  class='code-terminal'>user@PC:~$ <b>sudo bash simontok_audit_linux.sh</b></div>
                        </div>"; 
    } 
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) 
    {

        $manual_help =  "";
    } 
    elseif (preg_match('/windows|win32|win64/i', $u_agent)) 
    {
        $manual_help =  "<div>
                            untuk os windows jalankan perintah seperti berikut
                            <div  class='code-terminal'> 
                            <br />C:\><b>C:\WINDOWS\system32\cscript.exe \"C:\Documents and Settings\USER\My Documents\Downloads\pln-komputer-audit-.-to-10.5.11.2-from-10,5.11.253.vbs\"</b>
                            </div>
                            <br/>script diatas dijalankan dengan asumsi download script vbs berada di folder my document download
                        </div>"; 
    }


    $FORM_MAIN = "<form id='form-main' action='' target='' method='post'></form>";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-download-scriptvbs' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-download'></i> Audit Script
                        </button>
                        <button type='button' id='button-manual-entry' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus-square-o'></i> Manual Input
                        </button>
                        <button style='display:none'type='button' id='button-print-label' class='btn btn-flat  btn-sm btn-primary pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-print' ></i> Cetak Label
                        </button>
                    ";
    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/komputer/komputer.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";

    $s_group_id_admin = 'SUPER_ADMINISTRATOR';
    $s_group_id_helpdesk = 'GROUP_HELPDESK';
    $s_group_name = "";   
    if($oGroup->isGroup($USER[0]['userID'],$s_group_id_admin))
    {
        $s_group_name = "ADMINISTRATOR"; 
        $s_condition = " WHERE komputerStatus != 9 ";       
    }
    elseif($oGroup->isGroup($USER[0]['userID'],$s_group_id_helpdesk))
    {
        $s_group_name = "HELPDESK";
        $s_condition = " WHERE helpdeskGroupHandle = '{$USER[0]['wilayahPenempatan']}' AND komputerStatus != 9 ";

    }

     $a_data = $oKomputer->getList($s_condition, $s_order, $s_limit);
     if(isset($a_data))
     {
          
          $s_table_container ="";
          $s_table_container .="
                    <table  id='table-computer' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
          $s_table_container .="</tr>
                         </thead>";
          $s_table_container .="<tbody>";



               for($i=0;$i<count($a_data);$i++)
               {
                    $s_status = "";
                    $s_button = "";
                    $s_button_delete = "";
                    $s_button_update = "";
                    $s_bg_column = "";
                    $s_checked  = "";

                    //------------- begin icon-------------------
                    $s_icon ="";
                    if($a_data[$i]['komputerTipe'] == 'SERVER')
                    {
                        $s_icon ="<i class='fa fa-fw fa-server'></i>";
                    }
                    elseif($a_data[$i]['komputerPosisi'] == 'TOWER' || $a_data[$i]['komputerPosisi'] == 'RACK MOUNT CHASSIS' ||$a_data[$i]['komputerPosisi'] == 'MAIN SYSTEM CHASSIS')
                    {
                        $s_icon ="<i class='fa fa-fw fa-server'></i>";
                    }
                    elseif($a_data[$i]['komputerPosisi'] == 'MINI TOWER' ||$a_data[$i]['komputerPosisi'] == 'DESKTOP')
                    {
                        $s_icon ="<i class='fa fa-fw fa-desktop'></i>";
                    }
                    elseif($a_data[$i]['komputerPosisi'] == 'NOTEBOOK' ||$a_data[$i]['komputerPosisi'] == 'LAPTOP')
                    {
                        $s_icon ="<i class='fa fa-fw fa-laptop'></i>";
                    }
                    //-------------end icon-------------------

                    //-------------button-------------------
                    $s_button_ba = "";
                    $s_button_user = "";
					$s_button_pengadaan = "";
                    if($a_data[$i]['komputerStatus'] == '3')
                    {

                        $s_button_delete ="<button class='button-cancel-delete btn btn-flat    btn-warning btn-sm '  record-id='{$a_data[$i]['komputerID']}' style='margin-left: 2px;'>Batalkan Penghapusan</button>";
                    }
                    else
                    {
                        if($a_data[$i]['komputerUser'] != "")
                        {
                            $s_button_ba = "<button class='button-ba-penyerahan btn btn-flat   btn-primary btn-sm '  record-id='{$a_data[$i]['komputerID']}'  style='margin-left: 2px;'>BA Penyerahan</button>";
                            $s_button_user ="<button class='button-user-off btn btn-flat   btn-danger btn-sm '  record-id='{$a_data[$i]['komputerID']}' style='margin-left: 2px;'>Pengguna Off</button>";

                        }
                        else
                        {
                            if($oKomputer->ifExistHistorisPemakaian($a_data[$i]['komputerID'])){
                                $s_button_ba = "<button class=' button-ba-pengembalian btn btn-flat   btn-success btn-sm '  record-id='{$a_data[$i]['komputerID']}' style='margin-left: 2px;'>BA Pengembalian</button>";
                            }
                            else
                            {
                                $s_button_user = "";
                            }

                            $s_button_delete ="<button class='button-delete btn btn-flat    btn-danger btn-sm '  record-id='{$a_data[$i]['komputerID']}' style='margin-left: 2px;'>Pengajuan Penghapusan</button>";
                            $s_button_user ="<button class='button-user-on btn btn-flat   btn-success btn-sm '  record-id='{$a_data[$i]['komputerID']}' style='margin-left: 2px;'>Pengguna</button>";
                        }
						
                        if($a_data[$i]['komputerPengadaan'] != "")
                        {
							$s_button_pengadaan =  "<button class='button-pengadaan btn btn-flat    btn-warning btn-sm '  record-id='{$a_data[$i]['komputerID']}' style='margin-left: 2px;'>Ubah Vendor Pengadaan</button>";
						}
						else
						{
							$s_button_pengadaan =  "<button class='button-pengadaan btn btn-flat    btn-danger btn-sm '  record-id='{$a_data[$i]['komputerID']}' style='margin-left: 2px;'> Vendor Pengadaan</button>";
							
						}
                        $s_button_update ="<button class='button-update btn btn-flat    btn-warning btn-sm '  record-id='{$a_data[$i]['komputerID']}' style='margin-left: 2px;'>Ubah Spek</button>";

                    }
                        
                    $s_button = $s_button_pengadaan.$s_button_ba.$s_button_user.$s_button_update.$s_button_delete;
                    //-------------button-------------------s
                    //untuk informasi jumlah soal
                    $s_table_container .="<tr >";
                    $s_table_container .= "<td  align='left'>"
                                            ."<input $s_checked  type='checkbox' class='checkboxes' name='komputerID[]' value='{$a_data[$i]['komputerID']}' />"
                                       ."</td>";
                    $s_table_container .= "<td  align='left'>"
                                             .$s_icon.strtoupper ($a_data[$i]['komputerID'])."<br>"
                                             ."<p class='hostname'>".$a_data[$i]['komputerIdentifikasi']."</p>"
                                        ."</td>";
                    $s_table_container .= "<td  align='left'>"
                                             ."<p class='ipaddress'>".strtoupper ($a_data[$i]['komputerIPUtama'])."</p><br>"
                                        ."</td>";
                    $s_table_container .= "<td  align='left' class='realName'>"
                                             .strtoupper ($a_data[$i]['realName'])
                                        ."</td>";
                    $s_table_container .= "<td  > $s_button</td>";
                    $s_table_container .="</tr>";
               }    
          $s_table_container .="</tbody>";
          $s_table_container .="</table>";
     }



    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div id='manual-help' class='hidden'>$manual_help</div> 
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>KOMPUTER / LAPTOP</h4>
                                </div>
                                <div style='float:right'>$BUTTON_MAIN </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                              {$FORM_MAIN}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oUser->closeDB();
    $oKomputer->closeDB();
    $oGroup->closeDB();
?>