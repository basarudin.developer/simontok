$(document).ready(function() {

			
    /*************************************************
    |   BEGIN BUTTON MAIN
    **************************************************/
    jQuery('#button-back').click(function() 
    {

        document.location='index.php?page=komputer&action=list';
    });

    jQuery('#button-download-scriptvbs').click(function() 
    {
        manual_help = $('#manual-help').html();
        $('#notification-container').hide(); 
        $('#notification-container').removeClass('alert-danger');
        $('#notification-container').addClass('alert-success'); 
        $('#notification-content').html(manual_help);
        $('#notification-container').show(); 
        document.location='index.php?page=audit&type=auditscriptdownload';
    });
    jQuery('#button-manual-entry').click(function() 
    {
        document.location='index.php?page=komputer&action=create';
    });
        

    jQuery('#button-apply-user').click(function() 
    {
        userID =$('#select2-user').val();
        komputerID =  $('#komputer-id').val();
        if( userID == "")
        {
            $('#notification-container').removeClass('alert-success');
            $('#notification-container').addClass('alert-danger'); 
            $('#notification-content').html('pilih pengguna terlebih dahulu');
            $('#notification-container').show(); 
        }
        else
        {
            $.ajax({
                type: 'POST',
                url: "index.php?page=komputer&type=model&action=user_on",
                dataType:"json",
                data: "userID=" +userID+'&komputerID='+komputerID,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();
                            setTimeout(function() {
                                document.location='index.php?page=komputer&action=list'
                            }, 1500);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#container-alert').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        $.Notify({
                            caption: '',
                            content: 'Terjadi kegagalan proses transaksi',
                            keepOpen: true,
                            type: 'alert'
                        });
                    }
            });//end of ajaxs

        }

        $('html, body').animate({
        scrollTop: $('#notification-content').offset().top
        }, 500);
    });

    /*   END BUTTON MAIN
    **************************************************/

    /*************************************************
    |   BEGIN TABLE-COMPUTER
    **************************************************/
    if($('#table-computer').length)
    {


        $('#table-computer tbody').on( 'click', '.button-delete', function () 
        {
            komputerID = $(this).attr('record-id');
            //alert(komputerSpekID);
            keterangan_komputer = $(this).parent().parent().find(".hostname").html() + " " +$(this).parent().parent().find(".ipaddress").html();  
            
           

            $('#modal-content').html('apakah yakin komputer   <b>'+keterangan_komputer+'</b> ?');
            $('#modal-container').modal('show');
            $('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=komputer&type=model&action=delete",
                    dataType:"json",
                    data: 'komputerID='+komputerID,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //salert(msg);
                            $('#modal-container').modal('hide');
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=komputer'
                                }, 1200);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#notification-container').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            $.Notify({
                                caption: '',
                                content: 'Terjadi kegagalan proses transaksi',
                                keepOpen: true,
                                type: 'alert'
                            });
                        }
                });//end of ajax
            });
        });//end table
		
        $('#table-computer tbody').on( 'click', '.button-pengadaan', function () 
        {
            komputerID = $(this).attr('record-id');
            document.location='index.php?page=komputer&action=pengadaan&komputerID='+komputerID;
        });//end table
		
        $('#table-computer tbody').on( 'click', '.button-cancel-delete', function () 
        {
            komputerID = $(this).attr('record-id');
            $.ajax({
            type: 'POST',
            url: "index.php?page=komputer&type=model&action=cancel_delete_computer",
            dataType:"json",
            data: 'komputerID='+komputerID,
            cache:false,
            //jika complete maka
            complete:
                function(data,status)
                {
                 return false;
                },
            success:
                function(msg,status)
                {
                    //alert(msg);
                    $('#modal-container').modal('hide');
                    if(msg.result == 'success')
                    {
                        $('#notification-container').removeClass('alert-danger');
                        $('#notification-container').addClass('alert-success'); 
                        $('#notification-content').html(msg.desc);
                        $('#notification-container').show();
                        setTimeout(function() {
                            document.location='index.php?page=komputer'
                        }, 1200);

                    }
                    else
                    {
                        $('#notification-container').removeClass('alert-success');
                        $('#notification-container').addClass('alert-danger'); 
                        $('#notification-content').html(msg.desc);
                        $('#notification-container').show(); 
                    }
                    $('html, body').animate({
                    scrollTop: $('#notification-container').offset().top
                    }, 500);

                },
            //untuk sementara tidak digunakan
            error:
                function(msg,textStatus, errorThrown)
                {
                    $.Notify({
                        caption: '',
                        content: 'Terjadi kegagalan proses transaksi',
                        keepOpen: true,
                        type: 'alert'
                    });
                }
            });//end of ajax
        });//end table


        $("#button-print-label").click(function() {

            if($('input[name="komputerID[]"]:checked').length > 0)
            {
                var s_data= "";
                $('input[name="komputerID[]"]:checked').each(function (i)
                {
                    if(s_data == "")
                    {
                        s_data = 'komputerID[]=' + $(this).val();
                    }
                    else
                    {
                        s_data = s_data + '&komputerID[]='  +$(this).val();
                    }
                });
                $('#form-main').attr("action","index.php?page=komputer&type=pdf_label&"+s_data);
                $('#form-main').submit();
            }
            else
            {
                $('#notification-container').removeClass('alert-success');
                $('#notification-container').addClass('alert-danger'); 
                $('#notification-content').html("Harap mencentang komputer yang akan dicetak");
                $('#notification-container').show();
            }
        });

        $('#table-computer').find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('tr').addClass(" bg-yellow ");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass(" bg-yellow ");
                }
            });
            if($('input[name="komputerID[]"]:checked').length > 0)
            {
                $("#button-print-label").show();
            }
            else
            {
                $("#button-print-label").hide();
            }
            jQuery.uniform.update(set);
        });

        $('#table-computer').on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass(" bg-yellow ");
            if($('input[name="komputerID[]"]:checked').length > 0)
            {
                $("#button-print-label").show();
            }
            else
            {
                $("#button-print-label").hide();
            }

        });
           
        $('#table-computer').DataTable
        ({        
            rowReorder: true,
            columnDefs: [
                { orderable: true, className: 'reorder', targets:[ 1, 3 ] },
                { orderable: false, targets: '_all' }
            ],
            lengthMenu: [[10, 25, -1], [10, 25, "All"]],
             order: [[ 3, "desc" ]]
        });

        $('#table-computer tbody').on( 'click', '.button-user-on', function () 
        {
            komputerID = $(this).attr('record-id');
            document.location='index.php?page=komputer&action=user&komputerID='+komputerID;
        });

        $('#table-computer tbody').on( 'click', '.button-user-off', function () 
        {
            komputerID = $(this).attr('record-id');
            nama_pengguna = $(this).parent().parent().find(".realName").html();  
            nama_pc = $(this).parent().parent().find(".hostname").html();  
            $('#modal-content').html('apakah yakin menghapus pengguna <b>'+nama_pengguna+'</b> pada komputer '+nama_pc+'?');
            $('#modal-container').modal('show');
            jQuery('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=komputer&type=model&action=user_off",
                    dataType:"json",
                    data: 'komputerID='+komputerID,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=komputer&action=list'
                                }, 1500);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            $.Notify({
                                caption: '',
                                content: 'Terjadi kegagalan proses transaksi',
                                keepOpen: true,
                                type: 'alert'
                            });
                        }
                });//end of ajax
            });
        });

        $('#table-computer tbody').on( 'click', '.button-ba-penyerahan', function () 
        {
            komputerID = $(this).attr('record-id');
            document.location='index.php?page=komputer&type=pdf_ba_penyerahan&komputerID='+komputerID;
        });
        $('#table-computer tbody').on( 'click', '.button-ba-pengembalian', function () 
        {
            komputerID = $(this).attr('record-id');
            document.location='index.php?page=komputer&type=pdf_ba_pengembalian&komputerID='+komputerID;
        });

        $('#table-computer tbody').on( 'click', '.button-update', function () 
        {
            komputerID = $(this).attr('record-id');
            document.location='index.php?page=komputer&action=update&komputerID='+komputerID;
        });

    }
    /*   END TABLE-COMPUTER
    **************************************************/
        

    if($('.select2').length> 0)
    {
        $('.select2').select2();
    }

    if($('[data-mask]').length>0)
    {
        $('[data-mask]').inputmask();
    }
    /*************************************************
    |   BEGIN TABLE-COMPUTER-ITEM
    **************************************************/
    if($('#table-computer-item').length)
    {
        $('#table-computer-item').DataTable
        ({        
            "paging": false,
            "searching": false,
            "ordering": false
        });
        $('#table-computer-item tbody').on( 'click', '.button-delete-item', function () 
        {
            komputerSpekID = $(this).attr('record-id');
            //alert(komputerSpekID);
            nama_item = $(this).parent().parent().find(".namaItem").html();  
            hostname = $('#hostname').val();
            ipaddress= $('#ipaddress').val();
            komputer_id =  $('#komputer-id').val();

            $('#modal-content').html('apakah yakin menghapus  <b>'+nama_item+'</b> ?');
            $('#modal-container').modal('show');
            $('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=komputer&type=model&action=delete_item",
                    dataType:"json",
                    data: 'itemID='+komputerSpekID,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //salert(msg);
                            $('#modal-container').modal('hide');
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=komputer&action=update&komputerID='+komputer_id+'&ipaddress='+ipaddress+'&hostname='+hostname
                                }, 1200);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#notification-container').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            $.Notify({
                                caption: '',
                                content: 'Terjadi kegagalan proses transaksi',
                                keepOpen: true,
                                type: 'alert'
                            });
                        }
                });//end of ajax
            });
        });//end table
        $('#table-computer-item tbody').on( 'click', '.button-cancel-item', function () 
        {
            komputerSpekID = $(this).attr('record-id');
            
            hostname = $('#hostname').val();
            ipaddress= $('#ipaddress').val();
            komputer_id =  $('#komputer-id').val();
            $('#notification-container').hide(); 
            $.ajax({
                type: 'POST',
                url: "index.php?page=komputer&type=model&action=cancel_item",
                dataType:"json",
                data: 'itemID='+komputerSpekID,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //salert(msg);
                        $('#modal-container').modal('hide');
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();
                            setTimeout(function() {
                                document.location='index.php?page=komputer&action=update&komputerID='+komputer_id+'&ipaddress='+ipaddress+'&hostname='+hostname
                            }, 1200);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#notification-container').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html('terjadi kesalahan sistem');
                            $('#notification-container').show(); 
                    }
            });//end of ajax
        });//end table
		
		
		
        $('#button-update').click(function() 
        {
            hostname = $('#hostname').val();
            ipaddress= $('#ipaddress').val();
            komputer_id =  $('#komputer-id').val();
            s_error = "";
            if(komputer_id == "")
            {
                s_error = s_error + "komputer tidak dikenali<br/>";
            }
            if(hostname == "")
            {
                s_error = s_error + "hostname harus diisi<br/>";
            }
            if(ipaddress == "")
            {
                s_error = s_error + "ip address harus diisi<br/>";
            }
            if( s_error != "")
            {
                $('#notification-container').removeClass('alert-success');
                $('#notification-container').addClass('alert-danger'); 
                $('#notification-content').html(s_error);
                $('#notification-container').show(); 

                $('html, body').animate({
                scrollTop: $('#notification-container').offset().top
                }, 500);
            }
            else
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=komputer&type=model&action=update",
                    dataType:"json",
                    data: 'komputerID='+komputer_id+'&hostname='+hostname+'&ipaddress='+ipaddress,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=komputer&action=update&komputerID='+komputer_id
                                }, 1000);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#notification-container').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            $.Notify({
                                caption: '',
                                content: 'Terjadi kegagalan proses transaksi',
                                keepOpen: true,
                                type: 'alert'
                            });
                        }
                });//end of ajax

            }

        });

    }
    /*   END TABLE-COMPUTER-ITEM
    **************************************************/

    /*************************************************
    |   BEGIN FORM MANUAL CREATE
    **************************************************/

    if($('#form-computer-create').length)
    {
        $('#button-create').click(function() 
        {
            $.ajax({
                type: 'POST',
                url: "index.php?page=komputer&type=model&action=add",
                dataType:"json",
                data: $("#form-computer-create").serialize(),
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        

                        //alert(msg);
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();
                            setTimeout(function() {
                                document.location='index.php?page=komputer&action=user&komputerID='+msg.komputerID
                            }, 1000);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#notification-container').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        $.Notify({
                            caption: '',
                            content: 'Terjadi kegagalan proses transaksi',
                            keepOpen: true,
                            type: 'alert'
                        });
                    }
            });//end of ajax

        });
    }
    /*   END FORM MANUAL CREATE
    **************************************************/
    
    /*************************************************
    |   BEGIN FORM MANUAL CREATE
    **************************************************/

    if($('#button-pengadaan-apply').length)
    {
	
		komputer_id =  $('#komputer-id').val();		
        $('#button-pengadaan-apply').click(function() 
        {
			
			var selected_val = "";
			var selected = $("input[type='radio'][name='options_pengadaan']:checked");
			if (selected.length > 0) 
			{
				selected_val = selected.val();
			}
			$.ajax({
                type: 'POST',
                url: "index.php?page=komputer&type=model&action=pengadaan",
                dataType:"json",
				data: 'komputerID='+komputer_id+'&vendor='+selected_val,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        

                        //alert(msg);
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();
                            setTimeout(function() {
                                document.location='index.php?page=komputer&action=list'
                            }, 1000);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#notification-container').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        $.Notify({
                            caption: '',
                            content: 'Terjadi kegagalan proses transaksi',
                            keepOpen: true,
                            type: 'alert'
                        });
                    }
            });//end of ajax
		});
	}
    /*   END FORM MANUAL CREATE
    **************************************************/

});