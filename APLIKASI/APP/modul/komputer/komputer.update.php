<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer update
    |--------------------------------------------------------------------------
    |Update jenis komputer
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUser = new UserInfo();
    $oKomputer = new Computer();

    $LAYOUT_JS_EXTENDED .= "
                    <!-- DataTables -->
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>
                    <!-- InputMask -->
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.date.extensions.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.extensions.js'></script>
                    <script src='modul/komputer/komputer.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                        <link rel='stylesheet' href='assets/css/adminlte.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-update' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus-square-o'></i> Terapkan
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> Kembali
                        </button>
                    ";
    $s_form_input = "";
    $s_table_container ="";
    $s_hostname = "";
    $s_ip = "";

    //---table
    $a_title  = array();
    $a_title[] = "ITEM";
    $a_title[] = "MERK";
    $a_title[] = "KAPASITAS";
    $a_title[] = "KETERANGAN";
    $a_title[] = "";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " style='width:250px;' ";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = "  ";
    $a_title_class[] = " style='width:50px;' ";
    if(isset($_REQUEST['komputerID']))
    {
     
        $_REQUEST['komputerID'] =  $oKomputer->antiInjection($_REQUEST['komputerID']); 
        $a_data = $oKomputer->getList("WHERE komputerID = '{$_REQUEST['komputerID']}'","","");

        $a_data_item = $oKomputer->getSpek($_REQUEST['komputerID']);
        if(isset($a_data))
        {
            if(isset($_REQUEST['hostname']))
            {
                $s_hostname = $_REQUEST['hostname'];
            }
            else
            {
                $s_hostname = $a_data[0]['komputerIdentifikasi'];
            }
            if(isset($_REQUEST['ipaddress']))
            {
                $s_ip = $_REQUEST['ipaddress'];
            }
            else
            {
                $s_ip = $a_data[0]['komputerIPUtama'];
            }
            $s_form_input = "
                            <div >
                                <input type='hidden' id='komputer-id' value='{$_REQUEST['komputerID']}'>
                                <!-- /.box-header -->
                                <div class='box-body'>
                                    <div class='row'>
                                        <div class='col-md-12'>
                                            <!-- KomputerID -->
                                            <div class='form-group'>
                                                <label>Komputer ID:</label>
                                                <input type='text' class='form-control'  value='{$a_data[0]['komputerID']}' disabled>
                                                <input type='hidden'   id='komputer-id'  value='{$a_data[0]['komputerID']}'>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                        </div>
                                        <div class='col-md-6'>
                                                <!-- Hostname -->
                                                <div class='form-group'>
                                                    <label>Hostname:</label>
                                                    <div class='input-group'>
                                                        <div class='input-group-addon'>
                                                            <i class='fa fa-text-width'></i>
                                                        </div>
                                                        <input type='text' class='form-control'  value='{$s_hostname}' id='hostname'>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                        <div class='col-md-6'>
                                                <!-- IP mask -->
                                                <div class='form-group'>
                                                    <label>IP mask:</label>
                                                    <div class='input-group'>
                                                        <div class='input-group-addon'>
                                                            <i class='fa fa-laptop'></i>
                                                        </div>
                                                        <input type='text' class='form-control inputmask' data-inputmask=\"'alias': 'ip'\" data-mask  value='{$s_ip}' id='ipaddress'>
                                                     </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                        <div class='box-body'>





                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                            </div>";



            if(isset($a_data_item))
            {

                $s_table_container ="";
                $s_table_container .="
                                        <table  id='table-computer-item' class='table  table-bordered ' width='100%'  border='1px'>
                                            <thead>
                                                <tr >";


                for($i=0;$i<count($a_title);$i++)
                {   
                    $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                }
                $s_table_container .="          </tr>
                                            </thead>";
                $s_table_container .="      <tbody>";
                for($i=0;$i<count($a_data_item);$i++)
                {
                    if( ($a_data_item[$i]['komputerItemNama'] == "LANCARD") && (substr_count($a_data_item[$i]['komputerSpekSerialNumber'], ':') >=5 ))
                    {
                        $s_button = "";    
                    }
                    else
                    {
                        $s_button = "<button class='button-delete-item btn btn-flat    btn-danger btn-sm '  record-id='{$a_data_item[$i]['komputerSpekID']}'>Hapus</button>";
                    }
                    $s_delete = "";
                    if($a_data_item[$i]['komputerSpekStatus'] == "0")
                    {
                        $s_delete = " class='table-danger' ";

                        $s_button = "<button class='button-cancel-item btn btn-flat    btn-warning btn-sm '  record-id='{$a_data_item[$i]['komputerSpekID']}'>Batalkan</button>";
                        $a_data_item[$i]['komputerSpekSerialNumber'] = "<span  class='badge bg-red'>menunggu persetujuan SPV TI untuk dihapus</span>" ;
                    }
                    //untuk informasi jumlah soal
                    $s_table_container  .="      <tr {$s_delete}>";
                    $s_table_container  .= "         <td  align='left' class='namaItem'>"
                                        .strtoupper ($a_data_item[$i]['komputerItemNama'])
                                        ."</td>";
                    $s_table_container  .= "<td  align='left'>"
                                        .strtoupper ($a_data_item[$i]['komputerSpekMerk'])
                                        ."</td>";
                    $s_table_container  .= "<td  align='right'>"
                                        .strtoupper ($a_data_item[$i]['komputerSpekKapasitas'])
                                        ."</td>";
                    $s_table_container  .= "<td  align='left'>"
                                        . $a_data_item[$i]['komputerSpekSerialNumber']
                                        ."</td>";
                    $s_table_container  .= "<td  > $s_button</td>";
                    $s_table_container  .="</tr>";
                }    
                $s_table_container .="</tbody>";
                $s_table_container .="</table>";
            }

        }
        else
        {
            $s_form_input ="komputer tidak dikenali";
        }

    }
    else
    {
        $s_form_input ="komputer tidak terdefinisi";    
    } 
            
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>UPDATE KOMPUTER / LAPTOP</h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                                {$s_form_input}

                                {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oUser->closeDB();
    $oKomputer->closeDB();
?>