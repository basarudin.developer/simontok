<?php
/**
 *
 * @version         1.0
 * @author          basarudin
 * @created     17 November 2018
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   spenulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/

    $PAGE_ID = "KOM009";
    require($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    $oUser = new UserInfo();
    $oKomputer = new Computer();
    $oGroup = new Group();
 
    //---table
    $a_title  = array();
    $a_title[] = "HOSTNAME";
    $a_title[] = "IP ADDRESS";
    $a_title[] = "PENGGUNA";
    $a_title[] = "ITEM";
    $a_title[] = "KETERANGAN ITEM";
    $a_title[] = "";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " style='width:100px;' ";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:50px;' ";

    $s_table_container = "";
    $s_condition = " WHERE komputerSpekStatus='0' AND helpdeskGroupHandle = '{$USER[0]['wilayahPenempatan']}' ";
    $s_limit = "  ";
    $s_order = " ";

    $FORM_MAIN = "<form id='form-main' action='' target='' method='post'></form>";

    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/komputer/komputer.delete.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";


     $a_data_item = $oKomputer->getListSpekUpper($s_condition, $s_order, $s_limit);
     if(isset($a_data_item))
     {
          
          $s_table_container ="";
          $s_table_container .="
                    <table  id='table-item-delete' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
          $s_table_container .="</tr>
                         </thead>";
          $s_table_container .="<tbody>";



               for($i=0;$i<count($a_data_item);$i++)
               {

                  

                    $s_button_delete_approval ="<button class='button-delete btn btn-flat    btn-danger btn-sm '  record-id='{$a_data_item[$i]['komputerSpekID']}' style='margin-left: 2px;'>setujui penghapusan item</button>";
                    $s_button = $s_button_delete_approval;
                    //-------------button-------------------s
                    //untuk informasi jumlah soal

                    $a_data_item[$i]['komputerSpekMerk'] = str_replace("\n", "", $a_data_item[$i]['komputerSpekMerk']);
                    $s_table_container .="<tr >";
                    $s_table_container .= "<td  align='left'>"
                                             .strtoupper ($a_data_item[$i]['komputerID'])."<br>"
                                             ."<p class='hostname'>".$a_data_item[$i]['komputerIdentifikasi']."</p>"
                                        ."</td>";
                    $s_table_container .= "<td  align='left' >"
                                             .strtoupper ($a_data_item[$i]['komputerIPUtama'])."<br>"
                                        ."</td>";
                    $s_table_container .= "<td  align='left' class='realName'>"
                                             .strtoupper ($a_data_item[$i]['realName'])
                                        ."</td>";
                    $s_table_container .= "<td  align='left' class='item-name' >"
                                             .strtoupper ($a_data_item[$i]['komputerItemNama'])
                                        ."</td>";
                    $s_table_container .= "<td  align='left' class='item-desc'>"
                                             .strtoupper ($a_data_item[$i]['komputerSpekMerk']." ".$a_data_item[$i]['komputerSpekKeterangan'])
                                        ."</td>";
                    $s_table_container .= "<td  > $s_button</td>";
                    $s_table_container .="</tr>";
               }    
          $s_table_container .="</tbody>";
          $s_table_container .="</table>";
     }


    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>PERSETUJUAN PENGHAPUSAN ITEM KOMPONEN KOMPUTER</h4>
                                </div>
                                <div style='float:right'></div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                              {$FORM_MAIN}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oUser->closeDB();
    $oKomputer->closeDB();
    $oGroup->closeDB();
?>