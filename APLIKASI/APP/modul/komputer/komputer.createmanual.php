<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer Create Manual
    |--------------------------------------------------------------------------
    |Form untuk entry komputer secara manual entry
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUser = new UserInfo();
    $oKomputer = new Computer();

    $a_os[] = 'WINDOWS 10';
    $a_os[] = 'WINDOWS 8';
    $a_os[] = 'WINDOWS 7';
    $a_os[] = 'WINDOWS XP';
    $a_os[] = 'LINUX';
    $LAYOUT_JS_EXTENDED .= "

                    <!-- InputMask -->
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.date.extensions.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.extensions.js'></script>
                    <script src='assets/bower_components/select2/dist/js/select2.full.min.js'></script>
                    <script src='modul/komputer/komputer.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/bower_components/select2/dist/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/adminlte.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-create' class='btn btn-flat  btn-sm btn-primary pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus-square-o'></i> Tambah
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> kembali
                        </button>
                    ";
    $s_form_input = "";

    $s_select2_os = "<select class='form-control select2' name='os'   >";
                $s_select2_os .= "<option value=''>Silahkan Pilih Sistm Operasi</option>";
                for($i=0;$i < count($a_os);$i++)
                {
                    $s_select2_os .= "<option value='{$a_os[$i]}'>{$a_os[$i]}</option>";
                }
                $s_select2_os .= "</select>";
    $s_form_input = "
                    <form id='form-computer-create' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                <div class='row'>
                                    <div class='col-md-6'>
                                            <!-- Hostname -->
                                            <div class='form-group'>
                                                <label>Hostname:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-text-width'></i>
                                                    </div>
                                                    <input type='text' class='form-control' id='hostname' name='hostname'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-6'>
                                            <!-- Kategori -->
                                            <div class='form-group'>
                                                <label>Kategori:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa  fa-clone'></i>
                                                    </div>
                                                    <select class='form-control select2' name='kategori' >
                                                        <option value=''>Silahkan Pilih Kategori</option>
                                                        <option value='DESKTOP'>PC Client</option>
                                                        <option value='LAPTOP'>Laptop</option>
                                                        <option value='SERVER'>Server</option>
                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-6'>
                                            <!-- Mac Address -->
                                            <div class='form-group'>
                                                <label>Mac Address:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-laptop'></i>
                                                    </div>
                                                    <input type='text' class='form-control inputmask' data-inputmask=\"'alias': 'mac'\" data-mask  name='macaddress'>
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-6'>
                                            <!-- IP mask -->
                                            <div class='form-group'>
                                                <label>IP mask:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-laptop'></i>
                                                    </div>
                                                    <input type='text' class='form-control inputmask' data-inputmask=\"'alias': 'ip'\" data-mask  value='' name='ipaddress'>
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='box-body'>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-md-6'>
                                            <!-- Sistem Operasi -->
                                            <div class='form-group'>
                                                <label>Sistem Operasi:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-windows'></i>
                                                    </div>
                                                    $s_select2_os
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-6'>
                                            <!-- Prosesor -->
                                            <div class='form-group'>
                                                <label>Kecepatan Prosesor:</label>
                                                <div class='input-group'>
                                                    <input type='text' class='form-control' name='prosesor'>
                                                    <span class='input-group-addon'>GB</span>
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-6'>
                                            <!-- Memori -->
                                            <div class='form-group'>
                                                <label>Total Kapasitas Memori:</label>
                                                <div class='input-group'>
                                                    <input type='text' class='form-control' name='memori'>
                                                    <span class='input-group-addon'>GB</span>
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-6'>
                                            <!-- Memori -->
                                            <div class='form-group'>
                                                <label>Total Kapasitas Harddisk:</label>
                                                <div class='input-group'>
                                                    <input type='text' class='form-control' name='harddisk'>
                                                    <span class='input-group-addon'>GB</span>
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-6'>
                                            <!-- Monitor -->
                                            <div class='form-group'>
                                                <label>Ukuran Monitor:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-television'></i>
                                                    </div>
                                                    <input type='text' class='form-control' name='monitor'>
                                                    <span class='input-group-addon'>inch</span>

                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
                        
            
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>ENTRY MANUAL KOMPUTER / LAPTOP</h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oUser->closeDB();
    $oKomputer->closeDB();
?>