<?php
    /*
    |--------------------------------------------------------------------------
    | Berita Acara Penyerahan komputer kepada pegawai
    |--------------------------------------------------------------------------
    |modul cetak pdf berita acara penyerahan komputer kepada pegawai
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.date.php");
    require_once($SYSTEM['DIR_PATH']."/class/fpdf/class.fpdf.php");
    require_once($SYSTEM['DIR_PATH']."/class/fpdf/class.qrcode.php");

    $oKomputer = new Computer();
    $oWilayah = new Wilayah();

    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "";

    $i_margin_height_header = 4;
    $i_margin_height_body = 5;
    $i_margin_height_table = 4;
    //garis debugger   
    $b_debug = 0;
    if($b_debug == 1)
    {
        $i_border = 1;
    }
    else
    {
        $i_border = 0;
    }

    $a_komputer_item['APLIKASI'] = 'KMITAPT0000000000001';
    $a_komputer_item['CAM'] = 'KMITCAM0000000000001';
    $a_komputer_item['SPEAKER'] = 'KMITDPK0000000000001';
    $a_komputer_item['CD_ROM'] = 'KMITCDR0000000000001';
    $a_komputer_item['DVD_ROM'] = 'KMITDVR0000000000001';
    $a_komputer_item['FLOPPY'] = 'KMITFLP0000000000001';
    $a_komputer_item['HARDDISK'] = 'KMITHDK0000000000001';
    $a_komputer_item['KEYBOARD'] = 'KMITKYB0000000000001';
    $a_komputer_item['LANCARD'] = 'KMITLNC0000000000001';
    $a_komputer_item['MEMORY'] = 'KMITMEM0000000000001';
    $a_komputer_item['MONITOR'] = 'KMITMNT0000000000001';
    $a_komputer_item['MOTHERBOARD'] = 'KMITMTH0000000000001';
    $a_komputer_item['MOUSE'] = 'KMITMUS0000000000001';
    $a_komputer_item['SISTEM_OPERASI'] = 'KMITOS00000000000001';
    $a_komputer_item['PROCESSOR'] = 'KMITPRC0000000000001';
    $a_komputer_item['PRINTER'] = 'KMITPRN0000000000001';
    $a_komputer_item['SCANNER'] = 'KMITSCN0000000000001';


    $a_no_satuan[] = $a_komputer_item['APLIKASI'];
    $a_no_satuan[] = $a_komputer_item['SISTEM_OPERASI'];

    $a_satuan_biji[] = $a_komputer_item['CAM'];
    $a_satuan_biji[] = $a_komputer_item['SPEAKER'];
    $a_satuan_biji[] = $a_komputer_item['CD_ROM'];
    $a_satuan_biji[] = $a_komputer_item['DVD_ROM'];
    $a_satuan_biji[] = $a_komputer_item['FLOPPY'];
    $a_satuan_biji[] = $a_komputer_item['KEYBOARD'];
    $a_satuan_biji[] = $a_komputer_item['MONITOR'];
    $a_satuan_biji[] = $a_komputer_item['MOTHERBOARD'];
    $a_satuan_biji[] = $a_komputer_item['MOUSE'];
    $a_satuan_biji[] = $a_komputer_item['PRINTER'];
    $a_satuan_biji[] = $a_komputer_item['SCANNER'];


    $a_satuan_mb[] = $a_komputer_item['HARDDISK'];
    $a_satuan_mb[] = $a_komputer_item['LANCARD'];
    $a_satuan_mb[] = $a_komputer_item['MEMORY'];
    $a_satuan_mb[] = $a_komputer_item['PROCESSOR'];

    //jika idnya sudah diset
    if(isset($_REQUEST['komputerID']))
    {

        $a_komputer_detail = $oKomputer->peminjamTerakhir($_REQUEST['komputerID']);
        if(count($a_komputer_detail)>0)
        {
            $a_item = $oKomputer->getSpek($a_komputer_detail[0]['komputerID']);

			$a_wilayah = $oWilayah->getList("WHERE child.`idWilayah` = '".$a_komputer_detail[0]['helpdeskGroupHandle']."'","","");
            $oPDF=new FPDF();
            $oPDF->AddPage();
            //Logo
            $oPDF->Image('assets/img/pln-medium.jpg',11,10,9,12);
            //Arial bold 15
            $oPDF->SetFont('Arial','B',25);
            //lebar, tinggi ,text , border , ln,align
            $oPDF->Cell(15);
            $oPDF->Cell(52,8,'PT. PLN (Persero)',$i_border,1,'L');
            //Line break
            $oPDF->Ln(1);
            //Arial bold 15
            $oPDF->SetFont('Arial','B',10);
            //lebar, tinggi ,text , border , ln,align
            $oPDF->Cell(15);
            $oPDF->Cell(12,$i_margin_height_header,$a_wilayah[0]['namaWilayah'],$i_border,0,'L');
            //Line break
            $oPDF->Ln(1);

            //membuat garis
            $oPDF->Line(10, 30, 200, 30);
            $oPDF->Line(10, 30.5, 200, 30.5);
            $oPDF->Ln(0);

            $oPDF->setXY(10,35);
            $oPDF->SetFont('Arial','',14);
            $oPDF->Cell(190,$i_margin_height_header,'BERITA ACARA SERAH TERIMA PENGGUNAAN KOMPUTER',$i_border,1,'C');

            $oPDF->SetFont('Arial','',10);

            $oPDF->setXY(10,45);
            $oPDF->Cell(50,$i_margin_height_body,'Nomor Komputer',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->SetFont('Arial','B',14);
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerID'],$i_border,1,'L');

            $oPDF->SetFont('Arial','',10);
            $oPDF->Cell(50,$i_margin_height_body,'Hostname',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerIdentifikasi'],$i_border,1,'L');

            $oPDF->Cell(50,$i_margin_height_body,'Pengadaan',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerPengadaan'],$i_border,1,'L');
            $oPDF->Cell(50,$i_margin_height_body,'Nama Pengguna',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['realName'],$i_border,1,'L');

            $oPDF->Cell(50,$i_margin_height_body,'Jenis',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerPosisi'],$i_border,1,'L');
            $oPDF->Cell(50,$i_margin_height_body,'IP',$i_border,0,'L');
            $oPDF->Cell(5,$i_margin_height_body,':',$i_border,0,'L');
            $oPDF->Cell(100,$i_margin_height_body,$a_komputer_detail[0]['komputerIPUtama'],$i_border,1,'L');
            $oPDF->Ln();

            $qrcode = new QRcode($a_komputer_detail[0]['komputerID'],'H');
            $qrcode->displayFPDF($oPDF,170 ,45 ,30);

           
           
            $oPDF->Ln();
            if(count($a_item)>0){
                
                $oPDF->SetFont('Arial','',10);
                $oPDF->Cell(30,$i_margin_height_table,"ITEM",1,0,'C');
                $oPDF->Cell(30,$i_margin_height_table,"MERK",1,0,'C');
                $oPDF->Cell(25,$i_margin_height_table,"KAPASITAS",1,0,'C');
                $oPDF->Cell(105,$i_margin_height_table,"Keterangan",1,0,'C');
                $oPDF->Ln();
                
                $oPDF->SetFont('Arial','',8);
                for($i=0;$i<count($a_item);$i++){
                     
                     
                    $s_satuan = "";
                    if(in_array($a_item[$i]['komputerItemID'], $a_no_satuan))
                    {
                        $s_satuan = "-";
                    }
                    elseif(in_array($a_item[$i]['komputerItemID'], $a_satuan_biji))
                    {
                        $s_satuan = "1 biji";
                    }
                    elseif(in_array($a_item[$i]['komputerItemID'], $a_satuan_mb))
                    {
                        $s_satuan = number_format($a_item[$i]['komputerSpekKapasitas'])." (MB)";
                    }
                    else
                    {
                        $s_satuan = "";
                    }
                    $oPDF->Cell(30,$i_margin_height_table,$a_item[$i]['komputerItemNama'],1,0,'L');
                    $oPDF->Cell(30,$i_margin_height_table,substr($a_item[$i]['komputerSpekMerk'],0,20),1,0,'L');
                    $oPDF->Cell(25,$i_margin_height_table,$s_satuan,1,0,'R');
                    $oPDF->Cell(105,$i_margin_height_table,substr($a_item[$i]['komputerSpekKeterangan'],0,60),1,0,'L');
                    $oPDF->Ln();
                }
            }

           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->MultiCell(190,$i_margin_height_body,"Pengguna komputer ".$a_komputer_detail[0]['realName']." bertanggujawab terhadap komputer dengan spesifikasi diatas  pertanggal ".formatTanggalPanjang($a_komputer_detail[0]['komputerHistorisTgl']),$i_border);

           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Cell(190,$i_margin_height_body,"Surabaya, ".formatTanggalPanjang(date('Y-m-d')),$i_border,0,'L');
           $oPDF->Ln();
           $oPDF->Cell(120,$i_margin_height_body,"Pengguna, ",$i_border,0,'L');
           $oPDF->Cell(60,$i_margin_height_body,"SPV TI, ",$i_border,0,'L');
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Cell(120,$i_margin_height_body,"( ".$a_komputer_detail[0]['realName']." )",$i_border,0,'L');
           $oPDF->Cell(60,$i_margin_height_body,"(_____________________) ",$i_border,0,'L');
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Cell(60,$i_margin_height_body,"",$i_border,0,'L');
           $oPDF->Cell(120,$i_margin_height_body,"Mengetahui, ",$i_border,0,'L');
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Ln();
           $oPDF->Cell(60,$i_margin_height_body,"",0,0,'L');
           $oPDF->Cell(120,$i_margin_height_body,"(_____________________) ",0,0,'L');
           $oPDF->Output("BA PENGEMBALIAN".$_GET['komputerID'].".pdf","D");
        }
    }

    $oKomputer->closeDB();
    $oWilayah->closeDB();

?>