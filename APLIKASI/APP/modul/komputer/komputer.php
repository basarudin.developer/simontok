<?php
    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    |Controler  modul user
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    if(isset($_REQUEST['type']))
    {
        //direk json
        if ($_REQUEST['type'] == "model") 
        {
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.model.php");
        } 
        //direk pdf
        elseif ($_REQUEST['type'] == "pdf_ba_penyerahan") 
        {
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.ba.penyerahan.php");
        }
        //direk pdf
        elseif ($_REQUEST['type'] == "cetak_pdf") 
        {
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.cetak.pdf.php");
        }
        //direk pdf
        elseif ($_REQUEST['type'] == "pdf_ba_pengembalian") 
        {
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.ba.pengembalian.php");
        }
        //direk pdf
        elseif ($_REQUEST['type'] == "pdf_label") 
        {
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.label.php");
        }
        //selain direk langsung
        elseif($_REQUEST['type'] != "")
        { 
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.view.php");
            include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.view.php");
        include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
    }

?>