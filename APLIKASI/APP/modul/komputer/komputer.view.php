<?php
    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    |view  modul user
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    
    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "list") 
        {
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.list.php");
        } 
        if ($_REQUEST['action'] == "update") 
        {
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.update.php");
        } 
        if ($_REQUEST['action'] == "pengadaan") 
        {
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.pengadaan.php");
        } 
        elseif($_REQUEST['action'] == "user")
        { 
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.user.php");
        }
        elseif($_REQUEST['action'] == "create")
        { 
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.createmanual.php");
        }
        elseif($_REQUEST['action'] == "delete-item")
        { 
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.item.delete.approval.php");
        }
        elseif($_REQUEST['action'] == "delete-confirm")
        { 
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.delete.approval.php");
        }
        elseif($_REQUEST['action'] != "")
        { 
            require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.list.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/komputer/komputer.list.php");
    }

?>