<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer User
    |--------------------------------------------------------------------------
    |Mengkaitkan komputer dengan user
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUser = new UserInfo();
    $oKomputer = new Computer();

    $LAYOUT_JS_EXTENDED .= "

                    <script src='assets/bower_components/select2/dist/js/select2.full.min.js'></script>
                    <script src='modul/komputer/komputer.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/bower_components/select2/dist/css/select2.min.css'>
        <link rel='stylesheet' href='assets/css/adminlte.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-apply-user' class='btn btn-flat  btn-sm btn-primary pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus-square-o'></i> Terapkan
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> kembali
                        </button>
                    ";
    $s_form_input = "";
    if(isset($_REQUEST['komputerID']))
    {
     
        $_REQUEST['komputerID'] =  $oKomputer->antiInjection($_REQUEST['komputerID']); 
        $a_data = $oKomputer->getList("WHERE komputerID = '{$_REQUEST['komputerID']}'","","");
        if(isset($a_data))
        {
            $s_where_pegawai = "";
            $a_data_user = $oUser->getUserList($s_where_pegawai,"","");
            if(isset($a_data_user))
            {
                $s_select2 = "<select class='form-control select2' id='select2-user'  >";
                $s_select2 .= "<option value=''>Silahkan Pilih Pengguna</option>";  
                foreach ($a_data_user as $key => $value) 
                {
                    $s_select2 .= "<option value='{$value['userID']}'>{$value['userID']} <br />{$value['realName']}</option>"; 
                }
                $s_select2 .= "</select";
            }
            $s_form_input = "
                            <div >
                                <input type='hidden' id='komputer-id' value='{$_REQUEST['komputerID']}'>
                                <!-- /.box-header -->
                                <div class='box-body'>
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <div class='col-sm-4'>
                                                    ID Komputer
                                                </div>
                                                <div class='col-sm-8'>
                                                    <b>{$a_data[0]['komputerID']}</b>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <div class='col-sm-4'>
                                                    Hostname
                                                </div>
                                                <div class='col-sm-8'>
                                                    {$a_data[0]['komputerIdentifikasi']}
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <div class='col-sm-4'>
                                                    IP Address
                                                </div>
                                                <div class='col-sm-8'>
                                                    {$a_data[0]['komputerIPUtama']}
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class='col-md-6'>
                                            <div class='form-group'>
                                                <div class='col-sm-4'>
                                                    Tanggal Audit
                                                </div>
                                                <div class='col-sm-8'>
                                                    {$a_data[0]['komputerAuditEntry']}
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        
                                    </div>
                                    <!-- /.row -->
                                    <div class='row'>
                                        <br />
                                        <!-- /.col -->
                                        <div class='col-md-12'>
                                            <div class='form-group'>
                                                <div class='col-sm-2' style='padding-top: 7px;'>
                                                    Pengguna
                                                </div>
                                                <div class='col-sm-10'>
                                                    $s_select2
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                            </div>";

        }
        else
        {
            $s_form_input ="komputer tidak dikenali";
        }

    }
    else
    {
        $s_form_input ="komputer tidak terdefinisi";    
    } 
            
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>PENGGUNA KOMPUTER / LAPTOP</h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oUser->closeDB();
    $oKomputer->closeDB();
?>