<?php
/**
 *
 * @version		1.0
 * @author 		basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/


    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/meta.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/css.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/js.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/sidebar.php");
    require_once($SYSTEM['DIR_MODUL_LAYOUT']."/footer.php");

    $LAYOUT_CSS  .= $LAYOUT_CSS_CONTENT;
    $LAYOUT_JS  .= $LAYOUT_JS_CONTENT;

    $LAYOUT_CSS  .= $LAYOUT_CSS_EXTENDED;
    $LAYOUT_JS  .= $LAYOUT_JS_EXTENDED;
?>
<?php
	$CONTENTS = "<!DOCTYPE html>

				<html lang='en'>

				    <!-- BEGIN HEAD -->
				    <head>
";
	$CONTENTS .= $LAYOUT_META;
	$CONTENTS .= $LAYOUT_CSS;
	$CONTENTS .= "
				    </head>

				    <body>

				        <div class=' {$SYSTEM['SKIN']}page-wrapper  toggled'>
				            <button type='button' id='show-sidebar' class='btn btn-sm ' href='#'>
				                <i class='fa fa-bars'></i>
				            </button>

";				            
	$CONTENTS .= $LAYOUT_SIDEBAR;

	$CONTENTS .= "
				            <!-- BEGIN CONTENT CONTAINER -->
";				            
	$NOTIFICATION_CONTAINER = "<div id='notification-container' class='alert alert-success alert-dismissible ' style='display: none;'>
				                <p id='notification-content'>
				                </p>
				              </div>"

	;

	$MODAL_CONTAINER = "
     <div class='modal fade' id='modal-container' style='display: none;'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                    <div class='modal-header'>

                        <div style='float:left'>
                            <h4>Konfirmasi</h4>
                        </div>
                        <div style='float:right'>
	                        <button type='button' id='button-modal-ok' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
	                            <i class='  fa fa-check'></i> Lanjut
	                        </button>
	                        <button type='button' class=' btn btn-flat  btn-sm btn-danger pull-right' data-dismiss='modal' aria-label='Close' style='margin-left: 5px;'>
	                            <i class='fa fa-undo'></i> Batal
	                        </button>
                        </div>
                    </div>
                    <div class='modal-body'>
		                <p id='modal-content'></p>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        <!-- /.modal-dialog -->
        </div>
	";
	$CONTENTS .= 
					"
                		<section class='page-content'>	
                		$MODAL_CONTAINER
						$NOTIFICATION_CONTAINER
						$CONTENT_MAIN
						</section>";	

	$CONTENTS .= "
				            <!-- END CONTENT CONTAINER -->
				        </div>
";				            
				        
	$CONTENTS .= $LAYOUT_JS;
	$CONTENTS .= "

				    </body>
				</html>";
?>