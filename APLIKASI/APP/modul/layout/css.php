<?php
$LAYOUT_CSS = "
        

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- Bootstrap 3.3.7 -->
        <link rel='stylesheet' href='assets/bower_components/bootstrap/dist/css/bootstrap.css'>
        <!-- Font Awesome -->
        <link rel='stylesheet' href='assets/bower_components/font-awesome/css/font-awesome.min.css'>
        
        <!-- SIDEBAR STYLE -->
        <link rel='stylesheet' href='assets/css/jquery.mCustomScrollbar.min.css' />
        <link rel='stylesheet' href='assets/css/custom.css'>
        <link rel='stylesheet' href='assets/css/adminlte.css'>

        <link rel='stylesheet' href='assets/css/skins/skin-dark.css'>
        <!-- END GLOBAL MANDATORY STYLES -->

        
";
?>