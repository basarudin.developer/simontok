<?php
$real_name = "";
$group_name = "";
$s_menu_container = "";
if(isset($USER[0]['realName']))
{
    $real_name =$USER[0]['realName']; 

    include_once($SYSTEM['DIR_PATH']."/class/class.menu.php");
    $oMenu = new MenuLinks();
    $s_menu_container = $oMenu->buildMenu($USER[0]['userID']);
    $oMenu->closeDB();


    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");

    $oGroup = new Group();
    $a_group_name = $oGroup->getListUserGroup(" WHERE A.userID='{$USER[0]['userID']}' AND A.groupID != 'GRP00000000000000000' ","","");
    if(count($a_group_name)>0)
    {
        $group_name = $a_group_name[0]['name'];
    }
    $oGroup->closeDB();
}


$LAYOUT_SIDEBAR = "  

            <!-- BEGIN SIDEBAR NAVIGATION MENU -->
            <section id='sidebar' class='sidebar-wrapper'>
                
                <div class='sidebar-content'>
                    <div class='sidebar-brand' id='close-sidebar'>
                        <a href='#'>{$SYSTEM['APP_NAME']}</a>
                        <div >
                            <i class='fa     fa-angle-double-left'></i>
                        </div>
                    </div>
                    <div class='sidebar-header'>
                        <div class='user-pic'>
                            <img class='img-responsive img-rounded' src='assets/img/user.jpg' alt='User picture'>
                        </div>
                        <div class='user-info'>
                            <span class='user-name'>
                                <strong>{$real_name}</strong>
                            </span> 
                            <span class='user-role'>{$group_name}</span>
                            <span class='user-status'>
                                <button type='button' class=' btn btn-block btn-danger btn-xs' id='btn-logout'>Logout</button>
                            </span>
                        </div>
                    </div>
                    <!-- sidebar-header  -->
                    {$s_menu_container}
                </div>
                <!-- sidebar-content  -->
            </section>
            <!-- END SIDEBAR NAVIGATION MENU -->

  ";
?>