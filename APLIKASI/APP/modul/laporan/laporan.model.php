<?php
    /*
    |--------------------------------------------------------------------------
    | Laporan
    |--------------------------------------------------------------------------
    |model  modul ticket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "LAP001";

    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");

    $oHelpdesk = new Helpdesk();
    $oTicket = new Ticket();
    $oGroup = new Group();
    $oWilayah = new Wilayah();
    $oKomputer = new Computer();


    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "tidak tereksekusi";
    $s_error = "";
    $s_group_id_admin = 'SUPER_ADMINISTRATOR';
    $s_group_id_helpdesk = 'GROUP_HELPDESK';
    $s_group_id_spvti = 'GROUP_SPV_IT';
    $s_ext_condition_date = "";
    $s_ext_condition_status = "";
    $s_ext_condition_status_recovery = " AND (T.ticketStatus = 2 OR T.ticketStatus = 3 OR T.ticketStatus = 8) " ;//yang masuk ke laporan detail / rekap recovery adalah tiket yang pending atau selesai
    $s_condition_recovery_wilayah = " true ";
    $s_condition= "";


    if($oGroup->isGroup($USER[0]['userID'],$s_group_id_admin))
    {
        $s_group_name = "ADMINISTRATOR"; 
        $s_condition = " WHERE  ";       
    }
    elseif($oGroup->isGroup($USER[0]['userID'],$s_group_id_helpdesk))
    {
        $s_group_name = "HELPDESK";
        $s_where_helpdesk = " WHERE helpdeskID = '{$USER[0]['userID']}' ";
        $i_count_wilayah_handle = $oHelpdesk->getCount($s_where_helpdesk);  
        if( $i_count_wilayah_handle > 0)
        {
            $a_wilayah_kerja = $oHelpdesk->getList($s_where_helpdesk,"","");
            for($i=0;$i<$i_count_wilayah_handle;$i++)
            {
                if($i == 0)
                {
                    $s_wilayah_kerja = "'".$a_wilayah_kerja[$i]['unitHandle']."'";
                }
                else
                {
                    $s_wilayah_kerja .= ","."'".$a_wilayah_kerja[$i]['unitHandle']."'";   
                }
            }
            $s_wilayah_kerja = "(".$s_wilayah_kerja.")";
        }
        $s_condition = " WHERE UR.wilayahPenempatan IN {$s_wilayah_kerja} ";
    }
    elseif($oGroup->isGroup($USER[0]['userID'],$s_group_id_spvti))
    {
        $s_group_name = "SPV TI";
        $a_data_child = $oWilayah->getList(" WHERE child.parentWilayah = '{$USER[0]['wilayahPenempatan']}' ", " ORDER BY child.idWilayah ", "");
        $s_child_wilayah = "";
        if(isset($a_data_child))
        {
            for($j=0;$j<count($a_data_child);$j++)
            {
                $s_child_wilayah .= ",'{$a_data_child[$j]['idWilayah']}' ";
            }
        }
        $s_wilayah_kerja = "('{$USER[0]['wilayahPenempatan']}' $s_child_wilayah)";
        $s_condition = " WHERE UR.wilayahPenempatan IN {$s_wilayah_kerja} ";    
    }


    if(isset($_REQUEST['action']))
    {
        if($_REQUEST['action'] == 'ticket')
        {
            $valid_date = array();
            if($_REQUEST['daterangepicker'] == "")
            {
                $a_errors[] = "tanggal tidak boleh kosong";
            }
            else
            {
                //begin status
                $_REQUEST['daterangepicker'] = str_replace(" ", "", $_REQUEST['daterangepicker']);
                $a_tanggal = explode('-', $_REQUEST['daterangepicker']);
                if(count($a_tanggal) == 2)
                {

                    $s_format = 'd/m/Y';
                    $date[0] = DateTime::createFromFormat($s_format, $a_tanggal[0]);
                    $date[1] = DateTime::createFromFormat($s_format, $a_tanggal[1]);
                    $interval = date_diff($date[0],$date[1]);
                    $interval_date = $interval->format('%d');
                    if($interval_date <0 )
                    {
                        $result['result'] = "error";
                        $result['desc'] = "kesalahan tanggal";
                    }
                    //jika selisih waktu adalah 31hari / 1 bulan
                    elseif($interval_date >31)
                    {

                        $result['result'] = "error";
                        $result['desc'] = "selisih waktu terlalu besar. maksimal 1 bulan";
                    }
                    else
                    {
                        $valid_date[0] = date_format($date[0], 'Y-m-d');
                        $valid_date[1] = date_format($date[1], 'Y-m-d');
                        $s_ext_condition_date = " AND ticketRequestDate  >= '{$valid_date[0]}' AND ticketRequestDate  <= '{$valid_date[1]} 23:59:59' ";
                    }

                }
                //end date
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "format tanggal belum benar";
                }

                //begin status
                if(isset($_REQUEST['status']))
                {
                    if($_REQUEST['status'] == 'SEMUA STATUS')
                    {
                        $s_ext_condition_status = " AND TRUE ";
                    }
                    elseif($_REQUEST['status'] == 'BELUM DIKERJAKAN')
                    {
                        $s_ext_condition_status = " AND ticketStatus=0 ";
                    }
                    elseif($_REQUEST['status'] == 'PENDING')
                    {
                        $s_ext_condition_status = " AND (ticketStatus=2 OR ticketStatus=3) ";
                    }
                    elseif($_REQUEST['status'] == 'SELESAI')
                    {
                        $s_ext_condition_status = " AND ticketStatus=8 ";
                    }
                }
                else
                {

                    $result['result'] = "error";
                    $result['desc'] = "status tiket tidak ada";
                }
                //end status
            }

            //print_r($_REQUEST); exit();
            if (!$a_errors && $s_ext_condition_status != "" && $s_ext_condition_date !="") 
            {
                $s_complit_condition =  $s_condition . $s_ext_condition_date. $s_ext_condition_status;
                $a_data_ticket = $oTicket->getList($s_complit_condition,"","");
                if(isset($a_data_ticket ))
                {

                    if(count($a_data_ticket) > 0 )
                    {

                        $_SESSION['temp_query'] = $s_complit_condition;
                        //echo $_SESSION['temp_query'];
                        $result['result'] = "success";
                        $result['desc'] = "data  tersedia";
                    }
                    else
                    {
                        $result['result'] = "error";
                        $result['desc'] = "data tidak tersedia";
                    }
                }
                else
                {
                    
                    $result['result'] = "error";
                    $result['desc'] = "data tidak tersedia";
                }
                //$i_ticket = $oTicket->getList();
            }
        }
        elseif($_REQUEST['action'] == 'detilrecovery')
        {

            $s_wilayah_kerja = "";
            $valid_date = array();
            if($_REQUEST['daterangepicker'] == "")
            {
                $a_errors[] = "tanggal tidak boleh kosong";
            }
            else
            {
                //begin status
                $_REQUEST['daterangepicker'] = str_replace(" ", "", $_REQUEST['daterangepicker']);
                $a_tanggal = explode('-', $_REQUEST['daterangepicker']);
                if(count($a_tanggal) == 2)
                {

                    $s_format = 'd/m/Y';
                    $date[0] = DateTime::createFromFormat($s_format, $a_tanggal[0]);
                    $date[1] = DateTime::createFromFormat($s_format, $a_tanggal[1]);
                    $interval = date_diff($date[0],$date[1]);
                    $interval_date = $interval->format('%d');
                    if($interval_date <0 )
                    {
                        $result['result'] = "error";
                        $result['desc'] = "kesalahan tanggal";
                    }
                    //jika selisih waktu adalah 31hari / 1 bulan
                    elseif($interval_date >31)
                    {

                        $result['result'] = "error";
                        $result['desc'] = "selisih waktu terlalu besar. maksimal 1 bulan";
                    }
                    else
                    {
                        $valid_date[0] = date_format($date[0], 'Y-m-d');
                        $valid_date[1] = date_format($date[1], 'Y-m-d');
                        $s_ext_condition_date = " AND ticketRequestDate  >= '{$valid_date[0]}' AND ticketRequestDate  <= '{$valid_date[1]} 23:59:59' ";
                    }

                }
                //end date
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "format tanggal belum benar";
                }

                //begin status
                if(isset($_REQUEST['wilayah_kerja']))
                {
                    if($_REQUEST['wilayah_kerja'] != "")
                    {

                        $a_data_wilayah_child = $oWilayah->getList(" WHERE child.parentWilayah = '{$_REQUEST['wilayah_kerja']}' ", " ORDER BY child.idWilayah ", "");



                        if( count($a_data_wilayah_child) > 0)
                        {
                            for($i=0;$i<count($a_data_wilayah_child);$i++)
                            {
                                
                                    $s_wilayah_kerja .= ","."'".$a_data_wilayah_child[$i]['idWilayah']."'"; 
                            }
                        }
                        $s_condition_recovery_wilayah = "  UR.wilayahPenempatan IN ( '{$_REQUEST['wilayah_kerja']}' $s_wilayah_kerja ) ";


                    }
                    
                }
                else
                {

                    $result['result'] = "error";
                    $result['desc'] = "status tiket tidak ada";
                }
                //end status
            }
            if (!$a_errors  && $s_ext_condition_date !="") 
            {
                $s_complit_condition =  $s_condition .  $s_condition_recovery_wilayah. $s_ext_condition_date .$s_ext_condition_status_recovery ;
                //echo $s_complit_condition; exit();

                $a_data_ticket = $oTicket->getList($s_complit_condition,"","");
                if(isset($a_data_ticket ))
                {

                    if(count($a_data_ticket) > 0 )
                    {

                        $_SESSION['temp_query'] = $s_complit_condition;
                        if($_REQUEST['wilayah_kerja'] != "")
                        {
                            $a_data_wilayah_detail  = $oWilayah->getList(" WHERE child.idWilayah = '{$_REQUEST['wilayah_kerja']}' ","","");
                            $_SESSION['temp_wilayah'] = $a_data_wilayah_detail[0]['namaWilayah'];
                        }
                        else
                        {
                            $_SESSION['temp_wilayah'] = "";
                        }
                            
                        if(isset($valid_date))
                        {
                            $_SESSION['temp_date_range'] = "TANGGAL  ". $valid_date[0]. " SAMPAI ".$valid_date[1];
                        }
                        //echo $_SESSION['temp_query'];
                        $result['result'] = "success";
                        $result['desc'] = "data  tersedia";
                    }
                    else
                    {
                        $result['result'] = "error";
                        $result['desc'] = "data tidak tersedia";
                    }
                }
                else
                {
                    
                    $result['result'] = "error";
                    $result['desc'] = "data tidak tersedia";
                }
                //$i_ticket = $oTicket->getList();
            }
        }
        elseif($_REQUEST['action'] == 'asset')
        {

            $i_asset = 0;
            $s_condition = " WHERE helpdeskGroupHandle = '{$USER[0]['wilayahPenempatan']}' ";
            //jika komputer
            if($_REQUEST['asset_id'] == 0)
            {
                $s_ext_condition = " AND komputerID like 'KOM%' AND komputerStatus != '9'";
            }
            elseif($_REQUEST['asset_id'] == 1)
            {
                $s_ext_condition = " AND komputerID  like 'NET%' AND komputerStatus != '9'";
            }
            elseif($_REQUEST['asset_id'] == 2)
            {
                $s_ext_condition = " AND komputerID  like 'PRT%' AND komputerStatus != '9'";
            }

            $i_asset = $oKomputer->getCount($s_condition . $s_ext_condition);
            if($i_asset >0)
            {
                $result['result'] = "success";
                $result['desc'] = "asset ditemukan : {$i_asset}";
                $result['count'] = $i_asset;
            }
            else
            {
                $result['result'] = "error";
                $result['desc'] = "asset tidak ditemukan";
            }

        }
        
        else
        {
            foreach ($a_errors as $error) {
                $s_error .= "$error  <br />";
            }
            $result['desc']  = $s_error;
        }
    }
    echo json_encode($result);
    $oHelpdesk->closeDB();
    $oGroup->closeDB();
    $oTicket->closeDB();
    $oWilayah->closeDB();
    $oKomputer->closeDB();
?>