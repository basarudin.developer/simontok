<?php
    /*
    |--------------------------------------------------------------------------
    | laporan ticket view
    |--------------------------------------------------------------------------
    |view  modul laporan ticket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $LAYOUT_JS_EXTENDED .= "
                    
                    <!-- range datepicker -->
                    <script src='assets/bower_components/moment/min/moment.min.js'></script>
                    <script src='assets/bower_components/bootstrap-daterangepicker/daterangepicker.js'></script>
                    <script src='modul/laporan/laporan.js'></script>


                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <!-- range datepicker -->


                    <link rel='stylesheet' href='assets/bower_components/bootstrap-daterangepicker/daterangepicker.css'>
                    <link rel='stylesheet' href='assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'>

                    ";
    $s_table_container ="";
    //jangan diganti. ngefek ke laporan.model.php
    $a_status[] = 'SEMUA STATUS';
    $a_status[] = 'BELUM DIKERJAKAN';
    $a_status[] = 'PENDING';
    $a_status[] = 'SELESAI';
    $BUTTON_MAIN  = "
                        <button type='button' id='button-download-ticket' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-download'></i> Download Laporan Tiket
                        </button>
                    ";



    $s_select2_status = "<select class='form-control select2' name='status'   >";
                for($i=0;$i < count($a_status);$i++)
                {
                    $s_select2_status .= "<option value='{$a_status[$i]}'>{$a_status[$i]}</option>";
                }
                $s_select2_status .= "</select>";
    $s_form_input = "
                    <form id='form-laporan-ticket' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-md-12'>
                                            <!-- Hostname -->
                                            <div class='form-group'>
                                                <label>BATAS TANGGAL:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-calendar'></i>
                                                    </div>
                                                    <input type='text' class='form-control' id='daterangepicker' name='daterangepicker'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-12'>
                                            <!-- Sistem Operasi -->
                                            <div class='form-group'>
                                                <label>STATUS PEKERJAAN:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-wrench'></i>
                                                    </div>
                                                    $s_select2_status
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>LAPORAN TIKET</h4>
                                </div>
                                <div style='float:right'>$BUTTON_MAIN </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
?>