<?php
    /*
    |--------------------------------------------------------------------------
    | Laporan ticket
    |--------------------------------------------------------------------------
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID="LAP001";

    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.csv.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    $oTicket = new Ticket();

    $s_default_title="";
    $a_header[] = "NO";
    $a_header[] = "START TIME";
    $a_header[] = "END TIME";
    $a_header[] = "RECOVERY TIME (menit)";
    $a_header[] = "TARGET";
    $a_header[] = "PENCAPAIAN";
    $a_header[] = "NOMOR TIKET";
    $a_header[] = "TIKET STATUS";
    $a_header[] = "OBJECT ID";
    $a_header[] = "PEMILIK";
    $a_header[] = "UNIT";
    $a_header[] = "PETUGAS";


    if(isset($_SESSION['temp_query']))
    {
        if($_SESSION['temp_query'] != "")
        {
            if(isset($_SESSION['temp_wilayah']) && isset($_SESSION['temp_date_range'])  )
            {
                $s_default_title = "DETIL RECOVERY {$_SESSION['temp_wilayah']} {$_SESSION['temp_date_range']}";
            }
            $a_title[] = $s_default_title ;
            for($i=0;$i<count($a_header)-1;$i++)
            {
                $a_title[] = "";
            }
            $a_data = $oTicket->getList($_SESSION['temp_query'],"","");
            $csv = new CSV("laporan_detil_recovery");
            //$csv->headerColumns($s_default_title);

            $csv->addRow($a_title);
            $csv->addRow($a_header);

            for($i=0;$i<count($a_data);$i++)
            {

                $i_minute = 0;
                $a_row_data = null;
                $s_end_time = $a_data[$i]['ticketSolveDateFinish'];

                $s_solved ="";
                $status_perkerjaan = "";
                if($a_data[$i]['ticketStatus'] == "8")
                {
                    $i_minute = round(abs(strtotime($a_data[$i]['ticketRequestDate']) - strtotime($a_data[$i]['ticketSolveDateFinish']) ) / 60);
                    $s_solved  = $a_data[$i]['ticketSolveDesc'];
                    $status_perkerjaan = "CLOSED";
                }
                elseif($a_data[$i]['ticketStatus'] == "2" || $a_data[$i]['ticketStatus'] == "3")
                {
                
                    $s_end_time = $a_data[$i]['ticketSolveDateStart'];

                    $i_minute = round(abs(strtotime($a_data[$i]['ticketRequestDate']) - strtotime($a_data[$i]['ticketSolveDateStart']) ) / 60);
                    $s_solved  = $a_data[$i]['ticketStatusDesc'];
                    $status_perkerjaan = "PENDING";
                }
                elseif($a_data[$i]['ticketStatus'] == "0" )
                {
                    $status_perkerjaan = "belumm dikerjakan";
                }

                $a_row_data =  array(
                                    ($i+1),
                                    $a_data[$i]['ticketRequestDate'],
                                    $s_end_time,
                                    $i_minute,
                                    "",
                                    "",
                                    $a_data[$i]['ticketNumber'],
                                    $status_perkerjaan,
                                    $a_data[$i]['ticketKomputerID'],
                                    $a_data[$i]['userRequestName'],
                                    $a_data[$i]['requestWilayah'],
                                    $a_data[$i]['helpdeskHandleName']
                                );
                $csv->addRow($a_row_data);
            }

            $csv->export();
    
        }
        unset($_SESSION['temp_query']);
        unset($_SESSION['temp_wilayah']);
        if(isset($_SESSION['temp_date_range']))
        {
            unset($_SESSION['temp_date_range']);
        }
    
    }

    $oTicket->closeDB();
?>