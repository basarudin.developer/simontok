
    if($('.select2').length)
    {
        $('.select2').select2();
    }

    if($('#daterangepicker').length)
    {
        $('#daterangepicker').daterangepicker({
            showCustomRangeLabel:false,
            locale:{
                format:'DD/MM/YYYY'
            },
            ranges   : {
                'Sekarang'       : [moment(), moment()],
                'Kemaren'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7hari lalu' : [moment().subtract(6, 'days'), moment()],
                '30hari lalu': [moment().subtract(29, 'days'), moment()],
                'Bulan ini'  : [moment().startOf('month'), moment().endOf('month')],
                'Bulan kemaren'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment(),
                function (start, end) {
                    $('#daterangepicker').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            }
        );
    }
    if($('#daterangepickermonth').length)
    {
        $('#daterangepickermonth').daterangepicker({
            showCustomRangeLabel:false,
            locale:{
                format:'DD/MM/YYYY'
            },
            ranges   : {
                '30hari lalu': [moment().subtract(29, 'days'), moment()],
                'Bulan ini'  : [moment().startOf('month'), moment().endOf('month')],
                'Bulan kemaren'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            startDate: moment().subtract(29, 'days'),
            endDate  : moment(),
                function (start, end) {
                    $('#daterangepickermonth').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            }
        );
    }

    /*************************************************
    |   BEGIN DOWNLOAD TICKET HELPDESK
    **************************************************/
    if($('#button-download-ticket').length)
    {
        $("#button-download-ticket").click(function() 
        {

            daterangepicker = $('#daterangepicker').val();

            if(daterangepicker == "")
            {
                $('#notification-container').hide(); 
                $('#notification-container').removeClass('alert-success');
                $('#notification-container').addClass('alert-danger'); 
                $('#notification-content').html('batas tanggal harus diisi');
                $('#notification-container').show(); 
            }
            else
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=laporan&type=model&action=ticket",
                    dataType:"json",
                    data: $("#form-laporan-ticket").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            $('#notification-container').hide(); 
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                document.location='index.php?page=laporan&type=ticket_csv';

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                            }
                            $('#notification-container').show(); 
                            $('html, body').animate({
                            scrollTop: $('#notification-container').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                        }
                });//end of ajaxs
            }
        });


    }
    /*   END DOWNLOAD TICKET HELPDESK
    **************************************************/


    /*************************************************
    |   BEGIN DOWNLOAD ASSET
    **************************************************/
    if($('#button-download-asset').length)
    {
        $("#button-download-asset").click(function() 
        {
            $('#notification-container').hide(); 
            $.ajax({
                type: 'POST',
                url: "index.php?page=laporan&type=model&action=asset",
                dataType:"json",
                data: $("#form-laporan-asset").serialize(),
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        $('#notification-container').hide(); 
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            document.location='index.php?page=laporan&type=pusat_komputer_csv';

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                        }
                        $('#notification-container').show(); 
                        $('html, body').animate({
                        scrollTop: $('#notification-container').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                    }
            });//end of ajaxs
        });


    }
    /*   END DOWNLOAD ASSET
    **************************************************/



    /*************************************************
    |   BEGIN DOWNLOAD DETIL RECOVERY
    **************************************************/

    if($('#button-download-detil-recovery').length)
    {

        $("#button-download-detil-recovery").click(function() 
        {


            daterangepicker = $('#daterangepicker').val();

            if(daterangepicker == "")
            {
                $('#notification-container').hide(); 
                $('#notification-container').removeClass('alert-success');
                $('#notification-container').addClass('alert-danger'); 
                $('#notification-content').html('batas tanggal harus diisi');
                $('#notification-container').show(); 
            }
            else
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=laporan&type=model&action=detilrecovery",
                    dataType:"json",
                    data: $("#form-laporan-ticket").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            $('#notification-container').hide(); 
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                document.location='index.php?page=laporan&type=detil_recovery_csv&action=detil_recovery_csv';

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                            }
                            $('#notification-container').show(); 
                            $('html, body').animate({
                            scrollTop: $('#notification-container').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                        }
                });//end of ajaxs
            }
        });


    }
    /*   END DOWNLOAD DETIL RECOVERY
    **************************************************/


