<?php
    /*
    |--------------------------------------------------------------------------
    | Laporan deviceregister
    |--------------------------------------------------------------------------
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID="LAP001";

    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.csv.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oWilayah = new Wilayah();
	require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
	$oUser = new UserInfo();
    require_once($SYSTEM['DIR_PATH']."/class/class.device.php");
    $oDevice = new Device();

    $a_header[] = "NO";
    $a_header[] = "START TIME";
    $a_header[] = "END TIME";
    $a_header[] = "RECOVERY TIME (menit)";
    $a_header[] = "TARGET (menit)";
    $a_header[] = "PENCAPAIAN";
    $a_header[] = "NO TICKET";
    $a_header[] = "OBJECT ID";
    $a_header[] = "UNIT INDUK";
    $a_header[] = "UNIT";
    $a_header[] = "SUB UNIT";
    $a_header[] = "PETUGAS";
    $s_limit = "  ";
    $s_order = " ";
    $a_data_pegawai = array();

    $s_condition = " WHERE wilayahPenempatan = '{$USER[0]['wilayahPenempatan']}' ";
    $a_data_pegawai_parent = $oUser->getUserList($s_condition, $s_order, $s_limit);
    if(isset($a_data_pegawai_parent))
    {
    	for($i=0;$i<count($a_data_pegawai_parent);$i++)
    	{
    		$a_data_device = $oDevice->getList(" WHERE U.firebaseToken ='{$a_data_pegawai_parent[$i]['firebaseToken']}'  ","",
    			"");
    		if(isset($a_data_device[0]))
    		{
    			$a_data_pegawai_parent[$i]['deviceSDK'] = $a_data_device[0]['deviceSDK'];
    			$a_data_pegawai_parent[$i]['deviceModel'] = $a_data_device[0]['deviceModel'];
    			$a_data_pegawai_parent[$i]['deviceMerk'] = $a_data_device[0]['deviceMerk'];
    			$a_data_pegawai_parent[$i]['screenSize'] = $a_data_device[0]['screenSize'];
    		}
    		else
    		{
    			$a_data_pegawai_parent[$i]['deviceSDK'] = "";
    			$a_data_pegawai_parent[$i]['deviceModel'] = "";
    			$a_data_pegawai_parent[$i]['deviceMerk'] = "";
    			$a_data_pegawai_parent[$i]['screenSize'] = "";

    		}
    		$a_data_pegawai[] =$a_data_pegawai_parent[$i];
    	}
    }

	$a_data_wilayah_child = $oWilayah->getList(" WHERE child.parentWilayah = '{$USER[0]['wilayahPenempatan']}' ", " ORDER BY child.idWilayah ", "");

    if(isset($a_data_wilayah_child))
    {
        for($j=0;$j<count($a_data_wilayah_child);$j++)
        {

		    $s_condition = " WHERE wilayahPenempatan = '{$a_data_wilayah_child[$j]['idWilayah']}' ";
		    $a_data_pegawai_child = $oUser->getUserList($s_condition, $s_order, $s_limit);
		    if(isset($a_data_pegawai_child))
		    {
		    	for($k=0;$k<count($a_data_pegawai_child);$k++)
		    	{
		    		$a_data_device = $oDevice->getList(" WHERE U.firebaseToken ='{$a_data_pegawai_child[$k]['firebaseToken']}'  ","",
		    			"");
		    		if(isset($a_data_device[0]))
		    		{
		    			$a_data_pegawai_child[$k]['deviceSDK'] = $a_data_device[0]['deviceSDK'];
		    			$a_data_pegawai_child[$k]['deviceModel'] = $a_data_device[0]['deviceModel'];
		    			$a_data_pegawai_child[$k]['deviceMerk'] = $a_data_device[0]['deviceMerk'];
		    			$a_data_pegawai_child[$k]['screenSize'] = $a_data_device[0]['screenSize'];
		    		}
		    		else
		    		{
		    			$a_data_pegawai_child[$k]['deviceSDK'] = "";
		    			$a_data_pegawai_child[$k]['deviceModel'] = "";
		    			$a_data_pegawai_child[$k]['deviceMerk'] = "";
		    			$a_data_pegawai_child[$k]['screenSize'] = "";

		    		}
		    		$a_data_pegawai[] =$a_data_pegawai_child[$k];
		    	}
		    }
        }
    }


    if(isset($a_data_pegawai))
    {
        $csv = new CSV("laporan_komputer");
        $csv->headerColumns($a_header);
        for($i=0;$i<count($a_data_pegawai);$i++)
        {
            $a_row_data =  array(
                                ($i+1),
                                $a_data_pegawai[$i]['userID'],
                                $a_data_pegawai[$i]['realName'],
                                $a_data_pegawai[$i]['deviceMerk'],
                                $a_data_pegawai[$i]['deviceModel'],
                                $a_data_pegawai[$i]['deviceSDK'],
                                $a_data_pegawai[$i]['screenSize']
                            );
            $csv->addRow($a_row_data);
        }
        $csv->export();
    }
    $oWilayah->closeDB();
    $oUser->closeDB();
    $oDevice->closeDB();
?>