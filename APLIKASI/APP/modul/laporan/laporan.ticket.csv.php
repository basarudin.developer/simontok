<?php
    /*
    |--------------------------------------------------------------------------
    | Laporan ticket
    |--------------------------------------------------------------------------
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID="LAP001";

    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.csv.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    $oTicket = new Ticket();

    $a_header[] = "NO";
    $a_header[] = "TANGGAL";
    $a_header[] = "NOMOR TIKET";
    $a_header[] = "WILAYAH";
    $a_header[] = "NAMA PEGAWAI";
    $a_header[] = "PERMINTAAN";
    $a_header[] = "PENGERJAAN";
    $a_header[] = "STATUS";

    if(isset($_SESSION['temp_query']))
    {
        if($_SESSION['temp_query'] != "")
        {
            $a_data = $oTicket->getList($_SESSION['temp_query'],"","");
            $csv = new CSV("laporan_tiket");
            $csv->headerColumns($a_header);

            for($i=0;$i<count($a_data);$i++)
            {

                $a_row_data = null;

                $s_solved ="";
                $status_perkerjaan = "";
                if($a_data[$i]['ticketStatus'] == "8")
                {
                    $s_solved  = $a_data[$i]['ticketSolveDesc'];
                    $status_perkerjaan = "selesai";
                }
                elseif($a_data[$i]['ticketStatus'] == "2" || $a_data[$i]['ticketStatus'] == "3")
                {
                    $s_solved  = $a_data[$i]['ticketStatusDesc'];
                    $status_perkerjaan = "pending";
                }
                elseif($a_data[$i]['ticketStatus'] == "0" )
                {
                    $status_perkerjaan = "belumm dikerjakan";
                }

                $a_row_data =  array(
                                    ($i+1),
                                    $a_data[$i]['ticketRequestDate'],
                                    $a_data[$i]['ticketNumber'],
                                    $a_data[$i]['requestWilayah'],
                                    $a_data[$i]['userRequestName'],
                                    $a_data[$i]['ticketProblemDesc'],
                                    $s_solved ,
                                    $status_perkerjaan
                                );
                $csv->addRow($a_row_data);
            }

            $csv->export();
    
        }
        unset($_SESSION['temp_query']);
    
    }

    $oTicket->closeDB();
?>