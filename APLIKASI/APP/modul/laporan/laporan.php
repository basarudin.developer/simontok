<?php
    /*
    |--------------------------------------------------------------------------
    | laporan
    |--------------------------------------------------------------------------
    |Controler  modul laporan
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "LAP001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    if(isset($_REQUEST['type']))
    {
        //direk json
        if ($_REQUEST['type'] == "model") 
        {
            require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.model.php");
        }
        //direk json
        elseif ($_REQUEST['type'] == "ticket_csv") 
        {
            require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.ticket.csv.php");
        } 
        //direk json
        elseif ($_REQUEST['type'] == "pusat_komputer_csv") 
        {
            require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.pusat.komputer.csv.php");
        } 
        //direk json
        elseif ($_REQUEST['type'] == "komputer_csv") 
        {
            if(isset($_REQUEST['action']))
            {
                if($_REQUEST['action'] == "komputer_csv")
                {
                    require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.komputer.csv.php");
                }
            }
        }
        //direk json
        elseif ($_REQUEST['type'] == "os_csv") 
        {
            if(isset($_REQUEST['action']))
            {
                if($_REQUEST['action'] == "os_csv")
                {
                    require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.sistemoperasi.csv.php");
                }
            }
        }
        //direk json
        elseif ($_REQUEST['type'] == "device_csv") 
        {
            if(isset($_REQUEST['action']))
            {
                if($_REQUEST['action'] == "device_csv")
                {
                    require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.deviceregister.csv.php");
                }
            }
        }
        //direk json
        elseif ($_REQUEST['type'] == "detil_recovery_csv") 
        {
            if(isset($_REQUEST['action']))
            {
                if($_REQUEST['action'] == "detil_recovery_csv")
                {
                    require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.recoverydetil.csv.php");
                }
            }
        }
        //direk json
        elseif ($_REQUEST['type'] == "rekap_recovery_csv") 
        {
            if(isset($_REQUEST['action']))
            {
                if($_REQUEST['action'] == "rekap_recovery_csv")
                {
                    require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.recoveryrekap.csv.php");
                }
            }
        }
        //selain direk langsung
        elseif($_REQUEST['type'] != "")
        { 
            require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.view.php");
            include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.view.php");
        include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
    }

?>