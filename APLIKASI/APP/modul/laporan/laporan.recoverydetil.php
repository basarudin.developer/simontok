<?php
    /*
    |--------------------------------------------------------------------------
    | laporan ticket view
    |--------------------------------------------------------------------------
    |view  modul laporan ticket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    $PAGE_ID = "LAP100";
    include($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oWilayah = new Wilayah();

    $LAYOUT_JS_EXTENDED .= "
                    
                    <!-- range datepicker -->
                    <script src='assets/bower_components/moment/min/moment.min.js'></script>
                    <script src='assets/bower_components/bootstrap-daterangepicker/daterangepicker.js'></script>
                    <script src='assets/bower_components/select2/dist/js/select2.full.min.js'></script>

                    <script src='modul/laporan/laporan.js'></script>


                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <!-- range datepicker -->


                    <link rel='stylesheet' href='assets/bower_components/bootstrap-daterangepicker/daterangepicker.css'>
                    <link rel='stylesheet' href='assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'>

                    <link rel='stylesheet' href='assets/bower_components/select2/dist/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/adminlte.css'>

                    ";
    $s_table_container ="";

    $s_wilayah_kerja = "";
    $a_data_wilayah_kerja  = $oWilayah->getList("  WHERE child.parentWilayah is null ","","");
    if(isset($a_data_wilayah_kerja))
    {
        $s_wilayah_kerja = "<select class='form-control select2' name='wilayah_kerja'  id='wilayah_kerja'  >";
        $s_wilayah_kerja .= "<option value=''>SEMUA WILAYAH </option>";
        for($i=0;$i < count($a_data_wilayah_kerja);$i++)
        {
            $s_wilayah_kerja .= "<option value='{$a_data_wilayah_kerja[$i]['idWilayah']}'>".strtoupper($a_data_wilayah_kerja[$i]['namaWilayah'])."</option>";
        }
        $s_wilayah_kerja .= "</select>";
    }
    $BUTTON_MAIN  = "
                        <button type='button' id='button-download-detil-recovery' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-download'></i> Download Laporan
                        </button>
                    ";


    $s_form_input = "
                    <form id='form-laporan-ticket' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-md-12'>
                                            <!-- Hostname -->
                                            <div class='form-group'>
                                                <label>BATAS TANGGAL:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-calendar'></i>
                                                    </div>
                                                    <input type='text' class='form-control' id='daterangepicker' name='daterangepicker'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-12'>
                                            <!-- Sistem Operasi -->
                                            <div class='form-group'>
                                                <label>STATUS PEKERJAAN:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-home'></i>
                                                    </div>
                                                    $s_wilayah_kerja
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>LAPORAN  DETIL RECOVERY TIME LAN</h4>
                                </div>
                                <div style='float:right'>$BUTTON_MAIN </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oWilayah = new Wilayah();

?>