<?php
    /*
    |--------------------------------------------------------------------------
    | laporan view
    |--------------------------------------------------------------------------
    |view  modul device register
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "ticket") 
        {
            require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.ticket.php");
        } 
        elseif ($_REQUEST['action'] == "asset") 
        {
            require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.asset.php");
        } 
        elseif ($_REQUEST['action'] == "detil_recovery_csv") 
        {
            require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.recoverydetil.php");
        } 
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/laporan/laporan.list.php");
    }

?>