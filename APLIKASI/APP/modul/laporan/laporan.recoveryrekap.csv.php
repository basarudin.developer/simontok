<?php
    /*
    |--------------------------------------------------------------------------
    | Laporan ticket
    |--------------------------------------------------------------------------
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID="LAP001";

    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.csv.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    

    $oTicket = new Ticket();
    $oWilayah = new Wilayah();


    //yang perlu diganti
    $s_default_title = "REKAP RECOVERY  ".strtoupper(date('F Y'));
    $s_file_name = "laporan_rekap_recovery";




    $a_data =  array();
    $s_wilayah_kerja = "";
    $a_data_wilayah_kerja_parent  = $oWilayah->getList("  WHERE child.parentWilayah is null ",""," ORDER BY  `idWilayah` ASC");

    $s_ext_status_condition = " AND (ticketStatus=2 OR ticketStatus=3 OR  ticketStatus=8)  ";//status pending atau selesai

    $a_wilayah_kerja =  array();

    for($i=0;$i<count($a_data_wilayah_kerja_parent);$i++)
    {
        $a_wilayah_kerja[] = $a_data_wilayah_kerja_parent[$i]; 
        $a_data_wilayah_kerja_child  = $oWilayah->getList("  WHERE child.parentWilayah = '{$a_data_wilayah_kerja_parent[$i]['idWilayah']}'  ",""," ORDER BY  `idWilayah` ASC");
        for($j=0;$j<count($a_data_wilayah_kerja_child);$j++)
        {
            $a_wilayah_kerja[] = $a_data_wilayah_kerja_child[$j];
        }
    }

    if(isset($a_wilayah_kerja))
    {
        if(count($a_wilayah_kerja) > 0)
        {

            for($i=0;$i<count($a_wilayah_kerja);$i++)
            {
                $i_avg_recovery_time = 0;
                $i_insiden = 0;
                $s_condition = " WHERE UR.wilayahPenempatan =  '{$a_wilayah_kerja[$i]['idWilayah']}'  AND MONTH(`ticketRequestDate`) = '".date('n')."' AND  YEAR(`ticketRequestDate`) = '".date('Y')."'";
                $i_insiden = $oTicket->getCount($s_condition);

                $s_condition = $s_condition.$s_ext_status_condition;//untuk mencari recovery time difilter yang pending atau selesai
                $a_data_ticket = $oTicket->getList(" $s_condition ", "","");
                if(isset($a_data_ticket))
                {
                    if(count($a_data_ticket) > 0)
                    {

                        for($k=0;$k<count($a_data_ticket);$k++)
                        {

                            $i_total_recovery = 0;//dalam menit
                            if($a_data_ticket[$k]['ticketStatus'] == "8")
                            {
                                $i_minute = round(abs(strtotime($a_data_ticket[$k]['ticketRequestDate']) - strtotime($a_data_ticket[$k]['ticketSolveDateFinish']) ) / 60);
                            }
                            elseif($a_data_ticket[$k]['ticketStatus'] == "2" || $a_data_ticket[$k]['ticketStatus'] == "3")
                            {
                                $i_minute = round(abs(strtotime($a_data_ticket[$k]['ticketRequestDate']) - strtotime($a_data_ticket[$k]['ticketSolveDateStart']) ) / 60);
                            }

                            $i_total_recovery = $i_total_recovery +$i_minute;//dalam menit

                        }
                        $i_avg_recovery_time = abs($i_total_recovery/ count($a_data_ticket));

                    }
                }

                $a_data[$i] = $a_wilayah_kerja[$i];
                $a_data[$i]['insiden'] = $i_insiden;
                $a_data[$i]['avg_rect'] = $i_avg_recovery_time;

                //menghitung recovertime 
                //begin recovery time
                //$a_ticket = $oTicket->getList();

                //end recovery time
            }



        }
    }






    $a_header[] = "NO";
    $a_header[] = "UNIT";
    $a_header[] = "AVERAGE RECOVERY TIME (menit)";
    $a_header[] = "TARGET";
    $a_header[] = "PENCAPAIAN";
    $a_header[] = "JUMLAH INSIDEN";


    $csv = new CSV($s_file_name);
    //$csv->headerColumns($s_default_title);

    $a_title[] = $s_default_title ;
    for($i=0;$i<count($a_header)-1;$i++)
    {
        $a_title[] = "";
    }

    $csv->addRow($a_title);
    $csv->addRow($a_header);

    for($i=0;$i<count($a_data);$i++)
    {

        $a_row_data =  array(
                            ($i+1),
                            $a_data[$i]['namaWilayah'],
                            $a_data[$i]['avg_rect'],
                            "",
                            "",
                            $a_data[$i]['insiden']
                        );
        $csv->addRow($a_row_data);
    }

    $csv->export();

    $oTicket->closeDB();
    $oWilayah->closeDB();

?>