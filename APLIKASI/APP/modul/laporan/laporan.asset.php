<?php
    /*
    |--------------------------------------------------------------------------
    | laporan ticket view
    |--------------------------------------------------------------------------
    |view  modul laporan ticket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $LAYOUT_JS_EXTENDED .= "
                    
                    <!-- range datepicker -->
                    <script src='assets/bower_components/moment/min/moment.min.js'></script>
                    <script src='assets/bower_components/bootstrap-daterangepicker/daterangepicker.js'></script>
                    <script src='assets/bower_components/select2/dist/js/select2.full.min.js'></script>
                    <script src='modul/laporan/laporan.js'></script>


                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <!-- range datepicker -->


                    <link rel='stylesheet' href='assets/bower_components/bootstrap-daterangepicker/daterangepicker.css'>
                    <link rel='stylesheet' href='assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'>

                    <link rel='stylesheet' href='assets/bower_components/select2/dist/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/adminlte.css'>

                    ";
    $s_table_container ="";
    //jangan diganti. ngefek ke laporan.model.php
    $a_status[] = 'PC , LAPTOP DAN SERVER';
    $a_status[] = 'ASET JARINGAN';
    $a_status[] = 'LAINNYA';
    $BUTTON_MAIN  = "
                        <button type='button' id='button-download-asset' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-download'></i> Download Laporan
                        </button>
                    ";



    $s_select2_status = "<select class='form-control select2' name='asset_id'   >";
                for($i=0;$i < count($a_status);$i++)
                {
                    $s_select2_status .= "<option value='$i'>{$a_status[$i]}</option>";
                }
                $s_select2_status .= "</select>";
    $s_form_input = "
                    <form id='form-laporan-asset' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                
                                <!-- /.row -->
                                <div class='row'>
                                    <div class='col-md-12'>
                                            <!-- Sistem Operasi -->
                                            <div class='form-group'>
                                                <label>JENIS ASSET:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-laptop'></i>
                                                    </div>
                                                    $s_select2_status
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>LAPORAN ASSET</h4>
                                </div>
                                <div style='float:right'>$BUTTON_MAIN </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
?>