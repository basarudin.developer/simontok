<?php
    /*
    |--------------------------------------------------------------------------
    | Laporan ticket
    |--------------------------------------------------------------------------
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID="LAP001";

    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.csv.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    $oKomputer = new Computer();

    $a_header[] = "NO";
    $a_header[] = "SISTEM OPERASI";
    $a_header[] = "JUMLAH";

    $column_name = 'komputerSpekSerialNumber';
    $s_where_os = " WHERE helpdeskGroupHandle = '{$USER[0]['wilayahPenempatan']}' AND KS.komputerItemID='{$a_komputer_item['SISTEM_OPERASI']}' AND K.komputerStatus != 9 AND KS.komputerSpekStatus !=9";  
    $a_data = $oKomputer->getCountSpekDashboard($column_name,$s_where_os);

    if(isset($a_data))
    {
        if(count($a_data) > 0)
        {

            $csv = new CSV("laporan_sistem_operasi");
            $csv->headerColumns($a_header);
            for($i=0;$i<count($a_data);$i++)
            {
                
                $a_row_data =  array(
                                    ($i+1),
                                    $a_data[$i][$column_name],
                                    $a_data[$i]['total']
                                );
                $csv->addRow($a_row_data);
            }
            $csv->export();
        }
    }
    $oKomputer->closeDB();
?>