<?php
    /*
    |--------------------------------------------------------------------------
    | Laporan ticket
    |--------------------------------------------------------------------------
    |
    |
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID="LAP001";

    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.csv.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    $oKomputer = new Computer();

    $a_header[] = "NO";
    $a_header[] = "JENIS ASSET";
    $a_header[] = "MERK";
    $a_header[] = "TYPE";
    $a_header[] = "MICROPROCESSOR";
    $a_header[] = "SISTEM OPERASI";
    $a_header[] = "RAM";
    $a_header[] = "STORAGE";
    $a_header[] = "DISPLAY CARD";
    $a_header[] = "MONITOR";
    $a_header[] = "INTERFACE";
    $a_header[] = "WARRANTY";
    $a_header[] = "KLASIFIKASI";
    $a_header[] = "SEWA/BELI";
    $a_header[] = "TAHUN TERIMA";
    $a_header[] = "NIP";
    $a_header[] = "NAMA";
    $a_header[] = "DESKRIPSI";

    $s_limit = "  ";
    $s_order = " ";
    $s_condition = " WHERE helpdeskGroupHandle = '{$USER[0]['wilayahPenempatan']}' AND komputerID like 'KOM%' AND komputerStatus != '9' ";
    $a_data = $oKomputer->getList($s_condition, $s_order, $s_limit);

    if(isset($a_data))
    {
        $csv = new CSV("laporan_komputer_pusat");
        $csv->headerColumns($a_header);
        for($i=0;$i<count($a_data);$i++)
        {
            //motherboard
            $a_data_spek = $oKomputer->getListSpek(" WHERE komputerID='{$a_data[$i]['komputerID']}' AND komputerItemID   ='{$a_komputer_item['MOTHERBOARD']}' AND komputerSpekStatus!=9 ","","");
            $merk = "";
            $tipe = "";
            if(isset($a_data_spek[0]['komputerSpekMerk']))
            {
                $merk = $a_data_spek[0]['komputerSpekMerk'] ;
            }
            if(isset($a_data_spek[0]['komputerSpekKeterangan']))
            {
                $tipe = $a_data_spek[0]['komputerSpekKeterangan'] ;
            }
            //sistem operasi
            $a_data_spek = $oKomputer->getListSpek(" WHERE komputerID='{$a_data[$i]['komputerID']}' AND komputerItemID   ='{$a_komputer_item['SISTEM_OPERASI']}' AND komputerSpekStatus!=9 ","","");
            $sistem_operasi = "";
            if(isset($a_data_spek[0]['komputerSpekKeterangan']))
            {
                $sistem_operasi = $a_data_spek[0]['komputerSpekKeterangan'] ;
            }

            //processor
            $a_data_spek = $oKomputer->getListSpek(" WHERE komputerID='{$a_data[$i]['komputerID']}' AND komputerItemID   ='{$a_komputer_item['PROCESSOR']}' AND komputerSpekStatus=1 ","","");
            $processor = "";
            if(isset($a_data_spek[0]['komputerSpekKeterangan']))
            {
                $processor = $a_data_spek[0]['komputerSpekKeterangan']." {$a_data_spek[0]['komputerSpekSerialNumber']}"."core" ;
            }

            //memori
            $a_data_spek = $oKomputer->getListSpek(" WHERE komputerID='{$a_data[$i]['komputerID']}' AND komputerItemID   ='{$a_komputer_item['MEMORY']}' AND komputerSpekStatus=1 ","","");
            $memori = "";
            $memori_total = 0;
            $memori_keping = 0;
            if(isset($a_data_spek))
            {   
                for ($x=0; $x < count($a_data_spek) ; $x++) { 
                    $memori_total = $memori_total +  $a_data_spek[$x]['komputerSpekKapasitas']; 
                }

                if($memori_total > 1000000)
                {
                    $memori_total = $memori_total/1000000;
                    $s_memori_total = round($memori_total)."TB"; 
                }
                elseif ($memori_total > 1000) 
                {
                    $memori_total = $memori_total/1000;
                    $s_memori_total = round($memori_total)."GB"; 
                }
                else 
                {
                    $s_memori_total = round($memori_total)."MB"; 
                }
                $memori = $s_memori_total;
            }

            //harddisk
            $a_data_spek = $oKomputer->getListSpek(" WHERE komputerID='{$a_data[$i]['komputerID']}' AND komputerItemID   ='{$a_komputer_item['HARDDISK']}' AND komputerSpekStatus=1 ","","");
            $harddisk = "";
            $harddisk_total = 0;
            $harddisk_biji = 0;
            if(isset($a_data_spek))
            {   
                for ($x=0; $x < count($a_data_spek) ; $x++) { 
                    $harddisk_total = $harddisk_total +  $a_data_spek[$x]['komputerSpekKapasitas']; 
                }
                if($harddisk_total > 1000000)
                {
                    $harddisk_total = $harddisk_total/1000000;
                    $s_harddisk_total = round($harddisk_total)."TB"; 
                }
                elseif ($harddisk_total > 1000) 
                {
                    $harddisk_total = $harddisk_total/1000;
                    $s_harddisk_total = round($harddisk_total)."GB"; 
                }
                else 
                {
                    $s_harddisk_total = round($harddisk_total)."MB"; 
                }

                $harddisk = $s_harddisk_total;
            }

            //monitor
            $a_data_spek = $oKomputer->getListSpek(" WHERE komputerID='{$a_data[$i]['komputerID']}' AND komputerItemID   ='{$a_komputer_item['MONITOR']}' AND komputerSpekStatus=1 ","","");
            $monitor = "";
            if(isset($a_data_spek[0]['komputerSpekKapasitas']))
            {
                $monitor = $a_data_spek[0]['komputerSpekMerk']." ".$a_data_spek[0]['komputerSpekKapasitas']." inch";
            }

            //pengadaan
            $pengadaan = "BELI";
            if($a_data[$i]['komputerPengadaan'] == "PLN")
            {
                $pengadaan = "BELI";
            }
            elseif($a_data[$i]['komputerPengadaan'] == "")
            {
                $pengadaan = "";
            }
            else
            {
                $pengadaan = "SEWA";
            }


            $a_row_data =  array(
                                ($i+1),
                                $a_data[$i]['komputerPosisi'],
                                "{$merk}",
                                "{$merk}",
                                "{$processor}",
                                "{$sistem_operasi}",
                                "{$memori}",
                                "{$harddisk}",
                                "{$monitor}",
                                "",
                                "",
                                "",
                                "",
                                "{$pengadaan}",
                                "",
                                $a_data[$i]['komputerUser'],
                                $a_data[$i]['realName'],
                                ""
                            );
            $csv->addRow($a_row_data);
        }
        $csv->export();
    }
    else
    {
        echo "maaf data tidak tersedia";
    }
    $oKomputer->closeDB();
?>