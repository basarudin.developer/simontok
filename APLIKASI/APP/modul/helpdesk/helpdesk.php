<?php
    /*
    |--------------------------------------------------------------------------
    | Helpdesk
    |--------------------------------------------------------------------------
    |Controler  modul helpdesk
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";

    $COMMON_GROUP = "GRP00000000000000000";
    $GROUP_ADMIN = "SUPER_ADMINISTRATOR";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");
    if(isset($_REQUEST['type']))
    {
        if ($_REQUEST['type'] == "model") 
        {
            require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.model.php");
        } 
        elseif($_REQUEST['type'] != "")
        { 
            include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
            require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.view.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.view.php");
        include_once($SYSTEM['DIR_MODUL_LAYOUT']."/layout.php");
    }

?>