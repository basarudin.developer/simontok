<?php
    /*
    |--------------------------------------------------------------------------
    | helpdesk list
    |--------------------------------------------------------------------------
    |Controler  modul helpdesk
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");
    $oHelpdesk = new Helpdesk();


    $LAYOUT_JS_EXTENDED .= "                
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/helpdesk/helpdesk.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-create-helpdesk-handle' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-user-secret'></i> Tambah
                        </button>
                        <button type='button' id='button-wilayah-perhelpdesk'  class=' btn btn-flat  btn-sm btn-warning pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-eye'></i> Tampilan per Helpdesk
                        </button>
                        <button type='button' id='button-wilayah-perwilayah' class=' btn btn-flat  btn-sm btn-warning pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-eye'></i> Tampilan per Wilayah
                        </button>
                        ";
    //---table
    $a_title  = array();
    $a_title[] = "WILAYAH";
    $a_title[] = "NAMA";
    $a_title[] = "";
    $a_title_class[] = " style='width:300px;' ";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:50px;' ";
    $s_table_container = "";
    $s_condition = " WHERE true ";
    $s_limit = "  ";
    $s_order = " ";

    $a_data = $oHelpdesk->getList($s_condition, $s_order, $s_limit);
    if(isset($a_data))
    {
          
        $s_table_container ="";
        $s_table_container .="
                <table  id='table-helpdesk' class='table  table-bordered table-hover' width='100%'  border='1px'>
                    <thead>
                          <tr >";
                               for($i=0;$i<count($a_title);$i++)
                               {
                                    $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                               }
        $s_table_container .="</tr>
                    </thead>";
        $s_table_container .="<tbody>";



        for($i=0;$i<count($a_data);$i++)
        {
            $s_button = "";

            $s_button =  "<button class='btn  btn-danger btn-sm button-delete'  record-id='{$a_data[$i]['hhID']}' >Hapus</button>";
            //untuk informasi jumlah soal
            $s_table_container .="<tr >";
            $s_table_container .= "<td  align='left' class='wilayah' >"
                                     .$a_data[$i]['unitHandle']."<br />"
                                     .$a_data[$i]['namaWilayah']
                                ."</td>";
            $s_table_container .= "<td  align='left' class='helpdeskName'>"
                                     .strtoupper ($a_data[$i]['helpdeskHandleName'])
                                ."</td>";
            $s_table_container .= "<td  > $s_button</td>";
            $s_table_container .="</tr>";
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
    }


    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                        <section class='page-head'>
                            <div class='row'>

                                <div style='float:left'>
                                    <h4>HELPDESK</h4>
                                </div>
                                <div style='float:right'>$BUTTON_MAIN </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                                <hr>
                            </div>
                        </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oHelpdesk->closeDB();
?>