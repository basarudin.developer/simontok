<?php
    /*
    |--------------------------------------------------------------------------
    | Helpdesk Handle Create 
    |--------------------------------------------------------------------------
    |Form untuk entry helpdesk handle secara manual entry
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oHelpdesk = new Helpdesk();
    $oGroup = new Group();
    $oUser = new UserInfo();

    $LAYOUT_JS_EXTENDED .= "

                    <script src='assets/bower_components/select2/dist/js/select2.full.min.js'></script>
                    <script src='modul/helpdesk/helpdesk.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/bower_components/select2/dist/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/adminlte.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-create' class='btn btn-flat  btn-sm btn-primary pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus-square-o'></i> Tambah
                        </button>
                        <button type='button' id='button-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> kembali
                        </button>
                    ";
    $s_form_input = "";

    $a_data_list_helpdesk = $oGroup->getListUserGroup(" WHERE A.groupID = 'GROUP_HELPDESK' ","","");

    $s_helpdesk = "<select class='form-control select2' id='helpdesk-id'   >";
                $s_helpdesk .= "<option value=''>Silahkan Pilih Helpdesk</option>";
                if(isset($a_data_list_helpdesk))
                {
                 
                    for($i=0;$i < count($a_data_list_helpdesk);$i++)
                    {
                        $s_helpdesk .= "<option value='{$a_data_list_helpdesk[$i]['userID']}'>{$a_data_list_helpdesk[$i]['realName']}</option>";
                    }   
                }
                $s_helpdesk .= "</select>";

    $a_data_list_wilayah = $oUser->getWilayahKerja("","","");

    $s_wilayah = "<select class='form-control select2' id='wilayah-id'  >";
                $s_wilayah .= "<option value=''>Silahkan Pilih Wilayah</option>";
                for($i=0;$i < count($a_data_list_wilayah);$i++)
                {
                    $s_wilayah .= "<option value='{$a_data_list_wilayah[$i]['idWilayah']}'>{$a_data_list_wilayah[$i]['idWilayah']} {$a_data_list_wilayah[$i]['namaWilayah']}</option>";
                }
                $s_wilayah .= "</select>";

    $s_form_input = "
                    <form id='form-create' action='' method='post'><div >
                            <!-- /.box-header -->
                            <div class='box-body'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                            <!-- Sistem Operasi -->
                                            <div class='form-group'>
                                                <label>Helpdesk:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-user'></i>
                                                    </div>
                                                    $s_helpdesk
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>

                                    <div class='col-md-12'>
                                            <!-- Sistem Operasi -->
                                            <div class='form-group'>
                                                <label>Wilayah:</label>
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa  fa-map-marker'></i>
                                                    </div>
                                                    $s_wilayah
                                                 </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
                        
            
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>TAMBAH HELPDESK HANDLE WILAYAH</h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oHelpdesk->closeDB();
    $oGroup->closeDB();
    $oUser->closeDB();

?>