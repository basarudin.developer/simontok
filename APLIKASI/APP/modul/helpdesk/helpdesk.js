
    jQuery('#button-create-helpdesk-handle').click(function() 
    {
        document.location='index.php?page=helpdesk&action=create';
    });
    /*************************************************
    |   BEGIN TABLE-HELPDESK
    **************************************************/
    if($('#table-helpdesk').length)
    {

        jQuery('#button-wilayah-perwilayah').click(function() 
        {
            document.location='index.php?page=helpdesk&action=perwilayah';
        });
        jQuery('#button-wilayah-perhelpdesk').click(function() 
        {
            document.location='index.php?page=helpdesk&action=perhelpdesk';
        });
        $('#table-helpdesk').DataTable
        ({        
            rowReorder: true,
            columnDefs: [
                { orderable: true, className: 'reorder', targets:[ 0, 1 ] },
                { orderable: false, targets: '_all' }
            ],
            lengthMenu: [[25, 50, -1], [25, 50, "All"]],
             order: [[ 0, "desc" ]]
        });
        
        $('#table-helpdesk tbody').on( 'click', '.button-delete', function () 
        {

            hh_id = $(this).attr('record-id');
            nama_helpdesk = $(this).parent().parent().find(".helpdeskName").html();  
            wilayah = $(this).parent().parent().find(".wilayah").html(); 
            $('#modal-content').html('apakah yakin menghapus  helpdesk <b>'+nama_helpdesk+'</b> yang menghandle wilayah '+wilayah+'?');
            $('#modal-container').modal('show');
            jQuery('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=helpdesk&type=model&action=delete",
                    dataType:"json",
                    data: 'hhID='+hh_id,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=helpdesk&action=list'
                                }, 1000);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                        
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html('terjadi kesalahan pengiriman data');
                            $('#notification-container').show(); 
                        }
                });//end of ajax
            });
            //document.location='index.php?page=helpdesk&action=delete&hhID='+hh_id;
        });
    }
    /*   END  TABLE-HELPDESK
    **************************************************/

    /*************************************************
    |   BEGIN HELPDESK HANDLE
    **************************************************/
    if($('#button-create').length)
    {
        
        if($('.select2').length)
        {
            $('.select2').select2();
        }


        jQuery('#button-create').click(function() 
        {
            //alert('asas');
            helpdesk_id = $('#helpdesk-id').val();
            wilayah_id = $('#wilayah-id').val();
            $.ajax({
                type: 'POST',
                url: "index.php?page=helpdesk&type=model&action=create",
                dataType:"json",
                data: 'helpdeskID='+helpdesk_id+'&wilayahID='+wilayah_id,
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        //alert(msg);
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();
                            setTimeout(function() {
                                document.location='index.php?page=helpdesk&action=list'
                            }, 1000);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#container-alert').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                    
                        $('#notification-container').removeClass('alert-success');
                        $('#notification-container').addClass('alert-danger'); 
                        $('#notification-content').html('terjadi kesalahan pengiriman data');
                        $('#notification-container').show(); 
                    }
            });//end of ajax
        });
    }
    /*   END HELPDESK HANDLE
    **************************************************/

    /*************************************************
    |   BEGIN TABLE-HELPDESK PERHELPDESK
    **************************************************/
    if($('#table-helpdesk-simplemode').length)
    {
        $('#table-helpdesk-simplemode').DataTable();
    }
    if($('#button-back').length)
    {
        jQuery('#button-back').click(function() 
        {
            document.location='index.php?page=helpdesk&action=list';
        });
    }

    