<?php
    /*
    |--------------------------------------------------------------------------
    | Komputer Model
    |--------------------------------------------------------------------------
    |model  modul komputer
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.notification.local.php");
    $oHelpdesk = new Helpdesk();
    $oNotification = new Notification();
    $oUser = new UserInfo();
    $oWilayah = new Wilayah();
    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "tidak tereksekusi";


    if(isset($_REQUEST['action']))
    {
        if($_REQUEST['action'] == 'delete')
        {
            if($_REQUEST['hhID'] == "")
            {
                $a_errors[] = " ID handle tidak ada";
            }
            if (!$a_errors) 
            {
                $a_data_helpdesk_handle = $oHelpdesk->getList(" WHERE HH.hhID = '{$_REQUEST['hhID']}' ","","");
                if($oHelpdesk->delete($_REQUEST['hhID']))
                {
                    $result['result'] = "success";
                    $result['desc'] = "helpdesk handle berhasil dihapus";
                    $s_notif_message = "{$USER[0]['realName']} menghapus helpdesk <span class='label label-danger'>{$a_data_helpdesk_handle[0]['helpdeskHandleName']}</span> pada wilayah kerja <span class='label label-danger'>{$a_data_helpdesk_handle[0]['namaWilayah']}</span>";
                    create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);

                    
                }
                else
                {
                    $result['result'] = "error";
                    $result['desc'] = "helpdesk handle gagal dihapus";
                }
            }
        }

        elseif($_REQUEST['action'] == 'create')
        {
            if($_REQUEST['helpdeskID'] == "")
            {
                $a_errors[] = " helpdesk harus  dipilih";
            }
            if($_REQUEST['wilayahID'] == "")
            {
                $a_errors[] = " wilayah harus dipilih";
            }
            if (!$a_errors) 
            {
                if($oHelpdesk->getCount(" WHERE `helpdeskID` = '{$_REQUEST['helpdeskID']}' AND `unitHandle` = '{$_REQUEST['wilayahID']}'  ") > 0)
                {
                    $result['result'] = "error";
                    $result['desc'] = "wilayah  sudah dihandle oleh helpdesk";
                }
                else
                {

                    if($oHelpdesk->create($_REQUEST['helpdeskID'],$_REQUEST['wilayahID']))
                    {
                        $result['result'] = "success";
                        $result['desc'] = "wilayah berhasil dihandle oleh helpdesk";
                        $a_data_wilayah = $oWilayah->getList(" WHERE child.idWilayah = '{$_REQUEST['wilayahID']}'  ","","");
                        $a_data_helpdesk = $oUser->getUserDetail($_REQUEST['helpdeskID']);


                        $s_notif_message = "{$USER[0]['realName']} menambah  helpdesk <span class='label label-success'>{$a_data_helpdesk[0]['realName']}</span> pada wilayah kerja <span class='label label-success'>{$a_data_wilayah[0]['namaWilayah']}</span>";
                        //echo $s_notif_message;
                        create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
                    }
                    else
                    {
                        $result['result'] = "error";
                        $result['desc'] = "wilayah gagal dihandle";
                    }   
                }
            }
        }
        if($a_errors)
        {
            $sError =  '';
            foreach ($a_errors as $error) {
                $sError .= "$error<br />";
            }
            $result['result'] = 'error';
            $result['desc'] = $sError;
        }  
        echo json_encode($result);
     }
    $oHelpdesk->closeDB();
    $oNotification->closeDB();
    $oUser->closeDB();
    $oWilayah->closeDB();

?>