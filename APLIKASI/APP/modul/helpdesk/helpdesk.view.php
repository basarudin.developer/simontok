<?php
    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    |view  modul helpdesk
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "list") 
        {
            require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.list.php");
        }
        elseif($_REQUEST['action'] == "create")
        { 
            require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.create.php");
        }
        elseif($_REQUEST['action'] == "perwilayah")
        { 
            require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.list.perwilayah.php");
        }
        elseif($_REQUEST['action'] == "perhelpdesk")
        { 
            require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.list.perhelpdesk.php");
        }
        elseif($_REQUEST['action'] != "")
        { 
            require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.list.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/helpdesk/helpdesk.list.php");
    }

?>