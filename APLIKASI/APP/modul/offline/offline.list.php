<?php
    /*
    |--------------------------------------------------------------------------
    | Tiket
    |--------------------------------------------------------------------------
    |Controler  modul tiket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");

    $oHelpdesk = new Helpdesk();
    $oWilayah = new Wilayah();
    $oGroup = new Group();
 
    //---table
    $a_title  = array();
    $a_title[] = "HELPDESK";
    $a_title[] = "";
    $a_title_class[] = " ";
    $a_title_class[] = " style='width:50px;' ";


    $s_wilayah_kerja = "";
    $a_wilayah_kerja =  array();


    $s_table_container = "";
    $s_condition = " WHERE false ";
    $s_limit = "  ";
    $s_order = "  ORDER BY `ticketRequestDate` DESC ";
    $s_group_name = '';

    $s_group_id_admin = 'SUPER_ADMINISTRATOR';
    $s_group_id_spv_it = 'GROUP_SPV_IT';



    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>
                    <script src='modul/offline/offline.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";
    if($oGroup->isGroup($USER[0]['userID'],$s_group_id_admin))
    {
        $s_group_name = "ADMINISTRATOR";
        //if($_isset($_REQUEST['']))
        //$a_wilayah_kerja = $oWilayah->getList(" WHERE child.idWilayah =  ")

        $s_condition = " WHERE true ";  
    }
    elseif($oGroup->isGroup($USER[0]['userID'],$s_group_id_spv_it))
    {
        $s_group_name = "SPVTI";  
        $a_wilayah_kerja = $oWilayah->getList(" WHERE child.idWilayah = '{$USER[0]['wilayahPenempatan']}' OR child.parentWilayah = '{$USER[0]['wilayahPenempatan']}' ","","");

        if(isset($a_wilayah_kerja))
        {
            if(count($a_wilayah_kerja)>0)
            {

                $s_wilayah_kerja .= "(";
                for($i=0;$i<count($a_wilayah_kerja);$i++)
                {
                    if($i == 0)
                    {
                        $s_wilayah_kerja .= "'{$a_wilayah_kerja[$i]['idWilayah']}'"; 
                    }
                    else
                    {
                        $s_wilayah_kerja .= ",'{$a_wilayah_kerja[$i]['idWilayah']}'"; 
                    }
                }
                $s_wilayah_kerja .= ")";
            } 
        }
        $s_condition = " WHERE HH.unitHandle IN {$s_wilayah_kerja} ";
    }

    $a_helpdesk = $oHelpdesk->getList($s_condition, " GROUP BY `helpdeskID` ","");
    if(isset($a_helpdesk))
     {
          
          $s_table_container ="";
          $s_table_container .="
                    <table  id='table-offline' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
          $s_table_container .="</tr>
                         </thead>";
          $s_table_container .="<tbody>";



               for($i=0;$i<count($a_helpdesk);$i++)
               {

                    $i_handle = 0;
                    $s_button = "";
                    $s_wilayah_handle = "";
                    $a_wilayah_handle = $oHelpdesk->getList( " WHERE HH.helpdeskID = '{$a_helpdesk[$i]['helpdeskID']}' " ,"","");


                    for($j=0;$j<count($a_wilayah_handle);$j++)
                    {
                        $label = "";
                        if($a_wilayah_handle[$j]['isActive'] == 1)
                        {
                            $i_handle++;
                            $label = "label-success";
                        }
                        else
                        {
                            $label = "label-danger";
                        }
                        $s_wilayah_handle .= " <span class='label $label'>{$a_wilayah_handle[$j]['namaWilayah']}</span> "; 
                    }

                    //jika wilayah yang aktif lebih dari 0 maka beri opsi offline kan
                    //jika jumlahnya 0 maka beri opsi online
                    if($i_handle>0)
                    {

                      $s_button =  "<button class='btn  btn-danger btn-sm button-offline'  record-id='{$a_helpdesk[$i]['helpdeskID']}' >Off - line</button>";
                    }
                    else
                    {

                      $s_button =  "<button class='btn  btn-success btn-sm button-online'  record-id='{$a_helpdesk[$i]['helpdeskID']}' >On - line</button>";   
                    }




                    
                    //untuk informasi jumlah soal
                    $s_table_container .="<tr >";
                    $s_table_container .= "<td  align='left'><p class='nama-helpdesk'>"
                                             .strtoupper ($a_helpdesk[$i]['helpdeskHandleName'])
                                             ."</p><br />".$s_wilayah_handle 
                                        ."</td>";
                    $s_table_container .= "<td  > $s_button</td>";
                    $s_table_container .="</tr>";
               }    
          $s_table_container .="</tbody>";
          $s_table_container .="</table>";
     }



    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>KETERSEDIAAN HELPDESK</h4>
                                </div>
                                <div style='float:right'></div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oHelpdesk->closeDB();
    $oWilayah->closeDB();
    $oGroup->closeDB();


?>