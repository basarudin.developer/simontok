<?php
    /*
    |--------------------------------------------------------------------------
    | Ticket
    |--------------------------------------------------------------------------
    |model  modul ticket
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    $PAGE_ID = "IND001";

    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");

    $oHelpdesk = new Helpdesk();
    $oWilayah = new Wilayah();
    $oGroup = new Group();



    $a_errors = array();
    $respone['result'] = 'error';
    $respone['desc'] = "tidak tereksekusi";
    $s_error = "";

    if(isset($_REQUEST['action']))
    {
        if($_REQUEST['action'] == 'setoffline')
        {
            
            if($_REQUEST['helpdesk_id'] == "")
            {
                $a_errors[] = "id helpdesk kosong";
            }

            //print_r($_REQUEST); exit();
            if (!$a_errors) 
            {
                    //mencari helpdesk menghandle wilayah mana saja
                    //jika wilayah yang dihandle mempunyai helpdesk cuma 1 yaitu yang offline maka set offline ditolak
                    $a_wilayah_handle =  $oHelpdesk->getList("  WHERE HH.helpdeskID = '{$_REQUEST['helpdesk_id']}' ", " ","");
                    if(isset($a_wilayah_handle))
                    {
                        if(count($a_wilayah_handle)>0)
                        {
                            $b_error = false;
                            for($i=0;$i<count($a_wilayah_handle);$i++)
                            {
                                //menghitung jumlah helpdesk dalam wilayah tersebut
                                //jika cuma 1 maka tidak boleh offline
                                $n_helpdesk = $oHelpdesk->getCount(" WHERE unitHandle = '{$a_wilayah_handle[$i]['unitHandle']}' AND isActive=1 ");
                                if($n_helpdesk <= 1)
                                {
                                    $b_error = true;
                                    $a_errors[] = "helpdesk tidak dapat melaksanakan offline di {$a_wilayah_handle[$i]['namaWilayah']} kerena tidak ada pengganti";
                                }
                            }
                            if(!$b_error)
                            {
                                if($oHelpdesk->setStatus($_REQUEST['helpdesk_id'],0))
                                {

                                    $respone['result'] = "success";
                                    $respone['desc'] = "helpdesk berhasil diset offline";
                                }
                                else
                                {

                                    $respone['result'] = "error";
                                    $respone['desc'] = "helpdesk gagal diset offline";
                                }
                            }
                            else
                            {

                                $respone['result'] = "error";
                                foreach ($a_errors as $error) {
                                    $s_error .= "$error  <br />";
                                }
                                $respone['desc']  = $s_error;
                            }
                        }
                        else
                        {
                            $respone['result'] = "error";
                            $respone['desc'] = "helpdesk tidak menghandle satupun wilayah kerja";
                        }

                    }
                    else
                    {

                        $respone['result'] = "error";
                        $respone['desc'] = "data helpdesk tidak ada";
                    }

            }


        }
        elseif($_REQUEST['action'] == 'setonline')
        {
            if($_REQUEST['helpdesk_id'] == "")
            {
                $a_errors[] = "id helpdesk kosong";
            }

            if (!$a_errors) 
            {
                if($oHelpdesk->setStatus($_REQUEST['helpdesk_id'],1))
                {

                    $respone['result'] = "success";
                    $respone['desc'] = "helpdesk berhasil diset online";
                }
                else
                {

                    $respone['result'] = "error";
                    $respone['desc'] = "helpdesk gagal diset online";
                }
            }
            else
            {
                $respone['result'] = "error";
                foreach ($a_errors as $error) {
                    $s_error .= "$error  <br />";
                }
                $respone['desc']  = $s_error;
            }
        }
        else
        {
            foreach ($a_errors as $error) {
                $s_error .= "$error  <br />";
            }
            $respone['desc']  = $s_error;
        }
    }
    echo json_encode($respone);
    $oHelpdesk->closeDB();
    $oWilayah->closeDB();
    $oGroup->closeDB();
?>