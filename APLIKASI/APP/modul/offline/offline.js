$(document).ready(function() {

    /*************************************************
    |   BEGIN TABLE-TIKET
    **************************************************/
    if($('#table-offline').length)
    {
        $('#table-offline').DataTable
        ({        
            rowReorder: false,
            ordering: false,
            lengthMenu: [[25, 50, -1], [25, 50, "All"]]
        });
        
        $('#table-offline tbody').on( 'click', '.button-offline', function () 
        {

            $('#notification-container').hide();
            helpdesk = $(this).parent().parent().find(".nama-helpdesk").html(); 
            $('#modal-content').html('apakah yakin <span class="label label-danger">menonaktifkan</span> sementara <b>'+helpdesk+'</b> ?');
            $('#modal-container').modal('show');

            helpdesk_id = $(this).attr('record-id');
            jQuery('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=offline&type=model&action=setoffline",
                    dataType:"json",
                    data: 'helpdesk_id='+helpdesk_id,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {

                            $('#modal-container').modal('hide');
                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=offline&action=list'
                                }, 1500);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                                $('#modal-container').modal('hide');
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                        }
                });//end of ajax
            });
        });

        $('#table-offline tbody').on( 'click', '.button-online', function () 
        {

            helpdesk = $(this).parent().parent().find(".nama-helpdesk").html(); 
            $('#modal-content').html('apakah yakin <span class="label label-success">mengaktifkan</span> <b>'+helpdesk+'</b> kembali ?');
            $('#modal-container').modal('show');
            helpdesk_id = $(this).attr('record-id');
            jQuery('#button-modal-ok').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=offline&type=model&action=setonline",
                    dataType:"json",
                    data: 'helpdesk_id='+helpdesk_id,
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {

                            $('#modal-container').modal('hide');
                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=offline&action=list'
                                }, 1500);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#container-alert').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                                $('#modal-container').modal('hide');
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                        }
                });//end of ajax
            });
            
        });
    }
    /*   END FORM MANUAL CREATE
    **************************************************/
    

});