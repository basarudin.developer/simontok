$(document).ready(function() {

    /*************************************************
    |   BEGIN TABLE-TIKET
    **************************************************/
    
    if($('#table-load-ticket').length)
    {
        $('#table-load-ticket').DataTable
        ({        
            rowReorder: false,
            ordering: true,
            lengthMenu: [[2, 5, -1], [2, 5, "All"]],
            columnDefs: [
                {targets: 1,className: 'bg-red '},
                {targets: 2,className: 'bg-yellow '},
                {targets: 3,className: 'bg-green '}
            ]
        });

    }

});