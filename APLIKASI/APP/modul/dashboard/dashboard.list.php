<?php
    /*
    |--------------------------------------------------------------------------
    | dashboard view
    |--------------------------------------------------------------------------
    |list dashboard 
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.computer.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.device.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.helpdesk.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.chart.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.ticket.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.date.php");

    $oGroup = new Group();
    $oHelpdesk = new Helpdesk();
    $oKomputer = new Computer();
    $oUser = new UserInfo();
    $oDevice = new Device();
    $oTicket = new Ticket();
    $oWilayah = new Wilayah();


    $LAYOUT_JS_EXTENDED .= "
                    

                    <!-- ChartJS -->
                    <!--<script src='assets/bower_components/chart.js/Chart.js'></script>-->
                    <script src='assets/bower_components/chart.js/Chart.js'></script>


                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/dashboard/dashboard.js'></script>

                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='modul/dashboard/chart.css'>
                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    
                    ";

    $s_group_id_admin = 'SUPER_ADMINISTRATOR';
    $s_group_id_helpdesk = 'GROUP_HELPDESK';
    $s_group_id_spvti = 'GROUP_SPV_IT';
    $s_where_komputer = " WHERE komputerStatus != 9 ";


    $pie_chart_sdk_container  = "";
    $pie_chart_sdk_js  = "";

    $pie_chart_brand_container  = "";
    $pie_chart_brand_js  = "";

    $pie_chart_screen_container  = "";
    $pie_chart_screen_js  = "";

    $s_dashboard_device_container = "";

    $s_table_load_ticket = "";
    $a_helpdesk = array();
    $flag_ticket_open = 0;
    $flag_ticket_pending_persetujuan = 2;
    $flag_ticket_pending_material = 3;
    $flag_ticket_closed = 8;


    if($oGroup->isGroup($USER[0]['userID'],$s_group_id_admin))
    {
        $s_group_name = "ADMINISTRATOR"; 
        $s_condition = " WHERE true ";       
        /*
        //  -------------------BEGIN SDK-------------------
        */

        $a_sdk_flag =  array();
        $a_sdk_value =  array();
        $column_name = 'deviceSDK';
        $a_device_sdk = $oDevice->getCountDashboard($column_name);
        if(isset($a_device_sdk))
        {
            if(count($a_device_sdk) > 0)
            {
                for($i=0;$i<count($a_device_sdk);$i++)
                {
                    $a_sdk_flag[] = "SDK ".$a_device_sdk[$i][$column_name];
                    $a_sdk_value[] = $a_device_sdk[$i]['total'];
                }

                $sdk_pie_chart = new Chart('pie', $column_name.'_chart');
                $sdk_pie_chart->set('data', $a_sdk_value);
                $sdk_pie_chart->set('legend', $a_sdk_flag);
                $sdk_pie_chart->set('displayLegend', true);
                //return js dan return container tidak boleh terbalik urutannya
                $pie_chart_sdk_js =  $sdk_pie_chart->returnJS();
                $pie_chart_sdk_container =  $sdk_pie_chart->returnContainer();
                $LAYOUT_JS_EXTENDED .= '<script>'.$pie_chart_sdk_js.'</script>';
            }

        }

        /*  -------------------END SDK-------------------*/


        /*
        //  -------------------BEGIN BRAND SIZE-------------------
        */
        $a_brand_flag =  array();
        $a_band_value =  array();
        $column_name = 'deviceMerk';
        $a_device_brand = $oDevice->getCountDashboard($column_name);
        if(isset($a_device_brand))
        {

            if(count($a_device_brand) > 0)
            {
                for($i=0;$i<count($a_device_brand);$i++)
                {
                    $a_brand_flag[] = $a_device_brand[$i][$column_name];
                    $a_band_value[] = $a_device_brand[$i]['total'];
                }

                $brand_pie_chart = new Chart('pie', $column_name.'_chart');
                $brand_pie_chart->set('data', $a_band_value);
                $brand_pie_chart->set('legend', $a_brand_flag);
                $brand_pie_chart->set('displayLegend', true);
                //return js dan return container tidak boleh terbalik urutannya
                $pie_chart_brand_js =  $brand_pie_chart->returnJS();
                $pie_chart_brand_container =  $brand_pie_chart->returnContainer();
                $LAYOUT_JS_EXTENDED .= '<script>'.$pie_chart_brand_js.'</script>';
            }
        }
        /*  -------------------END BRAND SIZE-------------------*/

        /*
        //  -------------------BEGIN SCREEN SIZE-------------------
        */
        $a_screen_flag =  array();
        $a_screen_value =  array();
        $column_name = 'screenSize';
        $a_device_screen = $oDevice->getCountDashboard($column_name);
        if(isset($a_device_screen))
        {

            if(count($a_device_screen) > 0)
            {
                for($i=0;$i<count($a_device_screen);$i++)
                {
                    $a_screen_flag[] = $a_device_screen[$i][$column_name]." inch";
                    $a_screen_value[] = $a_device_screen[$i]['total'];
                }

                $screen_pie_chart = new Chart('pie', $column_name .'_chart');
                $screen_pie_chart->set('data', $a_screen_value);
                $screen_pie_chart->set('legend', $a_screen_flag);
                $screen_pie_chart->set('displayLegend', true);
                //return js dan return container tidak boleh terbalik urutannya
                $pie_chart_screen_js =  $screen_pie_chart->returnJS();
                $pie_chart_screen_container =  $screen_pie_chart->returnContainer();
                $LAYOUT_JS_EXTENDED .= '<script>'.$pie_chart_screen_js.'</script>';
            }
            /*  -------------------END SCREEN SIZE-------------------*/
        }

    }
    elseif($oGroup->isGroup($USER[0]['userID'],$s_group_id_helpdesk))
    {
        $s_group_name = "HELPDESK";
        $s_where_helpdesk = " WHERE helpdeskID = '{$USER[0]['userID']}' ";
        $s_where_komputer = " WHERE komputerStatus != 9  AND helpdeskGroupHandle = '".$USER[0]['wilayahPenempatan']."'";
        $i_count_wilayah_handle = $oHelpdesk->getCount($s_where_helpdesk);  
        if( $i_count_wilayah_handle > 0)
        {
            $a_wilayah_kerja = $oHelpdesk->getList($s_where_helpdesk,"","");
            for($i=0;$i<$i_count_wilayah_handle;$i++)
            {
                if($i == 0)
                {
                    $s_wilayah_kerja = "'".$a_wilayah_kerja[$i]['unitHandle']."'";
                }
                else
                {
                    $s_wilayah_kerja .= ","."'".$a_wilayah_kerja[$i]['unitHandle']."'";   
                }
            }
            $s_wilayah_kerja = "(".$s_wilayah_kerja.")";
            $s_condition = " WHERE wilayahPenempatan IN {$s_wilayah_kerja} ";
        }
        else
        {

            $s_condition = " WHERE false; ";
        }


    }
    elseif($oGroup->isGroup($USER[0]['userID'],$s_group_id_spvti))
    {
        $s_group_name = "SPV TI";
        $s_where_komputer = " WHERE komputerStatus != 9  AND helpdeskGroupHandle = '".$USER[0]['wilayahPenempatan']."'";




        
        $a_data_child = $oWilayah->getList(" WHERE child.parentWilayah = '{$USER[0]['wilayahPenempatan']}' ", " ORDER BY child.idWilayah ", "");
        $s_child_wilayah = "";
        if(isset($a_data_child))
        {
            for($j=0;$j<count($a_data_child);$j++)
            {
                $s_child_wilayah .= ",'{$a_data_child[$j]['idWilayah']}' ";
            }
        }
        $s_wilayah_kerja = "('{$USER[0]['wilayahPenempatan']}' $s_child_wilayah)";
        $s_condition = " WHERE wilayahPenempatan IN {$s_wilayah_kerja} ";

        /*
        //  -------------------BEGIN TABLEE LOAD TICKET-------------------
        */

        //untuk load ticket
        $s_condition_list_helpdesk = " WHERE HH.unitHandle IN {$s_wilayah_kerja} ";
        $a_helpdesk = $oHelpdesk->getList($s_condition_list_helpdesk, " GROUP BY `helpdeskID` ","");

        if(isset($a_helpdesk))
        {

            //---table
            $a_title  = array();
            $a_title[] = "HELPDESK";
            $a_title[] = "OPEN";
            $a_title[] = "PENDING";
            $a_title[] = "CLOSED";
            $a_title_class[] = " ";
            $a_title_class[] = " style='width:50px;' ";
            $a_title_class[] = " style='width:50px;' ";
            $a_title_class[] = " style='width:50px;' ";

            $s_table_container_ticket_load ="
            <!-- Info boxes -->
            <div class='row'>
                <div class='col-md-12 col-sm-12 col-xs-12'>
                    <div class='info-box'>
                        <div class='box-header with-border'>
                            <h3 class='box-title'>LOAD KERJA HELPDESK  ".strtoupper(getMonth(date('m'))).date(" Y"). "</h3>
                        </div>
                        <div class='box-body'>
                            ";

            $s_table_container_ticket_load .="
                    <table  id='table-load-ticket' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container_ticket_load .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
            $s_table_container_ticket_load .="</tr>
                         </thead>";
            $s_table_container_ticket_load .="<tbody>";



            for($i=0;$i<count($a_helpdesk);$i++)
            {

                $i_handle = 0;
                $s_wilayah_handle = "";
                $a_wilayah_handle = $oHelpdesk->getList( " WHERE HH.helpdeskID = '{$a_helpdesk[$i]['helpdeskID']}' " ,"","");


                for($j=0;$j<count($a_wilayah_handle);$j++)
                {
                    $label = "";
                    if($a_wilayah_handle[$j]['isActive'] == 1)
                    {
                        $i_handle++;
                        $label = "label-primary";
                    }
                    else
                    {
                        $label = "label-danger";
                    }
                    $s_wilayah_handle .= " <span class='label $label'>{$a_wilayah_handle[$j]['namaWilayah']}</span> "; 
                }




                $i_ticket_open = $oTicket->getCount(" WHERE `ticket`.ticketHelpdeskHandle = '{$a_helpdesk[$i]['helpdeskID']}' AND `ticket`.ticketStatus  = '{$flag_ticket_open}' ");

                $i_ticket_pending = $oTicket->getCount(" WHERE `ticket`.ticketHelpdeskHandle = '{$a_helpdesk[$i]['helpdeskID']}' AND (`ticket`.ticketStatus  = '{$flag_ticket_pending_persetujuan}' OR `ticket`.ticketStatus  = '{$flag_ticket_pending_material}') AND  YEAR(`ticketSolveDateStart`) = '".date('Y')."' AND  MONTH(`ticketSolveDateStart`) = '".date('n')."'  ");
                $i_ticket_closed = $oTicket->getCount(" WHERE `ticket`.ticketHelpdeskHandle = '{$a_helpdesk[$i]['helpdeskID']}' AND (`ticket`.ticketStatus  = '{$flag_ticket_pending_persetujuan}' OR `ticket`.ticketStatus  = '{$flag_ticket_closed}') AND  YEAR(`ticketSolveDateStart`) = '".date('Y')."' AND  MONTH(`ticketSolveDateStart`) = '".date('n')."'  ");
                    //untuk informasi jumlah soal
                $s_table_container_ticket_load .="<tr >";
                $s_table_container_ticket_load .= "<td  align='left'><p class='nama-helpdesk'>"
                                         .strtoupper ($a_helpdesk[$i]['helpdeskHandleName'])
                                         ."</p>".$s_wilayah_handle 
                                    ."</td>";
                $s_table_container_ticket_load .= "<td  > $i_ticket_open</td>";
                $s_table_container_ticket_load .= "<td  > $i_ticket_pending</td>";
                $s_table_container_ticket_load .= "<td  > $i_ticket_closed</td>";
                $s_table_container_ticket_load .="</tr>";
            }    
            $s_table_container_ticket_load .="</tbody>";
            $s_table_container_ticket_load .="</table>";
            $s_table_container_ticket_load .="

                        </div>
                    </div>
            <!-- Info boxes -->
                </div>
            </div>";


        }

        /*  -------------------END  TABLE TICKET LOAD------------------- */
 
    }

    $i_jumlah_pegawai = $oUser->getUserCount($s_condition );
    $a_device = $oDevice->getList($s_condition ,"","");
    if($s_group_name != "ADMINISTRATOR")
    {
        $s_condition = " WHERE U.wilayahPenempatan IN {$s_wilayah_kerja} ";
    } 
    if(isset($a_device) )
    {
       $i_jumlah_device = count($a_device); 
    }
    else
    {
        $i_jumlah_device  = 0;
    }
    
    $i_jumlah_komputer = $oKomputer->getCount($s_where_komputer);






    /*
    //  -------------------BEGIN OS-------------------
    */
    $computer_os_chart_container  = "";
    $computer_os__chart_js  = "";
    $a_computer_os_flag =  array();
    $a_computer_os_value =  array();
    $column_name = 'komputerSpekMerk';
    $s_where_os = $s_where_komputer." AND KS.komputerItemID='{$a_komputer_item['SISTEM_OPERASI']}' AND K.komputerStatus != 9 AND KS.komputerSpekStatus !=9";  
    $a_computer_os = $oKomputer->getCountSpekDashboard($column_name,$s_where_os);
    if(isset($a_computer_os))
    {

        if(count($a_computer_os) > 0)
        {
            for($i=0;$i<count($a_computer_os);$i++)
            {
                $a_computer_os_flag[] = $a_computer_os[$i][$column_name];
                $a_computer_os_value[] = $a_computer_os[$i]['total'];
            }

            $os_chart = new Chart('pie', $column_name.'_chart');
            $os_chart->set('data', $a_computer_os_value);
            $os_chart->set('legend', $a_computer_os_flag);
            $os_chart->set('displayLegend', true);
            //return js dan return container tidak boleh terbalik urutannya
            $computer_os__chart_js =  $os_chart->returnJS();
            $computer_os_chart_container =  $os_chart->returnContainer();
            $LAYOUT_JS_EXTENDED .= '<script>'.$computer_os__chart_js.'</script>';
            unset($os_chart);
        }
    }
    /*  -------------------END OS-------------------*/



    /*
    //  -------------------BEGIN KOMPUTER POSISI-------------------
    */
    $computer_posisi_chart_container  = "";
    $computer_poisi_chart_js  = "";
    $a_computer_posisi_flag =  array();
    $a_computer_posisi_value =  array();
    $column_name = 'komputerPosisi';
    $a_computer_posisi = $oKomputer->getCountKomputerDashboard($column_name,$s_where_komputer);
    if(isset($a_computer_posisi))
    {
        
        if(count($a_computer_posisi) > 0)
        {
            for($i=0;$i<count($a_computer_posisi);$i++)
            {
                $a_computer_posisi_flag[] = $a_computer_posisi[$i][$column_name];
                $a_computer_posisi_value[] = $a_computer_posisi[$i]['total'];
            }

            $posisi_chart = new Chart('pie', $column_name.'_chart');
            $posisi_chart->set('data', $a_computer_posisi_value);
            $posisi_chart->set('legend', $a_computer_posisi_flag);
            $posisi_chart->set('displayLegend', true);
            //return js dan return container tidak boleh terbalik urutannya
            $computer_poisi_chart_js =  $posisi_chart->returnJS();
            $computer_posisi_chart_container =  $posisi_chart->returnContainer();
            $LAYOUT_JS_EXTENDED .= '<script>'.$computer_poisi_chart_js.'</script>';
            unset($posisi_chart);
        }
    }
    /*  -------------------END KOMPUTER POSISI-------------------*/





    /*
    //  -------------------BEGIN PERMINTAAN TIKET-------------------
    */
    $ticket_chart_container  = "";
    $ticket_chart_js  = "";
    $a_ticket_flag =  array();
    $a_ticket_value =  array();
    $max_date = date('d');
    for($i=1;$i<=$max_date;$i++)
    {
        $number_date = str_pad($i, 2, "0", STR_PAD_LEFT);
        if($s_group_name == "ADMINISTRATOR" )
        {
            $s_where_ticket = " WHERE  date(`ticketRequestDate`) = '".date('Y-m-')."$number_date' ";
        }
        elseif( $s_group_name == "HELPDESK"  )
        {
            $s_where_ticket = " WHERE  ticketHelpdeskHandle = '{$USER[0]['userID']}' AND date(`ticketRequestDate`) = '".date('Y-m-')."$number_date' ";
        }
        elseif( $s_group_name == "SPV TI"  )
        {
            $s_where_ticket = " WHERE  UR.wilayahPenempatan IN {$s_wilayah_kerja} AND date(`ticketRequestDate`) = '".date('Y-m-')."$number_date' ";
        }
        $a_ticket_flag[] = $number_date;
        $a_ticket_value[] = $oTicket->getCount($s_where_ticket );
    }

    $ticket_line_chart = new Chart('line', 'request_ticket_chart');
    $ticket_line_chart->set('data', $a_ticket_value);
    $ticket_line_chart->set('legend', $a_ticket_flag);
    $ticket_line_chart->set('displayLegend', false);
    //return js dan return container tidak boleh terbalik urutannya
    $ticket_chart_js =  $ticket_line_chart->returnJS();
    $ticket_chart_container =  $ticket_line_chart->returnContainer();
    $LAYOUT_JS_EXTENDED .= '<script>'.$ticket_chart_js.'</script>';

    /*  -------------------END PERMINTAAN TIKET-------------------*/

    $table_ticket_load = "";
    if(isset($s_table_container_ticket_load))
    {
        $table_ticket_load = $s_table_container_ticket_load; 
    }

    $s_dashboard_common_container= "
            <!-- Info boxes -->
            <div class='row'>
                <div class='col-md-4 col-sm-4 col-xs-12'>
                    <div class='info-box'>
                        <span class='info-box-icon bg-green'><i class='fa fa-group '></i></span>

                        <div class='info-box-content'>
                            <span class='info-box-text'>Jumlah Pegawai</span>
                            <span class='info-box-number'>{$i_jumlah_pegawai}<small> orang</small></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class='col-md-4 col-sm-4 col-xs-12'>
                    <div class='info-box'>
                        <span class='info-box-icon bg-yellow'><i class='fa fa-tv'></i></span>

                        <div class='info-box-content'>
                            <span class='info-box-text'>Jumlah Komputer</span>
                            <span class='info-box-number'>{$i_jumlah_komputer}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class='col-md-4 col-sm-4 col-xs-12'>
                    <div class='info-box'>
                        <span class='info-box-icon bg-aqua'><i class='fa fa-android'></i></span>

                        <div class='info-box-content'>
                            <span class='info-box-text'>Jumlah Device Register</span>
                            <span class='info-box-number'>{$i_jumlah_device}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->               
            </div>
            <!-- /.row -->
                            ";

    if($s_group_name == "ADMINISTRATOR" )
    {
        $s_dashboard_device_container= "
            <!-- Info boxes -->
            <div class='row '>
                <div class='col-md-4 col-sm-4 col-xs-12 '>
                    <div class='info-box'>
                        <div class='box-header with-border'>
                            <h3 class='box-title'>ANDROID SDK</h3>
                        </div>
                        <div class='box-body'>
                            {$pie_chart_sdk_container}
                        </div>
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->   
                <div class='col-md-4 col-sm-4 col-xs-12 '>
                    <div class='info-box'>
                        <div class='box-header with-border'>
                            <h3 class='box-title'>MERK PERANGKAT</h3>
                        </div>
                        <div class='box-body'>
                            {$pie_chart_brand_container}
                        </div>
                    </div>

                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                
                <div class='col-md-4 col-sm-4 col-xs-12 '>
                    <div class='info-box'>
                        <div class='box-header with-border'>
                            <h3 class='box-title'>UKURAN LAYAR </h3>
                        </div>
                        <div class='box-body'>
                            {$pie_chart_screen_container}
                        </div>
                    </div>

                    <!-- /.info-box -->
                </div>
                <!-- /.col -->      
            </div>
            <!-- /.row -->
        ";
    }
        


    $s_dashboard_computer_container= "
            <!-- Info boxes -->
            <div class='row '>
                <div class='col-md-8 col-sm-8 col-xs-12 '>
                    <div class='info-box'>
                        <div class='box-header with-border'>
                            <h3 class='box-title'>SISTEM OPERASI</h3>
                        </div>
                        <div class='box-body'>
                            {$computer_os_chart_container}
                        </div>
                    </div>

                    <!-- /.info-box -->
                </div>
                <!-- /.col --><div class='col-md-4 col-sm-4 col-xs-12 '>
                    <div class='info-box'>
                        <div class='box-header with-border'>
                            <h3 class='box-title'>JENIS KOMPUTER</h3>
                        </div>
                        <div class='box-body'>
                            {$computer_posisi_chart_container}
                        </div>
                    </div>

                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                
                      
            </div>
            <!-- /.row -->
        ";
    $s_dashboard_ticket_container= "
            <!-- Info boxes -->
            <div class='row '>
                <div class='col-md-12 col-sm-12 col-xs-12 '>
                    <div class='info-box'>
                        <div class='box-header with-border'>
                            <h3 class='box-title'>PERMINTAAN LAYANAN HELPDESK</h3>
                        </div>
                        <div class='box-body'>
                            {$ticket_chart_container}
                        </div>
                    </div>

                    <!-- /.info-box -->
                </div>
                
                      
            </div>
            <!-- /.row -->
        ";

    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>DASHBOARD</h4>
                                </div>
                                <div style='float:right'></div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_dashboard_common_container}
                              {$s_dashboard_device_container}
                              {$s_dashboard_computer_container}
                              {$s_table_container_ticket_load}
                              {$s_dashboard_ticket_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";

    $oKomputer->closeDB();
    $oUser->closeDB();
    $oDevice->closeDB();
    $oGroup->closeDB();
    $oTicket->closeDB();
    $oHelpdesk->closeDB();
    $oWilayah->closeDB();

?>