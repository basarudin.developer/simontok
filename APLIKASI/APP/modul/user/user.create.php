<?php
    /*
    |--------------------------------------------------------------------------
    | User Create 
    |--------------------------------------------------------------------------
    |Form untuk entry pengguna secara manual entry
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.wilayah.php");
    $oUser = new UserInfo();
    $oWilayah = new Wilayah();


    //*******************BEGIN USER OPTION********************
    $i = 0;
    $a_hak_akses[$i]['name'] = 'ADMINISTRATOR';
    $a_hak_akses[$i]['desc'] = 'mengatur jalannya semua sistem termasuk membuat data master';
    $a_hak_akses[$i]['image'] = 'assets/img/admin.png';
    $a_hak_akses[$i]['value'] = 'SUPER_ADMINISTRATOR';
    $a_hak_akses[$i]['checked'] = '';
    $a_hak_akses[$i]['selected'] = '';

    $i++;
    $a_hak_akses[$i]['name'] = 'HELPDESK';
    $a_hak_akses[$i]['desc'] = 'melaksanakan permintaan tiket pegawai';
    $a_hak_akses[$i]['image'] = 'assets/img/helpdesk.png';
    $a_hak_akses[$i]['value'] = 'GROUP_HELPDESK';
    $i++;
    $a_hak_akses[$i]['name'] = 'SUPERVISOR IT';
    $a_hak_akses[$i]['desc'] = 'menyetujui adanya perubahan upgrade pada komputer';
    $a_hak_akses[$i]['image'] = 'assets/img/spvti.png';
    $a_hak_akses[$i]['value'] = 'GROUP_SPV_IT';
    $i++;
    $a_hak_akses[$i]['name'] = 'PEGAWAI';
    $a_hak_akses[$i]['desc'] = 'membuat tiket permintaan layanan helpdesk';
    $a_hak_akses[$i]['image'] = 'assets/img/karyawan.png';
    $a_hak_akses[$i]['value'] = '';
    $i++;
    //*******************END USER OPTION********************

    $LAYOUT_JS_EXTENDED .= "

                    <!-- InputMask -->
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.date.extensions.js'></script>
                    <script src='assets/bower_components/inputmask/plugins/jquery.inputmask.extensions.js'></script>
                    <script src='assets/bower_components/select2/dist/js/select2.full.min.js'></script>
                    <script src='modul/user/user.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/bower_components/select2/dist/css/select2.min.css'>
                    <link rel='stylesheet' href='assets/css/adminlte.css'>
                    <link rel='stylesheet' href='modul/user/user.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-create-user-apply' class='btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-plus'></i> Simpan
                        </button>
                        <button type='button' id='button-user-back' class='btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                            <i class='fa fa-caret-left'></i> kembali
                        </button>
                    ";

    $s_hak_akses_container = "";
    if(isset($a_hak_akses))
    {
        if(count($a_hak_akses) > 0)
        {

            $s_hak_akses_container = "<div class='col-md-12'><label>Hak Akses:</label></div><div class='clear'></div>
                                        <div class='row form-group product-chooser row-table'>
                                         ";
            for($i=0;$i<count($a_hak_akses);$i++)
            {
                $s_hak_akses_container .= "
                                        <div class='col-xs-12 col-sm-12 col-md-3 col-lg-3  col-table' >
                                            <div class='product-chooser-item   col-content '>
                                                <img src='{$a_hak_akses[$i]['image']}' class='img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12' alt='Mobile and Desktop'>
                                                <div class='col-xs-8 col-sm-8 col-md-12 col-lg-12'>
                                                    <span class='title'>{$a_hak_akses[$i]['name']}</span>
                                                    <span class='description'>{$a_hak_akses[$i]['desc']} </span>
                                                    <input type='radio' name='hak_akses' value='{$a_hak_akses[$i]['value']}' >
                                                </div>
                                                <div class='clear'></div>
                                            </div>
                                        </div>";
            }
            $s_hak_akses_container .= "</div>";
        }
    }
    $a_data_wilayah = $o_data_wilayah = $oWilayah->getList("","","");
    $s_wilayah ="";
    if(isset($a_data_wilayah))
    {
        $s_wilayah = "<select class='form-control select2' name='wilayah'  id='wilayah_umum' >";
        $s_wilayah .= "<option value=''>Silahkan Pilih Wilayah Kerja</option>";
        for($i=0;$i < count($a_data_wilayah);$i++)
        {
            $s_wilayah .= "<option value='{$a_data_wilayah[$i]['idWilayah']}'>{$a_data_wilayah[$i]['namaWilayah']}</option>";
        }
        $s_wilayah .= "</select>";
    }
    $s_wilayah_helpdesk ="";
    $a_data_wilayah_helpdesk  = $oWilayah->getList("  WHERE child.parentWilayah is null ","","");
    if(isset($a_data_wilayah_helpdesk))
    {
        $s_wilayah_helpdesk = "<select class='form-control select2' name='wilayah_helpdesk'  id='wilayah_helpdesk' style='display:none' >";
        $s_wilayah_helpdesk .= "<option value=''>Silahkan Pilih Area Kerja </option>";
        for($i=0;$i < count($a_data_wilayah_helpdesk);$i++)
        {
            $s_wilayah_helpdesk .= "<option value='{$a_data_wilayah_helpdesk[$i]['idWilayah']}'>{$a_data_wilayah_helpdesk[$i]['namaWilayah']}</option>";
        }
        $s_wilayah_helpdesk .= "</select>";
    }
    $s_form_input = "";

    $s_form_input = "
                    <form id='form-user-create' action='' method='post'>
                        <div >
                            <!-- /.box-header -->
                            <div class='box-body' >
        
                                {$s_hak_akses_container}
                                <div class='row'>
                                   <div class='col-md-6'>
                                           <!-- Hostname -->
                                           <div class='form-group'>
                                               <label>NIP:</label>
                                               <div class='input-group'>
                                                   <div class='input-group-addon'>
                                                       <i class='fa fa-user'></i>
                                                   </div>
                                                   <input type='text' class='form-control' name='userID'>
                                               </div>
                                               <!-- /.input group -->
                                           </div>
                                           <!-- /.form group -->
                                   </div>
                                   <div class='col-md-6'>
                                           <!-- Hostname -->
                                           <div class='form-group'>
                                               <label>Nama:</label>
                                               <div class='input-group'>
                                                   <div class='input-group-addon'>
                                                       <i class='fa fa-text-width'></i>
                                                   </div>
                                                   <input type='text' class='form-control' name='realName'>
                                               </div>
                                               <!-- /.input group -->
                                           </div>
                                           <!-- /.form group -->
                                   </div>
                                   <div class='col-md-6'>
                                           <!-- Hostname -->
                                           <div class='form-group'>
                                               <label>Email:</label>
                                               <div class='input-group'>
                                                   <div class='input-group-addon'>
                                                       <i class='fa fa-envelope'></i>
                                                   </div>
                                                   <input type='text' class='form-control'  name='mail'>
                                               </div>
                                               <!-- /.input group -->
                                           </div>
                                           <!-- /.form group -->
                                   </div>
                                   <div class='col-md-6'>
                                           <!-- Kategori -->
                                           <div class='form-group'>
                                               <label>Wilayah Kerja:</label>
                                               <div class='input-group'>
                                                   <div class='input-group-addon'>
                                                       <i class='fa  fa-home'></i>
                                                   </div>
                                                   $s_wilayah
                                                   $s_wilayah_helpdesk
                                               </div>
                                               <!-- /.input group -->
                                           </div>
                                           <!-- /.form group -->
                                   </div>
                                   
                               </div>
                                <!-- /.row -->
                                <div class='row'>
                                    
                                    <div class='col-md-12'>
                                            <!-- Prosesor -->
                                            <div class='form-group'>
                                                <label>Username</label>


                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-user'></i>
                                                    </div>
                                                    <input  name='username' class='form-control'  placeholder='Username'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    <div class='col-md-6'>
                                            <!-- Prosesor -->
                                            <div class='form-group'>
                                                <label>Password</label>


                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-ellipsis-h'></i>
                                                    </div>
                                                    <input type='password' name='password' class='form-control'  placeholder='Password'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    
                                    <div class='col-md-6'>
                                            <!-- Prosesor -->
                                            <div class='form-group'>
                                                <label>Ulangi Password</label>

                                                
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-ellipsis-h'></i>
                                                    </div>
                                                    <input type='password' name='password-replay' class='form-control'  placeholder='Password'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    
                                </div>
                                <!-- /.row -->
                                
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </form>";
                        
            
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>MENAMBAH PENGGUNA</h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>
                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oUser->closeDB();
    $oWilayah->closeDB();
?>