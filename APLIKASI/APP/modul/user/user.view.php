<?php
    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    |view  modul user
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */

    if(isset($_REQUEST['action']))
    {
        if ($_REQUEST['action'] == "list") 
        {
            require_once($SYSTEM['DIR_MODUL']."/user/user.list.php");
        } 
        elseif($_REQUEST['action'] == "privileges")
        { 
            require_once($SYSTEM['DIR_MODUL']."/user/user.privileges.php");
        }
        elseif($_REQUEST['action'] == "update")
        { 
            require_once($SYSTEM['DIR_MODUL']."/user/user.update.php");
        }
        elseif($_REQUEST['action'] == "delete")
        { 
            require_once($SYSTEM['DIR_MODUL']."/user/user.delete.php");
        }
        elseif($_REQUEST['action'] == "create")
        { 
            require_once($SYSTEM['DIR_MODUL']."/user/user.create.php");
        }
        elseif($_REQUEST['action'] == "ganti_password")
        { 
            require_once($SYSTEM['DIR_MODUL']."/user/user.password.php");
        }
        elseif($_REQUEST['action'] != "")
        { 
            require_once($SYSTEM['DIR_MODUL']."/user/user.list.php");
        }
    }
    else
    {
        require_once($SYSTEM['DIR_MODUL']."/user/user.list.php");
    }

?>