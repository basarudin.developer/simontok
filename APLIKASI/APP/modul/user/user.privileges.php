<?php
/**
 *
 * @version		1.0
 * @author 		basarudin
 * @created     Juli 05 ,2015
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/
 
    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/user/user.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";

    //---table
    $a_title  = array();
    $a_title[] = "<input type='checkbox' class='group-checkable' data-set='#table-group .checkboxes'/>";
    $a_title[] = "NAMA";
    $a_title[] = "KETERANGAN";
    $a_title_class[] = " style='width:10px;' class='table-checkbox' ";
    $a_title_class[] = " style='width:200px;' ";
    $a_title_class[] = " style='' ";

    $s_table_container = "";
    $s_button_add_user = "";

    $s_condition = " WHERE true ";
    $s_limit = "  ";
    $s_order = " ";


    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.array.php");
    $oGroup = new Group();
    if(isset($_REQUEST['userID']))
    {
        $a_data = $oGroup->getList($s_condition, $s_order, $s_limit);
        $a_data_group_user =  $oGroup->getListUserGroup(" WHERE C.userID='{$_REQUEST['userID']}' ", $s_order, $s_limit);
        $s_table_container ="<form id='form-privileges' action='' method='post'>";
        $s_table_container .="
                    <table  id='table-group' class='table  table-bordered ' width='100%' border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
        $s_table_container .="</tr>
                 </thead>";
        $s_table_container .="<tbody>";
        for($i=0;$i<count($a_data);$i++)
        {
            if($a_data[$i]['groupID'] != $COMMON_GROUP)
            {
                $s_checked = "";
                $s_active = "";
                if(count($a_data_group_user)>0)
                {
                    if(deepInArray($a_data[$i]['groupID'], $a_data_group_user))
                        {
                            $s_checked = " checked ";
                            $s_active = " bg-yellow ";
                        }
                }
                //untuk informasi jumlah soal
                $s_table_container .="<tr class='$s_active'>";
                $s_table_container .= "<td  align='left'>"
                                        ."<input $s_checked  type='checkbox' class='checkboxes' name='group[]' value='{$a_data[$i]['groupID']}'/>"
                                   ."</td>";
                $s_table_container .= "<td  align='left'>"
                                        .strtoupper ($a_data[$i]['name'])
                                   ."</td>";
                $s_table_container .= "<td  align='left'>"
                                        .$a_data[$i]['notes']
                                   ."</td>";
                $s_table_container .="</tr>";
            }
                
        }    
        $s_table_container .="</tbody>";
        $s_table_container .="</table>";
        $s_table_container .="<input   type='hidden'  name='group[]' value='$COMMON_GROUP'>";


        $s_table_container .="<input type='hidden' name='userID' value='{$_REQUEST['userID']}'>";
        $s_table_container .="</form>";

    }  

     
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                        <div class='row'>
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>HAK AKSES</h4>
                                </div>
                                <div style='float:right'>


                                        <button type='button' class='button-privileges-apply btn btn-flat  btn-sm btn-primary pull-right' style='margin-left: 5px;'>
                                            <i class='  fa fa-save'></i>Terapkan
                                        </button>
                                        <button type='button' class='btn-sm button-privileges-back btn btn-flat  btn-sm btn-danger pull-right' style='margin-left: 5px;'>
                                            <i class='fa fa-caret-left'></i> kembali
                                        </button>
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        </div>
                            
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>

                                   {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
	$oGroup->closeDB();
?>