$(document).ready(function() {

    $('#table-user tbody').on( 'click', '.button-aktifasi', function () 
    {
        userID = $(this).attr('record-id');
        $.ajax({
        type: 'POST',
        url: "index.php?page=user&type=model&action=aktifasi",
        dataType:"json",
        data: "userID=" +userID,
        cache:false,
        //jika complete maka
        complete:
            function(data,status)
            {
             return false;
            },
        success:
            function(msg,status)
            {
                if(msg.result == 'success')
                {
                    $('#notification-container').removeClass('alert-danger');
                    $('#notification-container').addClass('alert-success'); 
                    $('#notification-content').html(msg.desc);
                    $('#notification-container').show(); 
                        setTimeout(function() {
                            document.location='index.php?page=user&action=list'
                        }, 2000);

                }
                else
                {
                    $('#notification-container').removeClass('alert-success');
                    $('#notification-container').addClass('alert-danger'); 
                    $('#notification-content').html(msg.desc);
                    $('#notification-container').show(); 
                }
                $('html, body').animate({
                scrollTop: $('#notification-content').offset().top
                }, 500);
            },
        //untuk sementara tidak digunakan
        error:
            function(msg,textStatus, errorThrown)
            {
                $.Notify({
                    caption: '',
                    content: 'Terjadi kegagalan proses transaksi',
                    keepOpen: true,
                    type: 'alert'
                });
            }
        });//end of ajax
    });

    // Handler for .ready() called.
    $('#table-user tbody').on( 'click', '.button-deaktifasi', function () 
        {
            userID = $(this).attr('record-id');
            $.ajax({
            type: 'POST',
            url: "index.php?page=user&type=model&action=deaktifasi",
            dataType:"json",
            data: "userID=" +userID,
            cache:false,
            //jika complete maka
            complete:
                function(data,status)
                {
                 return false;
                },
            success:
                function(msg,status)
                {
                    //alert(msg);
                    if(msg.result == 'success')
                    {
                        $('#notification-container').removeClass('alert-danger');
                        $('#notification-container').addClass('alert-success'); 
                        $('#notification-content').html(msg.desc);
                        $('#notification-container').show();
                        setTimeout(function() {
                            document.location='index.php?page=user&action=list'
                        }, 2000);

                    }
                    else
                    {
                        $('#notification-container').removeClass('alert-success');
                        $('#notification-container').addClass('alert-danger'); 
                        $('#notification-content').html(msg.desc);
                        $('#notification-container').show(); 
                    }
                    $('html, body').animate({
                    scrollTop: $('#container-alert').offset().top
                    }, 500);

                },
            //untuk sementara tidak digunakan
            error:
                function(msg,textStatus, errorThrown)
                {
                    $.Notify({
                        caption: '',
                        content: 'Terjadi kegagalan proses transaksi',
                        keepOpen: true,
                        type: 'alert'
                    });
                }
            });//end of ajax
        });


        $('#table-user tbody').on( 'click', '.button-hak-akses', function () {
            userID = $(this).attr('record-id');
            document.location='index.php?page=user&action=privileges&userID='+userID;
        });


        //check datatable untuk user dan priviliges
        if($('#table-group').length)
        {
            $('#table-group').DataTable
            ({        
                rowReorder: true,
                columnDefs: [
                    { orderable: true, className: 'reorder', targets:[ 1, 2 ] },
                    { orderable: false, targets: '_all' }
                ]
            });


            var table_group = $('#table-group');

            table_group.find('.group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        $(this).parents('tr').addClass(" bg-yellow ");
                    } else {
                        $(this).attr("checked", false);
                        $(this).parents('tr').removeClass(" bg-yellow ");
                    }
                });
                jQuery.uniform.update(set);
            });

            table_group.on('change', 'tbody tr .checkboxes', function () {
                $(this).parents('tr').toggleClass(" bg-yellow ");
            });


            jQuery('.button-privileges-apply').click(function() {
                $.ajax({
                       type: 'POST',
                        url: "index.php?page=user&type=model&action=update_privileges",
                       dataType:"json",
                       data: $("#form-privileges").serialize(),
                       cache:false,
                       //jika complete maka
                       complete:
                       function(data,status){
                            return false;
                       },
                       success:
                            function(msg,status){
                                //jika menghasilkan result
                                //alert(msg);
                                if(msg.result == 'success')
                                {
                                    $('#notification-container').removeClass('alert-danger');
                                    $('#notification-container').addClass('alert-success'); 
                                    $('#notification-content').html(msg.desc);
                                    $('#notification-container').show();
                                    setTimeout(function() {
                                        document.location='index.php?page=user&action=list'
                                    }, 2000);

                                }
                                else
                                {
                                    $('#notification-container').removeClass('alert-success');
                                    $('#notification-container').addClass('alert-danger'); 
                                    $('#notification-content').html(msg.desc);
                                    $('#notification-container').show(); 
                                }
                                $('html, body').animate({
                                scrollTop: $('#container-alert').offset().top
                                }, 500);
                            },
                       //untuk sementara tidak digunakan
                       error:
                            function(msg,textStatus, errorThrown){
                                $('#info').html('Terjadi error saat proses kirim data');
                            }
                });//end of ajax
            });

            jQuery('.button-privileges-back').click(function() {
                document.location='index.php?page=user&action=list'
            });

        }
        $('#table-group').find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).prop("checked", true);
                } else {
                    $(this).prop("checked", false);
                }
            });
            jQuery.uniform.update(set);
        });


        if($('#table-user').length)
        {
            
            jQuery('.button-create-user').click(function() {
                document.location='index.php?page=user&action=create'
            });
            $('#table-user').DataTable
            ({        
                rowReorder: true,
                columnDefs: [
                    { orderable: true, className: 'reorder', targets:[ 0, 1 ] },
                    { orderable: false, targets: '_all' }
                ]
            });
        }
        if($('#button-create-user-apply').length)
        {

            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
                if($(this).find('input[type="radio"]').val() == 'GROUP_HELPDESK'  ||  $(this).find('input[type="radio"]').val() == 'GROUP_SPV_IT')
                {
                    $('#wilayah_helpdesk').show();
                    $('#wilayah_umum').hide();
                }
                else
                {
                    $('#wilayah_helpdesk').hide();
                    $('#wilayah_umum').show();
                }
                $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
                $(this).addClass('selected');
                $(this).find('input[type="radio"]').prop("checked", true);
            });
            $('#button-create-user-apply').click(function() 
            {
                $.ajax({
                    type: 'POST',
                    url: "index.php?page=user&type=model&action=create",
                    dataType:"json",
                    data: $("#form-user-create").serialize(),
                    cache:false,
                    //jika complete maka
                    complete:
                        function(data,status)
                        {
                         return false;
                        },
                    success:
                        function(msg,status)
                        {
                            

                            //alert(msg);
                            if(msg.result == 'success')
                            {
                                $('#notification-container').removeClass('alert-danger');
                                $('#notification-container').addClass('alert-success'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show();
                                setTimeout(function() {
                                    document.location='index.php?page=user&action=list'
                                }, 1000);

                            }
                            else
                            {
                                $('#notification-container').removeClass('alert-success');
                                $('#notification-container').addClass('alert-danger'); 
                                $('#notification-content').html(msg.desc);
                                $('#notification-container').show(); 
                            }
                            $('html, body').animate({
                            scrollTop: $('#notification-container').offset().top
                            }, 500);

                        },
                    //untuk sementara tidak digunakan
                    error:
                        function(msg,textStatus, errorThrown)
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html('Terjadi kegagalan proses transaksi');
                            $('#notification-container').show(); 
                        }
                });//end of ajax

            });
        }
        //end create user------------------------------------
//BEGIN  GANTI PASSWORD
    if($('#button-ganti-password-apply').length)
    {
        $('#button-ganti-password-apply').click(function() 
        {
            $.ajax({
                type: 'POST',
                url: "index.php?page=user&type=model&action=ganti_password",
                dataType:"json",
                data: $("#form-ganti-password").serialize(),
                cache:false,
                //jika complete maka
                complete:
                    function(data,status)
                    {
                     return false;
                    },
                success:
                    function(msg,status)
                    {
                        

                        //alert(msg);
                        if(msg.result == 'success')
                        {
                            $('#notification-container').removeClass('alert-danger');
                            $('#notification-container').addClass('alert-success'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show();
                            setTimeout(function() {
                                document.location='index.php?page=logout'
                            }, 2000);

                        }
                        else
                        {
                            $('#notification-container').removeClass('alert-success');
                            $('#notification-container').addClass('alert-danger'); 
                            $('#notification-content').html(msg.desc);
                            $('#notification-container').show(); 
                        }
                        $('html, body').animate({
                        scrollTop: $('#notification-container').offset().top
                        }, 500);

                    },
                //untuk sementara tidak digunakan
                error:
                    function(msg,textStatus, errorThrown)
                    {
                        $('#notification-container').removeClass('alert-success');
                        $('#notification-container').addClass('alert-danger'); 
                        $('#notification-content').html('Terjadi kegagalan proses transaksi');
                        $('#notification-container').show(); 
                    }
            });//end of ajax

        });   
    }
    

    //END  GANTI PASSWORD
    //-------------------------------
});