<?php
/**
 *
 * @version         1.0
 * @author          basarudin
 * @created     17 November 2018
 * @log
 *
 * prefix parameter:
 *    n  - node
 *    o  - object
 *    a  - array
 *    s  - string
 *    b  - boolean
 *    f  - float
 *    i  - integer
 *    fn - function
 *    _  - parameter
 *   penulisan variabel pemisah = _
 *   penulisan variabel untuk dipakai disemua halaman menggunakan huruf besar semua contoh $USER;
 *   penulisan method huruf pertama kecil selanjutnya besar
 **/
     require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
     require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
     $oUser = new UserInfo();
     $oGroup = new Group();
 
     $BREADCRUMB['title'][] = "Pengguna";
     $BREADCRUMB['url'][] = "#";
     //---table
     $a_title  = array();
     $a_title[] = "NIP";
     $a_title[] = "NAMA";
     $a_title[] = "WILAYAH";
     $a_title[] = "STATUS";
     $a_title[] = "";
     $a_title_class[] = " style='width:100px;' ";
     $a_title_class[] = " ";
     $a_title_class[] = " style='width:50px;' ";
     $a_title_class[] = " style='width:50px;' ";
     $a_title_class[] = " style='width:200px;' ";

     $s_table_container = "";
     $s_condition = " WHERE true ";
     $s_limit = "  ";
     $s_order = " ";

     $a_data = $oUser->getUserList($s_condition, $s_order, $s_limit);
     if(isset($a_data))
     {
          
          $s_table_container ="";
          $s_table_container .="
                    <table  id='table-user' class='table  table-bordered table-hover' width='100%'  border='1px'>
                         <thead>
                              <tr >";
                                   for($i=0;$i<count($a_title);$i++)
                                   {
                                        $s_table_container .="<td {$a_title_class[$i]} >" .$a_title[$i]."</td>";
                                   }
          $s_table_container .="</tr>
                         </thead>";
          $s_table_container .="<tbody>";



               for($i=0;$i<count($a_data);$i++)
               {
                    $s_status = "";
                    $s_button = "";
                    $s_group = "";

                    //span untuk group yang diikuti

                    $a_data_group_user =  $oGroup->getListUserGroup(" WHERE A.userID='{$a_data[$i]['userID']}' ", $s_order, $s_limit);
                    if(isset($a_data_group_user))
                    {
                         foreach ($a_data_group_user as $value) 
                         {
                            if($value['groupID'] == $GROUP_ADMIN )
                            {
                                $s_group .= "  <span class='label label-danger'>{$value['name']}</span> ";
                            }
                            elseif ($value['groupID'] ==$COMMON_GROUP) {
                                $s_group .= "";
                            }
                            else
                            {
                                $s_group .= "  <span class='label label-primary'>{$value['name']}</span> ";
                            }
                         }
                    }
                    else
                    {
                        $s_group = "belum mempunyai hak akses.";
                    }
                    if($s_group == "")
                    {
                        $s_group = "belum mempunyai hak akses.";
                    }


                    if($a_data[$i]['active'] == 0)
                    {
                         $s_status  = "<span class='label label-sm label-default'>belum daftar</span>";
                         $s_button = "<button class='btn  btn-success btn-sm button-aktifasi'  record-id='{$a_data[$i]['userID']}' >Aktifkan</button>";
                    }
                    elseif($a_data[$i]['active'] == 2)
                    {
                         $s_status  = "<span class='label label-sm  label-warning'>aktifasi admin</span>";
                         $s_button = "<button class='btn  btn-success btn-sm button-aktifasi'  record-id='{$a_data[$i]['userID']}' >Aktifkan</button>";
                    }
                    elseif($a_data[$i]['active'] == 1)
                    {

                         $s_status  = "<span class='label label-sm label-success'>aktif</span>";
                         $s_button = "<button class='btn btn-danger btn-sm button-deaktifasi'   record-id='{$a_data[$i]['userID']}' >Disable</button>";
                         $s_button .= "<button class='btn btn-primary btn-sm button-hak-akses'   record-id='{$a_data[$i]['userID']}' >Hak Akses</button>";
                    }
                    //untuk informasi jumlah soal
                    $s_table_container .="<tr >";
                    $s_table_container .= "<td  align='left' >"
                                             .$a_data[$i]['userID']."<br>"
                                             .$a_data[$i]['mail']
                                        ."</td>";
                    $s_table_container .= "<td  align='left'>"
                                             .strtoupper ($a_data[$i]['realName'])."<br>".$s_group
                                        ."</td>";
                    $s_table_container .= "<td  align='left' >"
                                             .$a_data[$i]['wilayahPenempatan']
                                        ."</td>";
                    $s_table_container .= "<td  align='left'>"
                                             .$s_status
                                        ."</td>";
                    $s_table_container .= "<td  > $s_button</td>";
                    $s_table_container .="</tr>";
               }    
          $s_table_container .="</tbody>";
          $s_table_container .="</table>";
     }


    $LAYOUT_JS_EXTENDED .= "
                    
                    <script src='assets/bower_components/datatables.net/js/jquery.dataTables.min.js'></script>
                    <script src='assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'></script>


                    <script src='modul/user/user.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "

                    <link rel='stylesheet' href='assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'>
                    ";

    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                        <section class='page-head'>
                            <div style='float:left'>
                                <h4>PENGGUNA</h4>
                            </div>
                            <div style='float:right'>


                                    <button type='button' class='button-create-user btn btn-flat  btn-sm btn-success pull-right' style='margin-left: 5px;'>
                                        <i class='  fa fa-user'></i> Tambah
                                    </button>
                            </div>
                            <div style=' clear: both;'>
                                <hr>
                            </div>
                        </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_table_container}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
     $oUser->closeDB();
     $oGroup->closeDB();
?>