<?php
    /*
    |--------------------------------------------------------------------------
    | User Password 
    |--------------------------------------------------------------------------
    |Form untuk ganti password
    |
    |
    |
    |Digunakan untuk membuat log
    |prefix parameter pada class:
    |     _ :  parameter 
    |     i :  integer 
    |     b :  boolean 
    |     a :  array 
    |     s :  string
    */
    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    $oUser = new UserInfo();
    $LAYOUT_JS_EXTENDED .= "
                    <script src='modul/user/user.js'></script>
                    ";
    $LAYOUT_CSS_EXTENDED .= "
                    <link rel='stylesheet' href='assets/css/adminlte.css'>
                    ";

    $BUTTON_MAIN  = "
                        <button type='button' id='button-ganti-password-apply' class='btn btn-flat  btn-sm btn-primary pull-right' style='margin-left: 5px;'>
                            <i class='  fa fa-user'></i> Ganti
                        </button>
                    ";
    $s_form_input = "";

    $s_form_input = "
                        <form id='form-ganti-password' action='' method='post'>
                            <!-- /.box-header -->
                            <div class='box-body' style='margin-top:100px'>
                                <div class='row'>
                                    <div class='col-md-12'>
                                            <!-- Prosesor -->
                                            <div class='form-group'>
                                                <label>Password Lama</label>


                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-ellipsis-h'></i>
                                                    </div>
                                                    <input type='password' name='password_lama' class='form-control'  placeholder='Password Lama'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    
                                    <div class='col-md-12'>
                                            <!-- Prosesor -->
                                            <div class='form-group'>
                                                <label>Password Baru</label>


                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-ellipsis-h'></i>
                                                    </div>
                                                    <input type='password' name='password_baru' class='form-control'  placeholder='Password Baru'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    
                                    <div class='col-md-12'>
                                            <!-- Prosesor -->
                                            <div class='form-group'>
                                                <label>Ulangi Password Baru</label>

                                                
                                                <div class='input-group'>
                                                    <div class='input-group-addon'>
                                                        <i class='fa fa-ellipsis-h'></i>
                                                    </div>
                                                    <input type='password' name='password_ulang' class='form-control'  placeholder='Password Baru'>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                    </div>
                                    
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </form>";
                        
            
    $CONTENT_MAIN = "
                <!-- BEGIN CONTENT CONTAINER -->
                    <div class='container-fluid'>
                        <!-- BEGIN PAGE HEAD-->
                            <section class='page-head'>
                                <div style='float:left'>
                                    <h4>GANTI PASSWORD</h4>
                                </div>
                                <div style='float:right'>
                                    {$BUTTON_MAIN}
                                </div>
                                <div style=' clear: both;'>
                                    <hr>
                                </div>
                            </section>
                        
                        <!-- END PAGE HEAD-->
                        <!-- BEGIN PAGE CONTENT BODY -->
                        <section class='page-body'>

                          <!-- Info boxes -->
                          <div class='row'>
                              {$s_form_input}
                          </div>
                        </section>
                            
                        <!-- END PAGE CONTENT BODY -->
                    </div>
              ";
    $oUser->closeDB();
?>
