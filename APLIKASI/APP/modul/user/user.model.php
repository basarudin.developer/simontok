<?PHP
     /**
      * transaksi.svr.php
      * 
      * File ini digunakan untuk administrasi transaksi
      *  
      *
      * @version         1.0
      * @author          basarudin
      * @created     feb 17 ,2013
      * @log
      *        - 24 maret 2014 create new file
      *
      * prefix parameter:
      *    n  - node
      *    o  - object
      *    a  - array
      *    s  - string
      *    b  - boolean
      *    f  - float
      *    i  - integer
      *    uk  - unknown
      *    fn - function
      *    _  - parameter
      **/
    $PAGE_ID = "IND001";
    include_once($SYSTEM['DIR_MODUL_CORE']."/secure.php");

    require_once($SYSTEM['DIR_PATH']."/class/class.user.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.group.php");
    require_once($SYSTEM['DIR_PATH']."/class/class.notification.php");
    require_once($SYSTEM['DIR_PATH']."/class/function.notification.local.php");

    $oUser = new UserInfo();
    $oUser_i = new UserInfo();
    $oGroup = new Group();
    $oNotification = new Notification();

    $a_errors = array();
    $result['result'] = 'error';
    $result['desc'] = "";


     
    if(isset($_REQUEST['action']))
    {

        if($_REQUEST['action'] == 'create')
        {
            if(!isset($_REQUEST['hak_akses']))
            {
                $a_errors[] = "hak akses harus dipilih";
            }
            else
            {
                if($_REQUEST['hak_akses'] == "GROUP_HELPDESK" || $_REQUEST['hak_akses'] == "GROUP_SPV_IT")
                {
                    $_REQUEST['wilayah'] = $_REQUEST['wilayah_helpdesk'];       
                }
            }
            if($_REQUEST['userID'] == "")
            {
                $a_errors[] = "NIP tidak ada";
            }
            if($_REQUEST['realName'] == "")
            {
                $a_errors[] = "nama harus diisi";
            }
            if($_REQUEST['username'] == "")
            {
                $a_errors[] = "username harus diisi";
            }
            if($_REQUEST['mail'] == "")
            {
                $a_errors[] = "email harus diisi";
            }
            if($_REQUEST['wilayah'] == "")
            {
                $a_errors[] = "wilayah harus diisi";
            }

            if($_REQUEST['password'] == "")
            {
                $a_errors[] = "password harus diisi";
            }
            if($_REQUEST['password-replay'] == "")
            {
                $a_errors[] = "password-ulang harus diisi";
            }

            if (!$a_errors) 
            {
                if($_REQUEST['password'] != $_REQUEST['password-replay'])
                {
                    $result['result'] = 'error';
                    $result['desc'] = "Password tidak sama";
                }
                else
                {

                    $sWhereCheck = " WHERE `username` = '{$_REQUEST['username']}' or userID = '{$_REQUEST['userID']}' ";
                    if($oUser->getUserCount($sWhereCheck)> 0)
                    {
                        $result['result'] = 'error';
                        $result['desc'] = "NIP / username  sudah pernah dimasukan";
                    }
                    else
                    {
                        $a_hak_akses = array();
                        if($_REQUEST['hak_akses'] != "")
                        {
                            $a_hak_akses[]  = $_REQUEST['hak_akses'];
                        }
                        if($oUser_i->create(
                            $a_hak_akses,
                            $_REQUEST['userID'],
                            $_REQUEST['username'],
                            $_REQUEST['password'],
                            $_REQUEST['realName'],
                            $_REQUEST['mail'],
                            $_REQUEST['wilayah']
                        ))
                        {
                            $result['result'] = 'success';
                            $result['desc'] = "user berhasil dimasukan";
                            $result['userID'] = $_REQUEST['userID'];

                            $s_notif_message = "{$USER[0]['realName']} membuat pengguna baru dengan nama <span class='label label-success'>{$_REQUEST['realName']}</span>";
                            
                            create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
                        }
                        else
                        {
                            $result['result'] = 'error';
                            $result['desc'] = "user gagal dimasukan";
                        }
                    }
                }

            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) 
                {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
           echo json_encode($result);

        }

        elseif($_REQUEST['action'] == 'aktifasi')
        {
            if($_REQUEST['userID'] == "")
            {
                $a_errors[] = "NIP tidak ada";
            }
            if (!$a_errors) 
            {        
                if($oUser->setActive($_REQUEST['userID'] ))
                {
                    $result['result'] = 'success';
                    $result['desc'] = " User berhasil di aktifkan";

                    $s_notif_message = "{$USER[0]['realName']} berhasil mengaktifasi pengguna  <span class='label label-success'>{$_REQUEST['userID']}</span>";
                    create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);

                }
                else
                {
                    $result['result'] = 'error';
                    $result['desc'] = "  gagal diaktifasi";
                }
                    
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);

        }
        elseif($_REQUEST['action'] == 'deaktifasi')
        {
            if(!isset($_REQUEST['userID']))
            {
                $a_errors[] = "NIP tidak ada";
            }
            else
            {
                if($_REQUEST['userID'] == "")
                {
                    $a_errors[] = "NIP tidak ada";
                }
            }
            if (!$a_errors) 
            {        
                if($oUser->setNonActive($_REQUEST['userID'] ))
                {
                    $result['result'] = 'success';
                    $result['desc'] = " User berhasil di non-aktifkan";
                    $s_notif_message = "{$USER[0]['realName']} berhasil menonaktifkan pengguna  <span class='label label-danger'>{$_REQUEST['userID']}</span>";
                    create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);

                }
                else
                {
                    $result['result'] = 'error';
                    $result['desc'] = "  gagal diaktifasi";
                }
                    
            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) {
                    $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);

        }
        elseif($_REQUEST['action'] == 'reset')
        {
            if($_REQUEST['userID'] == "")
            {
                $a_errors[] = "NIP tidak ada";
            }
            if (!$a_errors) 
            {

                if($oUser->setNonActive($_REQUEST['userID'] ))
                {
                    $result['result'] = 'success';
                    $result['desc'] = " User berhasil di nonaktifkan";

                    $s_notif_message = "{$USER[0]['realName']} berhasil mereset pengguna  <span class='label label-danger'>{$_REQUEST['userID']}</span>";
                    create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);

                    //dapatkan data user
                    $a_user_detaiil =  $oUser->getUserDetail($_REQUEST['userID'] );
                    //kirim notifikasi
                    $s_message = str_replace("[USER]", $a_user_detaiil[0]['realName'], $MESSAGE_ADMIN_RESET);
                    sendNotification($ADMIN_KOPERASI,$s_message,$CANGKRUK);
                    $user_koperasi[]['messenger'] = $a_user_detaiil[0]['messenger'];
                    sendNotification($user_koperasi,$MESSAGE_USER_RESET,$CANGKRUK);
                 
                }
                else
                {
                     $result['result'] = 'error';
                     $result['desc'] = " '{$_REQUEST['tiang_id']}' gagal dimasukan";
                }

            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) 
                {
                     $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }

        if($_REQUEST['action'] == 'update_privileges')
        {
            if($oGroup->update($_REQUEST['userID'],$_REQUEST['group']))
            {
                $result['result'] = 'success';
                $result['desc'] = "Hak akses berhasil di update";
                $s_notif_message = "{$USER[0]['realName']} berhasil mengubah hak akses  <span class='label label-warning'>{$_REQUEST['userID']}</span>";
                create_notification_to_group($SYSTEM,SUPER_ADMIN_GROUP,$s_notif_message);
            }
            else
            {
                $result['result'] = 'error';
                $result['desc'] = "Hak akses gagal  diupdate";

            }
            echo json_encode($result);
        }
        

        if($_REQUEST['action'] == 'ganti_password')
        {
            if(!isset($_REQUEST['password_lama']))
            {
                $a_errors[] = "password lama tidak ada";
            }
            elseif($_REQUEST['password_lama'] == "")
            {
                $a_errors[] = "password lama harus diisi";
            }


            if(!isset($_REQUEST['password_baru']))
            {
                $a_errors[] = "password baru tidak ada";
            }
            elseif($_REQUEST['password_baru'] == "")
            {
                $a_errors[] = "password lama harus diisi";
            }


            if(!isset($_REQUEST['password_ulang']))
            {
                $a_errors[] = "password ulang  tidak ada";
            }
            elseif($_REQUEST['password_ulang'] == "")
            {
                $a_errors[] = "password ulang harus diisi";
            }

            if (!$a_errors) 
            {
                //cocokan input password baru dengan password ulang
                if($_REQUEST['password_ulang'] != $_REQUEST['password_baru'])
                {
                    $result['result'] = 'error';
                    $result['desc'] = " pasword baru dan password ulang tidak sama";
                }
                else
                {
                    if($USER[0]['password'] != md5($_REQUEST['password_lama']))
                    {

                        $result['result'] = 'error';
                        $result['desc'] = " pasword lama tidak sesuai dengan yang ada di sistem";
                    }
                    else
                    {
                        if($oUser->setUserPassword($USER[0]['userID'],$_REQUEST['password_baru']))
                        {
                            $result['result'] = 'success';
                            $result['desc'] = " password sudah diganti";


                            //notif ke admin
                            $notif_user_to =  $USER[0]['userID']; 
                            $notif_title  = "GANTI PASSWORD";
                            $notif_message = "anda mengganti password  ";
                            $notif_link  = "";
                            $notif_user_from = "";
                            $oNotification->create(
                                $notif_title ,
                                $notif_message ,
                                $notif_link ,
                                $notif_user_to ,
                                "" ,
                                $notif_user_from ,
                                "" 
                            );    

                        }
                        else
                        {
                            $result['result'] = 'error';
                            $result['desc'] = " password tidak dapat diganti. harap hubungi admin";
                        }
                    }
                }

                

            }
            else
            {
                $sError =  '';
                foreach ($a_errors as $error) 
                {
                     $sError .= "$error<br />";
                }
                $result['result'] = 'error';
                $result['desc'] = $sError;
            }
            echo json_encode($result);
        }


          
     }
     $oUser->closeDB();
     $oUser_i->closeDB();
     $oGroup->closeDB();
    $oNotification->closeDB();

?>