<?php
/*
|--------------------------------------------------------------------------
| Ticket
|--------------------------------------------------------------------------
|
|class untuk menghandle ticket
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
include_once($SYSTEM['DIR_PATH']."/class/class.master_db.php");
class Ticket extends  masterDB
{
    function Ticket()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }

    /**
    * periksa list ticket yang ada didatabase
    */
    function getList($_condition,$_order,$_limit)
    {
        $sql =      "   SELECT T.*,UR.realName as userRequestName,UR.wilayahPenempatan ,UR.firebaseToken as userRequestFirebaseToken,HH.realName as helpdeskHandleName,UR.userID as requestUserID,HH.userID as helpdeskID,`WK`.namaWilayah as requestWilayah
                        FROM `ticket`  T
                        LEFT JOIN `user` UR  ON T.ticketRequestID = `UR`.`userID`
                        LEFT JOIN `user` HH  ON T.ticketHelpdeskHandle = `HH`.`userID`
                        LEFT JOIN `wilayahKerja` WK ON UR.wilayahPenempatan = `WK`.`idWilayah`
                        {$_condition}  {$_order} {$_limit} 

                    " ;
        return $this->getResult($sql);
    }
    function getCount($_condition)
    {
        $sql =  "   SELECT count(*) as total 
                    FROM `ticket` 
                    LEFT JOIN `user` UR  ON `ticket`.ticketRequestID = `UR`.`userID`
                    {$_condition} ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];

    }
    function analisa(
            $_ticket_id,
            $_komputer_id,
            $_status_id,
            $_kategori_id,
            $_problem_solve_desc,
            $_status_desc
        )
    {
        $s_query_tambahan = "";
        if($_status_id == '8')
        {
            $s_query_tambahan = ' ticketSolveDateFinish = now(), ';
        }
        $a_query[] =  "   
                    UPDATE `ticket` 
                    SET 
                        `ticketKomputerID` = '$_komputer_id',  
                        `ticketSolveCategory` = '$_kategori_id', 
                        `ticketSolveDesc` = '$_problem_solve_desc', 
                        `ticketStatusDesc` = '$_status_desc',
                        ticketSolveDateStart = now(),
                        {$s_query_tambahan} 
                        `ticketStatus` = '$_status_id' 
                    WHERE `ticketID` = $_ticket_id; ";
                    //echo $a_query[0];
        return $this->queryTransaction($a_query);  
    }


}