<?php
/*
|--------------------------------------------------------------------------
| helpdesk
|--------------------------------------------------------------------------
|
|class untuk menghandle helpdesk
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/
include_once($SYSTEM['DIR_PATH']."/class/class.master_db.php");
class Helpdesk extends  masterDB
{
    function Helpdesk()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }

    /**
    * periksa list ticket yang ada didatabase
    */
    function getList($_condition,$_order,$_limit)
    {
        $sql =      "   SELECT U.realName as helpdeskHandleName,HH.*,WK.namaWilayah
                        FROM `helpdeskHandle`  HH
                        LEFT JOIN `user` U  ON HH.helpdeskID = `U`.`userID`
                        LEFT JOIN `wilayahKerja` WK  ON HH.unitHandle = `WK`.`idWilayah`
                        {$_condition}  {$_order} {$_limit} 
                    " ;
        return $this->getResult($sql);
    }
    function getCount($_condition)
    {
        $sql =  "   SELECT count(*) as total 
                    FROM `helpdeskHandle` 
                    {$_condition} ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];
    }
    function delete($_hh_id)
    {
        $a_query[] =  "
                   DELETE FROM `helpdeskHandle` 
                   WHERE `hhID` = $_hh_id ;" ;                
        return $this->queryTransaction($a_query);  
    }
    function create($_helpdesk_id,$_wilayah_id)
    {
        $a_query[] =  "
                   INSERT INTO `helpdeskHandle` 
                    ( `helpdeskID`, `unitHandle`) 
                    VALUES ( '{$_helpdesk_id}', '{$_wilayah_id}');" ;
        return $this->queryTransaction($a_query);  
    }
    

    function setStatus($_helpdesk_id,$_status_id)
    {

        $a_query[] =  "
                    UPDATE `helpdeskHandle` 
                    SET isActive= '{$_status_id}'
                    WHERE  helpdeskID = '{$_helpdesk_id}' 
                    " ;
        return $this->queryTransaction($a_query);  
    }

    function isHelpdesk($_user_id)
    {
        $sql =  "   SELECT count(*) as total 
                    FROM `userGroup`
                    WHERE groupID = 'GROUP_HELPDESK' AND userID='{$_user_id}'; ";
        $a_hasil =  $this->getResult($sql);
        if($a_hasil[0]['total'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //khusus untul group hdelpsek
    function getGroupHelpdeskByUser($_user_id)
    {
        $sql = "    SELECT groupID
                    FROM  `userGroup` 
                    WHERE groupID like 'GROUP_HELPDESK' AND userID='{$_user_id}'";
        $a_hasil =  $this->getResult($sql);
        if(isset($a_hasil))
        {
            return $a_hasil[0]['groupID'];
        }
        else
        {
            return "";
        }

    }

    function getCountGroupHelpdeskByGroup($_group_id)
    {
        $sql =  "   SELECT count(*) as total 
                   FROM  `userGroup` 
                    WHERE groupID like 'GROUP_HELPDESK_%' AND groupID='{$_group_id}' ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];
    }
}