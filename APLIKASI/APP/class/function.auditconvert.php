<?php
    function audit_convert($_parameters)
    {
        if (empty($_parameters))
        {
            return false;
        }

        $input = $_parameters;


        if (is_string($input)) {
            # See if we have stringified XML
            $xml = html_entity_decode($input);
            if (mb_detect_encoding($xml) !== 'UTF-8') {
                $xml = utf8_encode($xml);
            }
            $xml = iconv('UTF-8', 'UTF-8//TRANSLIT', $xml);
            $xml = preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F]/u', '', $xml);
            libxml_use_internal_errors(true);
            $xml = @simplexml_load_string($xml);
            if ($xml === false) {
                foreach (libxml_get_errors() as $error) {
                    print_r($error);
                    echo 'Could not convert string to XML';
                }
                return false;
            }
            if ($xml) {
                                
                $json = json_encode($xml);
                $array = json_decode($json,TRUE);
            }
        }
        return $array;
    }
?>