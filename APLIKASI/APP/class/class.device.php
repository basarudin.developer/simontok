<?php
/*
|--------------------------------------------------------------------------
| Device
|--------------------------------------------------------------------------
|
|menghandle device android yang digunakan login aplikasi
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

include_once($SYSTEM['DIR_PATH']."/class/class.master_db.php");
class Device extends  masterDB
{
    /**
    * Constructor
    */
    function Device()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }

     /**
    * periksa list device yang ada didatabase
    *
    * @return array list device
    */
    function getList($_condition,$_order,$_limit)
    {
        $sql =    "     
                    SELECT U.firebaseToken AS mainFirebase,U.realName ,DUR.* 
                    FROM `deviceUserRegister` DUR 
                    LEFT JOIN user U on U.userID = DUR.userID  
                    {$_condition}  {$_order} {$_limit} 

        " ;
        return $this->getResult($sql);
    }

    /**
    * periksa jumlah baris pada device
    *
    * @return integer total
    */
    function getCount($_condition)
    {
        $sql =    "     SELECT count(*) as total 
                        FROM `deviceUserRegister` 
                        {$_condition} ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];
    }
    function create($_user_id,$_firebase_token,$_device_model)
    {
        $a_query[] = " 
                        INSERT INTO `deviceUserRegister` 
                            (
                                `userID`, 
                                `firebaseToken`, 
                                `deviceModel`, 
                                `dateRegister`, 
                                `registerStatus`
                            ) 
                        VALUES (
                        '$_user_id', '$_firebase_token', '$_device_model', now(), '0');";
        //echo $a_query[0];
        return $this->queryTransaction($a_query);
    }

    function activationDeviceRegister($_dur_id)
    {
        $sql = "    SELECT userID,firebaseToken
                    FROM  `deviceUserRegister` 
                    WHERE durID  = '$_dur_id'";
        $a_hasil =  $this->getResult($sql);
        if(isset($a_hasil))
        {

            $a_query[] = " 
                            UPDATE `user` 
                            SET `firebaseToken` = '{$a_hasil[0]['firebaseToken']}', 
                                `loginToken` = '{$a_hasil[0]['firebaseToken']}' 
                            WHERE `user`.`userID` = '{$a_hasil[0]['userID']}' ;";
                            
            $a_query[] = "  UPDATE `deviceUserRegister` 
                            SET `registerStatus` = '1' 
                            WHERE `deviceUserRegister`.`durID` = '{$_dur_id}';";

                            
            return $this->queryTransaction($a_query);
        }
        else
        {
            return false;
        }


    }
    function delete($_dur_id)
    {

        $a_query[] = "  UPDATE `deviceUserRegister` 
                        SET `registerStatus` = '2' 
                        WHERE `deviceUserRegister`.`durID` = '{$_dur_id}';";
                        
        return $this->queryTransaction($a_query);
    }

    function testNotif($_dur_id)
    {

        $a_query[] = "  UPDATE `deviceUserRegister` 
                        SET `registerStatus` = '2' 
                        WHERE `deviceUserRegister`.`durID` = '{$_dur_id}';";
                        
        return $this->queryTransaction($a_query);
    }
    
    function getCountDashboard($_column_name)
    {
        $function_column = $_column_name;

        //unconditional function 
        if($_column_name == 'screenSize')
        { 
            $function_column = "ROUND(`screenSize`,1) as $_column_name";
        }



        $sql =    "     
                    SELECT $function_column,count( *) as total 
                    FROM `deviceUserRegister` 
                    GROUP by $_column_name
                    ORDER BY $_column_name 

        " ;
        return $this->getResult($sql);
    }
}

?>