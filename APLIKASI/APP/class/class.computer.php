<?php
/*
|--------------------------------------------------------------------------
| Computer
|--------------------------------------------------------------------------
|
|Class untuk komputer
|    
|Digunakan untuk membuat log
|prefix parameter pada class:
|     _ :  parameter 
|     i :  integer 
|     b :  boolean 
|     a :  array 
|     s :  string
*/

include_once($SYSTEM['DIR_PATH']."/class/class.master_db.php");
class Computer extends  masterDB
{
    /**
    * Constructor
    */
    function Computer()
    {
        parent::__construct(DB_USER,DB_PASSWORD,DB_NAME,DB_HOST);
    }


    /**
    * periksa list komputer yang ada didatabase
    */
    function getList($_condition,$_order,$_limit)
    {
        $sql =      "   SELECT K.*,U.realName,U.wilayahPenempatan as 'penggunaWilayah'
                        FROM `komputer`  K
                        LEFT JOIN `user` U  ON K.komputerUser = `U`.`userID`
                        {$_condition}  {$_order} {$_limit} 

                    " ;
        return $this->getResult($sql);
    }

    /**
    * periksa jumlah baris pada tabel komputer
    *
    * @return integer total
    */
    function getCount($_condition)
    {
        $sql =  "   SELECT count(*) as total 
                    FROM `komputer` 
                    {$_condition} ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];
    }
    
    function createAudit($_tipe_id,$_posisi_id,$_ip,$_identification,$_group_helpdesk_handle,$_token_server)
    {

        // periksa username dan password apakah berada disini
        $sql = "       
                    SELECT getNextID(
                    'komputer', TRUE
                    ) AS computer_id ";
        $a_hasil =  $this->getResult($sql);
        if(isset($a_hasil))
        {
            $computer_id = $a_hasil[0]['computer_id'];
            $a_query[]  = " INSERT INTO `komputer` (
                                            `komputerID` ,
                                            `komputerTipe` ,
                                            `KomputerPosisi` ,
                                            komputerIPUtama,
                                            `komputerStatus`,
                                            komputerIdentifikasi,
                                            komputerAuditEntry,
                                            helpdeskGroupHandle,
                                            komputerTokenServer
                                    )
                            VALUES (
                                            '{$computer_id}', '{$_tipe_id}' , '{$_posisi_id}' , '{$_ip}' ,'0','{$_identification}',now(),'{$_group_helpdesk_handle}','{$_token_server}'
                                    );";
            return $this->queryTransaction($a_query);                
        }
        else
        {
            return false;
        }
    }

    function createServiceSpesification(
        $_komputerID,
        $_komputerItemID,
        $_komputerSpekKapasitas,
        $_komputerSpekMerk,
        $_komputerSpekKeterangan,
        $_komputerSpekSerialNumber,
        $_token_server )
    {

        if(is_array($_komputerSpekKapasitas))
        {
            $_komputerSpekKapasitas = "0";
        }
        if(is_array($_komputerSpekMerk))
        {
            $_komputerSpekMerk = "";
        }
        if(is_array($_komputerSpekKeterangan))
        {
            $_komputerSpekKeterangan = "";
        }
        if(is_array($_komputerSpekSerialNumber))
        {
            $_komputerSpekSerialNumber = "";
        }
        $a_query[] = "  
                    INSERT INTO `komputerSpek`
                        (
                            `komputerID`, 
                            `komputerItemID`, 
                            `komputerSpekKapasitas`, 
                            `komputerSpekMerk`, 
                            `komputerSpekKeterangan`,
                            komputerSpekSerialNumber,
                            komputerTokenServer,
                            komputerSpekDate
                        )
                    VALUES 
                        ( 
                            '{$_komputerID}', 
                            '{$_komputerItemID}', 
                            {$_komputerSpekKapasitas}, 
                            '{$_komputerSpekMerk}', 
                            '{$_komputerSpekKeterangan}',
                            '{$_komputerSpekSerialNumber}',
                            '{$_token_server}',
                            date(now())
                        );";
        return $this->queryTransaction($a_query);   
    }
    function createServiceSpesificationApprovalUpgrade(
        $_komputerID,
        $_komputerItemID,
        $_komputerSpekKapasitas,
        $_komputerSpekMerk,
        $_komputerSpekKeterangan,
        $_komputerSpekSerialNumber,
        $_token_server )
    {
        
        if(is_array($_komputerSpekKapasitas))
        {
            $_komputerSpekKapasitas = "0";
        }
        if(is_array($_komputerSpekMerk))
        {
            $_komputerSpekMerk = "";
        }
        if(is_array($_komputerSpekKeterangan))
        {
            $_komputerSpekKeterangan = "";
        }
        if(is_array($_komputerSpekSerialNumber))
        {
            $_komputerSpekSerialNumber = "";
        }
        $a_query[] = "  
                    INSERT INTO `tempKomputerSpekUpgrade`
                        (
                            `komputerID`, 
                            `komputerItemID`, 
                            `komputerSpekKapasitas`, 
                            `komputerSpekMerk`, 
                            `komputerSpekKeterangan`,
                            komputerSpekSerialNumber,
                            komputerTokenServer,
                            komputerSpekDate
                        )
                    VALUES 
                        ( 
                            '{$_komputerID}', 
                            '{$_komputerItemID}', 
                            {$_komputerSpekKapasitas}, 
                            '{$_komputerSpekMerk}', 
                            '{$_komputerSpekKeterangan}',
                            '{$_komputerSpekSerialNumber}',
                            '{$_token_server}',
                            date(now())
                        );";
        return $this->queryTransaction($a_query);   
    }
    //periksa jika ada mac address di spek komputer
    function ifExistMacAdress($_mac_address)
    {
        $sql =    "     SELECT A.*
                        FROM `komputerSpek` A
                        left join komputer B on A.komputerID = B.komputerID
                        WHERE A.`komputerSpekSerialNumber` = '{$_mac_address}' AND B.komputerStatus != '3';";
        $a_data_spek = $this->getResult($sql); 
        if(isset($a_data_spek))
        {
            if(count($a_data_spek) > 0){

                return $a_data_spek[0]['komputerID'];
            }else{
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    function lastInsertKomputerID()
    {

        // periksa username dan password apakah berada disini
        $sql = "        SELECT komputerID 
                        FROM  `komputer` 
                        ORDER BY  `KomputerAuditEntry` DESC
                        LIMIT 1   ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['komputerID'];
    }

    function ifExistHistorisPemakaian($_computer_id){
        $sql =    "     SELECT *
                        FROM `komputerHistorisPemakaian`
                        WHERE `komputerID` = '{$_computer_id}';";
        $a_hastoris_pemakaian = $this->getResult($sql);
        if(isset($a_hastoris_pemakaian))
        {
            if(count($a_hastoris_pemakaian) > 0){
                return true;
            }
            else
            {
                return false;
            }
        } 
        else
        {
            return false;
        }
    }


    function setPengadaan($_komputer_id,$_vendor){
		$_komputer_id = $this->antiInjection($_komputer_id);
		$_vendor = $this->antiInjection($_vendor);
        $a_query[] =    "   UPDATE `komputer` 
                            SET `komputerPengadaan` = '{$_vendor}' 
                            WHERE `komputerID` = '{$_komputer_id}';";
                            
        return $this->queryTransaction($a_query);  
    }
	
    function setUserOn($_komputer_id,$_user_id){
        $a_query[] =    "   UPDATE `komputer` 
                            SET `komputerUser` = '{$_user_id}' 
                            WHERE `komputerID` = '{$_komputer_id}';";
        $a_query[] =    "   INSERT INTO `komputerHistorisPemakaian` (
                                `komputerID` ,
                                `komputerUserID` ,
                                `komputerHistorisTgl` ,
                                `komputerHistorisKet` 
                            )
                            VALUES (
                                '{$_komputer_id}', '{$_user_id}' , now(), 'ON'
                            );";
                            
        return $this->queryTransaction($a_query);  
    }
    function setUserOff($_komputer_id,$_user_id){
        $a_query[] =    "   UPDATE `komputer` 
                            SET `komputerUser` = ''
                            WHERE `komputerID` = '{$_komputer_id}';";
        $a_query[] =    "   INSERT INTO `komputerHistorisPemakaian` (
                                `komputerID` ,
                                `komputerUserID` ,
                                `komputerHistorisTgl` ,
                                `komputerHistorisKet` 
                            )
                            VALUES (
                                '{$_komputer_id}', '{$_user_id}' , now(), 'OFF'
                            );";
                            
        return $this->queryTransaction($a_query);  
    }

    function peminjamTerakhir($_komputer_id)
    {

        $sql =      "   SELECT KHP.komputerUserID, KHP.`komputerHistorisTgl` , KHP.`komputerHistorisKet` , U.realName, K. * 
                        FROM  `komputerHistorisPemakaian` KHP
                        LEFT JOIN komputer K ON K.`komputerID` = KHP.komputerID
                        LEFT JOIN user U ON U.`userID` = KHP.komputerUserID
                        WHERE KHP.komputerID = '{$_komputer_id}' 
                        ORDER BY `KHP`.`komputerHistorisTgl` DESC
                        limit 0,1";
        return $this->getResult($sql);
    }
    function getSpek($_komputer_id)
    {
        $sql =  "
                    SELECT KS. * , KI.komputerItemNama
                    FROM  `komputerSpek` KS
                    LEFT JOIN komputer K ON K.komputerID = KS.komputerID
                    LEFT JOIN komputerItem KI ON KI.komputerItemID = KS.komputerItemID
                    WHERE KS.komputerID = '{$_komputer_id}' AND komputerSpekStatus != '9';
                ";
        return $this->getResult($sql);
    }
    function getListSpek($_condition,$_order,$_limit)
    {
        $sql =      "   SELECT *
                        FROM `komputerSpek` 
                        {$_condition}  {$_order} {$_limit} 

                    " ;
        return $this->getResult($sql);
    }

    //dari child item ke komputer kemudian ke pengguna
    function getListSpekUpper($_condition,$_order,$_limit)
    {
        $sql =      "   SELECT *
                        FROM `komputerSpek` S 
                        LEFT JOIN komputerItem I on S.komputerItemID = I.komputerItemID
                        LEFT JOIN komputer K on S.komputerID = K.komputerID
                        LEFT JOIN `user` U on U.userID = K.komputerUser
                        {$_condition}  {$_order} {$_limit} 

                    " ;
        return $this->getResult($sql);
    }


    function getCountSpek($_condition)
    {
        $sql =  "   SELECT count(*) as total 
                    FROM `komputerSpek` 
                    {$_condition} ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];
    }
    function getCountSpekUpdate($_condition)
    {
        $sql =  "   SELECT count(*) as total 
                    FROM `tempKomputerSpekUpgrade` 
                    {$_condition} ";
        $aHasil =  $this->getResult($sql);
        return $aHasil[0]['total'];
    }
    function delete($_komputer_id)
    {
        $a_query[] =  "
                    UPDATE `komputer` 
                    SET  `komputerStatus` =  '3' 
                    WHERE  `komputerID` = '{$_komputer_id}';
                ";


        return $this->queryTransaction($a_query);  
    }
    function deleteConfirm($_komputer_id)
    {
        $a_query[] =  "
                    UPDATE `komputer` 
                    SET  `komputerStatus` =  '9' 
                    WHERE  `komputerID` = '{$_komputer_id}';
                ";


        return $this->queryTransaction($a_query);  
    }
    function cancel_delete($_komputer_id)
    {
        $a_query[] =  "
                    UPDATE `komputer` 
                    SET  `komputerStatus` =  '0' 
                    WHERE  `komputerID` = '{$_komputer_id}';
                ";


        return $this->queryTransaction($a_query);  
    }

    function deleteItem($_item_id)
    {
        $a_query[] =  "
                    UPDATE `komputerSpek` 
                    SET  `komputerSpekStatus` =  '0' 
                    WHERE  `komputerSpekID` = '{$_item_id}';
                ";


        return $this->queryTransaction($a_query);  
    }
    function cancelItem($_item_id)
    {
        $a_query[] =  "
                    UPDATE `komputerSpek` 
                    SET  `komputerSpekStatus` =  '1' 
                    WHERE  `komputerSpekID` = '{$_item_id}';
                ";


        return $this->queryTransaction($a_query);  
    }

    function deleteItemConfirm($_item_id)
    {
        $a_query[] =  "
                    UPDATE `komputerSpek` 
                    SET  `komputerSpekStatus` =  '9' 
                    WHERE  `komputerSpekID` = '{$_item_id}';
                ";


        return $this->queryTransaction($a_query);  
    }
    function update($_komputer_id,$_hostname,$_ip)
    {
        $a_query[] =  "
                    UPDATE `komputer` 
                    SET 
                        `komputerIdentifikasi` = '{$_hostname}',
                        `komputerIPUtama` = '{$_ip}' 
                    WHERE `komputerID`= '{$_komputer_id}' ;
                ";
        return $this->queryTransaction($a_query);  
    }


    function createManual(
        $_tipe_id,
        $_posisi_id,
        $_ip,
        $_identification,
        $_group_helpdesk_handle,
        $_a_item)
    {
        $b_result = true;
        if($this->createAudit(
            $_tipe_id,
            $_posisi_id,
            $_ip,
            $_identification,
            $_group_helpdesk_handle,
            ""))
        {
           $s_komputer_id = $this->lastInsertKomputerID(); 
           if(isset($s_komputer_id))
           {
                if(count($_a_item) > 0)
                {
                    for ($i=0 ; $i <count($_a_item)  ; $i++ ) 
                    { 
                        if(!$this->createServiceSpesification($s_komputer_id,$_a_item[$i]['komputerItemID'],$_a_item[$i]['komputerSpekKapasitas'],$_a_item[$i]['komputerSpekMerk'],"",$_a_item[$i]['komputerSpekSerialNumber'],""))
                        {
                            $b_result = false;
                        }
                    }
                }
           }
        }
        else 
        {
            $b_result = false;
        }
        return $b_result;

    }

    function getCountSpekDashboard($_column_name,$_where)
    {
        $function_column = $_column_name;
        $sql =    "     
                    SELECT $function_column,count( *) as total 
                    FROM `komputer` K 
                    LEFT JOIN komputerSpek KS on K.komputerID = KS.komputerID 
                    $_where  
                    GROUP by $_column_name
                    ORDER BY $_column_name 

        " ;
        return $this->getResult($sql);
    }
    function getCountKomputerDashboard($_column_name,$_where)
    {
        $function_column = $_column_name;
        $sql =    "     
                    SELECT $function_column,count( *) as total 
                    FROM `komputer`
                    $_where 
                    GROUP by $_column_name
                    ORDER BY $_column_name 

        " ;
        return $this->getResult($sql);
    }
 
}

?>