<?php

	//parameter
	// array tokens
	// array message
	function send_notification ($tokens, $message)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 'registration_ids' => $tokens,
			 /*
			 'notification' => [
								"body" => $message['message'],
								"notifID" => $message['notifID'],
								"sound" => $message['sound']
						],
						*/
			 'data' => $message
			);

		$headers = array(
			'Authorization:key = AIzaSyAK9A-9tQBx0_PgAYLWoPTYLr4SSWqrdPw ',
			'Content-Type: application/json'
			);
	 	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, (3*60));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);   
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		else
		{

		//var_dump(json_decode($result, true));
		}
		curl_close($ch);
		return $result;
	}


	//rekursive notification
	function cron_send_notification ($a_firebase_data,$system= array(),$increment = 0,$checksum = array())
	{
		require_once($system['DIR_PATH']."/class/class.notification.php");
		$oNotif = new Notification();
		if(!isset($checksum['total']))
		{
			$checksum['total'] = count($a_firebase_data); 
		}
		if(!isset($checksum['success']))
		{
			$checksum['success'] = 0; 
		}
		if(!isset($checksum['error']))
		{
			$checksum['error'] = 0; 
		}


		$url = $system['FIREBASE_URL'];
		$headers = $system['FIREBASE_HEADER'];
		if(count($a_firebase_data) > $increment )
		{
			$a_tokens[0] = $a_firebase_data[$increment]['notifToFirebaseID'];
			$tokens = $a_tokens;
			$message['notifID'] = $a_firebase_data[$increment]['notifID'];
			$message['message'] = $a_firebase_data[$increment]['notifMessage'];
			$fields = array(
				 'registration_ids' => $tokens,
				 /*
				 'notification' => [
									"body" => $message['message'],
									"notifID" => $message['notifID']
							],
							*/
				 'data' => $message
				);

			var_dump($fields);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, $system['FIREBASE_DELAY']);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);   
			if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));

		        $checksum['error']++;
		        $checksum['error_notif_id'][] = $a_firebase_data[$increment]['notifID'];
				$oNotif->update($a_firebase_data[$increment]['notifID'],$system['FIREBASE_FLAG_ERROR']);
			}
			else
			{
				$firebase_result = json_decode($result, true);
				if($firebase_result['failure'] == 0)
				{

					$oNotif->update($a_firebase_data[$increment]['notifID'],1);
			        $checksum['success']++;
					$oNotif->update($a_firebase_data[$increment]['notifID'],$system['FIREBASE_FLAG_SEND']);
				}
				else
				{
       	 			$checksum['error']++;
		        	$checksum['error_notif_id'][] = $a_firebase_data[$increment]['notifID'];
					$oNotif->update($a_firebase_data[$increment]['notifID'],$system['FIREBASE_FLAG_ERROR']);
				} 
			}
			curl_close($ch);

			$increment++;
			cron_send_notification($a_firebase_data,$system,$increment,$checksum);
		}
		else
		{

			var_dump($checksum);
			//return $checksum;	
		}

		$oNotif->closeDB();

	}
?>