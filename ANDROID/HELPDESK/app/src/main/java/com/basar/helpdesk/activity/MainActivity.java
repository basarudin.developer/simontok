package com.basar.helpdesk.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.helpdesk.BuildConfig;
import com.basar.helpdesk.R;
import com.basar.helpdesk.helper.Config;
import com.basar.helpdesk.helper.InputStreamVolleyRequest;
import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity     implements OnPermissionCallback, View.OnClickListener{



    private ProgressBar pb_progress;
    private TextView tv_open,tv_pending,tv_finish;


    private SwipeRefreshLayout swLayout;
    private Button button_scanner;
    private  String TAG = "DOWNLOADPDF";
    private  String komputer_id = "";
    private  CardView dashboard_ticket_open,dashboard_ticket_pending,dashboard_ticket_closed;




    private PermissionHelper permissionHelper;

    private boolean isSingle;
    private AlertDialog builder;
    private String[] neededPermission;
    private final static String[] MULTI_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.CAMERA

    };
    private  String alamat_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.v(TAG, ",masi ksk " );
        initComponent();
        initVariable();
        mainLogic();
    }

    private void initComponent()
    {

        pb_progress = (ProgressBar) findViewById(R.id.progress);
        pb_progress.setVisibility(ProgressBar.GONE);
        tv_open = (TextView) findViewById(R.id.tv_open);
        tv_pending = (TextView) findViewById(R.id.tv_pending);
        tv_finish = (TextView) findViewById(R.id.tv_finish);

        dashboard_ticket_open = findViewById(R.id.dashboard_ticket_open);
        dashboard_ticket_pending = findViewById(R.id.dashboard_ticket_pending);
        dashboard_ticket_closed = findViewById(R.id.dashboard_ticket_closed);

        button_scanner = (Button) findViewById(R.id.button_scan_barcode);
        button_scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                startActivity(intent);
            }
        });


        swLayout = (SwipeRefreshLayout) findViewById(R.id.swlayout);
        // Mengeset properti warna yang berputar pada SwipeRefreshLayout
        swLayout.setColorSchemeResources(R.color.colorPrimary,R.color.colorPrimaryDark);
        // Mengeset listener yang akan dijalankan saat layar di refresh/swipe
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //  berputar/refreshing
                swLayout.setRefreshing(true);

                downloadDashboard();
            }
        });


    }
    private  void initVariable()
    {

        permissionHelper = PermissionHelper.getInstance(this);
        permissionHelper
                .setForceAccepting(false) // default is false. its here so you know that it exists.
                .request( MULTI_PERMISSIONS);



        if(getIntent().getExtras()!=null){
            /**
             * Jika Bundle ada, ambil data dari Bundle
             */
            Bundle bundle = getIntent().getExtras();
            Log.v(TAG, bundle.getString("komputer_id") );

            komputer_id = bundle.getString("komputer_id");
            //tv_keterangan.setText(bundle.getString("komputer_id"));
        }



        dashboard_ticket_open.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){

                Intent intent = new Intent(v.getContext(), ListTicketActivity.class);
                intent.putExtra("status_ticket", "open");
                startActivity(intent);

                Log.v(TAG,"list ticket open");
            }
        });
        dashboard_ticket_pending.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){

                Intent intent = new Intent(v.getContext(), ListTicketActivity.class);
                intent.putExtra("status_ticket", "pending");
                startActivity(intent);

                Log.v(TAG,"list ticket pending");
            }
        });

        dashboard_ticket_closed.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){

                Intent intent = new Intent(v.getContext(), ListTicketActivity.class);
                intent.putExtra("status_ticket", "closed");
                startActivity(intent);
                Log.v(TAG,"list ticket open");
            }
        });
    }
    private void mainLogic()
    {
        downloadDashboard();
    }




    /*----------------begin transmision volley data---------------------*/
    private void downloadDashboard()
    {

        pb_progress.setVisibility(ProgressBar.VISIBLE);
         alamat_url = Config.getAppProtocol()+Config.getDomain(getBaseContext())+"/index.php";
        Log.v(TAG, "alamat domain" + alamat_url);

        Log.v(TAG, "tokenkupdf:" + Config.getLoginToken(getBaseContext()));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pb_progress.setVisibility(ProgressBar.GONE);
                        Log.v(TAG, "responnya :" + response);
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("result");
                            //Log.v(TAG, "dan hasilnya: " + result);
                            if(result.equalsIgnoreCase("success"))
                            {

                                String data_ticket = jsonObject.getString("data");
                                writeDashboard(data_ticket);

                                if(komputer_id.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    downloadPDF(komputer_id  );
                                }

                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                            }
                            //Log.v(TAG, "kok g ada" );

                        }catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pb_progress.setVisibility(ProgressBar.GONE);
                        Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("TOKEN_KEY", Config.getFirebaseToken(getBaseContext()));
                params.put("page","ticket");
                params.put("type","model");
                params.put("action","resume_onmonth");

                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //maksimal 27 detik
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);

        swLayout.setRefreshing(false);
    }

    private  void  writeDashboard(String _json_string)
    {
        try{

            JSONObject ticket_object = new JSONObject(_json_string);
            String s_open = ticket_object.getString("open");
            String s_pending = ticket_object.getString("pending");
            String s_finish = ticket_object.getString("finish");

            tv_open.setText(s_open);
            tv_pending.setText(s_pending);
            tv_finish.setText(s_finish);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
    /*----------------end transmision volley data---------------------*/



    /*---------------begin permission helper--------------------*/
    /**
     * Used to determine if the user accepted {@link android.Manifest.permission#SYSTEM_ALERT_WINDOW} or no.
     * <p/>
     * if you never passed the permission this method won't be called.
     */
    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        permissionHelper.onActivityForResult(requestCode);
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override public void onPermissionGranted(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Granted");
        //downloadDashboard();
        Log.v("PERMISSION onPermissionGranted", "Permission(s) " + Arrays.toString(permissionName) + " Granted");
    }

    @Override public void onPermissionDeclined(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Declined");
        Bundle bundle = new Bundle();
        bundle.putString("data_error", "memerlukan perizinan");
        Intent intent = new Intent(MainActivity.this, ErrorActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.i("PERMISSION onPermissionDeclined", "Permission(s) " + Arrays.toString(permissionName) + " Declined");
    }

    @Override public void onPermissionPreGranted(@NonNull String permissionsName) {
        //result.setText("Permission( " + permissionsName + " ) preGranted");
        Log.i("PERMISSION onPermissionPreGranted", "Permission( " + permissionsName + " ) preGranted");
    }

    @Override public void onPermissionNeedExplanation(@NonNull String permissionName) {
        Log.i("PERMISSION NeedExplanation", "Permission( " + permissionName + " ) needs Explanation");
        if (!isSingle) {
            neededPermission = PermissionHelper.declinedPermissions(this, MULTI_PERMISSIONS);
            StringBuilder builder = new StringBuilder(neededPermission.length);
            if (neededPermission.length > 0) {
                for (String permission : neededPermission) {
                    builder.append(permission).append("\n");
                }
            }
            //result.setText("Permission( " + builder.toString() + " ) needs Explanation");
            AlertDialog alert = getAlertDialog(neededPermission, builder.toString());
            if (!alert.isShowing()) {
                alert.show();
            }
        } else {
            //result.setText("Permission( " + permissionName + " ) needs Explanation");
            getAlertDialog(permissionName).show();
        }
    }

    @Override public void onPermissionReallyDeclined(@NonNull String permissionName) {
        //result.setText("Permission " + permissionName + " can only be granted from SettingsScreen");
        Log.v("PERMISSION ReallyDeclined", "Permission " + permissionName + " can only be granted from settingsScreen");
        /** you can call  {@link PermissionHelper#openSettingsScreen(Context)} to open the settings screen */
    }

    @Override public void onNoPermissionNeeded() {
        //result.setText("Permission(s) not needed");
        Log.v("PERMISSION onNoPermissionNeeded", "Permission(s) not needed");
    }

    @Override public void onClick(View v) {

        permissionHelper
                .request(Manifest.permission.SYSTEM_ALERT_WINDOW);/*you can pass it along other permissions,
                     just make sure you override OnActivityResult so you can get a callback.
                     ignoring that will result to not be notified if the user enable/disable the permission*/

    }

    public AlertDialog getAlertDialog(final String[] permissions, final String permissionName) {
        if (builder == null) {
            builder = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        builder.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionHelper.requestAfterExplanation(permissions);
            }
        });
        builder.setMessage("Permissions need explanation (" + permissionName + ")");
        return builder;
    }

    public AlertDialog getAlertDialog(final String permission) {
        if (builder == null) {
            builder = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        builder.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionHelper.requestAfterExplanation(permission);
            }
        });
        builder.setMessage("Permission need explanation (" + permission + ")");
        return builder;
    }
    /*----------------end permission helper---------------------*/


    /*----------------begin download file---------------------*/
    public void downloadPDF(String _komputer_id)
    {

        pb_progress.setVisibility(ProgressBar.VISIBLE);
        komputer_id = _komputer_id;
        String mUrl= alamat_url;
        Log.v(TAG, mUrl);
        InputStreamVolleyRequest request = new     InputStreamVolleyRequest(Request.Method.POST, mUrl,
                new Response.Listener<byte[]>() {
                    @Override
                    public void onResponse(byte[] response) {
                        // TODO handle the response
                        try {
                            if (response!=null) {

                                String name="komputer.pdf";
                                Log.v(TAG, "Download complete.");
                                saveToFile(response,name);
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                            Log.v(TAG, "Download error.");
                            e.printStackTrace();
                        }
                    }
                } ,new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO handle the error
                error.printStackTrace();

                pb_progress.setVisibility(ProgressBar.GONE);
            }
        }, null)

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("TOKEN_KEY", Config.getFirebaseToken(getBaseContext()));
                params.put("page","komputer");
                params.put("type","model");
                params.put("komputerID",komputer_id);

                return params;

            }
        };
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack());



        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        mRequestQueue.add(request);


    }
    public void saveToFile(byte[] byteArray, String pFileName){
        File f = new File(Environment.getExternalStorageDirectory() + "/helpdesk");
        if (!f.isDirectory()) {
            f.mkdir();
        }

        String fileName = Environment.getExternalStorageDirectory() + "/helpdesk/" + pFileName;

        try {

            FileOutputStream fPdf = new FileOutputStream(fileName);

            fPdf.write(byteArray);
            fPdf.flush();
            fPdf.close();
            Toast.makeText(this, "File successfully saved", Toast.LENGTH_LONG).show();

            pb_progress.setVisibility(ProgressBar.GONE);
            showPdf(pFileName);
            //super.onBackPressed();

        } catch (FileNotFoundException e) {
            Toast.makeText(this, "File create error", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(this, "File write error", Toast.LENGTH_LONG).show();
        }



    }

    public void showPdf(String _filename)
    {
        Log.v(TAG, _filename);
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            File f = new File(Environment.getExternalStorageDirectory().toString() + "/helpdesk/" +_filename );

            Uri uri = null;

            // Above Compile SDKversion: 25 -- Uri.fromFile Not working
            // So you have to use Provider
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.v(TAG, "nnn");
                Log.v(TAG, f.toString());
                //uri = Uri.parse("content://com.econnect.team.PdfContentProvider/"+"Induction.pdf");
                uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID +

                        ".provider", f);
                Log.v(TAG, uri.toString());

                Log.v(TAG, "nougat");
                // Add in case of if We get Uri from fileProvider.
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                this.grantUriPermission("com.google.android.apps.docs", uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);



                Log.v(TAG, "NOUGAT");
            }else{
                uri = Uri.fromFile(f);
            }

            intent.setDataAndType(uri, "application/pdf");
            startActivity(intent);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    /*----------------end download file---------------------*/
}
