package com.basar.helpdesk.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.basar.helpdesk.helper.Config;
import com.basar.helpdesk.helper.InputStreamVolleyRequest;
import com.google.zxing.Result;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity  implements ZXingScannerView.ResultHandler{

    private ZXingScannerView mScannerView;


    private String config_url = "192.168.43.34/simontok/APLIKASI/API";
    private  String alamat_url = "";

    private  String TAG = "SCANKU";
    public   MainActivity main_activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initComponent();
        initVariable();

    }
    private void initComponent()
    {

        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
    }
    private  void initVariable()
    {
        //String alamat_url = "http://"+Config.getDomain(getBaseContext())+"/index.php";
        alamat_url = Config.getAppProtocol() + config_url+"/index.php";
        //Log.v(TAG, alamat_url);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("tag", rawResult.getText()); // Prints scan results
        Log.v("tag", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        //MainActivity.tvresult.setText(rawResult.getText());

        Vibrator getar = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);// Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= 26) {
            getar.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
        }
        else
        {
            getar.vibrate(200);
        }

        //onBackPressed();
        //downloadPDF();

        Bundle bundle = new Bundle();
        bundle.putString("komputer_id", rawResult.getText());
        Intent intent = new Intent(ScanActivity.this, MainActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

        Log.v(TAG, "masuki ini");
        //main_activity.downloadPDF();
        //c();

        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
    }
    /*----------------begin transmision volley data---------------------*/


    /*----------------end transmision volley data---------------------*/


}
