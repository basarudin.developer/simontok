package com.basar.helpdesk.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.helpdesk.R;
import com.basar.helpdesk.helper.Config;
import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity  implements OnPermissionCallback, View.OnClickListener {

    private static final String TAG = "aktifitas_splash";
    private ImageView img_splash;
    private ProgressBar loading;
    private TextView text_keterangan;



    private PermissionHelper permissionHelper;

    private boolean isSingle;
    private AlertDialog builder;
    private String[] neededPermission;
    private final static String[] MULTI_PERMISSIONS = new String[]{
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initComponent();
        initVariable();
        mainLogic();


    }

    private boolean periksaInternet()
    {
        ConnectivityManager koneksi = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }



    private void initComponent()
    {

        img_splash = findViewById(R.id.img_splash);
        loading = findViewById(R.id.loading);
        text_keterangan = findViewById(R.id.text_keterangan);
    }
    private  void initVariable()
    {

    }
    private void mainLogic()
    {
        permissionHelper = PermissionHelper.getInstance(this);
        permissionHelper
                .setForceAccepting(false) // default is false. its here so you know that it exists.
                .request( MULTI_PERMISSIONS);
    }

    private  void splashLogic()
    {
        if(periksaInternet())
        {
            //something koneksi internet lancar
            Log.v(TAG, "koneksi internet lancar");

            Log.v(TAG, "cek login token. posisis sekarang:" + Config.getLoginToken(getBaseContext()));
            Log.v(TAG, "cek firebase token. now:" + Config.getFirebaseToken(getBaseContext()));
            Log.v(TAG, "cek register status. sekarang:" + Config.getRegisterStatus(getBaseContext()));
            if(Config.getLoginToken(getBaseContext()).equalsIgnoreCase("") )
            {
                if(Config.getRegisterStatus(getBaseContext()).equalsIgnoreCase(""))
                {
                    //jika belum pernah register maka masuk ke setting
                    startActivity(new Intent(getBaseContext(),SettingActivity.class));
                }
                else
                {
                    //jika sudah pernah register maka masuk ke form tunggu
                    startActivity(new Intent(getBaseContext(),WaitingActivity.class));
                }
            }
            else
            {
                Log.v(TAG, "masuk fungsi periksaVersiApp");
                String alamat_url = Config.getAppProtocol()+Config.getDomain(getBaseContext())+"/index.php";
                Log.v(TAG, "alamat domain" + alamat_url);
                Log.v(TAG, "tokenku:" + Config.getLoginToken(getBaseContext()));
                StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.v(TAG, "responnya :" + response);
                                try{
                                    JSONObject jsonObject = new JSONObject(response);
                                    String result = jsonObject.getString("result");
                                    Log.v(TAG, "dan hasilnya: " + result);
                                    if(result.equalsIgnoreCase("success"))
                                    {
                                        Integer version = jsonObject.getInt("version");
                                        Log.v(TAG, "versi:"+String.valueOf(version));

                                        if(Config.getVersion()>= version)
                                        {
                                            startActivity(new Intent(getBaseContext(),MainActivity.class));
                                        }
                                        else
                                        {

                                            Log.v(TAG, "versi masih rendah perlu update");
                                            //img_splash.setImageResource(R.drawable.update);
                                            loading.setVisibility(View.INVISIBLE);
                                            Toast.makeText(getApplicationContext(),"aplikasi sudah kadaluwarsa.\n harap segera diupate.\nharap hubungi helpdesk",Toast.LENGTH_LONG).show();
                                            text_keterangan.setText("aplikasi sudah kadaluwarsa.\n harap segera diupate.\nharap hubungi helpdesk");
                                        }

                                    }
                                    else
                                    {
                                        Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                                        //startActivity(new Intent(getBaseContext(),WaitingRegistration.class));
                                        //Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                                    }

                                    Log.v(TAG, "kok g ada" );

                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                            }
                        }
                )
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                        params.put("page","app");
                        params.put("appName",Config.getAppName());
                        params.put("action","version");

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(this);

                //maksimal 27 detik
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        15000
                        ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                        ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(),"tidak ada koneksi internet",Toast.LENGTH_LONG).show();
            //tidak terkoneksi internet
            text_keterangan.setText("tidak ada koneksi internet");
            Log.v(TAG, "tidak terkoneksi internet");

        }
    }




    /*---------------begin permission helper--------------------*/
    /**
     * Used to determine if the user accepted {@link android.Manifest.permission#SYSTEM_ALERT_WINDOW} or no.
     * <p/>
     * if you never passed the permission this method won't be called.
     */
    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        permissionHelper.onActivityForResult(requestCode);
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override public void onPermissionGranted(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Granted");
        splashLogic();
        Log.v("PERMISSION onPermissionGranted", "Permission(s) " + Arrays.toString(permissionName) + " Granted");
    }

    @Override public void onPermissionDeclined(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Declined");
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Declined");
        Bundle bundle = new Bundle();
        bundle.putString("data_error", "memerlukan perizinan");
        Intent intent = new Intent(SplashActivity.this, ErrorActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.i("PERMISSION onPermissionDeclined", "Permission(s) " + Arrays.toString(permissionName) + " Declined");
    }

    @Override public void onPermissionPreGranted(@NonNull String permissionsName) {
        //result.setText("Permission( " + permissionsName + " ) preGranted");
        Log.i("PERMISSION onPermissionPreGranted", "Permission( " + permissionsName + " ) preGranted");
    }

    @Override public void onPermissionNeedExplanation(@NonNull String permissionName) {
        Log.i("PERMISSION NeedExplanation", "Permission( " + permissionName + " ) needs Explanation");
        if (!isSingle) {
            neededPermission = PermissionHelper.declinedPermissions(this, MULTI_PERMISSIONS);
            StringBuilder builder = new StringBuilder(neededPermission.length);
            if (neededPermission.length > 0) {
                for (String permission : neededPermission) {
                    builder.append(permission).append("\n");
                }
            }
            //result.setText("Permission( " + builder.toString() + " ) needs Explanation");
            AlertDialog alert = getAlertDialog(neededPermission, builder.toString());
            if (!alert.isShowing()) {
                alert.show();
            }
        } else {
            //result.setText("Permission( " + permissionName + " ) needs Explanation");
            getAlertDialog(permissionName).show();
        }
    }

    @Override public void onPermissionReallyDeclined(@NonNull String permissionName) {
        //result.setText("Permission " + permissionName + " can only be granted from SettingsScreen");
        Log.v("PERMISSION ReallyDeclined", "Permission " + permissionName + " can only be granted from settingsScreen");
        /** you can call  {@link PermissionHelper#openSettingsScreen(Context)} to open the settings screen */
    }

    @Override public void onNoPermissionNeeded() {
        //result.setText("Permission(s) not needed");
        Log.v("PERMISSION onNoPermissionNeeded", "Permission(s) not needed");
    }

    @Override public void onClick(View v) {

        permissionHelper
                .request(Manifest.permission.SYSTEM_ALERT_WINDOW);/*you can pass it along other permissions,
                     just make sure you override OnActivityResult so you can get a callback.
                     ignoring that will result to not be notified if the user enable/disable the permission*/

    }

    public AlertDialog getAlertDialog(final String[] permissions, final String permissionName) {
        if (builder == null) {
            builder = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        builder.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionHelper.requestAfterExplanation(permissions);
            }
        });
        builder.setMessage("Permissions need explanation (" + permissionName + ")");
        return builder;
    }

    public AlertDialog getAlertDialog(final String permission) {
        if (builder == null) {
            builder = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        builder.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionHelper.requestAfterExplanation(permission);
            }
        });
        builder.setMessage("Permission need explanation (" + permission + ")");
        return builder;
    }
    /*----------------end permission helper---------------------*/



}
