package com.basar.helpdesk.helper;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.basar.helpdesk.R;

public class OneButtonDialog extends DialogFragment {


    private ImageView ivDialogIcon;
    private TextView tvTitle,tvMessage;
    private Button btnNeutral;


    public static final String TAG = "OneButtonDialogTag";

    protected static final String ARG_BUTTON_TEXT = "ARG_BUTTON_TEXT";
    protected static final String ARG_COLOR_RESOURCE_ID = "ARG_COLOR_RESOURCE_ID";
    protected static final String ARG_TITLE = "ARG_TITLE";
    protected static final String ARG_MESSAGE = "ARG_MESSAGE";
    protected static final String ARG_IMAGE_RESOURCE_ID = "ARG_IMAGE_RESOURCE_ID";

    private static final double DIALOG_WINDOW_WIDTH = 0.85;

    private ButtonDialogAction buttonDialogAction;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.dialog_one_button, container, false);
        ivDialogIcon = (ImageView) view.findViewById(R.id.dlg_one_button_iv_icon);
        tvMessage = (TextView) view.findViewById(R.id.dlg_one_button_tv_message);
        tvTitle = (TextView) view.findViewById(R.id.dlg_one_button_tv_title);
        btnNeutral = (Button) view.findViewById(R.id.dlg_one_button_btn_ok);

        Window window = getDialog().getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }
        getDialog().setCanceledOnTouchOutside(false);

        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int titleRes = getArguments().getInt(ARG_TITLE);
        String messageRes = getArguments().getString(ARG_MESSAGE);
        int buttonTextRes = getArguments().getInt(ARG_BUTTON_TEXT);
        int image = getArguments().getInt(ARG_IMAGE_RESOURCE_ID);
        int color = getArguments().getInt(ARG_COLOR_RESOURCE_ID);

        tvTitle.setText(titleRes);
        tvTitle.setTextColor(getResources().getColor(color));
        tvMessage.setText(messageRes);
        btnNeutral.setText(buttonTextRes);
        ivDialogIcon.setImageResource(image);


        btnNeutral.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){

                closeDialog();
                if(buttonDialogAction != null) {
                    buttonDialogAction.onButtonClicked();
                }
            }
        });
    }

    public static OneButtonDialog newInstance(@StringRes int titleRes,
                                              String message,
                                              @StringRes int buttonTextRes,
                                              @DrawableRes int imageResId,
                                              @ColorRes int colorResId,
                                              ButtonDialogAction buttonDialogAction) {
        OneButtonDialog oneButtonDialog = new OneButtonDialog();
        oneButtonDialog.buttonDialogAction = buttonDialogAction;

        Bundle args = new Bundle();
        args.putInt(ARG_TITLE, titleRes);
        args.putString(ARG_MESSAGE, message);
        args.putInt(ARG_BUTTON_TEXT, buttonTextRes);
        args.putInt(ARG_IMAGE_RESOURCE_ID, imageResId);
        args.putInt(ARG_COLOR_RESOURCE_ID, colorResId);
        oneButtonDialog.setArguments(args);

        return oneButtonDialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        setDialogWindowWidth(DIALOG_WINDOW_WIDTH);
    }

    private void setDialogWindowWidth(double width) {
        Window window = getDialog().getWindow();
        Point size = new Point();
        Display display;
        if (window != null) {
            display = window.getWindowManager().getDefaultDisplay();
            display.getSize(size);
            int maxWidth = size.x;
            window.setLayout((int) (maxWidth* width), WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
        }
    }

    public void closeDialog() {
        if (getDialog().isShowing()) {
            closeKeyboard();
            getDialog().dismiss();
        }
    }

    protected void closeKeyboard() {
        InputMethodManager imm = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(
                getActivity().findViewById(android.R.id.content).getWindowToken(), 0);
    }

    public interface ButtonDialogAction {
        void onButtonClicked();
    }
}
