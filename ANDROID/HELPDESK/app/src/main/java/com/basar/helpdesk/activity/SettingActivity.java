package com.basar.helpdesk.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.helpdesk.R;
import com.basar.helpdesk.helper.Config;
import com.basar.helpdesk.helper.Device;
import com.basar.helpdesk.helper.DialogFactory;
import com.basar.helpdesk.helper.OneButtonDialog;
import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SettingActivity extends AppCompatActivity   implements OnPermissionCallback, View.OnClickListener{

    private static final String TAG = "setting_aktifitas";
    private EditText nik,email,domain ;

    private TextView button_setting;
    private ProgressBar loading;
    public String alamat_url;




    private PermissionHelper permissionHelper;

    private boolean isSingle;
    private AlertDialog builder;
    private String[] neededPermission;
    private final static String[] MULTI_PERMISSIONS = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA

    };

    private  String
            device_manufucture,
            device_model,
            device_sdk,
            device_version,
            device_pixel,
            device_dpi,
            device_inch,
            device_country,
            device_language,
            device_uuid,
            device_android_id,
            device_network_carrier,
            device_network_id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initComponent();
        initVariable();
        mainLogic();


    }


    private void initComponent()
    {

        nik = findViewById(R.id.nik);
        email = findViewById(R.id.email);
        domain =  findViewById(R.id.domain);

        loading  = findViewById(R.id.loading);

        loading.setVisibility(View.GONE);
        button_setting = findViewById(R.id.button_setting);

    }
    private  void initVariable()
    {
        permissionHelper = PermissionHelper.getInstance(this);
        permissionHelper
                .setForceAccepting(false) // default is false. its here so you know that it exists.
                .request( MULTI_PERMISSIONS);
    }

    private  void mainLogic()
    {




        if(Config.getFirebaseToken(getBaseContext())== "")
        {
            //generate token di firebase server
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SettingActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult)
                {
                    String new_token = instanceIdResult.getToken();
                    Config.setFirebaseToken(getBaseContext(),new_token);

                    Log.v(TAG,new_token);
                }
            });
        }

        button_setting.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){
                initSetting();
            }
        });

    }



    public void onBackPressed()
    {
        return;
    }

    private void initSetting()
    {

        getDeviceInfo();
        loading.setVisibility(View.VISIBLE);
        final String nik = this.nik.getText().toString().trim();
        final String email = this.email.getText().toString().trim();
        final String domain = this.domain.getText().toString().trim();

        alamat_url = Config.getAppProtocol()+domain+"/index.php";

        if(nik.equalsIgnoreCase("")  || email.equalsIgnoreCase( "")  || domain.equalsIgnoreCase("") )
        {

            //Toast.makeText(getApplicationContext(),"inputan harus diisi");
            loading.setVisibility(View.GONE);
            //Toast.makeText(getApplicationContext(),"inputan harus diisi",Toast.LENGTH_LONG).show();
            String message = getResources().getString(R.string.setting_dialog_error_input);
            OneButtonDialog oneButtonDialog =
                    DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                            message,
                            R.string.setting_dialog_error_button_text,
                            new OneButtonDialog.ButtonDialogAction() {
                                @Override
                                public void onButtonClicked() {


                                }
                            });
            oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);

        }
        else
        {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v(TAG,response);

                            loading.setVisibility(View.GONE);
                            //Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                            try{
                                JSONObject jsonObject = new JSONObject(response);
                                String  result = jsonObject.getString("result");
                                if(jsonObject.getString("result").equalsIgnoreCase("success"))
                                {
                                    Config.setRegisterStatus(getBaseContext(),"0");
                                    Config.setDomain(getBaseContext(),domain);
                                    startActivity(new Intent(getBaseContext(),WaitingActivity.class));
                                }
                                else
                                {
                                    OneButtonDialog oneButtonDialog =
                                            DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                                    jsonObject.getString("desc").toString(),
                                                    R.string.setting_dialog_error_button_text,
                                                    new OneButtonDialog.ButtonDialogAction() {
                                                        @Override
                                                        public void onButtonClicked() {


                                                        }
                                                    });
                                    oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);

                                    //Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(), Toast.LENGTH_LONG).show();
                                }


                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            loading.setVisibility(View.GONE);
                            String message_dialog = getResources().getString(R.string.setting_dialog_error_input);
                            OneButtonDialog oneButtonDialog =
                                    DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                            message_dialog,
                                            R.string.setting_dialog_error_button_text,
                                            new OneButtonDialog.ButtonDialogAction() {
                                                @Override
                                                public void onButtonClicked() {
                                                }
                                            });
                            oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
                            Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }
            )
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("userID",nik);
                    params.put("email",email);
                    params.put("appName",Config.getAppName());
                    params.put("appVersion",Integer.toString(Config.getVersion()));
                    params.put("firebaseToken", Config.getFirebaseToken(getBaseContext()));
                    params.put("deviceModel", device_model);
                    params.put("deviceSDK",device_sdk);
                    params.put("deviceMerk",device_manufucture);
                    params.put("deviceVersion",device_version);

                    params.put("devicePixel",device_pixel);
                    params.put("deviceDPI",device_dpi);
                    params.put("deviceInch",device_inch);

                    params.put("deviceCountry",device_country);
                    params.put("deviceLanguage",device_language);
                    params.put("deviceUUID",device_uuid);

                    params.put("deviceAndroidID",device_android_id);
                    params.put("deviceNetworkCarrier",device_network_carrier);
                    params.put("deviceNetworkID",device_network_id);





                    params.put("page","login");
                    params.put("type","model");
                    params.put("action","registerdevice");

                    return params;

                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            //maksimal 27 detik
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    15000
                    ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                    ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
    }

    private  void getDeviceInfo()
    {
        Device device = new Device();
        device.init(SettingActivity.this);
        device_manufucture = device.getManufacture();
        device_model = device.getModel();
        device_sdk = String.valueOf(device.getSDKVersion());
        device_version = device.getAndroidVersion();
        device_pixel = String.valueOf(device.getScreenPixel());
        device_dpi = String.valueOf(device.getScreenDPI());
        device_inch =  String.valueOf(device.getScreenSize());
        device_country = device.getCountry();
        device_language = device.getLanguage();
        device_uuid = device.getUUID();
        device_android_id = device.getAndroidID();
        device_network_carrier = device.getNetworkCarrier();
        device_network_id = device.getNetworkID();



    }





    /*---------------begin permission helper--------------------*/
    /**
     * Used to determine if the user accepted {@link android.Manifest.permission#SYSTEM_ALERT_WINDOW} or no.
     * <p/>
     * if you never passed the permission this method won't be called.
     */
    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        permissionHelper.onActivityForResult(requestCode);
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override public void onPermissionGranted(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Granted");
        //downloadDashboard();
        Log.v("PERMISSION onPermissionGranted", "Permission(s) " + Arrays.toString(permissionName) + " Granted");
    }

    @Override public void onPermissionDeclined(@NonNull String[] permissionName) {
        //result.setText("Permission(s) " + Arrays.toString(permissionName) + " Declined");
        Bundle bundle = new Bundle();
        bundle.putString("data_error", "memerlukan perizinan");
        Intent intent = new Intent(SettingActivity.this, ErrorActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.i("PERMISSION onPermissionDeclined", "Permission(s) " + Arrays.toString(permissionName) + " Declined");
    }

    @Override public void onPermissionPreGranted(@NonNull String permissionsName) {
        //result.setText("Permission( " + permissionsName + " ) preGranted");
        Log.i("PERMISSION onPermissionPreGranted", "Permission( " + permissionsName + " ) preGranted");
    }

    @Override public void onPermissionNeedExplanation(@NonNull String permissionName) {
        Log.i("PERMISSION NeedExplanation", "Permission( " + permissionName + " ) needs Explanation");
        if (!isSingle) {
            neededPermission = PermissionHelper.declinedPermissions(this, MULTI_PERMISSIONS);
            StringBuilder builder = new StringBuilder(neededPermission.length);
            if (neededPermission.length > 0) {
                for (String permission : neededPermission) {
                    builder.append(permission).append("\n");
                }
            }
            //result.setText("Permission( " + builder.toString() + " ) needs Explanation");
            AlertDialog alert = getAlertDialog(neededPermission, builder.toString());
            if (!alert.isShowing()) {
                alert.show();
            }
        } else {
            //result.setText("Permission( " + permissionName + " ) needs Explanation");
            getAlertDialog(permissionName).show();
        }
    }

    @Override public void onPermissionReallyDeclined(@NonNull String permissionName) {
        //result.setText("Permission " + permissionName + " can only be granted from SettingsScreen");
        Log.v("PERMISSION ReallyDeclined", "Permission " + permissionName + " can only be granted from settingsScreen");
        /** you can call  {@link PermissionHelper#openSettingsScreen(Context)} to open the settings screen */
    }

    @Override public void onNoPermissionNeeded() {
        //result.setText("Permission(s) not needed");
        Log.v("PERMISSION onNoPermissionNeeded", "Permission(s) not needed");
    }

    @Override public void onClick(View v) {

        permissionHelper
                .request(Manifest.permission.SYSTEM_ALERT_WINDOW);/*you can pass it along other permissions,
                     just make sure you override OnActivityResult so you can get a callback.
                     ignoring that will result to not be notified if the user enable/disable the permission*/

    }

    public AlertDialog getAlertDialog(final String[] permissions, final String permissionName) {
        if (builder == null) {
            builder = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        builder.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionHelper.requestAfterExplanation(permissions);
            }
        });
        builder.setMessage("Permissions need explanation (" + permissionName + ")");
        return builder;
    }

    public AlertDialog getAlertDialog(final String permission) {
        if (builder == null) {
            builder = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        builder.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                permissionHelper.requestAfterExplanation(permission);
            }
        });
        builder.setMessage("Permission need explanation (" + permission + ")");
        return builder;
    }
    /*----------------end permission helper---------------------*/
}
