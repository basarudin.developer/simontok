package com.basar.helpdesk.helper;


import android.support.annotation.StringRes;

import com.basar.helpdesk.R;

public class DialogFactory {

    private DialogFactory() {
    }

    public static OneButtonDialog makeSuccessDialog(@StringRes int titleId,
                                             String  message,
                                             @StringRes int buttonTextId,
                                             OneButtonDialog.ButtonDialogAction action) {
        return OneButtonDialog.newInstance(titleId,
                message,
                buttonTextId,
                R.drawable.ic_checked,
                R.color.dialog_green,
                action);
    }

    public static OneButtonDialog makeErrorDialog(@StringRes int titleId,
                                           String message,
                                           @StringRes int buttonTextId,
                                           OneButtonDialog.ButtonDialogAction action) {
        return OneButtonDialog.newInstance(titleId,
                message,
                buttonTextId,
                R.drawable.ic_close,
                R.color.dialog_red,
                action);
    }

}