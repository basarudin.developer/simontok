package com.basar.helpdesk.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.helpdesk.R;
import com.basar.helpdesk.helper.Config;
import com.basar.helpdesk.helper.DialogFactory;
import com.basar.helpdesk.helper.OneButtonDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class WaitingActivity extends AppCompatActivity {

    private static final String TAG = "waiting_aktifitas";
    private TextView button_cek_registrasi;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);
        initComponent();
        initVariable();
        mainLogic();
    }

    private void initComponent()
    {

        button_cek_registrasi = findViewById(R.id.button_cek_registrasi);
    }
    private  void initVariable()
    {

    }

    private  void mainLogic()
    {

        button_cek_registrasi.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){
                cekAntrian();
            }
        });
    }

    //disable onbackpress
    public void onBackPressed()
    {
        return;
    }

    public  void cekAntrian()
    {
        //jika register status = 0 dan sudah ada token firebase maka cek lagi register apakah sudah di aktifasi helpdesk
        String alamat_url = Config.getAppProtocol()+ Config.getDomain(getBaseContext())+"/index.php";
        Log.v(TAG, "alamayya" + alamat_url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v(TAG, "responnya :" + response);

                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("result");
                            Log.v(TAG, "dan hasilnya: " + result);
                            if(result.equalsIgnoreCase("success"))
                            {
                                Log.v(TAG, "harusnya masuyl ini " );
                                Log.v(TAG, "logintoken" +jsonObject.getString("token_login"));
                                Log.v(TAG, "realname"+jsonObject.getString("realName") );
                                Config.setRegisterStatus(getBaseContext(),"1");
                                Config.setLoginToken(getBaseContext(),jsonObject.getString("token_login"));
                                Config.setRealname(getBaseContext(),jsonObject.getString("realName"));
                                Log.v(TAG, "wesToken login"+ Config.getLoginToken(getBaseContext()));
                                Log.v(TAG, "wesRegistatio"+ Config.getRegisterStatus(getBaseContext()));
                                Log.v(TAG, "wesRealname"+ Config.getRealname(getBaseContext()));
                                startActivity(new Intent(getBaseContext(),MainActivity.class));
                            }
                            else
                            {

                                Log.v(TAG, "masuk sana");

                                OneButtonDialog oneButtonDialog =
                                        DialogFactory.makeSuccessDialog(R.string.setting_dialog_success_title,
                                                jsonObject.getString("desc").toString(),
                                                R.string.setting_dialog_error_button_text,
                                                new OneButtonDialog.ButtonDialogAction() {
                                                    @Override
                                                    public void onButtonClicked() {


                                                    }
                                                });
                                oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
                                //Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(), Toast.LENGTH_LONG).show();
                            }

                            Log.v(TAG, "kok g ada" );

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        OneButtonDialog oneButtonDialog =
                                DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                        error.toString(),
                                        R.string.setting_dialog_error_button_text,
                                        new OneButtonDialog.ButtonDialogAction() {
                                            @Override
                                            public void onButtonClicked() {


                                            }
                                        });
                        oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
                        //Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("firebaseToken", Config.getFirebaseToken(getBaseContext()));
                params.put("page","login");
                params.put("type","model");
                params.put("action","checkwaiting");
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //maksimal 27 detik
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}
