package com.basar.simontok.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.simontok.R;
import com.basar.simontok.helper.Config;
import com.basar.simontok.helper.DialogFactory;
import com.basar.simontok.helper.OneButtonDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class StarActivity extends AppCompatActivity {

    private static final String TAG = "STARACTIVITY";
    private  String ticket_id_param;
    private  Long ticket_id;
    LinearLayout linear_layout;
    RelativeLayout relative_layout;
    private TextView tv_helpdesk,btn_send;
    private ProgressBar loading;
    private RatingBar star;
    private EditText et_feedback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star);


        initVariable();
        initComponent();
        mainLogic();
    }

    public void onBackPressed() {
        return;
    }
    private void initVariable() {
        ticket_id_param = getIntent().getStringExtra("ticket_id");

        ticket_id = Long.valueOf(ticket_id_param);

    }

    private void initComponent() {
        linear_layout = findViewById(R.id.canvas_child);
        relative_layout = findViewById(R.id.canvas_parent);
        tv_helpdesk = findViewById(R.id.tv_helpdesk);
        loading  = findViewById(R.id.loading);
        star = findViewById(R.id.star);
        et_feedback =findViewById(R.id.et_feedback);
        btn_send = (TextView) findViewById(R.id.btn_send);

    }


    private void mainLogic() {


        btn_send.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){
                sendRating();
            }
        });

        if (ticket_id != 0)
        {
            loadData();
        }
        else
        {
            OneButtonDialog oneButtonDialog =
                    DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                            "ID tiket kosong",
                            R.string.setting_dialog_error_button_text,
                            new OneButtonDialog.ButtonDialogAction() {
                                @Override
                                public void onButtonClicked() {
                                }
                            });
            oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
        }

    }


    public void loadData()
    {
        loading.setVisibility(View.VISIBLE);
        //jika register status = 0 dan sudah ada token firebase maka cek lagi register apakah sudah di aktifasi helpdesk
        String alamat_url = Config.getAppProtocol()+ Config.getDomain(getBaseContext())+"/index.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.setVisibility(View.GONE);
                        Log.v(TAG, "responnya :" + response);

                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("result");
                            Log.v(TAG, "dan hasilnya: " + result);
                            if(result.equalsIgnoreCase("success"))
                            {
                                Log.v(TAG, "UTUH: " + response);
                                Log.v(TAG, "DARATA: " + jsonObject.getString("data"));
                                if(jsonObject.isNull("data") )
                                {
                                    Log.v(TAG, "datanya kosong " );
                                }
                                else
                                {
                                    String data_ticket = jsonObject.getString("data");

                                    JSONArray jArray = new JSONArray(data_ticket);

                                    JSONObject json_data = jArray.getJSONObject(0);
                                    tv_helpdesk.setText(json_data.getString("helpdeskHandleName").toUpperCase());

                                    //Toast.makeText(getApplicationContext(),json_data.getString("ticketRequestDate"),Toast.LENGTH_LONG).show();

                                    //renderTable(data_ticket);
                                    Log.v(TAG, "ada data " );

                                }

                            }
                            else
                            {

                                Log.v(TAG, "masuk sana");

                                OneButtonDialog oneButtonDialog =
                                        DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                                jsonObject.getString("desc").toString(),
                                                R.string.setting_dialog_error_button_text,
                                                new OneButtonDialog.ButtonDialogAction() {
                                                    @Override
                                                    public void onButtonClicked() {


                                                    }
                                                });
                                oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);

                                //Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                            }
                            Log.v(TAG, "kok g ada" );

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        loading.setVisibility(View.GONE);
                        OneButtonDialog oneButtonDialog =
                                DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                        error.toString(),
                                        R.string.setting_dialog_error_button_text,
                                        new OneButtonDialog.ButtonDialogAction() {
                                            @Override
                                            public void onButtonClicked() {


                                            }
                                        });
                        oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
                        //Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                params.put("page","ticket");
                params.put("type","model");
                params.put("action","detail");
                params.put("ticket_id",String.valueOf(ticket_id));
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //maksimal 27 detik
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000
                , DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void sendRating()
    {
        loading.setVisibility(View.VISIBLE);
        //jika register status = 0 dan sudah ada token firebase maka cek lagi register apakah sudah di aktifasi helpdesk
        String alamat_url = Config.getAppProtocol()+ Config.getDomain(getBaseContext())+"/index.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.setVisibility(View.GONE);
                        Log.v(TAG, "responnya :" + response);

                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("result");
                            Log.v(TAG, "dan hasilnya: " + result);
                            if(result.equalsIgnoreCase("success"))
                            {



                                //Toast.makeText(getApplicationContext(), "kode:"+json_data.getString("ticketID"), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getBaseContext(), TicketDetail.class);
                                intent.putExtra("ticket_id",ticket_id);
                                startActivity(intent);

                            }
                            else
                            {

                                Log.v(TAG, "masuk sana");

                                OneButtonDialog oneButtonDialog =
                                        DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                                jsonObject.getString("desc").toString(),
                                                R.string.setting_dialog_error_button_text,
                                                new OneButtonDialog.ButtonDialogAction() {
                                                    @Override
                                                    public void onButtonClicked() {


                                                    }
                                                });
                                oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);

                                //Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                            }
                            Log.v(TAG, "kok g ada" );

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        loading.setVisibility(View.GONE);
                        OneButtonDialog oneButtonDialog =
                                DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                        error.toString(),
                                        R.string.setting_dialog_error_button_text,
                                        new OneButtonDialog.ButtonDialogAction() {
                                            @Override
                                            public void onButtonClicked() {


                                            }
                                        });
                        oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
                        //Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                params.put("page","ticket");
                params.put("type","model");
                params.put("action","rating");
                params.put("ticket_id",String.valueOf(ticket_id));
                params.put("rating",String.valueOf(star.getRating()));
                params.put("feedback",et_feedback.getText().toString());

                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //maksimal 27 detik
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000
                , DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
