package com.basar.simontok.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.simontok.R;
import com.basar.simontok.component.TicketView;
import com.basar.simontok.helper.Config;
import com.basar.simontok.helper.DialogFactory;
import com.basar.simontok.helper.OneButtonDialog;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TicketDetail extends AppCompatActivity {

    private TicketView ticketView;
    private  long ticket_id;
    private static final String TAG = "DETILTIKET";

    private ProgressBar loading;
    private TextView tv_date,tv_ticket_id,tv_helpdesk,tv_status,tv_antrian,tv_kategori,tv_keluhan,tv_label_tindakan,tv_tindakan;
    private ImageView iv_qrcode,btn_back;
    private RatingBar star;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_detail);



        initVariable();
        initComponent();
        mainLogic();
    }

    public void onBackPressed() {
        Intent intent = new Intent(getBaseContext(), ListTicketActivity.class);
        startActivity(intent);

    }
    private  void initVariable()
    {

        ticket_id = getIntent().getLongExtra("ticket_id", 0);
    }

    private  void initComponent()
    {
        ticketView = findViewById(R.id.layout_ticket);
        ticketView.setVisibility(View.GONE);

        loading  = findViewById(R.id.loading);
        loading.setVisibility(View.VISIBLE);


        tv_date = findViewById(R.id.tv_date);
        tv_ticket_id = findViewById(R.id.tv_ticket_id);
        tv_helpdesk = findViewById(R.id.tv_helpdesk);

        tv_status = findViewById(R.id.textStatus);
        iv_qrcode = findViewById(R.id.iv_qrcode);
        tv_antrian = findViewById(R.id.tv_antrian);
        tv_kategori = findViewById(R.id.tv_kategori);
        tv_keluhan = findViewById(R.id.tv_keluhan);
        tv_label_tindakan = findViewById(R.id.tv_label_tindakan);
        tv_tindakan = findViewById(R.id.tv_tindakan);



        btn_back = findViewById(R.id.btn_back);
        star = findViewById(R.id.star);
        star.setVisibility(View.GONE);

    }
    private  void mainLogic()
    {
        btn_back.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){
                Intent intent = new Intent(getBaseContext(), ListTicketActivity.class);
                startActivity(intent);
            }
        });

        if (ticket_id != 0)
        {
            loadData();
            //Toast.makeText(getApplicationContext(), "kode:"+ String.valueOf(ticket_id), Toast.LENGTH_LONG).show();


        }
        else
        {
            OneButtonDialog oneButtonDialog =
                    DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                            "ID tiket kosong",
                            R.string.setting_dialog_error_button_text,
                            new OneButtonDialog.ButtonDialogAction() {
                                @Override
                                public void onButtonClicked() {
                                }
                            });
            oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
        }
    }

    public void loadData()
    {
        loading.setVisibility(View.VISIBLE);
        //jika register status = 0 dan sudah ada token firebase maka cek lagi register apakah sudah di aktifasi helpdesk
        String alamat_url = Config.getAppProtocol()+Config.getDomain(getBaseContext())+"/index.php";
        Log.v(TAG, "alamayya" + alamat_url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.setVisibility(View.GONE);
                        Log.v(TAG, "responnya :" + response);

                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("result");
                            Log.v(TAG, "dan hasilnya: " + result);
                            if(result.equalsIgnoreCase("success"))
                            {
                                Log.v(TAG, "UTUH: " + response);
                                Log.v(TAG, "DARATA: " + jsonObject.getString("data"));
                                if(jsonObject.isNull("data") )
                                {
                                    Log.v(TAG, "datanya kosong " );
                                }
                                else
                                {
                                    String data_ticket = jsonObject.getString("data");

                                    JSONArray jArray = new JSONArray(data_ticket);

                                    JSONObject json_data = jArray.getJSONObject(0);
                                    tv_date.setText(json_data.getString("ticketRequestDate"));
                                    tv_ticket_id.setText(json_data.getString("ticketNumber"));
                                    tv_keluhan.setText(json_data.getString("ticketProblemDesc"));

                                    tv_helpdesk.setText(json_data.getString("helpdeskHandleName").toUpperCase());
                                    if(!(json_data.getString("ticketSolveCategory").equalsIgnoreCase("NULL")))
                                    {
                                        tv_kategori.setText(json_data.getString("ticketSolveCategory").toUpperCase());
                                    }
                                    else
                                    {

                                        tv_kategori.setText("-");
                                    }

                                    if(json_data.getString("ticketStatus").equalsIgnoreCase("0"))
                                    {
                                        tv_label_tindakan.setText("");
                                        tv_tindakan.setText("");
                                        tv_antrian.setText(json_data.getString("countAtrian"));
                                        tv_status.setBackgroundResource( R.drawable.rect_danger);
                                        tv_status.setText("Mengantri");
                                    }
                                    else if(json_data.getString("ticketStatus").equalsIgnoreCase("2"))
                                    {
                                        tv_label_tindakan.setText("KETERANGAN PENDING");
                                        tv_tindakan.setText(json_data.getString("ticketStatusDesc"));
                                        tv_antrian.setText("-");
                                        tv_status.setBackgroundResource( R.drawable.rect_warning);
                                        tv_status.setText("Menunggu Persetujuan");
                                    }
                                    else if(json_data.getString("ticketStatus").equalsIgnoreCase("3"))
                                    {
                                        tv_label_tindakan.setText("KETERANGAN PENDING");
                                        tv_tindakan.setText(json_data.getString("ticketStatusDesc"));
                                        tv_antrian.setText("-");
                                        tv_status.setBackgroundResource( R.drawable.rect_warning);
                                        tv_status.setText("Menunggu Material");
                                    }
                                    else if(json_data.getString("ticketStatus").equalsIgnoreCase("8"))
                                    {
                                        tv_label_tindakan.setText("SOLUSI");
                                        tv_tindakan.setText(json_data.getString("ticketSolveDesc"));
                                        tv_antrian.setText("-");
                                        tv_status.setBackgroundResource( R.drawable.rect_success);
                                        tv_status.setText("Selesai");


                                        //jika tiket selesai dan bintang nya 0 maka dibawa ke aktivity saran/star
                                        if(json_data.getString("ticketStar").equalsIgnoreCase("0"))
                                        {


                                            //Toast.makeText(getApplicationContext(), "kode:"+json_data.getString("ticketID"), Toast.LENGTH_LONG).show();
                                            Intent intent = new Intent(getBaseContext(), StarActivity.class);
                                            intent.putExtra("ticket_id", json_data.getString("ticketID"));
                                            startActivity(intent);
                                        }
                                        else
                                        {
                                            star.setVisibility(View.VISIBLE);
                                            star.setRating(Float.parseFloat(json_data.getString("ticketStar")));
                                        }
                                    }




                                    String text2Qr = json_data.getString("ticketNumber");
                                    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                                    try {
                                        BitMatrix bitMatrix = multiFormatWriter.encode(text2Qr, BarcodeFormat.QR_CODE,200,200);
                                        BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                                        Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                                        iv_qrcode.setImageBitmap(bitmap);
                                    } catch (WriterException e) {
                                        e.printStackTrace();
                                    }



                                    ticketView.setVisibility(View.VISIBLE);

                                    //Toast.makeText(getApplicationContext(),json_data.getString("ticketRequestDate"),Toast.LENGTH_LONG).show();

                                    //renderTable(data_ticket);
                                    Log.v(TAG, "ada data " );

                                }

                            }
                            else
                            {

                                Log.v(TAG, "masuk sana");

                                OneButtonDialog oneButtonDialog =
                                        DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                                jsonObject.getString("desc").toString(),
                                                R.string.setting_dialog_error_button_text,
                                                new OneButtonDialog.ButtonDialogAction() {
                                                    @Override
                                                    public void onButtonClicked() {


                                                    }
                                                });
                                oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);

                                //Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                            }
                            Log.v(TAG, "kok g ada" );

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        loading.setVisibility(View.GONE);
                        OneButtonDialog oneButtonDialog =
                                DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                        error.toString(),
                                        R.string.setting_dialog_error_button_text,
                                        new OneButtonDialog.ButtonDialogAction() {
                                            @Override
                                            public void onButtonClicked() {


                                            }
                                        });
                        oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
                        //Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                params.put("page","ticket");
                params.put("type","model");
                params.put("action","detail");
                params.put("ticket_id",String.valueOf(ticket_id));
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //maksimal 27 detik
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}
