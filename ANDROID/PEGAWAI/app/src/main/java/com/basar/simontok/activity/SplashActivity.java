package com.basar.simontok.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.simontok.R;
import com.basar.simontok.helper.Config;
import com.basar.simontok.helper.DialogFactory;
import com.basar.simontok.helper.OneButtonDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "aktifitas_splash";
    private ImageView img_splash;
    private ProgressBar loading;
    private  TextView text_keterangan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        img_splash = findViewById(R.id.img_splash);
        loading = findViewById(R.id.loading);
        text_keterangan = findViewById(R.id.text_keterangan);



        if(periksaInternet())
        {
            //something koneksi internet lancar
            Log.v(TAG, "koneksi internet lancar");

            Log.v(TAG, "cek login token. posisis sekarang:" + Config.getLoginToken(getBaseContext()));
            Log.v(TAG, "cek firebase token. now:" + Config.getFirebaseToken(getBaseContext()));
            Log.v(TAG, "cek register status. sekarang:" + Config.getRegisterStatus(getBaseContext()));
            if(Config.getLoginToken(getBaseContext()).equalsIgnoreCase("") )
            {
                if(Config.getRegisterStatus(getBaseContext()).equalsIgnoreCase(""))
                {
                    //jika belum pernah register maka masuk ke setting
                    startActivity(new Intent(getBaseContext(),SettingActivity.class));
                }
                else
                {
                    //jika sudah pernah register maka masuk ke form tunggu
                    startActivity(new Intent(getBaseContext(),WaitingActivity.class));
                }
            }
            else
            {
                Log.v(TAG, "masuk fungsi periksaVersiApp");
                String alamat_url = Config.getAppProtocol()+Config.getDomain(getBaseContext())+"/index.php";
                Log.v(TAG, "alamat domain" + alamat_url);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.v(TAG, "responnya :" + response);
                                try{
                                    JSONObject jsonObject = new JSONObject(response);
                                    String result = jsonObject.getString("result");
                                    Log.v(TAG, "dan hasilnya: " + result);
                                    if(result.equalsIgnoreCase("success"))
                                    {
                                        Integer version = jsonObject.getInt("version");
                                        Log.v(TAG, "versi:"+String.valueOf(version));

                                        if(Config.getVersion()>= version)
                                        {
                                            startActivity(new Intent(getBaseContext(), ListTicketActivity.class));
                                        }
                                        else
                                        {

                                            Log.v(TAG, "versi masih rendah perlu update");
                                            img_splash.setImageResource(R.drawable.ic_system_update_black_24dp);
                                            loading.setVisibility(View.INVISIBLE);
                                            Toast.makeText(getApplicationContext(),"aplikasi sudah kadaluwarsa.\n harap segera diupate.\nharap hubungi helpdesk",Toast.LENGTH_LONG).show();
                                            text_keterangan.setText("aplikasi sudah kadaluwarsa.\n harap segera diupate.\nharap hubungi helpdesk");

                                        }

                                    }
                                    else
                                    {


                                        OneButtonDialog oneButtonDialog =
                                                DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                                        jsonObject.getString("desc").toString(),
                                                        R.string.setting_dialog_error_button_text,
                                                        new OneButtonDialog.ButtonDialogAction() {
                                                            @Override
                                                            public void onButtonClicked() {


                                                            }
                                                        });
                                        oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
                                        //Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                                        //startActivity(new Intent(getBaseContext(),WaitingRegistration.class));
                                        //Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                                    }

                                    Log.v(TAG, "kok g ada" );

                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {


                                OneButtonDialog oneButtonDialog =
                                        DialogFactory.makeErrorDialog(R.string.setting_dialog_error_title,
                                                "Aplikasi tidak dapat terhubung ke server",
                                                R.string.setting_dialog_error_button_text,
                                                new OneButtonDialog.ButtonDialogAction() {
                                                    @Override
                                                    public void onButtonClicked() {
                                                        text_keterangan.setText("Aplikasi tidak dapat terhubung ke server");
                                                        loading.setVisibility(View.GONE);
                                                        img_splash.setImageResource(R.drawable.ic_system_update_black_24dp);

                                                    }
                                                });
                                oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);
                                //Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                            }
                        }
                )
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                        params.put("page","app");
                        params.put("appName",Config.getAppName());
                        params.put("action","version");

                        return params;

                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(this);

                //maksimal 27 detik
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        15000
                        , DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                        ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(),"tidak ada koneksi internet",Toast.LENGTH_LONG).show();
            //tidak terkoneksi internet
            Log.v(TAG, "tidak terkoneksi internet");

        }

    }

    private boolean periksaInternet()
    {
        ConnectivityManager koneksi = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }

    //apakah kadaluwarsa atau tidak
    //true jika tidak kadaluwarsa.false jika kadaluwarsa
    private void periksaVersiApp()
    {

    }
}
