package com.basar.simontok.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.simontok.R;
import com.basar.simontok.helper.Config;
import com.basar.simontok.helper.DialogFactory;
import com.basar.simontok.helper.OneButtonDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TicketActivity extends AppCompatActivity {

    private static final String TAG = "TICKETAKTIFITAS";
    private EditText text_ticket ;
    private TextView button_ticket;
    private ImageView button_back;

    private ProgressBar pb_progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);
        text_ticket =  findViewById(R.id.text_ticket);
        button_ticket = findViewById(R.id.button_problem_send);
        button_back = findViewById(R.id.button_back);


        pb_progress = (ProgressBar) findViewById(R.id.progress);
        pb_progress.setVisibility(ProgressBar.GONE);

        button_ticket.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){
                kirimKeluhan();
            }
        });

        button_back.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void  onClick(View v){
                startActivity(new Intent(getBaseContext(), ListTicketActivity.class));
            }
        });
    }

    public void onBackPressed() {
        return;
    }
    private void kirimKeluhan(){


        pb_progress.setVisibility(ProgressBar.VISIBLE);
        final String problem_desc = this.text_ticket.getText().toString().trim();
        String alamat_url = Config.getAppProtocol()+Config.getDomain(getBaseContext())+"/index.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v(TAG, "responnya :" + response);
                        pb_progress.setVisibility(ProgressBar.GONE);

                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("result");
                            Log.v(TAG, "dan hasilnya: " + result);
                            if(result.equalsIgnoreCase("success"))
                            {
                                Log.v(TAG, "UTUH: " + response);
                                OneButtonDialog oneButtonDialog =
                                        DialogFactory.makeSuccessDialog(R.string.setting_dialog_success_title,
                                                jsonObject.getString("desc").toString(),
                                                R.string.setting_dialog_error_button_text,
                                                new OneButtonDialog.ButtonDialogAction() {
                                                    @Override
                                                    public void onButtonClicked() {

                                                        startActivity(new Intent(getBaseContext(), ListTicketActivity.class));

                                                    }
                                                });
                                oneButtonDialog.show(getSupportFragmentManager(), OneButtonDialog.TAG);

                            }
                            else
                            {

                                Log.v(TAG, "masuk sana");
                                Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                            }
                            Log.v(TAG, "kok g ada" );

                        }catch (JSONException e){
                            pb_progress.setVisibility(ProgressBar.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pb_progress.setVisibility(ProgressBar.GONE);
                        Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                params.put("page","ticket");
                params.put("type","model");
                params.put("action","create");
                params.put("problem_desc",problem_desc);

                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //maksimal 27 detik
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
