package com.basar.simontok.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.util.Locale;
import java.util.UUID;

import static android.content.Context.TELEPHONY_SERVICE;

public class Device {
    private String manufacture,model;
    private String screen_pixel;
    private Float screen_dpi;
    private  Double screen_size;
    private String cpu_model;
    private String country,language;
    private String uuid,android_id;
    private String network_carrier,network_id;
    private Integer android_sdk;
    private String android_version;

    private  Activity activity;

    int mWidthPixels, mHeightPixels;

    public   Device(){

    }


    public  void init(Activity _activity) {
        activity = _activity;
        manufacture = Build.MANUFACTURER;
        model = Build.MODEL;
        android_sdk = Build.VERSION.SDK_INT;
        android_version = Build.VERSION.RELEASE;

        setRealDeviceSizeInPixels();
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(mWidthPixels/dm.xdpi,2);
        double y = Math.pow(mHeightPixels/dm.ydpi,2);
        double diagonal_inches = Math.sqrt(x+y);
        screen_pixel = String.valueOf(mWidthPixels) + "x" + String.valueOf(mHeightPixels);
        screen_size = (double) Math.round(diagonal_inches*100)/100;
        screen_dpi =activity.getResources().getDisplayMetrics().density;

        cpu_model = Build.BOARD;
        language = Locale.getDefault().getLanguage();
        country = Locale.getDefault().getCountry();
        uuid = UUID.randomUUID().toString();
        android_id =  Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID) ;

        TelephonyManager telephony_manager = (TelephonyManager) activity.getSystemService(TELEPHONY_SERVICE);
        String imei_number = "UNKNOWN";
        network_carrier = "";

        if (Build.VERSION.SDK_INT >= 26) {
            if (telephony_manager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                imei_number = telephony_manager.getMeid();
                network_carrier = "CDMA";
            } else if (telephony_manager.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
                imei_number = telephony_manager.getImei();
                network_carrier = "GSM";
            } else {
                imei_number = "UNKNOWN"; // default!!!
                network_carrier = "UNKNOWN";
            }
        } else {
            imei_number = telephony_manager.getDeviceId();

            if (telephony_manager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
                network_carrier = "CDMA";
            } else if (telephony_manager.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
                network_carrier = "GSM";
            } else {
                network_carrier = "UNKNOWN";
            }
        }
        network_id = imei_number;





    }

    private void setRealDeviceSizeInPixels() {
        WindowManager windowManager = activity.getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);


        // since SDK_INT = 1;
        mWidthPixels = displayMetrics.widthPixels;
        mHeightPixels = displayMetrics.heightPixels;

        // includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17) {
            try {
                mWidthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                mHeightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (Exception ignored) {
            }
        }

        // includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(display, realSize);
                mWidthPixels = realSize.x;
                mHeightPixels = realSize.y;
            } catch (Exception ignored) {
            }
        }
    }

    public String getManufacture() {
        return manufacture;
    }
    public  String getModel(){
        return  model;
    }
    public Integer getSDKVersion(){
        return  android_sdk;
    }
    public String getAndroidVersion(){
        return  android_version;
    }
    public  String getCpuModel(){
        return  cpu_model;
    }
    public  String getScreenPixel(){
        return  screen_pixel;
    }
    public  Double getScreenSize(){
        return  screen_size;
    }
    public  Float getScreenDPI(){
        return  screen_dpi;
    }

    public  String getCountry(){
        return  country;
    }
    public  String getLanguage(){
        return  language;
    }

    public  String getUUID(){
        return  uuid;
    }

    public  String getAndroidID(){
        return  android_id;
    }
    public  String getNetworkCarrier(){
        return  network_carrier;
    }
    public  String getNetworkID(){
        return  network_id;
    }


}
