package com.basar.simontok.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basar.simontok.R;
import com.basar.simontok.adapter.AdapterTicket;
import com.basar.simontok.helper.Config;
import com.basar.simontok.model.DataTicket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListTicketActivity extends AppCompatActivity {

    private static final String TAG = "LISTTIKETAKTIFITAS";
    SwipeRefreshLayout swLayout;
    private RecyclerView mRcvTicket;
    private AdapterTicket mAdapter;
    private ProgressBar loading;
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_ticket);



        loading = findViewById(R.id.loading);
        swLayout = (SwipeRefreshLayout) findViewById(R.id.swlayout);
        // Mengeset properti warna yang berputar pada SwipeRefreshLayout
        swLayout.setColorSchemeResources(R.color.colorPrimary,R.color.colorPrimaryDark);
        // Mengeset listener yang akan dijalankan saat layar di refresh/swipe
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //  berputar/refreshing
                swLayout.setRefreshing(true);

                loadData();
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getBaseContext(),TicketActivity.class));
            }
        });
        loadData();

    }


    public void onBackPressed() {
        return;
    }
    public void loadData()
    {
        loading.setVisibility(View.VISIBLE);
        // Berhenti berputar/refreshing
        swLayout.setRefreshing(false);
        //jika register status = 0 dan sudah ada token firebase maka cek lagi register apakah sudah di aktifasi helpdesk
        String alamat_url = Config.getAppProtocol()+Config.getDomain(getBaseContext())+"/index.php";
        Log.v(TAG, "alamayya" + alamat_url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, alamat_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        loading.setVisibility(View.GONE);
                        Log.v(TAG, "responnya :" + response);

                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("result");
                            Log.v(TAG, "dan hasilnya: " + result);
                            if(result.equalsIgnoreCase("success"))
                            {
                                Log.v(TAG, "UTUH: " + response);
                                Log.v(TAG, "DARATA: " + jsonObject.getString("data"));
                                if(jsonObject.isNull("data") )
                                {
                                    Log.v(TAG, "datanya kosong " );
                                }
                                else
                                {
                                    String data_ticket = jsonObject.getString("data");
                                    renderTable(data_ticket);
                                    Log.v(TAG, "ada data " );

                                }

                            }
                            else
                            {

                                Log.v(TAG, "masuk sana");
                                Toast.makeText(getApplicationContext(),jsonObject.getString("desc").toString(),Toast.LENGTH_LONG).show();
                            }
                            Log.v(TAG, "kok g ada" );

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"error "+error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("TOKEN_KEY", Config.getLoginToken(getBaseContext()));
                params.put("page","ticket");
                params.put("type","model");
                params.put("action","list");
                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //maksimal 27 detik
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000
                ,DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    public void renderTable(String _json){
        List<DataTicket> data=new ArrayList<>();
        try {

            JSONArray jArray = new JSONArray(_json);
            String jsonku = "";
            jsonku = Integer.toString(jArray.length());
            Log.v("JSONKU",jsonku);

            // Extract data from json and store into ArrayList as class objects
            for(int i=0;i<jArray.length();i++){
                Log.v("nomorku",Integer.toString(i));
                JSONObject json_data = jArray.getJSONObject(i);
                DataTicket ticket_data = new DataTicket();
                ticket_data.ticket_problem_desc= json_data.getString("ticketProblemDesc");

                ticket_data.ticket_date = json_data.getString("ticketRequestDate");
                ticket_data.helpdesk_name = json_data.getString("helpdeskHandleName");
                ticket_data.ticket_status_flag = json_data.getString("ticketStatus");
                ticket_data.ticket_id = json_data.getLong("ticketID");

                data.add(ticket_data);
            }

            // Setup and Handover data to recyclerview
            mAdapter = new AdapterTicket(ListTicketActivity.this, data);
            mRcvTicket = (RecyclerView)findViewById(R.id.recycler_view);
            mRcvTicket.setAdapter(mAdapter);



            mRcvTicket.setLayoutManager(new LinearLayoutManager(ListTicketActivity.this));


        } catch (JSONException e) {
            Toast.makeText(ListTicketActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        }

    }



}
