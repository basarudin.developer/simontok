package com.basar.simontok.model;

public class DataTicket {
    public String ticket_problem_desc;
    public String helpdesk_name;
    public String ticket_status;
    public String ticket_status_flag;
    public String ticket_date;
    public Long ticket_id;
    public String ticket_pending_desc;
}