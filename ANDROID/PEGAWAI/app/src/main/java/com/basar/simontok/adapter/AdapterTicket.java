package com.basar.simontok.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.basar.simontok.R;
import com.basar.simontok.activity.TicketDetail;
import com.basar.simontok.model.DataTicket;

import java.util.Collections;
import java.util.List;

public class AdapterTicket extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<DataTicket> data= Collections.emptyList();
    DataTicket current;
    int currentPos=0;

    // create constructor to innitilize context and data sent from ListTicketActivity
    public AdapterTicket(Context context, List<DataTicket> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }

    // Inflate the layout when viewholder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_ticket, parent,false);
        MyHolder holder=new MyHolder(view);
        return holder;
    }

    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in recyclerview to bind data and assign values from list
        MyHolder myHolder= (MyHolder) holder;
        DataTicket current=data.get(position);
        myHolder.textTicketDesc.setText(current.ticket_problem_desc);
        myHolder.textHelpdesk.setText(current.helpdesk_name.toUpperCase());
        myHolder.textTicketDate.setText(current.ticket_date);
        if(current.ticket_status_flag.equalsIgnoreCase("0"))
        {
            myHolder.textTicketStatus.setBackgroundResource( R.drawable.rect_danger);
            myHolder.textTicketStatus.setText("Mengantri");
        }
        else if(current.ticket_status_flag.equalsIgnoreCase("2"))
        {
            myHolder.textTicketStatus.setBackgroundResource( R.drawable.rect_warning);
            myHolder.textTicketStatus.setText("Menunggu Persetujuan");
        }
        else if(current.ticket_status_flag.equalsIgnoreCase("3"))
        {
            myHolder.textTicketStatus.setBackgroundResource( R.drawable.rect_warning);
            myHolder.textTicketStatus.setText("Menunggu Material");
        }
        else if(current.ticket_status_flag.equalsIgnoreCase("8"))
        {
            myHolder.textTicketStatus.setBackgroundResource( R.drawable.rect_success);
            myHolder.textTicketStatus.setText("Selesai");
        }




        final long ticket_id = current.ticket_id;

        myHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Context context = v.getContext();
                Intent intent = new Intent(new Intent(context, TicketDetail.class));
                intent.putExtra("ticket_id", ticket_id);
                context.startActivity(intent);
                //Toast.makeText(v.getContext(), "Recycle Click" + String.valueOf(ticket_id), Toast.LENGTH_SHORT).show();
            }
        });
        /*
        myHolder.textSize.setText("Size: " + current.sizeName);
        myHolder.textType.setText("Category: " + current.catName);
        myHolder.textPrice.setText("Rs. " + current.price + "\\Kg");
        myHolder.textPrice.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        */
    }

    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }


    class MyHolder extends RecyclerView.ViewHolder{

        TextView textTicketDesc,textTicketStatus,textTicketDate,textHelpdesk;
        /*
        ImageView ivFish;
        TextView textSize;
        TextView textType;
        TextView textPrice;
        */

        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            textTicketDesc= (TextView) itemView.findViewById(R.id.textProblemDesc);
            textTicketStatus= (TextView) itemView.findViewById(R.id.textStatus);
            textHelpdesk= (TextView) itemView.findViewById(R.id.textHelpdesk);
            textTicketDate = (TextView) itemView.findViewById(R.id.textTicketDate);
            /*
            ivFish= (ImageView) itemView.findViewById(R.id.ivFish);
            textSize = (TextView) itemView.findViewById(R.id.textSize);
            textType = (TextView) itemView.findViewById(R.id.textType);
            textPrice = (TextView) itemView.findViewById(R.id.textPrice);
            */
        }

    }

}